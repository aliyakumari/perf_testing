from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
from GetSession import GetSession, getEmbibeToken
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
            'browser-id': 'gtabammana',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTI0IDEzOjUzOjU3IFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6MTAwNzg5OCwiZW1haWwiOiJndWVzdF8xNTk4Mjc3MjM3NjY2MTkzMzI1QGVtYmliZS5jb20ifQ.nt1dx3EL5EVsxvYyTnN4Bt3reNnpVNAv6Y_IyWwAdgbmRFteYDysJZ08gyl-wPkeGpqNR96zESEG7rX91MT6OQ',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({
            "type": "BookPractice",
            "learning_map": {
                "bundle_code": "5e9f887cb514c57476cdfc3a"
            },
            "language": "en",
            "namespace": "embibe"
        })

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def getQuestions(self):
        embibe_token = getEmbibeToken()
        self.url = f"/fiber_practice_ms/v1/practice/session"
        self.headers['embibe-token'] = embibe_token
        response = self.client.post(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
