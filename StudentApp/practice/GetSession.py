import requests
from random import randrange
import pandas as pd


# embibe_token = pd.read_csv("/Users/aliya.kumari/Embibe/perf_testing/StudentApp/practice/Embibe-Token.csv")
# embibe_token_ = embibe_token.values.tolist()


def GetSession():
    # rnum = randrange(len(embibe_token_))
    # embibe_token = embibe_token_[rnum][1]
    url = "https://preprodms.embibe.com/fiber_practice_ms/v1/practice/session"

    payload = "{\"type\":\"Normal\",\"learning_map\":{\"bundle_code\":\"kve98531\",\"exam_code\":\"kve97915\",\"level\":\"chapter\",\"chapter_code\":\"kve98531\",\"format_id\":\"5ec5867a0c88fe5860961943\",\"lm_name\":\"5ec5867a0c88fe5860961943--cbse--10th cbse--science--chemistry--metals and non-metals\",\"goal_name\":\"CBSE\",\"exam_name\":\"10th CBSE\"},\"title\":\"Metals and Non-Metals\",\"device\":\"Web\",\"source\":\"PracticeSummary\",\"language\":\"en\",\"namespace\":\"embibe\"}"
    headers = {
        'Connection': 'keep-alive',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36',
        'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjkyNzcwNDAsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTUwNDcyOCwiZXhwIjoxNjY5MzYzNDQwLCJkZXZpY2VJZCI6IjE2NjI3MTE0ODg0ODMiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoiNnB5dWU1dzN6MW8ubWFyMjJAZW1iaWJlLmNvbSJ9.ZRAItIUdH8M-0rmBm2zqUIhsulKmOP2BAwtUs06OQX_GDtayCK6-jgMxFE78qKaHibRZeybcF45OP1L9n8Kd4g',
        'Content-Type': 'application/json;charset=UTF-8',
        'Accept': 'application/json',
        'browser-id': 'MTcwMjYyODg0NTg0X3dlYg==',
        'sec-ch-ua-platform': '"macOS"',
        'Origin': 'https://staging-fiber-web.embibe.com',
        'Sec-Fetch-Site': 'same-site',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://staging-fiber-web.embibe.com/',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    session_id = response.json().get("session_id")
    print(response)

    print(headers['embibe-token'], session_id)
    return headers['embibe-token'], session_id


GetSession()
