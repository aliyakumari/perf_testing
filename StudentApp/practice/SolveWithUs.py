from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
from GetSession import GetSession
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
session_id = []
embibe_token = []

for i in range(1, 7000):
    embibe_token_, session_id_ = GetSession()
    embibe_token.append(embibe_token_)
    session_id.append(session_id_)


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
            'Connection': 'keep-alive',
            'Accept': 'application/json, text/plain, */*',
            'browser-id': 'undefined',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTEzIDA1OjM3OjQ3IFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwiaWQiOjYyNjUsImVtYWlsIjoiYzEzNDY0ODJkNzczQGppby1lbWJpYmUuY29tIn0.fsGCegi4ORSAPu6hku5OuokKmSujDnkeFYpJeMLZcvdxb1IaUDPadWHI4SdTiLiic41c9lFx1bYfKusUrzCnyg',
            'Content-Type': 'application/json;charset=UTF-8',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': 'V_ID=ultimate.2020-11-23.b3eb1a9d2bf375ba298bab58928a727e; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQ2OTUwNzI4MCwiZW1haWwiOiJndWVzdF8xNjA2MTE0OTA2MjY5ODMwQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTIzVDA3OjAxOjQ2LjEzMVoifQ.rQ-pxG4lrE1xLIw6HGJEFkNTHXYy53aLLV1o8PDvobyNfxIX7WUWkPset9X40DgPlS8bBnO3HpTV1DOkslKgoA; access-token=LyNp3ea2g6vxGaXzfuGRuQ; uid=guest_1606114906269830%40embibe.com; client=Ry9ca0qbYS_fywSQDHBNoQ; _test_app_session=8742be7df28512b02f452d4eb8b6c365; JSESSIONID=A322F6673DBD5AD61F7AE73EDACBA49E; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({
            "start_time": 1597500767855,
            "answers": [
                "answer"
            ]
        })

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def getQuestions(self):
        rnum = randrange(len(session_id))
        self.url = f"/fiber_practice_ms/v1/practice/{session_id[rnum]}/question/2474641/solve-with-us?namespace" \
                   f"=embibe&language=en&version=1&showAnswer=false&showSolution=false "
        self.headers['embibe-token'] = embibe_token[rnum]
        response = self.client.post(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
