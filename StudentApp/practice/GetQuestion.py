from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
from GetSession import GetSession

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
            'Accept': 'application/json',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Accept-Encoding': 'gzip, deflate, br',
            'Host': 'preprodms.embibe.com',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.0 Safari/605.1.15',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Accept-Language': 'en-gb',
            'Connection': 'keep-alive',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDIxMzUwLCJjcmVhdGVkIjoxNjM4MTg1NjcxLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjg5MTM2MjM5OTQ1NDIsImV4cCI6MTYzODM1ODQ3MSwiZGV2aWNlSWQiOiIxNjM3OTI2MTgzOTcyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDIxMzUwXzIwOTk1Nzg2OTg4NTMzNDI1QGVtYmliZS11c2VyLmNvbSJ9.DMe_6OFEY18EsPuoo3gSTXkp1O2AMueZ8gbQk0r5MrgeZGmt0Nr-UlmynIqZy2MEdlk8qERfDKLpv6H8cv44JA',
            'browser-id': 'MjI0NTQ2ODE2Nzg3X3dlYg==',
            'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def getQuestions(self):
        embibe_token, session_id = GetSession()
        self.url = f"/fiber_practice_ms/v1/practice/{session_id}/question?questionCode=&nextQuestion=false"
        self.headers['embibe-token'] = embibe_token
        response = self.client.get(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
