from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers =   {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxMTk5MjA0LCJjcmVhdGVkIjoxNjUyMzgyOTk2LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTkyMDYsImV4cCI6MTY1MjQ2OTM5NiwiZGV2aWNlSWQiOiIxNjUyMzgyOTk2MzUzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxMTk5MjA0XzM4MDE5ODg3MzAyMzE1MjRAZW1iaWJlLXVzZXIuY29tIn0.i_Wnr0xVfo7N57lZmJ-zpgmJw3vK7p0oMG08oJuHC07kejEa6V9EdPQqZeOJ5SEQsViI1Xi_WQukwY1tvgtM7w',
  'Content-Type': 'application/json'
}



        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({
    "id": "5ec58c1f0c88fe5860972d8b",
    "learnmap_id":"5ec5867a0c88fe5860961943/CBSE/10th CBSE/Science/Chemistry/Chemical reactions and Equation"
}
)
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/userhome/v1/share?type=practice_summary"

        with self.client.post(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 200:
                return response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
