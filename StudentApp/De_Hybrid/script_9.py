from locust import HttpUser,SequentialTaskSet,task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import pandas as pd
import random
import resource

all_commands = {}
with open("Static/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)        
        
        self.url = "/de/hybridapi/v3/all_homework_single_user_detail?userId=1501413831&schoolId=62bc63afc672252aaf556025&classId=634954228db9004e80e00d9f"
        self.method = "GET"
        self.host = "https://preprodms.embibe.com" 

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"
    @task
    def ondemandautomated_script(self):
        # self.headers['embibe-token'] = email_password[rnum][3]
        response = self.client.get(self.url)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return
        
@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
