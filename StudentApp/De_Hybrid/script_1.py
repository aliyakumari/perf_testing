from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
import pandas as pd
import random

all_commands = {}
with open("Static/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
            'Cookie': 'embibe-refresh-token=d042737d-414e-4b44-9753-11945edb1964; preprod_embibe-refresh-token=4d97ce1f-1205-41ab-8a62-55bbc72203d7'
                        }
        self.body = {}
        self.url = "/de/hybridapi/v3/homework_question_wise_analysis?homeworkId=63846ff066125267cf3fe396&userId=1501413831&schoolId=62bc63afc672252aaf556025&classId=634954228db9004e80e00d9f"
        self.method = "GET"
        self.host = "https://preprodms.embibe.com"

        dt = pd.read_csv("2.De_Hybrid_preprod_homework_master_v1.csv", low_memory=False)

        # self.id = dt["_id"].values.tolist()

        # self.classId = dt["classId"].values.tolist()
        # self.subjectCode = dt["kveCodeObject.subjectCode"].values.tolist()
        # self.chapterCode = dt["kveCodeObject.chapterCode"].values.tolist()
        # self.schoolId = dt["schoolId"].values.tolist()
        # self.startDate = dt["startDate"].values.tolist()
        # self.dueDate = dt["dueDate"].values.tolist()
        # self.userId = dt["userId"].values.tolist()
        # self.homeworkId = dt["homeworkId"].values.tolist()
        # self.examCode = dt["examCode"].values.tolist()

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def ondemandautomated_script(self):
        # self.headers['embibe-token'] = email_password[rnum][3]

        # self.classId = random.choice(self.classId)
        # self.homeworkId = random.choice(self.homeworkId)
        # self.subjectCode = random.choice(self.subjectCode)
        # self.schoolId = random.choice(self.schoolId)
        # self.userId = random.choice(self.userId)
        # self.examCode = random.choice(self.examCode)
        # self.chapterCode = random.choice(self.chapterCode)
        # self.startTime = random.choice(self.startDate)
        # self.endTime = random.choice(self.dueDate)

        self.url = "/de/hybridapi/v3/homework_question_wise_analysis?homeworkId=63846ff066125267cf3fe396&userId=1501413831&schoolId=62bc63afc672252aaf556025&classId=634954228db9004e80e00d9f"
        # print('URL : ', self.url)
        response = self.client.get(self.url, headers=self.headers)
        # print(response.json())
        # try:
        #     if response.status_code != 200:
        #         print('Header : ', self.headers)
        #         print('Body : ', self.body)
        #         print('Response Code :', response.status_code)
        #         print('Response : ', response.json())
        #         print('URL : ', self.url)
        #
        # except Exception as e:
        #     print(e)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
