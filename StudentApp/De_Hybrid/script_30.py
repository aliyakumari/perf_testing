from locust import HttpUser,SequentialTaskSet,task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import pandas as pd
import random
import resource

all_commands = {}
with open("Static/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)        
        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'embibe-refresh-token=d042737d-414e-4b44-9753-11945edb1964; preprod_embibe-refresh-token=4d97ce1f-1205-41ab-8a62-55bbc72203d7'
        }
        self.body = {
            "userId": "1501413823",
            "schoolAssignmentIds": [
                "638634c80227a240707358ff"
            ],
            "locale": "en"
        }
        self.url = "/de/hybridapi/v2/get_homework_content_progress"
        self.method = "POST"
        self.host = "https://preprodms.embibe.com" 

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"
    @task
    def ondemandautomated_script(self):
        # self.headers['embibe-token'] = email_password[rnum][3]
        response = self.client.post(self.url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return
        
@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
