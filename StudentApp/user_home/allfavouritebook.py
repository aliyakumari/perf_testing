from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

print(embibe_token[0])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDE1OTQwLCJjcmVhdGVkIjoxNjQzMDM3NDUxLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTY2MjEsImV4cCI6MTY0MzIxMDI1MSwiZGV2aWNlSWQiOiIxNjQyNzc5ODcxMTk0IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDE1OTQwXzcxMzE1NzcyMTg5NjQzOTJAZW1iaWJlLXVzZXIuY29tIn0.PnRmmAEFea-4MUsVCn__Smas7-lwZYcZzZ1QyPQAVzGN6aIn8yCTixjY6KAyst9QZza5vw0ToYyoTnObhSPGQA',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57; access-token=j49HWwIteqKJUbjU4H4Bqg; client=hhzO_QfVnXIfaXkDOZ6UKw; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6NTUzNywiZW1haWwiOiJndWVzdF8xNjQyMDgxMzI0ODIzNzY2QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIyLTAxLTEzVDEzOjQyOjA0LjQ4MVoifQ.08ym6N7xliVRPE0IW2ATH0B2aEL2rUlOXzl7PGz0L3X96UpB8aT2ZOAxdFbJEOxkanXmcJJG_dx2s01KIHZf_A; uid=guest_1642081324823766%40embibe.com'
}



    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({"favouriteBooks":[{"objectId":"6045b5af742fa7e224723a48/EL10P024/10/Physics","type":"fav_book","title":"ESSENTIALS OF PHYSICS 10","subject":"Science","thumbUrl":"https://embibe.blob.core.windows.net/images/book_tiles/e266f20e-8150-4e15-b805-48f291886005_3x.webp","teaserUrl":"https://jioembibe.cdn.jio.com/hlsmedia/embibe/1/20938_2406212146_mobile.m3u8","source":"","rank":{"arunachal pradesh board--10th arunachal pradesh board":25,"assam board--10th assam board":27,"bihar state board--10th bihar board":27,"cbse--10th cbse":33,"delhi board--10th delhi board":29,"goa board--10th goa board":25,"gujarat board--10th gujarat board":27,"haryana board--10th haryana board":30,"himachal pradesh board--10th himachal pradesh board":29,"jharkhand board--10th jharkhand board":25,"karnataka board--10th karnataka state board":29,"madhya pradesh board--10th madhya pradesh board":29,"meghalaya board--10th meghalaya board":27,"mizoram board--10th mizoram board":25,"nagaland board--10th nagaland board":25,"punjab board--10th punjab board":35,"sikkim board--10th sikkim board":25,"tripura board--10th tripura board":25,"uttar pradesh board--10th uttar pradesh board":25,"uttarakhand board--10th uttarakhand board":25},"locale":"en","selected":True,"thumb":"https://embibe.blob.core.windows.net/images/book_tiles/e266f20e-8150-4e15-b805-48f291886005_3x.webp"},{"objectId":"60212831c8dd2cdeaa7fa985/EL10M180/10/Mathematics","type":"fav_book","title":"MATHEMATICS CLASS X","subject":"Mathematics","thumbUrl":"https://embibe.blob.core.windows.net/images/book_tiles/afb0b36d-1a2f-4ba8-8682-dfead43566d1_3x.webp","teaserUrl":"https://jioembibe.cdn.jio.com/hlsmedia/embibe/3/42420_1211211821_mobile.m3u8","source":"","rank":{"andhra pradesh board--10th andhra pradesh board":4,"arunachal pradesh board--10th arunachal pradesh board":3,"assam board--10th assam board":5,"bihar state board--10th bihar board":5,"cbse--10th cbse":11,"chhattisgarh board--10th chhattisgarh board":5,"delhi board--10th delhi board":7,"goa board--10th goa board":3,"gujarat board--10th gujarat board":5,"haryana board--10th haryana board":8,"himachal pradesh board--10th himachal pradesh board":7,"jammu and kashmir board--10th jammu and kashmir board":6,"jharkhand board--10th jharkhand board":3,"karnataka board--10th karnataka state board":7,"kerala board--10th kerala board":10,"ladakh board--10th ladakh board":3,"madhya pradesh board--10th madhya pradesh board":7,"maharashtra board--10th maharashtra board":12,"manipur board--10th manipur board":4,"meghalaya board--10th meghalaya board":5,"mizoram board--10th mizoram board":3,"nagaland board--10th nagaland board":3,"punjab board--10th punjab board":8,"rajasthan board--10th rbse":16,"sikkim board--10th sikkim board":3,"tamil nadu board--10th tamil nadu state board":5,"telangana board--10th telangana state board":9,"tripura board--10th tripura board":3,"uttar pradesh board--10th uttar pradesh board":3,"uttarakhand board--10th uttarakhand board":3,"west bengal board--10th west bengal board":6},"locale":"en","selected":True,"thumb":"https://embibe.blob.core.windows.net/images/book_tiles/afb0b36d-1a2f-4ba8-8682-dfead43566d1_3x.webp"},{"objectId":"607eaf339ae32d0793a6d301/NC10SS006/10/Social Science","type":"fav_book","title":"Understanding Economic Development","subject":"Social Science","thumbUrl":"https://embibe.blob.core.windows.net/images/book_tiles/59d911c9-3734-439b-9b11-18b4a8eba6fe_3x.webp","teaserUrl":"","source":"","rank":{"arunachal pradesh board--10th arunachal pradesh board":6,"cbse--10th cbse":8,"delhi board--10th delhi board":6},"locale":"en","selected":True,"thumb":"https://embibe.blob.core.windows.net/images/book_tiles/59d911c9-3734-439b-9b11-18b4a8eba6fe_3x.webp"}],"exam":"10th CBSE","goal":"CBSE"})
        url = f"/userhome/v1/favouritebooks"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
