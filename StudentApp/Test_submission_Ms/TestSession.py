from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust import task, between, events
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("../jfAnalysis/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
df = pd.read_csv("../jfAnalysis/test_code.csv")
test_code = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDMyMTQzMzcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMDAxNTk0MCwiZXhwIjoxNjQ0NDIzOTM3LCJkZXZpY2VJZCI6IjE2NDMyMTQzMzQ2MDYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoidGVzdC1maWJlcl9jaGVja2xpc3RAZW1iaWJlLmNvbSJ9.IzE4xnwuqTXkWuw6ZFQ0FeqLvHVEIczWfLzLnUaHzZiUJUKq-IJ6o_A_3N-eLdZ5OxUiCSGPvNIfqgAKUhy1uw',
            'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        rnum_ = randrange(len(test_code))
        self.body = {}
        url = f"/testsubmission_ms_fiber/v1/test/sessions"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
