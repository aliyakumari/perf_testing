from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Cookie': 'V_ID=ultimate.2020-06-22.5f98f87b121d3d9ec5e3b23e6c574284; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQxMjk4NDc0NywiZW1haWwiOiJndWVzdF8xNTk5MjA4NDgzNjAyMTA2QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTA0VDA4OjM0OjQ0LjExMloifQ.VwC2DqU2OguoM0jMipqQXsLdr3ccHf62VCCuhMw4BwGDvfJRmDjfnJ0ULNRUzNv8YFFmmICKhpAuSHTLnSC8ZA; access-token=RSr5-aBQvk28_wRmumMeew; uid=guest_1599208483602106%40embibe.com; client=xdYxwXOoX3i51hP5XjwnFQ; V_ID=ultimate.2020-06-22.5f98f87b121d3d9ec5e3b23e6c574284; JSESSIONID=TITrmFLWYdaJCdABnx0HGtwl-ukMpQMfPxfg0I-Q; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxMTk3NzEzLCJjcmVhdGVkIjoxNjQxMzk5MzczLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTkxNzUsImV4cCI6MTY0MTU3MjE3MywiZGV2aWNlSWQiOiIxNjQxMzk3NDEzMTQxIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxMTk3NzEzXzQ2ODU2NzA1MTM2Nzk5NzdAZW1iaWJlLXVzZXIuY29tIn0.pt7N5zQRPt5BQ-8_49XAPrQV2o-F-X2g0bNROpphYavMz_fsmbOcqIaMG3pHrW_Zqgjutqgb57dEBadisp75-Q'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = {}
        url = "/fiber_ms/topics/content?lm_codes=lm88054,lm87972"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
