from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTIzIDE5OjMzOjQxIFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6NTg3MzQwNCwiZW1haWwiOiJndWVzdF8xNjA2MTYwMDIxMzEyMzU2NDY2QGVtYmliZS5jb20ifQ.88dX56FP1VwrQWGgSrJb508QS8OdG3wwhJ3FfA8elgUY2VOTA6r15JSRxPRZUK50lgAt_XfkAYOJFkibFfWYSw',
            'Cookie': 'JSESSIONID=B64ECA45E0E90CB4339BB48498694413; V_ID=ultimate.2020-09-09.3f5389341772f395afb1f6557ac11f3c; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQxODcyMzc1NSwiZW1haWwiOiJndWVzdF8xNTk5Njg4NzAyMTg1Nzg5QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTA5VDIxOjU4OjIzLjE5NVoifQ.ALnAI2w1V3kIfG9rtJYLmjN3WkQYOQjAQPyMS7rswmi81UduxV0C4FJ-8WlpvymhmfcVWXeyeio5mSg28nQf0g; access-token=G0FiDbUzoUR-RckPTa38jw; uid=guest_1599688702185789%40embibe.com; client=yyYNL4M397N3zi0b9egz9Q; V_ID=ultimate.2020-06-22.5f98f87b121d3d9ec5e3b23e6c574284; JSESSIONID=wjfpUqhyEADxidnf2CnozHVnDM9QhZqmAF8Uz12A; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = {}
        url = "/fiber_ms/v1/chapters/learning-objects?learnMapId=5ec5867a0c88fe5860961943%2FCBSE%2F10th%20CBSE" \
              "%2FScience%2FBiology%2FControl%20and%20Coordination&contentTypes=Video "
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
