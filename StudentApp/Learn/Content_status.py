from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDQ3ODY3LCJjcmVhdGVkIjoxNjQxNDA1MzUwLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTkxMjcsImV4cCI6MTY0MTU3ODE1MCwiZGV2aWNlSWQiOiIxNjQxMjE5NTA5NDk1IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDQ3ODY3XzI5NjIyODYyMzY3NjA1MDRAZW1iaWJlLXVzZXIuY29tIn0.z3ivymC5zyppDetZFAwFeP_yJX2ibDML3mM4JCMtlihL6OE_QCLNMY1OyaVL6atzISO78g4zMIFM2YMi87am2w',
            'Content-Type': 'application/json;charset=UTF-8',
            'Accept': 'application/json',
            'browser-id': 'MjE2NTI4NTM4NjEzX3dlYg==',
            'sec-ch-ua-platform': '"macOS"',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': 'JSESSIONID=TITrmFLWYdaJCdABnx0HGtwl-ukMpQMfPxfg0I-Q; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps(
            {"child_id": embibe_token[rnum][1], "content_id": "6116707", "content_status": "INPROGRESS",
             "content_type": "Video", "exam_name": "7th CBSE", "goal": "CBSE",
             "book_id": "5ec5867a0c88fe5860961943/CBSE/7th CBSE/Science", "context": "book_toc",
             "topic_learnpath_name": "cbse--7th cbse--science--biology--nutrition in plant--introduction to nutrition",
             "source": "web", "homework_id": "", "length": 175, "watched_duration": 168})
        url = "/fiber_ms/content-status"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
