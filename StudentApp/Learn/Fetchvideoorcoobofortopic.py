from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'content-type': 'application/json',
            'embibe-token': 'eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTIzMTgyNTE0MywiZW1haWwiOiJkc2xfdGVzdGluZzRkZW1vQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0IjpmYWxzZSwicm9sZSI6InN0dWRlbnQiLCJ0aW1lX3N0YW1wIjoiMjAyMC0wMS0xMlQxODoxODo0Mi4xNjRaIn0.Zwby2ICJOKrTXLQLALFwNrv1yuRtDk8fVlybDSDYzaRDhTks-yvkWp0bJ3ikhmbqqDMcgA0cI5nxILNh6ZgKyA',
            'Cookie': 'V_ID=ultimate.2020-06-22.5f98f87b121d3d9ec5e3b23e6c574284; JSESSIONID=TITrmFLWYdaJCdABnx0HGtwl-ukMpQMfPxfg0I-Q; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = {}
        url = "/fiber_ms/topics/more/cbse--10th cbse--science--chemistry--acids, bases and salts--strength of acids and bases?content_id=123456&format_id=5ec5867a0c88fe5860961943"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
