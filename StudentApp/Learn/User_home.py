from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTEyIDA4OjA4OjU4IFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwiaWQiOjE1ODgwNDIsImVtYWlsIjoiYzEwNjUzZGJiMWViQGppby1lbWJpYmUuY29tIn0.H17o-MDk62zSy5IoZmiKphXkqbr27ZL-PBkunxF5LBS56Dsl6J8Ws2p7YX9OxXlbbGzFLkV1fmC97uFMxICiuw',
            'Cookie': 'JSESSIONID=B64ECA45E0E90CB4339BB48498694413; V_ID=ultimate.2020-09-09.3f5389341772f395afb1f6557ac11f3c; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQxODcyMzc1NSwiZW1haWwiOiJndWVzdF8xNTk5Njg4NzAyMTg1Nzg5QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTA5VDIxOjU4OjIzLjE5NVoifQ.ALnAI2w1V3kIfG9rtJYLmjN3WkQYOQjAQPyMS7rswmi81UduxV0C4FJ-8WlpvymhmfcVWXeyeio5mSg28nQf0g; access-token=G0FiDbUzoUR-RckPTa38jw; uid=guest_1599688702185789%40embibe.com; client=yyYNL4M397N3zi0b9egz9Q; V_ID=ultimate.2020-06-22.5f98f87b121d3d9ec5e3b23e6c574284; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = len(embibe_token)
        self.body = json.dumps({
            "board": "10th CBSE",
            "child_id": "2043041",
            "exam": "10th-cbse",
            "exam_name": "10th CBSE",
            "goal": "CBSE",
            "grade": "10"
        })
        response = self.client.post(
            "/fiber_ms/v1/userHome",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
