from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjo1MDU0NzIyLCJjcmVhdGVkIjoxNjM4Nzc1Mjk4LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjY1MTg5NDIsImV4cCI6MTYzODk0ODA5OCwiZGV2aWNlSWQiOiIxNjM4MzU2MDY2OTMxIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjp0cnVlLCJlbWFpbCI6IjUwNTQ3MjJfODgzMjgzNjI0NTQyNTA4OUBlbWJpYmUtdXNlci5jb20ifQ.vTK3HH_YBxKRQW4bfUZHOZJCyeA0_F05mz5ZraxpItd0GuL61V15z5Q5OTY7cxmUxu5QRUsR2K1uPQB1Cu6XSw',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'browser-id': 'Mzg1OTk0NDUyOTM0X3dlYg==',
            'sec-ch-ua-platform': '"macOS"',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Accept-Language': 'en-US,en;q=0.9',
            'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({"videoIdList": ["5914905"
                                                ],
                                "examName": "8th ICSE",
                                "goalName": "ICSE",
                                "locale": "en",
                                "module": "learn"

                                })
        response = self.client.post("/fiber_ms/videoscontentstatus", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
