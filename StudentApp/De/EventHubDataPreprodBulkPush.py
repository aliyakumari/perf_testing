import json

from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms-cf.embibe.com"
resource.setrlimit(resource.RLIMIT_NOFILE, (250240, 250240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = json.dumps({'payload':{
                "event": "Page",
                "eventCategory": "Page",
                "eventAction": "PQ_SWU_BTN_Click",
                "page_path": "PQ_SWU_BTN_Click",
                "feature_name": None,
                "module_name": None,
                "error_type": None,
                "error_code": None,
                "userId": 2005199220,
                "primary_goal": "CBSE",
                "primary_exam": "6th CBSE",
                "exam_category": "K12",
                "ScreenName": "Learn Home",
                "account_id": 20051992009090,
                "locale": "en",
                "is_paid": "false",
                "account_type": "Student",
                "org_id": "1",
                "source": "web",
                "allow_branch": False,
                "timestamp": 1664297692871,
                "platform_session_id": "8c70d50a-fa85-4868-ac17-d3d5bbf8aea4",
                "learn_path_name": None,
                "gtm.uniqueEventId": 12
            }})

        self.headers = {
              'Accept': 'application/json',
              'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
              'Connection': 'keep-alive',
              'Content-Type': 'application/json',
              'Origin': 'https://www.embibe.com',
              'Referer': 'https://www.embibe.com/',
              'Sec-Fetch-Dest': 'empty',
              'Sec-Fetch-Mode': 'cors',
              'Sec-Fetch-Site': 'same-site',
              'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
              'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoyMDA1MTk5MjAwLCJjcmVhdGVkIjoxNjY0Mjk3NjkyLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjIwMDUxOTkyMjAsImV4cCI6MTY2NTUwNzI5MiwiZGV2aWNlSWQiOiIxNjY0Mjk3NDg1MDk4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIyMDA1MTk5MjAwXzc2MDg3ODc0MTcyOTMyM0BlbWJpYmUtdXNlci5jb20ifQ._Rsj5PLmBktjGQbSljyxpPHSKENEIgrS5qPARH3chrxQqvQed0MX7qQ_o3Udz5OBIFb-4xY-ee_ZAXaLVI2oUQ',
              'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"',
              'sec-ch-ua-mobile': '?0',
              'sec-ch-ua-platform': '"macOS"',
              'Cookie': 'embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MzM2Yjg1YjJmNWE5MDJiMWM3ZjczMWQiLCJzdWJkb21haW4iOiJodHRwczovLzYzMzZiODViMmY1YTkwMmIxYzdmNzMxZC1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.qLMTEhIWa0dttjLSQl9TpfSZMYOWT-h7rkCL6NPpyPtrksxArPNvSUztqhQ98aj_L4zptKNAimmMXWIwxSpG1w; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbGl5YSAiLCJsYXN0TmFtZSI6Ikt1bWFyaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wOS0zMCAwOTo1NTozOCBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzMzZhMWYwZWM5ZDIyMWU3NDhmYzNhZSIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODIxMDk1MDkyNiIsImFkbWluSWQiOiI2MzM2YTFmMWVjOWQyMjFlNzQ4ZmMzYWYiLCJpZCI6MTUwMzE2NzAyMCwicm9vdE9yZ0lkIjoiNjMzNmExZjBlYzlkMjIxZTc0OGZjM2FlIiwiZW1haWwiOiJhdXRvbWF0aW9uLnVpMTExMUBlbWJpYmUuY29tIn0.IUhu94lwn_qiIwEgijd9152eJHXLufRpk8McBtprlqvCAXtD3gVVJDC0_t_ChqoM2MusQ-36yfNWnZV4kNmrUA; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=b80aa0bc-6f99-481a-926d-8094ef9d4282; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjQ1MjQ4MDYsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMzZhMWYwZWM5ZDIyMWU3NDhmYzNhZSIsIm1vYmlsZSI6IjgyMTA5NTA5MjYiLCJpZCI6MTUwMzE2NzAyMCwiZXhwIjoxNjY0NjExMjA2LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiYXV0b21hdGlvbi51aTExMTFAZW1iaWJlLmNvbSJ9.A9RO4-WU_n4SsgQdA2CKRiOg4mbOxAiFKYyRX2mXMyWwAMWYtnFy7rHlxwFX1OaIrIeAQBcHIFaSblPjGZ_MzA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjQ1MjQ4MDYsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMzZhMWYwZWM5ZDIyMWU3NDhmYzNhZSIsIm1vYmlsZSI6IjgyMTA5NTA5MjYiLCJpZCI6MTUwMzE2NzAyMCwiZXhwIjoxNjY0NjExMjA2LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiYXV0b21hdGlvbi51aTExMTFAZW1iaWJlLmNvbSJ9.A9RO4-WU_n4SsgQdA2CKRiOg4mbOxAiFKYyRX2mXMyWwAMWYtnFy7rHlxwFX1OaIrIeAQBcHIFaSblPjGZ_MzA'
            }


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        event = ['fiber-redis-event', 'preprod-embibe-clickstream-events', 'preprod-embibe-create-events',
                 'preprod-embibe-dsl-services', 'preprod-embibe-teach-events']
        url = f"/de/events/push/v2?namespace={event[randrange(len(event))]}&topic=testing-hub"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
