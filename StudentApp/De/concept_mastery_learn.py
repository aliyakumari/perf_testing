from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2021-08-23.a2718262528a0e85f1c04f0de66362a7; embibe-refresh-token=122bec8e-7a39-471c-9843-c9ed8808316d; preprod_ab_version=null; preprod_embibe-refresh-token=ab4bc664-05c8-4f16-9103-b06ed187dcd1; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MzgxODMxNTIsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTMyMjc3ODQ5IiwiaWQiOjE1MDExODM2MzQsImV4cCI6MTYzODM1NTk1MiwiZGV2aWNlSWQiOiIwMTIzNDU2Ny04OUF0Q0RFRi0wMTIzNDU2Ny04OUFCQ0RFRiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjp0cnVlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZX0.H7VtFY0V0n-drtes9ITmDst41KgvlgHylPw_vsEJfAxXtAN3L4OwefcQGWka524eFdiGqzUK_axAUz530IsfdQ; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2Ny04OUF0Q0RFRi0wMTIzNDU2Ny04OUFCQ0RFRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1U1c0aUxDSnZjbWRmYVdRaU9pSXhJaXdpYjNSd1gzSmxjWFZsYzNSZmFXUWlPaUkzTlRjMk1EQTBaaTAxWldKbExUUTVORFl0T0dNeE9DMHhPRFppTXpSalpEazFPREVpTENKcFpDSTZJams1TXpJeU56YzRORGtpTENKc2IyTmhiR1VpT2lKd1lTSjkuTUstbTFhMDNVMTY2OGd5ZzNldVo2VFJMeGx0Ui1OSTdiZUNoczloclZ1c3ZqaVNOcjJWZWk1UmY3NTJwX1dFM0ZJanJTUktIUHhtc0ViWGVKZ1J6VmciLCJjcmVhdGVkIjoxNjM4MTgzMTI3LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduSW4iLCJhdHRlbXB0IjoxLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiOTkzMjI3Nzg0OSIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDExODM2MzQsImV4cGlyeSI6MTYzODE4MzcyNywiZXhwIjoxNjM4MTgzNzI3fQ.xmYJd0R9V_WsqRQN6U4GpYBgq253OyqtSS9iDVf4cX9AVsP51y_2IcnQnu6zJ-d38R0Zboz4elAeA2m9R0rlDQ; prod_embibe-refresh-token=4a01f727-c875-462b-8928-d1428e3652e5; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        self.body = json.dumps({
            "userId": 1500000030,
            "conceptId": "new_KG16255",
            "watchedDuration": 300,
            "totalDuration": 373,
            "contentId": 5915738,
            "timestamp": 3215187
        })
        response = self.client.post("/api/v1/de/cm/learn", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
