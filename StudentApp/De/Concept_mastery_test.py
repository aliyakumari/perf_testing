from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


# goal_rnum = randrange(len(goal))
# exam_rnum = randrange(len(exam[goal_rnum]))
#
# print(goal[goal_rnum],  exam[goal_rnum][exam_rnum])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        self.body = json.dumps({
            "user_id": "1501185234",
            "correctness": 1,
            "concepts": [
                {
                    "concept_code": "new_KG16326",
                    "concept_display_name": "n- Arithmetic Means between Two Numbers",
                    "concept_importance": 0.77,
                    "concept_title": "n- Arithmetic Means between Two Numbers",
                    "is_primary_concept": True,
                    "kg_concept_code": "KG13990"
                }
            ],
            "status": "NotVisited",
            "badge": "non-attempt",
            "attempt_created_at": 1637416638871,
            "attemptTypeBadge": "OvertimeCorrect",
            "question_id": 3215187,
            "difficulty_level": 3
        })
        response = self.client.post("/api/v1/de/cm/test", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
