from model.userms_connect import connect
import jwt
import random
import pandas as pd


def creatEmbibeToken_():
    user_id_and_parent_id_data = connect("select user_id, is_child_of from user_relations ur;")

    embibe_token  = []

    for data in user_id_and_parent_id_data:
        parent_app_embibe_token = []
        payload = {
  "country": 1,
  "user_type": 1,
  "parent_user_id": data[1],
  "created": 1662636221235,
  "organization_id": "1",
  "id": data[0],
  "exp": 1662718796,
  "deviceId": f"1646836153948{random.randint(1,10000000000)}",
  "mobile_verification_status": False,
  "email_verification_status": False,
  "email": "1500015940_7131577218964392@embibe-user.com"
}


        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af5"
                             "89c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")
        parent_app_embibe_token.append(encoded)
        parent_app_embibe_token.append(data[0])
        embibe_token.append(parent_app_embibe_token)

    return embibe_token

def creatEmbibeToken():
    cursor = connect("select * from users where email <> '';")
    embibe_token = []
    import requests
    cursor = list(cursor)
    for data in cursor[1::10000]:
        parent_app_embibe_token = []
        url = f"https://preprodms.embibe.com/user_auth_ms/user/details?email={data[3]}&autoAccountCreation=true"

        payload = {}
        headers = {
            'auth-token': 'ipTt6XyE4e67QDvRy4iw8a3XLH2P4vaW2P5y59Sg4Tvbgg33xqw6fuh9QDv8Wrc88QyzJTFUEQmgxpjpnrE5GQ8KEAHFtAZ5TarC5MqSZg4xexbUQWNBNK2zPafthyyDzdDiS8KH9CaYPBDMJgBxNeG62BNpUBXADcSSgJGHGaaBUPYiMeKDCnZyxAr2fDQEJg8V4cpv',
            'Cookie': 'embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415'
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code == 200:
            temp_embibe_token = response.json().get('data').get('embibe-token')
            parent_app_embibe_token.append(temp_embibe_token)
            parent_app_embibe_token.append(data[0])
            parent_app_embibe_token.append(temp_embibe_token)
            embibe_token.append(parent_app_embibe_token)

    return embibe_token

