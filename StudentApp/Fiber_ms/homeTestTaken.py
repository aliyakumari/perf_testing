from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


print(embibe_token[0])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'accept': '*/*',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDM2NjE1LCJjcmVhdGVkIjoxNjQ0NDc4NzExLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTg0NzQsImV4cCI6MTY0NDU2NTExMSwiZGV2aWNlSWQiOiIxNjQzOTY2OTk3NjMwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDM2NjE1XzI5ODgxMTYyODg2MTExNTVAZW1iaWJlLXVzZXIuY29tIn0.zX9Ssl8nkpJKAf-oWlLQ0iN-XQ63UUQTfHaCHzGHIpk7OXZeqXw9ZZKQc7tfDlq0Z4ZWjlY7yOqNqwOVjBCAEw',
  'Content-Type': 'application/json',
  'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({ "board": "cbse", "channel": "web", "child_id": embibe_token[rnum][0], "exam": "10th-cbse", "exam_name": "CBSE", "format_reference": "5f5206b031562a10f6a592ff", "goal": "cbse", "language": "en", "learnpath_name": "cbse--12th cbse--biology", "locale": "en", "mode_is": "web", "subject": "mathematics", "test_code": "mb108"})
        url = f"/fiber_ms/home/tests-taken"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
