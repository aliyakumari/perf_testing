import requests

url = "https://preprodms.embibe.com/fiber_ms/books_and_questions?format_reference=5f5207e158ae126dab8a370e&learnpath_name=ssc--ib security assistant or executive tier 1--general studies--computer awareness--computer and internet basics"

payload={}
headers = {
  'accept': '*/*',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDM2NjE1LCJjcmVhdGVkIjoxNjQzOTk0NzYxLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwMzY2MTYsImV4cCI6MTY0NDA4MTE2MSwiZGV2aWNlSWQiOiIxNjQzOTY2OTk3NjMwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDM2NjE1Xzk0OTg0MjQ3MDE0NjUzMTBAZW1iaWJlLXVzZXIuY29tIn0.tjb-DtB--s0yMobXtP8ADzj2N2zaz5eURr9EhojJCfubmFhN0loRbPToVDEB_pdeTkLVwiO0p8WvBSfqczSK8A',
  'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}

response = requests.request("GET", url, headers=headers, data=payload)

print(response.text)
