from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


print(embibe_token[0])


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:96.0) Gecko/20100101 Firefox/96.0',
            'Accept': 'application/json',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxMzM5Njk5LCJjcmVhdGVkIjoxNjQyNzU3OTIyLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDEzMzk3MDAsImV4cCI6MTY0MjkzMDcyMiwiZGV2aWNlSWQiOiIxNjQyNjY5MTkzMDAxIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxMzM5Njk5XzU5MTcxMjQ2NzcyNzM1MTRAZW1iaWJlLXVzZXIuY29tIn0.8a8lFKDsvGIugWisCsdcwQZg9ZvrOO-dpAwLt8t1gXhXI8PhaeXFCwWe33xSAbwLY17jcpHskjHXy4d9M9uVyw',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Connection': 'keep-alive',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57; access-token=j49HWwIteqKJUbjU4H4Bqg; client=hhzO_QfVnXIfaXkDOZ6UKw; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6NTUzNywiZW1haWwiOiJndWVzdF8xNjQyMDgxMzI0ODIzNzY2QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIyLTAxLTEzVDEzOjQyOjA0LjQ4MVoifQ.08ym6N7xliVRPE0IW2ATH0B2aEL2rUlOXzl7PGz0L3X96UpB8aT2ZOAxdFbJEOxkanXmcJJG_dx2s01KIHZf_A; uid=guest_1642081324823766%40embibe.com'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({"child_id": embibe_token[rnum][0], "exam_name": "9th CBSE", "goal": "CBSE",
                                "content_section_type": "BOOKS", "offset": 10, "size": 10, "locale": "en"})
        url = f"/fiber_ms/v1/home/sections"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
