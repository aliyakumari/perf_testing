from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


print(embibe_token[0])


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': '*/*',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDM2NjE1LCJjcmVhdGVkIjoxNjQ0NDc4NzExLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTg0NzQsImV4cCI6MTY0NDU2NTExMSwiZGV2aWNlSWQiOiIxNjQzOTY2OTk3NjMwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDM2NjE1XzI5ODgxMTYyODg2MTExNTVAZW1iaWJlLXVzZXIuY29tIn0.zX9Ssl8nkpJKAf-oWlLQ0iN-XQ63UUQTfHaCHzGHIpk7OXZeqXw9ZZKQc7tfDlq0Z4ZWjlY7yOqNqwOVjBCAEw',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps(
            {"exam_code": "kve99562", "format_reference_id": "5ec5867a0c88fe5860961943", "goal_code": "kve97670",
             "locale": "en", "query": "*", "section": "recommended practice", "size": 50, "source": "Test Page Summary",
             "test_code": "mb42881", "user_id": 1, "version": 0,
             "xpath": "/mock-test/10th-cbse/mathematics/real-numbers-chapter-test/real-numbers-test-1"})
        url = f"/fiber_ms/home/test-recommended-learn"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
