from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


print(embibe_token[0])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDM2NjE1LCJjcmVhdGVkIjoxNjQ0NDc4NzExLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTg0NzQsImV4cCI6MTY0NDU2NTExMSwiZGV2aWNlSWQiOiIxNjQzOTY2OTk3NjMwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDM2NjE1XzI5ODgxMTYyODg2MTExNTVAZW1iaWJlLXVzZXIuY29tIn0.zX9Ssl8nkpJKAf-oWlLQ0iN-XQ63UUQTfHaCHzGHIpk7OXZeqXw9ZZKQc7tfDlq0Z4ZWjlY7yOqNqwOVjBCAEw',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({"callback":"","format_reference":"5ec5867a0c88fe5860961943","learnpath_name":"cbse--10th cbse","organization_id":10304,"test_format":{"difficulty_level":"medium","difficulty_level_stats":{"avg_difficulty_level":3.36,"maximum_difficulty_level":6,"minimum_difficulty_level":1,"std_difficulty_level":1.2},"subject":[{"chapter":[{"display_name":"Statistics","name":"Statistics","no_of_questions":3,"display_name_locale":{"hi":"सांख्यिकी","en":"Statistics"},"chapter_learnpath":"cbse--10th cbse--mathematics--statistics and probability--statistics","selected":True},{"display_name":"Triangles","name":"Triangles","no_of_questions":2,"display_name_locale":{"hi":"त्रिभुज","en":"Triangles"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--triangles","selected":True},{"display_name":"Polynomials","name":"Polynomials","no_of_questions":4,"display_name_locale":{"hi":"बहुपद","en":"Polynomials"},"chapter_learnpath":"cbse--10th cbse--mathematics--algebra--polynomials","selected":True},{"display_name":"Surface Areas and Volumes","name":"Surface Areas and Volumes","no_of_questions":7,"display_name_locale":{"hi":"पृष्ठीय क्षेत्रफल और आयतन","en":"Surface Areas and Volumes"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--surface areas and volumes","selected":True},{"display_name":"Quadratic Equations","name":"Quadratic Equations","no_of_questions":4,"display_name_locale":{"hi":"द्विघात समीकरण","en":"Quadratic Equations"},"chapter_learnpath":"cbse--10th cbse--mathematics--algebra--quadratic equations","selected":True},{"display_name":"Pair of Linear Equations in Two Variables","name":"Pair of Linear Equations in Two Variables","no_of_questions":3,"display_name_locale":{"hi":"दो चर वाले रैखिक समीकरण युग्म","en":"Pair of Linear Equations in Two Variables"},"chapter_learnpath":"cbse--10th cbse--mathematics--algebra--pair of linear equations in two variables","selected":True},{"display_name":"Areas Related to Circles","name":"Areas Related to Circles","no_of_questions":3,"display_name_locale":{"hi":"वृत्तों से संबंधित क्षेत्रफल","en":"Areas Related to Circles"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--areas related to circles","selected":True},{"display_name":"Real Numbers","name":"Real Numbers","no_of_questions":4,"display_name_locale":{"hi":"वास्तविक संख्याएँ","en":"Real Numbers"},"chapter_learnpath":"cbse--10th cbse--mathematics--arithmetic--real numbers","selected":True},{"display_name":"Probability","name":"Probability","no_of_questions":4,"display_name_locale":{"hi":"प्रायिकता","en":"Probability"},"chapter_learnpath":"cbse--10th cbse--mathematics--statistics and probability--probability","selected":True},{"display_name":"Introduction to Trigonometry","name":"Introduction to Trigonometry","no_of_questions":4,"display_name_locale":{"hi":"त्रिकोणमिति का परिचय","en":"Introduction to Trigonometry"},"chapter_learnpath":"cbse--10th cbse--mathematics--trigonometry--introduction to trigonometry","selected":True},{"display_name":"Arithmetic Progressions","name":"Arithmetic Progressions","no_of_questions":3,"display_name_locale":{"hi":"समांतर श्रेणियाँ","en":"Arithmetic Progressions"},"chapter_learnpath":"cbse--10th cbse--mathematics--algebra--arithmetic progressions","selected":True},{"display_name":"Constructions","name":"Constructions","no_of_questions":3,"display_name_locale":{"hi":"रचनाएँ","en":"Constructions"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--constructions","selected":True},{"display_name":"Some Applications of Trigonometry","name":"Some Applications of Trigonometry","no_of_questions":5,"display_name_locale":{"hi":"त्रिकोणमिति के कुछ अनुप्रयोग","en":"Some Applications of Trigonometry"},"chapter_learnpath":"cbse--10th cbse--mathematics--trigonometry--some applications of trigonometry","selected":True},{"display_name":"Circles","name":"Circles","no_of_questions":3,"display_name_locale":{"hi":"वृत्त","en":"Circles"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--circles","selected":True},{"display_name":"Coordinate Geometry","name":"Coordinate Geometry","no_of_questions":3,"display_name_locale":{"hi":"निर्देशांक ज्यामिति","en":"Coordinate Geometry"},"chapter_learnpath":"cbse--10th cbse--mathematics--geometry--coordinate geometry","selected":True}],"display_name":"Mathematics","name":"Mathematics","no_of_questions":55,"question_type_data":{"FillInTheBlanks":{"negative_marks":0,"partial_mark":0,"positive_marks":1,"question_count":5},"SingleChoice":{"negative_marks":0,"partial_mark":0,"positive_marks":1,"question_count":27},"SubjectiveAnswer":{"negative_marks":0,"partial_mark":0,"positive_marks":1,"question_count":18},"TrueFalse":{"negative_marks":0,"partial_mark":0,"positive_marks":1,"question_count":5}},"display_name_locale":{"hi":"गणित","en":"Mathematics"},"subject_learnpath":"cbse--10th cbse--mathematics"}],"total_questions":135,"total_time":360},"test_name":"10th CBSE Test - 1","version":"2.0","language":"en","pre_req_exams":["cbse--9th cbse","cbse--8th cbse","cbse--7th cbse","cbse--6th cbse"],"locale":"en","difficulty_level":"medium","duration":5,"marking_scheme":{},"is_quick_test":True,"name":"","exam_code":"kve97915","created_at":"2022-02-10T08:28:05.354Z","last_modified_at":"2022-02-10T08:28:05.354Z","resource_type":"test","source":"fiber"})
        url = f"/fiber_ms/v1/atg/test_v2"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
