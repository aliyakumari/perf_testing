from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

print(embibe_token[0])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjI3MTE2NDUsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMjc3NDQ4NiwiZXhwIjoxNjYyNzk4MDQ1LCJkZXZpY2VJZCI6InRlbXAtc2Vzc2lvbi0wOGFhMzdjM2I2NmYzMjA0Njg2MWRmOGY4NGRjNDkwOCIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6dHJ1ZSwiZW1haWwiOiJ0ZXN0Y2V1dGU2NTg2Njc3QGVtYmliZS5jb20ifQ.Y2m_AOt1b3TjJwNXFJfmfcZemAScPf5Y32B-PoQtRmAfRj8-T27sLezwYL5yxcsKtYyhYEvZVg9hXJU3OtZIwQ',
  'sec-ch-ua': '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'Cookie': 'embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415'
}



    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = {}
        url = f"/fiber_ms/attempt_quality?&format_reference=5ec5867a0c88fe5860961943&learnpath_format=goal--exam" \
              f"--subject--unit--chapter--topic&learnpath_name=cbse--10th%20cbse--mathematics--algebra--pair%20of" \
              f"%20linear%20equations%20in%20two%20variables&type=practice&locale=en "
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
