from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# # from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe_Token - preprod profile token.csv' )
embibe_token = df.values.tolist()

print(embibe_token[0])


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ0ZW5hbnRfaWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxNDEzMjUzLCJjcmVhdGVkIjoxNjc4OTY5MTcwLCJvcmdhbml6YXRpb25faWQiOiIxIiwidGVuYW50X3R5cGUiOiJTY2hvb2wiLCJpZCI6MTUwMTQxMzI1NCwiZXhwIjoxNjc5MDU1NTcwLCJkZXZpY2VJZCI6IjE2Nzg5NjA5NzM0NTQiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDE0MTMyNTNfOTczMjAwNjY1ODA1MTQ0OEBlbWJpYmUtdXNlci5jb20ifQ.gS3OI3V5iLtZmcKbB4d90SA5g6J-Xd0f5TVtpMtydTSix_WK3IMYwM-q8_my19LYdAbvCp-BExERT4uz1OX8wQ',
}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = {}
        url = f"/fiber_ms/v2/chapters/chapterDetail/608901bb6397c0ec291f2393?type=Practise_Chapters&locale=en&kv_code=kve97918&format_reference=5ec5867a0c88fe5860961943"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
