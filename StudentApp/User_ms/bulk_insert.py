from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


def random_string_generator(str_size, allowed_chars=string.ascii_letters):
    return ''.join(random.choice(allowed_chars) for x in range(str_size))


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/95.0.4638.69 Safari/537.36',
            'sec-ch-ua-platform': '"macOS"',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': '_ga=GA1.2.693867182.1629562474; _gid=GA1.2.1576764851.1637644747; ajs_anonymous_id=%22b0bad659-39a7-4200-a15e-9ee886219913%22; ajs_user_id=1500021418; session=.eJyFkV1vgjAYhf8K6bVhLbAFSUxGBjKTAVHxA26WAp2AVIgUBhr_-wRRd7GPqzbnfXp6Tt4jCIr9xzvLtmQHlCPgfKAA2_GotZocbMNE1sqtbUcXzINae9qYetq0sbVZZGlhYrZ3x0vcRD-4n6MROA0AoThOzx44jRvMb0uK9_EzoX7sEz7IKOgJJLaMH8U7XF2oHyAB_mm02WdlfgvtG8MkXFvQM8bQdSZdmPg-xq8zGGhZ9XbQP98SHZpzqTZfpHO3xZOtqZI9veRPs6As2O2ZqV7kfB_ftIWmdlrBMCuLtkk_mK69CK_qyKV1egfaFr8AjHz7yukLTPQxCo0hc9ez3Bek0RUMcEGKe7C-Iosp6cSwtSjJgBNEzsoqToAC4uCTgmRFFDnDdDo622xScjMJhKgK-8Uxtr03NJbNOcZ0Plebbtbk5L-cm7mxjDyjTgPR7AzLgoTdmiPGcuXhAUEeSSKPeFlWZCgPwQXplvwdQXx7DB-vUEWBIp6-AF-AzD8.YZyNVw.PgK2XhnJYftrLDo9SgN9cpdJs6I; _dc_gtm_UA-48005755-23=1; _hp2_id.2562464476=%7B%22userId%22%3A%222111562215681409%22%2C%22pageviewId%22%3A%221914048664985761%22%2C%22sessionId%22%3A%228848534279487760%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; _ga_TT8L73VP3H=GS1.1.1637652196.6.0.1637652196.60; _hp2_ses_props.2562464476=%7B%22r%22%3A%22https%3A%2F%2Fstaging-fiber-web.embibe.com%2F%22%2C%22ts%22%3A1637652195160%2C%22d%22%3A%22staging-fiber-web.embibe.com%22%2C%22h%22%3A%22%2F%22%2C%22q%22%3A%22%3Fredirect_url%3D%252Fpractice%252Fsummary%252F5ec58c0b0c88fe58609725a9%253Flearnmap_id%253D5ec5867a0c88fe5860961943%25252FCBSE%25252F10th%252520CBSE%25252FMathematics%25252FAlgebra%25252FArithmetic%252520Progressions%22%7D; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=c72abe36-7c9a-4036-bf0b-aca2e59e4912; preprod_ab_version=0; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NTI0MjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5MDA0NzIxNDc4IiwiaWQiOjE1MDExNjU0MjksImV4cCI6MTYzNzgyNTIyNiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZX0.rTxDMWjxogbPtZ8BJ1151dIfsMGecHzlvgjMa_kEoURi78l68Ukg2oDuIsc-a-26BRq0xAf6p7lkaEXJPz6Czw; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVZYQWlMQ0p2Y21kZmFXUWlPaUl4SWl3aWIzUndYM0psY1hWbGMzUmZhV1FpT2lKa05HWTFPV0UwWWkwd01qWXhMVFF6TVdZdE9EYzBaQzAyWVRKbE16QXlPVGM1TldRaUxDSnBaQ0k2SWprd01EUTNNakUwTnpnaWZRLk43VjBQd0l2b2cwdW9rczhWYmt5NUJGUDlhUEJOUDlCQ1RhYjFTQ0VVcTNnYWR5WnBROFYxaTFXTzJQdmZYME51cC1Bc3VhaENmb3dPcjJsLTZFWXR3IiwiY3JlYXRlZCI6MTYzNzY1MjQyNiwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnblVwIiwiYXR0ZW1wdCI6MCwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6IjkwMDQ3MjE0NzgiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAxMTY1NDI5LCJleHBpcnkiOjE2Mzc2NTMwMjYsImV4cCI6MTYzNzY1MzAyNn0.LL3_a-GVl0H1-Nc4yIRG0pBz8B1HHwBPEDLk-A3uUmNOgbsZ2512UG4sg2GdHVIRKvwv6cvAODiCjkWXebty1Q; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
        }

        self.test = [123]

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        try:
            self.body = json.dumps({
                "org_id": "1",
                "data": [
                    {
                        "email": f"{random_string_generator(5)}_{random_string_generator(5)}@embibe.jan22.com",
                        "password": "Embibe@1234",
                        "first_name": "testing",
                        "user_type_id": 1
                    }
                ]
            })
        except:
            pass

        response = self.client.post("/user_auth_ms/bulk_insert", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
