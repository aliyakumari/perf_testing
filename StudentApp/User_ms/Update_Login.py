from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

df = pd.read_csv("embibe_token.csv")
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMjg4NDE2LCJjcmVhdGVkIjoxNjQxNDc1ODQ1LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTcxNTIsImV4cCI6MTY0MTU2MjI0NSwiZGV2aWNlSWQiOiJ0ZW1wLXNlc3Npb24tNmNlMGViNGM3MjQ4NjQ4NDRhNWRiZGM2NDRiOWUwMGYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDAyODg0MTZfMTUxMDU5NDY0MzY2NzgxMEBlbWJpYmUtdXNlci5jb20ifQ.XYimpXICvpF_aquo895wn5JzxW3KXxGVC27BiMOeGFQbazknJtChuz3Ej1YWONm6MnKg77gOQxdQEGsFmJsupQ',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57; preprod_ab_version=0'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        num = randrange(10000, 90000)
        # self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({
            "login": "8210950926",
            "mobile": f"82109{num}"
        })

        response = self.client.put("/user_auth_ms/update_login", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
