import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

# df = pd.read_csv('org_token.csv')
# org_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjc1MjMxMTczNzQwIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUkwWWpFMk1qTTRZeTAwWW1WbUxUUmtaRFF0T1RNMU5pMWhOekZsTUdFeE1tUXlaR0VpTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SWpkaFl6VTJaRGt4TFRNME1ESXRORE5qTUMwNE56a3dMVFZoTkdWalkyVTBOakUzTnlJc0ltbGtJam9pT0RJeE1EazFNRGt5TmlJc0lteHZZMkZzWlNJNkltVnVJaXdpY0d4aGRHWnZjbTBpT2lKWFJVSWlmUS5udG5EUjNUcE5lYTYtcHJ4TTIzSEVTME94cE5RSVVkRWFPNTBrVnZaX1FpTndGckY4LTJEQVE1bWNBQl9yUG5wS0VKa01WMmJxVkZJNndvcWY2akRWdyIsImNyZWF0ZWQiOjE2NzUyMzExNzUsIm90cF9vcGVyYXRpb25fdHlwZSI6IlNpZ25JbiIsImF0dGVtcHQiOjEsInR0bCI6IjYwMCIsInBsYXRmb3JtIjoiV0VCIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6IjgyMTA5NTA5MjYiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAwMjg4NDE2LCJleHBpcnkiOjE2NzUzMTc1NzUsImpvdXJuZXlfaWQiOiJTSUdOX0lOXzhmNTczMTAyLTY0MTctNDZlNS05NTU5LWFkMmRmZDIyOWNiNSIsImV4cCI6MTY3NTMxNzU3NX0.od726__eQCOtLHa3QThLynt3sftfRwv4yn6D5Mm8q2EaxmxNnAp_vwCD-y-_t3p-On9VYqEbcojCApyWg1j9xg',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjc1MjMxMTczNzQwIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUkwWWpFMk1qTTRZeTAwWW1WbUxUUmtaRFF0T1RNMU5pMWhOekZsTUdFeE1tUXlaR0VpTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SWpkaFl6VTJaRGt4TFRNME1ESXRORE5qTUMwNE56a3dMVFZoTkdWalkyVTBOakUzTnlJc0ltbGtJam9pT0RJeE1EazFNRGt5TmlJc0lteHZZMkZzWlNJNkltVnVJaXdpY0d4aGRHWnZjbTBpT2lKWFJVSWlmUS5udG5EUjNUcE5lYTYtcHJ4TTIzSEVTME94cE5RSVVkRWFPNTBrVnZaX1FpTndGckY4LTJEQVE1bWNBQl9yUG5wS0VKa01WMmJxVkZJNndvcWY2akRWdyIsImNyZWF0ZWQiOjE2NzUyMzExNzUsIm90cF9vcGVyYXRpb25fdHlwZSI6IlNpZ25JbiIsImF0dGVtcHQiOjEsInR0bCI6IjYwMCIsInBsYXRmb3JtIjoiV0VCIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6IjgyMTA5NTA5MjYiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAwMjg4NDE2LCJleHBpcnkiOjE2NzUzMTc1NzUsImpvdXJuZXlfaWQiOiJTSUdOX0lOXzhmNTczMTAyLTY0MTctNDZlNS05NTU5LWFkMmRmZDIyOWNiNSIsImV4cCI6MTY3NTMxNzU3NX0.od726__eQCOtLHa3QThLynt3sftfRwv4yn6D5Mm8q2EaxmxNnAp_vwCD-y-_t3p-On9VYqEbcojCApyWg1j9xg'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        token_data = []
        # self.headers['otp-token'] = org_token[randrange(len(org_token))][0]

        url = f"/user_auth_ms/verify-login-otp"
        body = {"otp": "123456"}
        response = self.client.post(url, data=json.dumps(body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
