from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Content-Type': 'application/json',
  'os-version': 'os-version',
  'device-model': 'model-device',
  'device-brand': 'Brand-device',
  'device-type': 'type-Device',
  'platform': '1',
  'os-name': 'os_name',
  'browser-name': 'browser-name',
  'Cookie': 'embibe-refresh-token=7efebd2a-520f-4a0a-9c20-4ac0e05bf454; preprod_embibe-refresh-token=a112a5d5-4687-4664-9075-c22b17a6ce25; prod_embibe-refresh-token=d8dfeeca-b88e-43a5-9f1c-bdf5496fcd29; school_prod_embibe-refresh-token=4be059a3-13d2-4509-925a-d8ad6c53b372; embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415; preprod_ab_version=0; preprod_embibe-refresh-token=a5af5ff1-032d-4a3d-a88c-082158acaff9; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjAwMjc5NjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTMyNzc1OTA0IiwiaWQiOjE1MDAwNTQwNTEsImV4cCI6MTY2MDExNDM2NiwiZGV2aWNlSWQiOiIwMTIzNDU2OC04OUNoc0JDREVkZkctZDAwMmQ4NGhzNTYwLTgwQUJDZERNRiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoidGVzdDAxQGdtYWlsLmNvbSJ9.jGDwW-LoW1_yybLS1EqToBv-aOn2ZzKD2ZnCUAoziVXxDRYbSoYwLcypWlZhkwB3Xe9gwbN1iJx0shBjnEL8FA; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNoc0JDREVkZkctZDAwMmQ4NGhzNTYwLTgwQUJDZERNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1U1c0aUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKbE56QXlZelUyT0MwMVlUYzBMVFJsTnpRdE9UQTROUzA1WldZeE16VXpZMll4WldFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNkltRmxPRFJtTUdZd0xURmxPVFl0TkRVMlpDMWlaRE16TFRjNE16RXpNR1ZpWkRRNFppSXNJbWxrSWpvaU9Ua3pNamMzTlRrd05DSXNJbXh2WTJGc1pTSTZJbVZ1SW4wLnBJVmdyZWhzRDNxdzFqOGV2dktWTmM2TGowT0xTamN5cmtOMXIzMmtsMm0xU01hM0hqY1pKNDZaczJRMGQ2OTRQS2YwX3Itc01fZFQxVk5uZjlfNjZRIiwiY3JlYXRlZCI6MTY2MDAyNzk2Niwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6InRlc3QwMUBnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAwMDU0MDUxLCJleHBpcnkiOjE2NjAxMTQzNjYsImV4cCI6MTY2MDExNDM2Nn0.sI9TbNe50rp7Wv0XNfIWMgJWRALi8Xsepnma-8R1_2DoA22c7mZbL16V6Eq8z2BdZ3A4BENC9l9LK9y3LMe8Lw'
}

        self.test = [123]

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        try:
            num = randint(400000000, 800000000)

            self.body = json.dumps({
                "email": "test01@gmail.com",
                "password": "password",
                "device_id": f"01234568-89ChsBCDEdfG-d002d84hs560-80ABCdDMF-load-test-{randint(1, 100000)}",
                "connected_response": True
            })
        except:
            pass

        if num not in self.test:
            response = self.client.post("/user_auth_ms/sign-in", data=self.body,
                                        headers=self.headers)

            self.test.append(num)
        else:
            num = randint(4000000, 8000000)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
