from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0',
            'Accept': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NDEzOTc0MTMsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTE5NzcxMywiZXhwIjoxNjQxNTcwMjEzLCJkZXZpY2VJZCI6IjE2NDEzOTc0MTMxNDEiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoicGxidWhrLmFjaGlldmUuZGVjMjFAZW1iaWJlLmNvbSJ9.La6otCnlLNNhe4mPC9WMGDX9ixzmYi7atjuiF2QS_Pth5Vu0ZScovKzDPPFYXtKz0d18ZI9f9y0v0cU40HTAVw',
            'Connection': 'keep-alive',
            'Cookie': 'embibe-refresh-token=4361e25b-3b7b-4606-8a2c-d1f81d5a2269; preprod_embibe-refresh-token=db6a8ee4-95e3-42fa-a06d-b93092cdef9d'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/user_auth_ms/connected_profiles", {}, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
