import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
from model.userms_connect import connect

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
main_data = []
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

user_id_and_parent_id_data = connect("select mobile  from users u ;")

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.count = 0
        self.headers = {
            'org-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiJpbnRlZ3JhdGlvbl90ZXN0aW5nIiwic3ViZG9tYWluIjoiaHR0cHM6Ly9zdHVkZW50LmludGVncmF0aW9uX3Rlc3RpbmcuZW1iaWJlLmNvbSIsInVzZXJfdHlwZV9pZCI6MX0.SxZTjr3zG7-jSM3j_AeWaEaP9RyWNVFoMrYOeWljgI87LGq1lWhVoCJndzpEWDyCGAsbKBg3sKP9PVrXBFbRrQ',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Cookie': 'preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rand_num = randrange(len(user_id_and_parent_id_data))
        url = f"/user_auth_ms/exists?mobile={user_id_and_parent_id_data[rand_num][0]}"
        body = {}
        with self.client.get(url, data=body, headers=self.headers, catch_response=True) as resp:
            if resp.status_code == 200:
                resp.success()
            elif resp.status_code == 400:
                self.count = self.count + 1
                resp.success()
                print(self.count)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
