from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'org-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJwbGF0Zm9ybV9zb3VyY2UiOiJFTUJJQkUiLCJvcmdfaWQiOiIxIiwic3ViZG9tYWluIjoiaHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tIiwidXNlcl90eXBlX2lkIjoxLCJ3aGl0ZV9saXN0X2lnbm9yZSI6ZmFsc2V9.hwhbt5lUX0ZBOqwLSnTspscb6ReKX9XUrWhsxU6gz4qofAGUSiJVGU6b7qScWAeQAQ0S69eJOEYWzD9JVI1V6g',
            'platform': 'platform',
            'os-version': 'os-version',
            'device-model': 'device-model',
            'device-brand': 'device-brand',
            'device-type': 'device-type',
            'Cookie': 'embibe-refresh-token=7efebd2a-520f-4a0a-9c20-4ac0e05bf454; preprod_ab_version=0; preprod_embibe-refresh-token=fdfd0cf3-8c1d-40ac-8d99-2a8fde222ffa; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjAwMjYxNDEsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTMyNzc1OTAwIiwiaWQiOjE1MDAwNTc0MzMsImV4cCI6MTY2MDExMjU0MSwiZGV2aWNlSWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoidGVzdDA2QGdtYWlsLmNvbSJ9.RlT4Ij7o8z5Wv_GvceYfEC7GIXerQiGI9LtjBedP6HkdG1a6-dyUZmkOe901ksMe9IUBOE8mBhBvW6I1m_vp-g; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1U1c0aUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lKaFptSXhOV05sTWkxbFlqa3hMVFJqTmpZdFltSmtZUzB4TkRBME9XVXlOakpsWkRraUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNkltWTFZelEwTm1JeExXVmpaVE10TkdSa01DMDVOVFU1TFRsbU5XRTFNalJpTWpBMk1TSXNJbWxrSWpvaU9Ua3pNamMzTlRrd01DSXNJbXh2WTJGc1pTSTZJbVZ1SW4wLlZsTEtCYTB4YkpHMHRmQWZKa0s0OG9vekJpaGs5eENNYUhYVGRHZGs0Q3poY1JiOFgyQzJ0akZERjUwV0dNbks3cDMwZ3dyeHlFNGpIYS1GUUdXQjd3IiwiY3JlYXRlZCI6MTY2MDAyNjE0MSwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6InRlc3QwNkBnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAwMDU3NDMzLCJleHBpcnkiOjE2NjAxMTI1NDEsImV4cCI6MTY2MDExMjU0MX0.KSp6aogyzsijSNKZYq8woTI5pcQAYYhuEdoYOy2h5BRlZ-Rp1FyK0auoJ3fjMChFmb9qQNj1X4vpsHxSkIZ2bw; prod_embibe-refresh-token=d8dfeeca-b88e-43a5-9f1c-bdf5496fcd29; school_prod_embibe-refresh-token=4be059a3-13d2-4509-925a-d8ad6c53b372; embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415; preprod_ab_version=0; preprod_embibe-refresh-token=aa839c85-b06f-4281-b6fa-280010b8545e; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjAwMjcwNzcsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTg3OTE0OCwiZXhwIjoxNjYwMTEzNDc3LCJkZXZpY2VJZCI6IjAxMjM0NTY4LTg5Q0JDREVHLTAwMjg0NTYwLTgwQUJDRE1GIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJ0ZXN0MDAwMDBAZ21haWwuY29tIn0.Q1HA6ZaoP5RFA10M7clTR7AxQLomr68h7Qh0b7qPuGW-ali9XOTTmFBBfxxYqQIyTG0yldZW2x-7uIQWs98krg; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRiIsIm90cF90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVelV4TWlKOS5leUpqYjNWdWRISjVYMk52WkdVaU9pSTVNU0lzSW1WMlpXNTBYM1I1Y0dVaU9pSlRhV2R1VlhBaUxDSnZjbWRmYVdRaU9pSXhJaXdpZEhKaFkydGZhV1FpT2lJM1pXVXlZekE1TlMwNU1UWmtMVFExWXprdFlqSTNZUzB4T1dZNVkyUmhaVFEyTmpRaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNklqWmpPV1V4WVRWbUxUVTBNalF0TkRZMk5pMDRPVFZtTFRka05HSmpOekZtTmpobE9TSXNJbWxrSWpvaWRHVnpkREF3TURBd1FHZHRZV2xzTG1OdmJTSXNJbXh2WTJGc1pTSTZJbVZ1SW4wLmk1N1NzYWhKb09EYjAxbGJYVlZNT0cyaFJiZmJsY2R3bkIzUnd5R3QtRFpqSHNkcHhVbmQ0SjNsTzFheHdVamRzS2hwZU83WlhHNFJRTEw5T2hEQUh3IiwiY3JlYXRlZCI6MTY2MDAyNzA3Nywib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnblVwIiwiYXR0ZW1wdCI6MCwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6InRlc3QwMDAwMEBnbWFpbC5jb20iLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAxODc5MTQ4LCJleHBpcnkiOjE2NjAxMTM0NzcsImV4cCI6MTY2MDExMzQ3N30.ncWyPyOIbK53b3L7geXdCyzLAS58UGdzhID2l6tWpYBqt6AWzSZW99TahC6TsAJs1UMr8uwwWvgXIbkZScMv9A'
        }

        self.test = [123]

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        try:
            num = randint(4000000, 800000000)
            self.body = json.dumps({
                "first_name": "Test5",
                "last_name": "Test",
                "email": f"test_QA_{num}@gmail.com",
                "password": "password",
                "device_id": f"01234568-89CBCD{num}EG-00284560-80ABCDMF",
                "meta": {
                    "books": "HC Verma",
                    "localtion": "Sri Lanka"
                }
            })
        except:
            pass

        if num not in self.test:

            response = self.client.post("/user_auth_ms/sign-up", data=self.body,
                                        headers=self.headers)
            self.test.append(num)

        else:
            num = randint(4000000, 8000000)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
