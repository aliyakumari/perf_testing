from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import time
import gevent
import resource
import pandas as pd

host = "https://preprod-api.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


df = pd.read_csv("email_id_password.csv")
email_id_password = df.values.tolist()
email_id_password.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': '*',
            'Origin': 'https://mahendras.partner.embibe.com'

        }

    wait_time = between(0, 1)
    host = "https://preprod-api.embibe.com"

    @task
    def userDB(self):

        num = randrange(len(email_id_password))
        self.body =json.dumps({
            "email": email_id_password[num][0],
            "password":email_id_password[num][1],
            "device_id": "1634944983000",
            "connected_response": True
        })
        print(email_id_password[num][0], email_id_password[num][1])

        response = self.client.post("/user_auth_ms/partner/sign-in", data=self.body,
                                    headers=self.headers)
        print(response.text)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
