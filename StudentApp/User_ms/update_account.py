from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.crt_email = 'abhishek+8@embibe.com'

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsInBhcmVudF91c2VyX2lkIjoxNTAxOTAxMDE2LCJjcmVhdGVkIjoxNjYyNzA1NDA0LCJvcmdhbml6YXRpb25faWQiOiI1ZmJkZjhmODBkNTBhNzYyMGE4ZGM0NmIiLCJpZCI6MTUwMzEyMzAwOCwiZGV2aWNlSWQiOiIxNjYyNTMxODg5MzA3IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxOTAxMDE2XzcyODE1MTY4Njg4ODA4MDRAZW1iaWJlLXVzZXIuY29tIn0.1y_Yy-smUbS4kOWvBcEhpzTlxBQa5zeNKsXIRbmj4c4sDeUrfdmO7DJEX3BaAK6Mo5dsWGuvsAL5Snt-yhMoOQ',
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=9de76f65-2675-454f-a036-f5ccb5a9fd11; embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(8, 10000)
        num_str = []
        if self.crt_email not in num_str:
            prev_mail = self.crt_email
            self.crt_email = f'abhishek+{rnum}@embibe.com'
            self.body = json.dumps({
                "resource_type": 3,
                "account_id": 1503139729,
                "profile_id": 1503139729,
                "email": {
                    "old_email": prev_mail,
                    "new_email": self.crt_email
                },
                "mobile": {
                    "old_mobile": "8318018645",
                    "new_mobile": "7318018645"
                }
            })
            num_str.append(self.crt_email)
        response = self.client.put("/user_auth_ms/admin/update_account", data=self.body,
                                       headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
