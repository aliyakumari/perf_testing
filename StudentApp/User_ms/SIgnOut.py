from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MzQ2NTUwMDIsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTMyMjc3ODQ5IiwiaWQiOjE1MDAwNDgyNTAsImV4cCI6MTYzNDgyNzgwMiwiZGV2aWNlSWQiOiIwMTIzNDU2Ny04OUFCQ0RFRi0wNzIzNDU2Ny04OUFCQ0RFRiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjp0cnVlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjp0cnVlLCJlbWFpbCI6InZpc2hhbEBlbWJpYmUuY29tIn0.WTbuuotBsxEyh-OzSxTR_nv4U_bs3sezeWgkguFIWlCs4oOk6sUudByMCHLOOMh5IBj6FAqryhO2vx4XAbsHcA',
  'Content-Type': 'application/json',
  'Cookie': 'embibe-refresh-token=7efebd2a-520f-4a0a-9c20-4ac0e05bf454; preprod_ab_version=0; preprod_embibe-refresh-token=918f3f8d-7bab-48f9-888f-d2922c182c52; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjAwMjg5ODEsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTMyNzc1OTA4IiwiaWQiOjE1MDAwNTUwNTcsImV4cCI6MTY2MDExNTM4MSwiZGV2aWNlSWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRjIzNDU2IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6InRlc3QwNUBnbWFpbC5jb20ifQ.Ix-Uv9fF5y49mkATMDWS33No2xV1kzvYv530aQgJk3hJunAH58OkTlE5C_ui4mZZEsIqNro4ElhKckLC_Yr-dA; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNCQ0RFRy0wMDI4NDU2MC04MEFCQ0RNRjIzNDU2Iiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUl5WW1Nek9EUmxOaTFpT1dRekxUUmhZMlV0WWpBNU1pMHhPVEpoTTJObE5XTmtZVEVpTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SWpWa01HSTFOV013TFdZMU16UXROR0kxTUMxaU5EaGxMVFptWkRVNFpqUmtNbVExWkNJc0ltbGtJam9pT1Rrek1qYzNOVGt3T0NJc0lteHZZMkZzWlNJNkltVnVJbjAuaE5ZSDFpR0w1M2NMcklwZmZRYUR3dUsyWkJLeXJEaGJfNjhWeUJmM2lIdTQwN2sxZVN5eWhXaEE3R0szYXJtZ09ReHhoRnVteVg0VGI1d1hVOVMtcmciLCJjcmVhdGVkIjoxNjYwMDI4OTgxLCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduSW4iLCJhdHRlbXB0IjoxLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoidGVzdDA1QGdtYWlsLmNvbSIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDAwNTUwNTcsImV4cGlyeSI6MTY2MDExNTM4MSwiZXhwIjoxNjYwMTE1MzgxfQ.Og37AtuhP5YaYDCpHGobovXU_ctmXGF5wZr-o-tn84zKafr8FmgsqCPSftqHXKeV-0O4EDCGjMBbtQXg_d4uRA; prod_embibe-refresh-token=d8dfeeca-b88e-43a5-9f1c-bdf5496fcd29; school_prod_embibe-refresh-token=4be059a3-13d2-4509-925a-d8ad6c53b372; embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIwMTIzNDU2OC04OUNobnZhYWJhc3Nnc3Zjc3ZtYm5zdnNoc2dzQkNERWRmRy1kMDAyZDg0aHM1NjAtODBBQkNkRE1GIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUppTkRrd05tVmhPUzB4TVRRM0xUUmxPRFl0WVRFek1pMHhNMkl4T1RNNVpUUmtZamNpTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SW1GbU1EaGhNamRsTFRRM016RXROREZqTnkwNU1tSmpMVGsxTjJFeE5USXhaV015TXlJc0ltbGtJam9pT1Rrek1qYzNOVGt3TkNJc0lteHZZMkZzWlNJNkltVnVJbjAuSHVqbnJOQnhzYW1wR0RLc2UzZjA5RC0zdTBBMkRmOExhYVVzOVJNTnc1WVBGUDNCWUNvVlMtVGt2MV9qYTdrTlI0YTJzUGhQT2RpYmJ5eXVKNnhVWVEiLCJjcmVhdGVkIjoxNjYwMDI4NzYxLCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduSW4iLCJhdHRlbXB0IjoxLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoidGVzdDAxQGdtYWlsLmNvbSIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDAwNTQwNTEsImV4cGlyeSI6MTY2MDExNTE2MSwiZXhwIjoxNjYwMTE1MTYxfQ.tAVk0U4_5tklBi-9f_UlQLvUHiYBdq7PiiLtPUPjbb7V1XdeeFucPyVrG5Eum_g4NP0OTHhhPJ4EJsL3b_trvw'
}



        self.test = [123]
    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = json.dumps({"clear_all_sessions": False})
        response = self.client.post("/user_auth_ms/sign-out", data=self.body,
                                    headers=self.headers)




def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
