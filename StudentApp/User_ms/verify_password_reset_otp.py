import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

# df = pd.read_csv('org_t oken.csv')
# org_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Cookie': '_gcl_au=1.1.472225750.1673444602; WZRK_G=1cb03ccbac44438f9a51bc0d222dddd8; _ga=GA1.2.905121092.1673444606; _gid=GA1.2.1247841337.1673444606; __insp_wid=82124457; __insp_slim=1673444610158; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tLw%3D%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; _fbp=fb.1.1673444610401.787216129; __insp_norec_howoften=true; __insp_norec_sess=true; preprod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjczNDQ0ODk4LCJnYV9pZCI6IjkwNTEyMTA5Mi4xNjczNDQ0NjA2Iiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzM1MzEyOTgsInV1aWQiOiJmN2RmNTVhYS0wZGQ3LTQ4MDgtYjZlYS04N2QwYTY2ZmU3ZGQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.tdhlclXnouc_6QsIKD9BvtGS-SieEYSlhYF4LF4kxrE; guest-session-id=f7df55aa-0dd7-4808-b6ea-87d0a66fe7dd; _dc_gtm_UA-48005755-23=1; _ga_YHBNHKH43S=GS1.1.1673444605.1.1.1673444962.60.0.0; WZRK_S_9RZ-869-956Z=%7B%22p%22%3A3%2C%22s%22%3A1673444606%2C%22t%22%3A1673444962%7D; preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiJTeXN0ZW0tZGV2aWNlLWlkLTE2NzM0NDU2OTI5NTAiLCJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVNXNGlMQ0p2Y21kZmFXUWlPaUl4SWl3aWRISmhZMnRmYVdRaU9pSmpaVEl4WXpkak9TMDNNVEV5TFRRME5EQXRZall4T0MwNE5EQTJOakZtWVRFNE5qTWlMQ0p2ZEhCZmNtVnhkV1Z6ZEY5cFpDSTZJalJtTXpZM1kyRTNMVEpsTjJZdE5HUTVOQzFpTldSbExUWXhaRGt3T0RZd01EVTRaaUlzSW1sa0lqb2lPREl4TURrMU1Ea3lOaUlzSW14dlkyRnNaU0k2SW1WdUlpd2ljR3hoZEdadmNtMGlPaUpYUlVJaWZRLldYZWkxc0VKT3V3cThMQ0htSnU2WGU5YldvQ0xEelJCcWhqVlNtMDIxS3V4YS0zOHg5TXQ4VGx1bE05b2ZwYjlaal9oQjBlZTJrcV9ucnFLdGFaWnlBIiwiY3JlYXRlZCI6MTY3MzUyMTg0Miwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwicGxhdGZvcm0iOiJXRUIiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiODIxMDk1MDkyNiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDAyODg0MTYsImV4cGlyeSI6MTY3MzYwODI0Miwiam91cm5leV9pZCI6IlNJR05fSU5fYTYwMzdmNzItM2ExZC00YWQyLWEzMzgtNGM1ODAxY2Q4MTdhIiwiZXhwIjoxNjczNjA4MjQyfQ.sRt9ZIlN3AE5RzhsBMuorg0R9HJZPoeBzQGvs9tqsSnoDiYDLEAvUJ2VnLTGgiJzKfwvToAyIDDQcmFEbfn9UQ',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Pragma': 'no-cache',
            'Referer': 'https://staging-fiber-web.embibe.com/forgot-password',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjc1MjMxMTczNzQwIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUkwWWpFMk1qTTRZeTAwWW1WbUxUUmtaRFF0T1RNMU5pMWhOekZsTUdFeE1tUXlaR0VpTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SWpkaFl6VTJaRGt4TFRNME1ESXRORE5qTUMwNE56a3dMVFZoTkdWalkyVTBOakUzTnlJc0ltbGtJam9pT0RJeE1EazFNRGt5TmlJc0lteHZZMkZzWlNJNkltVnVJaXdpY0d4aGRHWnZjbTBpT2lKWFJVSWlmUS5udG5EUjNUcE5lYTYtcHJ4TTIzSEVTME94cE5RSVVkRWFPNTBrVnZaX1FpTndGckY4LTJEQVE1bWNBQl9yUG5wS0VKa01WMmJxVkZJNndvcWY2akRWdyIsImNyZWF0ZWQiOjE2NzUyMzExNzUsIm90cF9vcGVyYXRpb25fdHlwZSI6IlNpZ25JbiIsImF0dGVtcHQiOjEsInR0bCI6IjYwMCIsInBsYXRmb3JtIjoiV0VCIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6IjgyMTA5NTA5MjYiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImlkIjoxNTAwMjg4NDE2LCJleHBpcnkiOjE2NzUzMTc1NzUsImpvdXJuZXlfaWQiOiJTSUdOX0lOXzhmNTczMTAyLTY0MTctNDZlNS05NTU5LWFkMmRmZDIyOWNiNSIsImV4cCI6MTY3NTMxNzU3NX0.od726__eQCOtLHa3QThLynt3sftfRwv4yn6D5Mm8q2EaxmxNnAp_vwCD-y-_t3p-On9VYqEbcojCApyWg1j9xg',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Brave";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        token_data = []
        # self.headers['otp-token'] = org_token[randrange(len(org_token))][0]

        url = f"/user_auth_ms/verify_password_reset_otp"
        body = {"otp": "123456"}
        response = self.client.post(url, data=json.dumps(body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
