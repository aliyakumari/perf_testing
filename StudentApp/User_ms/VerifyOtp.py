import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('org_token.csv')
org_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUp3WVhOemQyOXlaRjl5WlhObGRDSXNJbTl5WjE5cFpDSTZJakVpTENKMGNtRmphMTlwWkNJNkltSmpPREZqWkdOaUxURTBNelV0TkRNMU5pMWlZVEkzTFdNd1l6UTJZemt5Tm1FMk9DSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2labVF4TkRCbU9Ea3RabVpoT0MwMFltVXdMV0l3WTJFdE1qSmtPVFl3TjJNd09UZ3dJaXdpYVdRaU9pSTROVFV6T0RRNE16a3hJaXdpYkc5allXeGxJam9pWlc0aWZRLjNuSUdTcTNJamoxQ3c1UXJxbF9YV1dkYWk3Y2xQY210UEtiZV9kY2dfLWhzbThqdWNSUC1nWWg3VnZFOVU0Y2xramlZYlFyNk4xTi1pZXdaamFQM1N3IiwiY3JlYXRlZCI6MTY3MzUyMzU5OCwib3RwX29wZXJhdGlvbl90eXBlIjoicGFzc3dvcmRfcmVzZXQiLCJhdHRlbXB0IjoxLCJ0dGwiOiIzMDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOmZhbHNlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6Ijg1NTM4NDgzOTEiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImV4cGlyeSI6MTY3MzYwOTk5OCwiZXhwIjoxNjczNjA5OTk4fQ.9AC1zOL_IchigEt6qiPVhmFhPifruJYzSeseoo4rexexrMjNRzooeyNo4WBMqhB_wQDs9GPWZREX0Cvwvu2uKw',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiJTeXN0ZW0tZGV2aWNlLWlkLTE2NzM0NDU2OTI5NTAiLCJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVNXNGlMQ0p2Y21kZmFXUWlPaUl4SWl3aWRISmhZMnRmYVdRaU9pSmtOalprT0RZNFpDMWtaR1JpTFRSaFpHWXRPREl4TVMwMk0ySXhPV014WXpJMk56TWlMQ0p2ZEhCZmNtVnhkV1Z6ZEY5cFpDSTZJall6TWprM09UTXdMVFk1WVRrdE5EUTBNaTA1TUdKaUxXTmlZamN5TWpZeE1XUmlOU0lzSW1sa0lqb2lPREl4TURrMU1Ea3lOaUlzSW14dlkyRnNaU0k2SW1WdUlpd2ljR3hoZEdadmNtMGlPaUpYUlVJaWZRLkdJZWdhZ1VUSENWbDBWV25tUUQ3WjcxYklySUZ2RVVIRGdTTW16TmhWazUxZ0l1VFNwVnFITUV4bU9aYWRObXZhUFpqRlNZZVJ6cGkydGN5WXFnLVNRIiwiY3JlYXRlZCI6MTY3MzUyMzEwMCwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwicGxhdGZvcm0iOiJXRUIiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiODIxMDk1MDkyNiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDAyODg0MTYsImV4cGlyeSI6MTY3MzYwOTUwMCwiam91cm5leV9pZCI6IlNJR05fSU5fYTAwZTYyMjItZTE2Yy00OGNmLWI0YWItNWZkOGU2Mjg1ZjY1IiwiZXhwIjoxNjczNjA5NTAwfQ.ERf5YKuTbdpgHLMAvfXIs0xvJ2g9Ck_WvhJXHtfphZdKlYgpuEZU4yIx0EnjSx-gejVFoxTUCJtEbtU9vaAb-g'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        token_data = []
        self.headers['otp-token'] = org_token[randrange(len(org_token))][0]

        url = f"/user_auth_ms/verifyOtp"
        body = {"otp": "123456"}
        response = self.client.post(url, data=json.dumps(body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
