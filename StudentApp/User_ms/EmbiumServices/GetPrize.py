from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

embibe_token = []
parent_embibe_token = []

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MDg2NTIwNzMsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTIxMjIzNDUsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiMTUwMDAwMTYyNF8xMTk5NTAxNzc0NTE2OTg5N0BlbWJpYmUtdXNlci5jb20ifQ.VLiYggm56LL_vg5njAMOYFButBGqFBWg6pF777mIvBLQbvCTmGm4p7jMYhlKUo3JzlBRJH74Qsikh7nt7CwpYg',
  'parent-embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjIsImNyZWF0ZWQiOjE2MDg2NTIwNzMsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MjEyMiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDAxNjI0XzExOTk1MDE3NzQ1MTY5ODk3QGVtYmliZS11c2VyLmNvbSJ9.QbnuHgqU_ttP6hvyx88cRPxbXaRkMKBtsUdgHIRPpP5Kj-AV4RQKMFAOLH_cAAUxK1URHHMWZx9ocQW9PMklZA',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def getRewardDetails(self):
        index = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[index][0]
        self.headers['parent-embibe-token'] = embibe_token[index][0]
        self.url = f"/embium/transaction/service/prizes?reward_level={randrange(1,3)}"
        response = self.client.get(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
