import requests
import jwt
from random import randrange


def getEmbibeToken():
    randint = randrange(1000000, 10000000000000)
    payload = {
        "role": "student",
        "time_stamp": "2020-08-31 11:55:33 UTC",
        "is_guest": True,
        "id": randint,
        "email": "guest_1598874933855660934@embibe.com"
    }

    encoded = jwt.encode(payload,
                         "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968",
                         algorithm="HS512")

    return encoded


def getParent_embibe_token():
    randint = randrange(1000000, 10000000000000)
    payload = {
        "country": 1,
        "user_type": 2,
        "created": 1608652073,
        "organization_id": "1",
        "id": randint,
        "mobile_verification_status": False,
        "email_verification_status": False,
        "email": "1500001624_11995017745169897@embibe-user.com"
    }
    encoded = jwt.encode(payload,
                         "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968",
                         algorithm="HS512")

    return encoded



