import requests
from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
# from utils.CreateToken import creatEmbibeToken
resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

def Add_rule(embibe_token):
    url = "https://preprodms.embibe.com/embium/rule/engine/service/addrules"

    payload = json.dumps({
        "name": "randam",
        "evaluator_type": "VAL",
        "value": 200,
        "user_message": {
            "gyd": 1
        },
        "reward_id": 12,
        "operation": "INCREMENT",
        "description": "Reward user for accessing Test Home for the first time",
        "event_name": "first time screen load",
        "event_id": "LoadTest_",
        "inputs": {}
    })
    headers = {
        'Content-Type': 'application/json',
        'embibe-token': embibe_token,
        'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
    }

    response = requests.request("POST", url, headers=headers, data=payload)


embibe_token = []

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

for i in range(1, 15000):
    rnum = randrange(len(embibe_token))
    Add_rule(embibe_token[rnum][0])



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2MDU3MDE3NzAsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5MTIzNDU2NzEzIiwiaWQiOjUyMTMwMDIsImV4cGlyeSI6MTYwNTcwMzU3MCwib3RwX29wZXJhdGlvbl90eXBlIjoxLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6InFhdGVzdDEzQGdtYWlsLmNvbSJ9.cnw4asEvgldftXEy73gm2Mt4BctqTiyZCRFHT3DTWWgTC9Buif_PW9It4TOQsPBy7jpwX-9YJaEPAmIbw9kJFQ',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
}


        self.body = {}
        self.rule_id = 509213
    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def getRewardDetails(self):
        index = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[index][0]
        self.rule_id = self.rule_id + 1
        self.url = f"/embium/rule/engine/service/deactivaterules?rule_id={self.rule_id}"
        try:
            response = self.client.get(self.url, data=self.body, headers=self.headers)
        except Exception as e:
            print(e)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
