import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
from model.userms_connect import connect

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
main_data = []
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

user_id_and_parent_id_data = connect(
    "select mobile  from users u2 where id in ( select is_child_of from user_relations where is_child_of in (select id  from users u  order by id  desc));")


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.org_token = []
        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Pragma': 'no-cache',
            'Referer': 'https://staging-fiber-web.embibe.com/forgot-password',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Brave";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'Cookie': 'preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):

        rand_num = randrange(len(user_id_and_parent_id_data))
        url = f"/user_auth_ms/send_password_reset_otp?locale=en"
        body = {
            "mobile": user_id_and_parent_id_data[rand_num][0]
        }

        with self.client.post(url, data=json.dumps(body), headers=self.headers, catch_response=True) as resp:
            if resp.status_code == 200:
                self.org_token.append(resp.headers['otp-token'])
                df = pd.DataFrame(self.org_token)
                df.to_csv('org_token.csv', index=False)
                resp.success()
            elif resp.status_code == 404 or resp.status_code == 400:
                resp.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
