import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
from model.userms_connect import connect

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
main_data = []
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

user_id_and_parent_id_data = connect("select mobile  from users u ;")

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Cookie': '_gcl_au=1.1.472225750.1673444602; WZRK_G=1cb03ccbac44438f9a51bc0d222dddd8; _ga=GA1.2.905121092.1673444606; _gid=GA1.2.1247841337.1673444606; __insp_wid=82124457; __insp_slim=1673444610158; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tLw%3D%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; _fbp=fb.1.1673444610401.787216129; __insp_norec_howoften=true; __insp_norec_sess=true; preprod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjczNDQ0ODk4LCJnYV9pZCI6IjkwNTEyMTA5Mi4xNjczNDQ0NjA2Iiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzM1MzEyOTgsInV1aWQiOiJmN2RmNTVhYS0wZGQ3LTQ4MDgtYjZlYS04N2QwYTY2ZmU3ZGQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.tdhlclXnouc_6QsIKD9BvtGS-SieEYSlhYF4LF4kxrE; guest-session-id=f7df55aa-0dd7-4808-b6ea-87d0a66fe7dd; _dc_gtm_UA-48005755-23=1; _ga_YHBNHKH43S=GS1.1.1673444605.1.1.1673444962.60.0.0; WZRK_S_9RZ-869-956Z=%7B%22p%22%3A3%2C%22s%22%3A1673444606%2C%22t%22%3A1673444962%7D; preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Pragma': 'no-cache',
            'Referer': 'https://staging-fiber-web.embibe.com/forgot-password',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUp3WVhOemQyOXlaRjl5WlhObGRDSXNJbTl5WjE5cFpDSTZJakVpTENKMGNtRmphMTlwWkNJNklqWmpNMkkwTVdRM0xXTmtPVE10TkRjeFppMDRZV1ExTFdJek16STBNalEyWm1WbFlTSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2laakkwWWpVd05UTXRZekl3TUMwME1HUXlMVGxqTVRBdE56Um1OakJoWkdKbFpEVTJJaXdpYVdRaU9pSTROVFV6T0RRNE16a3hJaXdpYkc5allXeGxJam9pWlc0aWZRLnJtTFp3WkpZbWplSW5Ec0FfZEVGRHpKWURUMmFOT2d0aERHWk9FZWlVU3hJMlMxT2s2REpHTUFCa0UxcGZHYjkyaDdWNW9FOGJfdnFuN21aRWpzanJnIiwiY3JlYXRlZCI6MTY3MzQ0NDkxNCwib3RwX29wZXJhdGlvbl90eXBlIjoicGFzc3dvcmRfcmVzZXQiLCJhdHRlbXB0IjoxLCJ0dGwiOiIzMDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOmZhbHNlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6Ijg1NTM4NDgzOTEiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImV4cGlyeSI6MTY3MzUzMTMxNCwiZXhwIjoxNjczNTMxMzE0fQ.29Cea5aWy62BsdQZNraSYemUlmMVwpUEfe0kT6RUW-yYsdpS3P1ku5NT1jqwtAezTcFlrZT6y3-7_PoQzNK2rQ',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Brave";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/user_auth_ms/send-login-otp?locale=en"
        rand_num = randrange(len(user_id_and_parent_id_data))
        body = {
            "device_id": f"System-device-id-16734456{rand_num}92950",
            "mobile": user_id_and_parent_id_data[rand_num][0],
            "connected_response": True
        }
        with self.client.post(url, data=json.dumps(body), headers=self.headers, catch_response=True) as resp:
            if resp.status_code == 200:
                resp.success()
            elif resp.status_code == 404 or resp.status_code == 400:
                resp.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
