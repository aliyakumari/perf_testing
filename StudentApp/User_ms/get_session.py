from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
# embibe_token.pop()
# df = pd.read_csv("test_code.csv")
# test_code = df.values.tolist()
#
# print(embibe_token[0])


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDE1OTQwLCJjcmVhdGVkIjoxNjQzOTUwODA5LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTY2MjEsImV4cCI6MTY0NDAzNzIwOSwiZGV2aWNlSWQiOiIxNjQzOTEzMTU2MzQ4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDE1OTQwXzcxMzE1NzcyMTg5NjQzOTJAZW1iaWJlLXVzZXIuY29tIn0.ASwkj34viLPBxLZp-MI1S8zjoSidbWZPBy6kknsT234EJ1cPY9DrkP8R5RCtDC6QnSHl0vEhOkK0LForsVmbdA',
  'Cookie': 'preprod_embibe-refresh-token=12a6f9ca-9410-419c-a1f2-e87cbff146d7; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = {}
        # self.headers['embibe-token'] = embibe_token[rnum][1]
        print(embibe_token[rnum][0])
        url = f"/user_auth_ms/get-session"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
