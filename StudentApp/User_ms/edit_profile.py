from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0',
            'Accept': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxMTk4OTgwLCJjcmVhdGVkIjoxNjQxNDg1MjQ1LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTg5ODEsImV4cCI6MTY0MTY1ODA0NSwiZGV2aWNlSWQiOiIxNjQxNDgyOTk0ODg4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxMTk4OTgwXzMyMTI0MjQ2OTYzMjc4MzlAZW1iaWJlLXVzZXIuY29tIn0.2vAkLjIPRCBPkP5hD9XO1JBKfCMykSwUH2od9NPCELpA9HnnU9SeJG41V7orBbawVXx4Eo1AqWhrf68AjpEApw',
            'Connection': 'keep-alive',
            'Cookie': 'embibe-refresh-token=4361e25b-3b7b-4606-8a2c-d1f81d5a2269; preprod_embibe-refresh-token=db6a8ee4-95e3-42fa-a06d-b93092cdef9d'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = json.dumps({
            "gender": "female",
            "first_name": "testing2",
            "last_name": "lasttesting2",
            "primary_goal": "kve383630",
            "primary_exam": "ex1",
            "secondary_goal": "kve383630",
            "secondary_exam": "ex2"
        })
        response = self.client.put("/user_auth_ms/edit_profile", data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
