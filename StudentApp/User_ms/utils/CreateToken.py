import pandas as pd
import json
import requests

def SignIn(email):
    url = "https://preprodms.embibe.com/user_auth_ms/sign-in?locale=en"

    payload = json.dumps({
      "email": email,
      "password": "Embibe@1234",
      "device_id": "1662711488483",
      "connected_response": True
    })
    headers = {
      'Accept': 'application/json, text/plain, */*',
      'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
      'Connection': 'keep-alive',
      'Content-Type': 'application/json',
      'Cookie': '_fbp=fb.1.1662544612287.433228344; _gcl_au=1.1.1239537058.1662544614; _gid=GA1.2.1457936680.1662709062; prod_ab_version=0; prod_embibe-refresh-token=115fd8ec-9d4c-4bcb-8b23-dcfe774e1ff2; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjI3MTAxMzUsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3NDc3ODU4NTg2IiwiaWQiOjIwMDI3OTE4NDUsImV4cCI6MTY2MzkxOTczNSwiZGV2aWNlSWQiOiIxNjYyNzEwMTM0NzA2IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.GMLPg_ppzeoeBF-LmFNmiEjkd97l-Varcutz4rBZie_oA0-kO5s1O2CMGo1B3T__ZaZiSRD6jX36XGDWAmjKSw; _ga_TT8L73VP3H=GS1.1.1662709061.1.1.1662710137.43.0.0; __insp_wid=875671237; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL2xlYXJuL2hvbWU%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_sess=true; preprod_ab_version=0; preprod_embibe-refresh-token=3ada4b6a-58f5-4c72-94e0-6fb9cbba813a; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjI3MTEzODksIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3NDc3ODU4NTg2IiwiaWQiOjE1MDE2NDA4MzgsImV4cCI6MTY2Mjc5Nzc4OSwiZGV2aWNlSWQiOiIxNjYyNzExMzg5MDI4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.nD1iOMFEi2f7qVESu2oSSM0BN30Yr3FQTWN_YlWyDD4761oBEvEK5EyAoG0SyAIU_YqfeteIAH3IWR1jfklxDg; undefined=%22%22; _dc_gtm_UA-48005755-23=1; __insp_slim=1662711475735; _ga=GA1.2.1040598102.1662477865; _ga_YHBNHKH43S=GS1.1.1662711287.11.1.1662711488.43.0.0; embibe-refresh-token=5c6bb6a0-3cc9-41f4-8d5a-ce1ade912b70; preprod_ab_version=0; preprod_embibe-refresh-token=d2d4f1ce-1da7-415c-a275-8e1f7b34cc50; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NjI3MTE0ODgsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3NDc3ODU4NTg2IiwiaWQiOjE1MDE2NDA4MzgsImV4cCI6MTY2Mjc5Nzg4OCwiZGV2aWNlSWQiOiIxNjYyNzExNDg4NDgzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.OlNcUf425_4OKNBLF8OIWoL3qdL84yrQA5fWfmXGh6xjCo9kZi3yXMqQQo1xyP_W2_M1Ep64lZow-KpBfjtI_Q',
      'Origin': 'https://staging-fiber-web.embibe.com',
      'Referer': 'https://staging-fiber-web.embibe.com/',
      'Sec-Fetch-Dest': 'empty',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Site': 'same-site',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
      'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"macOS"'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    return response

def creatEmbibeToken():
    df  = pd.read_csv('utils/email.csv', )
    emails = df.values.tolist()
    emails.pop()
    embibe_token_ = []
    for email in emails:
        token_data = []
        res = SignIn(email[0])
        user_id = res.json().get('data').get('user_details').get('id')
        embibe_token = res.json().get('data').get('embibe_token')
        token_data.append(user_id)
        token_data.append(embibe_token)
        embibe_token_.append(token_data)

        df = pd.DataFrame(embibe_token_)
        df.to_csv('Embibe-Token.csv', index=False)
        
creatEmbibeToken()
