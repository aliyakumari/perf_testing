import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import requests

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUp3WVhOemQyOXlaRjl5WlhObGRDSXNJbTl5WjE5cFpDSTZJakVpTENKMGNtRmphMTlwWkNJNkltSmpPREZqWkdOaUxURTBNelV0TkRNMU5pMWlZVEkzTFdNd1l6UTJZemt5Tm1FMk9DSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2labVF4TkRCbU9Ea3RabVpoT0MwMFltVXdMV0l3WTJFdE1qSmtPVFl3TjJNd09UZ3dJaXdpYVdRaU9pSTROVFV6T0RRNE16a3hJaXdpYkc5allXeGxJam9pWlc0aWZRLjNuSUdTcTNJamoxQ3c1UXJxbF9YV1dkYWk3Y2xQY210UEtiZV9kY2dfLWhzbThqdWNSUC1nWWg3VnZFOVU0Y2xramlZYlFyNk4xTi1pZXdaamFQM1N3IiwiY3JlYXRlZCI6MTY3MzUyMzU5OCwib3RwX29wZXJhdGlvbl90eXBlIjoicGFzc3dvcmRfcmVzZXQiLCJhdHRlbXB0IjoxLCJ0dGwiOiIzMDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOmZhbHNlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6Ijg1NTM4NDgzOTEiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImV4cGlyeSI6MTY3MzYwOTk5OCwiZXhwIjoxNjczNjA5OTk4fQ.9AC1zOL_IchigEt6qiPVhmFhPifruJYzSeseoo4rexexrMjNRzooeyNo4WBMqhB_wQDs9GPWZREX0Cvwvu2uKw',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiJTeXN0ZW0tZGV2aWNlLWlkLTE2NzM0NDU2OTI5NTAiLCJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVNXNGlMQ0p2Y21kZmFXUWlPaUl4SWl3aWRISmhZMnRmYVdRaU9pSmtOalprT0RZNFpDMWtaR1JpTFRSaFpHWXRPREl4TVMwMk0ySXhPV014WXpJMk56TWlMQ0p2ZEhCZmNtVnhkV1Z6ZEY5cFpDSTZJall6TWprM09UTXdMVFk1WVRrdE5EUTBNaTA1TUdKaUxXTmlZamN5TWpZeE1XUmlOU0lzSW1sa0lqb2lPREl4TURrMU1Ea3lOaUlzSW14dlkyRnNaU0k2SW1WdUlpd2ljR3hoZEdadmNtMGlPaUpYUlVJaWZRLkdJZWdhZ1VUSENWbDBWV25tUUQ3WjcxYklySUZ2RVVIRGdTTW16TmhWazUxZ0l1VFNwVnFITUV4bU9aYWRObXZhUFpqRlNZZVJ6cGkydGN5WXFnLVNRIiwiY3JlYXRlZCI6MTY3MzUyMzEwMCwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwicGxhdGZvcm0iOiJXRUIiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiODIxMDk1MDkyNiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjE1MDAyODg0MTYsImV4cGlyeSI6MTY3MzYwOTUwMCwiam91cm5leV9pZCI6IlNJR05fSU5fYTAwZTYyMjItZTE2Yy00OGNmLWI0YWItNWZkOGU2Mjg1ZjY1IiwiZXhwIjoxNjczNjA5NTAwfQ.ERf5YKuTbdpgHLMAvfXIs0xvJ2g9Ck_WvhJXHtfphZdKlYgpuEZU4yIx0EnjSx-gejVFoxTUCJtEbtU9vaAb-g'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = "https://preprodms.embibe.com/user_auth_ms/send_password_reset_otp?locale=en"
        payload = json.dumps({
            "mobile": "8553848391"
        })
        headers_ = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Cookie': '_gcl_au=1.1.472225750.1673444602; WZRK_G=1cb03ccbac44438f9a51bc0d222dddd8; _ga=GA1.2.905121092.1673444606; _gid=GA1.2.1247841337.1673444606; __insp_wid=82124457; __insp_slim=1673444610158; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tLw%3D%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; _fbp=fb.1.1673444610401.787216129; __insp_norec_howoften=true; __insp_norec_sess=true; _dc_gtm_UA-48005755-23=1; preprod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjczNDQ0ODk4LCJnYV9pZCI6IjkwNTEyMTA5Mi4xNjczNDQ0NjA2Iiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzM1MzEyOTgsInV1aWQiOiJmN2RmNTVhYS0wZGQ3LTQ4MDgtYjZlYS04N2QwYTY2ZmU3ZGQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.tdhlclXnouc_6QsIKD9BvtGS-SieEYSlhYF4LF4kxrE; guest-session-id=f7df55aa-0dd7-4808-b6ea-87d0a66fe7dd; WZRK_S_9RZ-869-956Z=%7B%22p%22%3A3%2C%22s%22%3A1673444606%2C%22t%22%3A1673444914%7D; _ga_YHBNHKH43S=GS1.1.1673444605.1.1.1673444914.40.0.0; preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUp3WVhOemQyOXlaRjl5WlhObGRDSXNJbTl5WjE5cFpDSTZJakVpTENKMGNtRmphMTlwWkNJNklqZ3pabUpqTVdReExXUTJaall0TkRCaE9TMDVPR1pqTFRRMk1HSm1ZekUxWm1KbFpDSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2lZVFJqTlRBNFltWXRPR1EzT1MwMFpXVTRMVGsxTldNdFltRXdOamxsTmpNM01qZGlJaXdpYVdRaU9pSTROVFV6T0RRNE16a3hJaXdpYkc5allXeGxJam9pWlc0aWZRLlEyQ1pvUFFLZUl4eG9XMXlob0h2NkJ5ajhGSHBPbmljUm9Icmp3Qmp6SWY1SmJMa1JjVGNhczVhUUJVVzlUSnV6c21MaTBwaExETjcxNVdkbmZvc2RnIiwiY3JlYXRlZCI6MTY3MzUyNDgwMywib3RwX29wZXJhdGlvbl90eXBlIjoicGFzc3dvcmRfcmVzZXQiLCJhdHRlbXB0IjoyLCJ0dGwiOiIzMDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOmZhbHNlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6Ijg1NTM4NDgzOTEiLCJvcmdfaWQiOiIxIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjEsImV4cGlyeSI6MTY3MzYxMTIwMywiZXhwIjoxNjczNjExMjAzfQ.Ly4h3hTL8YTVcGSgIpnp_z7zjcC2A_FxZK5nJWdTECSB8cYUfyTIB0BcmPHzPRPlLffdIsu24WrNcPris6bcbA',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Pragma': 'no-cache',
            'Referer': 'https://staging-fiber-web.embibe.com/forgot-password',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Brave";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }
        response = requests.request("POST", url, headers=headers_, data=payload)
        self.headers['otp-token'] = response.headers['otp-token']
        url = f"/user_auth_ms/resend_otp?locale=en"
        body = {}
        response = self.client.post(url, data=json.dumps(body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
