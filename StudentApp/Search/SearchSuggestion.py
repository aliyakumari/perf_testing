from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv("topic.csv")
query = df.values.tolist()



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Referer': 'https://staging-fiber-web.embibe.com',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ0ZW5hbnRfaWQiOiI2MmZiNmZjM2U1NjZmMTUxNjZjZjRlOGIiLCJjb3VudHJ5IjoxLCJwYXJlbnRfdXNlcl9pZCI6MTUwNDgxNjM2NywiY3JlYXRlZCI6MTY3NzgzNTQ3NywidGVuYW50X3R5cGUiOiJTY2hvb2wiLCJkZXZpY2VJZCI6IjE2NzUyNTY0OTUwODAiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsInVzZXJfdHlwZSI6MSwib3JnYW5pemF0aW9uX2lkIjoiMSIsImlkIjoxNTA0ODE2MzY4LCJleHAiOjE2Nzc5MjE4NzcsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDQ4MTYzNjdfNTg1NDI0MjcwMjgyMDIyOEBlbWJpYmUtdXNlci5jb20ifQ.P0CmdEcQDnVAv_hmHHdsCMFuojS2gG10iD4LxMgkJS0QV6zH1AB-v3RekkspSpgx7TND6V9F60DSlD-AbnOTwg',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'Accept-Encoding': 'gzip, deflate, br'
}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {}
        rn_str = randrange(len(query))     
        temp_query = query[rn_str][0].replace(" ", "+")
        url = f"/fiber_ms/v2/search/suggestions?query={temp_query.lower()}&user_goal=Engineering&user_exam=JEE+Main&locale=en&goal=&exam="
        response = self.client.get(url, data=self.body, headers=self.headers)
        


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
