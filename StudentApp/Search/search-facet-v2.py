import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv("query.csv")
query = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {}
        rnum = randrange(len(query))
        temp_query = query[rnum][0]
        temp_query = temp_query.replace(" ", "+")
        url = f"/fs_ms/v2/search?query={temp_query}&facet=live_classes&start=0&size=10&user_goal=cbse&user_exam=10th%20cbse&source=Home%20Page&short_result=false&locale=en&qu=false&cross_grade=true&spell_check=true&side_bar=false&debug=false"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
