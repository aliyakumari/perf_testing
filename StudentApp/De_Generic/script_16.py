from locust import HttpUser,SequentialTaskSet,task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
import pandas as pd
import random

all_commands = {}
with open("Static/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)        
        self.headers = {'Content-type': 'application/json'}
        self.body = {'test_code': 'mb757049', 'school_id': '61af4b0ab15d44609750866c', 'class_id': '61af4bc94fe5742d75de77af', 'user_id': '1501339677'}
        self.url = "/de/generic/query?id=fe8d7175-4d53-4eab-8da3-830e14684770"
        self.method = "POST"
        self.host = "https://preprodms.embibe.com" 
        dt = pd.read_csv("De_Generic_preprod_homework_atomic_v1.csv", low_memory=False)

        self.id = dt["_id"].values.tolist()
        dc = pd.read_csv("De_Generic_testCode.csv", low_memory=False)
        self.testCode = dc["testCode"].values.tolist()
        self.classId = dt["classId"].values.tolist()
        self.subjectCode = dt["kveCodeObject.subjectCode"].values.tolist()
        self.topicCode = dt["kveCodeObject.topicCode"].values.tolist()
        self.schoolId = dt["schoolId"].values.tolist()
        self.startDate = dt["startDate"].values.tolist()
        self.dueDate = dt["dueDate"].values.tolist()
        self.userId = dt["userId"].values.tolist()
        self.homeworkId = dt["homeworkId"].values.tolist() 


    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"
    @task
    def ondemandautomated_script(self):
        self.body["school_id"] = random.choice(self.schoolId)
        self.body["class_id"] = random.choice(self.classId)
        self.body["test_code"] = random.choice(self.testCode)
        # self.body["subject_id"] = random.choice(self.subjectCode)
        # self.body["start_date"] = random.choice(self.startDate)
        # self.body["end_date"] = random.choice(self.dueDate)
        self.body["user_id"] = str(random.choice(self.userId))
        # self.body["level"] = random.choice(self.topicCode)
        # self.body["topic_code"] = random.choice(self.)
        # self.body["test_code"] = random.choice(self.testCode)
        # self.body["homework_id"] = random.choice(self.homework_id)
        # self.body["achieve_homework_bundle_id"] = random.choice(self.level)
        # self.body["homework_id"] = random.choice(self.homeworkId)
        response = self.client.post(self.url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return
        
@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
