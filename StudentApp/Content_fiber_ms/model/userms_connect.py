#!/usr/bin/python
import psycopg2
from model.config import config

# "NOne"
def connect(query):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        print('PostgreSQL database Query:')
        cur.execute(query)
        # display the PostgreSQL database Query data
        query_data = cur.fetchall()

        # close the communication with the PostgreSQL
        cur.close()
        return query_data
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


