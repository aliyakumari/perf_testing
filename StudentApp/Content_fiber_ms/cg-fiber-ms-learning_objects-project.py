from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust import task, between, events
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {}
        url = '/cg-fiber-ms/learning_objects/project?where=%7B%22type%22%3A%20%22Question%22%2C%22status%22%3A%22Published%22%7D&lm_filter=%7B%22format_reference%22%3A%20%225ec5867a0c88fe5860961943%22%2C%20%22learnpath_name%22%3A%20%7B%22%24regex%22%3A%20%22cbse--10th%20cbse--science--chemistry--metals%20and%20non-metals--chemical%20properties%20of%20metals%22%7D%7D&projection=%7B%22id%22%3A%201%2C%20%22content.question_meta_tags.bloom_level%22%3A%201%2C%20%22content.question_meta_tags.primary_concept%22%3A%201%2C%20%22content.question_meta_tags.difficulty_level%22%3A%201%2C%20%22content.question_discrimination_factor.qdf_value%22%3A%201%2C%20%22content.cluster_info.cluster_id%22%3A%201%2C%20%22content.book_tags.book_qnt%22%3A%201%2C%22subtype%22%3A1%7D&locale=en'
        
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
