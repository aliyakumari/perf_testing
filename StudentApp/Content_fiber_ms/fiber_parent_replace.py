from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust import task, between, events
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("../jfAnalysis/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
df = pd.read_csv("../jfAnalysis/test_code.csv")
test_code = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2021-01-09.c67349946ec9a32856dc6a6ba021ac54; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; preprod_embibe-refresh-token=6c29df66-3f04-4750-af34-8510543c39f4; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        # rnum_ = randrange(len(test_code))
        self.body = {
            "questionId": "EM6829403-en",
            "questionType": "SingleChoice",
            "excludeQuestionIDs": [],
            "difficultyLevel": "Easy",
            "learningMaps": {
                "name": "STRESS AND STRAIN",
                "description": "In this topic, we will study stress and strain. It also explains tensile, compressive stress, longitudinal and shearing strain along with formulas and illustration.",
                "learningPathFormatName": "goal--exam--subject--unit--chapter--topic",
                "learningPath": "cbse--11th cbse--physics--mechanical properties of matter--mechanical properties of solids",
                "meta": {
                    "chapter": "ncert11p02--11--physics--mechanical properties of solids",
                    "topic": "ncert11p02--11--physics--mechanical properties of solids--stress and strain",
                    "classId": "5ec5867a0c88fe5860961943|CBSE/11th CBSE|cbse--11th cbse",
                    "display_name": "Stress and Strain",
                    "topic_code": "kve315514",
                    "bookId": "lmf1841"
                }
            }
        }
        url = f"/content_ms_fiber/v1/embibe/en/fiber/parent/replace"
        response = self.client.post(url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
