from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust import task, between, events
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe-cf.com"

all_commands = {}
with open("../jfAnalysis/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
df = pd.read_csv("../jfAnalysis/test_code.csv")
test_code = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA3LTE1IDEyOjMwOjEyIFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwiaWQiOjE1MDAwNDcyODUsImVtYWlsIjoiY2hpbGRncjlAamlvLWVtYmliZS5jb20ifQ.h03QWnPrOF1Rv1j4-PgZMHp1ocz9PnkCSZRZ830E8_nI3AR1SIRV22JeA9KtbL3xcKw6foCXHmf-jWUwadoFsQ',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2021-01-09.c67349946ec9a32856dc6a6ba021ac54; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        # rnum_ = randrange(len(test_code))
        self.body = {
            "format_reference": "5ef30dcce52bc182d359ccfd",
            "callback": "",
            "test_type": "Exam Readiness",
            "user_id": embibe_token[rnum][1],
            "learnpath_name": "maharashtra board--6th maharashtra board",
            "test_format": {
                "subject": {
                    "maharashtra board--6th maharashtra board--mathematics": {
                        "chapter": {
                            "maharashtra board--6th maharashtra board--mathematics--arithmetic--divisibility": {
                                "name": "Divisibility",
                                "display_name": "Divisibility"
                            }
                        },
                        "name": "Mathematics",
                        "display_name": "Mathematics"
                    }
                }
            },
            "difficulty_level": "Medium",
            "marking_scheme": {
                "nmarks": "-1",
                "partial_marking": False,
                "pmarks": "+4"
            },
            "name": "Achieve Journey",
            "exam_code": "kve273003"
        }
        url = f"/content_ms_fiber/v1/embibe/en/fiber/achieve/generate/test"
        response = self.client.post(url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
