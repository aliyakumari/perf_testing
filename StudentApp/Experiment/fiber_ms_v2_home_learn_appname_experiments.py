from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'authority': 'preprodms-cf.embibe.com',
  'accept': 'application/json',
  'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNoZWlraCIsInRpbWVfc3RhbXAiOiIyMDIzLTAyLTIzIDA2OjM3OjEyIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4OTk4OTk5MDAiLCJyb290T3JnSWQiOiI2MjIxZmZlZGFmOTZlMTQ0ODNhMjJlZjYiLCJkZXZpY2VJZCI6IjAuMDcyNjE5MjExOTY5MDQyNTMiLCJmaXJzdE5hbWUiOiJBbndhciIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjIyMjAwMjNiNzYzY2UyMjFmZDhhMDdjIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjIyMjAwZGI0ZTliMmQzZTNkMDg2MTkyIiwiaWQiOjE1MDE0MTMyMzcsImVtYWlsIjoiYW53YXJfdGVhY2hlcjJAZW1iaWJlLmNvbSJ9.8UuitXbH6XeFvNemIT-fOt8uSOycDPo1cZ2vpEmt_1huefLCy0Cz4HuwJYi720ocDOrS87Vp22WJZs-1m-XT5Q',
  'origin': 'https://staging-fiber-web.embibe.com',
  'referer': 'https://staging-fiber-web.embibe.com/learn/virtual-lab-experiment/234/lm83404',
  'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-site',
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
  'Cookie': 'preprod_embibe-refresh-token=3f1538c6-894e-4977-a644-b947411ad045; school_preprod_embibe-refresh-token=322ed536-0bed-43ab-86cd-5246cee85aec; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
}



    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {}
        url = "/fiber_ms/v2/home/learn/?exam_code=kve97671&goal_code=kve97670&locale=en&app_name=embibe_experiments"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
