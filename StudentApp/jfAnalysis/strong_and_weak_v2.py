from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe-Token.csv')
embibe_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Imt1bWFyaSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTA2IDA5OjE0OjM2IFVUQyIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJyb290T3JnSWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJmaXJzdE5hbWUiOiJBbGl5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQwNThiMjY3M2YzMTcwMDI3Y2E4MWFiIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQwNThiMjc3M2YzMTcwMDI3Y2E4MWFjIiwiaWQiOjE1MDQ5NTI1NzYsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.l3g0xsRJtqaMCy-nHMuQA2WYJsRgzghgN4jsO8NOV4hH2NkNvO63kfFdAFZ0cpqaaEszhTwAqmFLhz3SBGgw9w; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }
        self.QNO = 51446

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({
            "level": "topic",
            "user_id": embibe_token[rnum][0],
            "learnpath_format": "Goal--Exam",
            "level_list": ["uttarakhand board--12th uttarakhand board"]
        })
        url = f"/de/jf_analyse/strong_and_weak_v2"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
