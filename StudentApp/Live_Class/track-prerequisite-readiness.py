import resource
import time
import json

import gevent
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IktoYW4iLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMy0xNCAxMTo0OToyMyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI4MjE4NTQ1NDM3Iiwicm9vdE9yZ0lkIjoiNjJiYzYzODNjNjcyMjUyYWFmNTU2MDIxIiwiZGV2aWNlSWQiOiIwLjU4MDQzODI4MTE1NjQ4ODUiLCJmaXJzdE5hbWUiOiJTYWxtYW4gQW53YXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidXNlcl90eXBlIjozLCJwYXJlbnRPcmdJZCI6IjYyYmM2M2FmYzY3MjI1MmFhZjU1NjAyNSIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzNmI4MGJlNWE0NGI2NmVmMGRjMmMxMSIsImlkIjoxNTA0Nzk4NjU1LCJlbWFpbCI6InNhbG1hbi5raGFuQGVtYmliZS5jb20ifQ.eiZmrP45xBu3natfTbtlfcChATbqWisC-Rm2XxKzJPbrUDT8VXImWE6uv2iTxglSc20jeVfaHslOHsT_CPs8UQ',
            'Content-Type': 'application/json'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {
            "classId": "6388c2a1d495fd00a30ee9de",
            "subjectId": "kve97670--kve97671--kve97678",
            "kvCode": [
                "kve103637"
            ],
            "slotId": "640b04a64ab5a132b907ce19",
            "isLiveClass": True
        }
        url = "/track/v2/prerequisite-readiness?locale=en"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
