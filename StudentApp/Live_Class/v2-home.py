import resource
import time
import pandas as pd
import json
import logging

import gevent
from random import randrange
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from utils import DbConnector, ResultSaver
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
        
logger = logging.getLogger(__name__)

df = pd.read_csv("Embibe_Token - TOKEN.csv")
token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    
    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        url = '/userhome/liveclasses/v2/home?examCode=kve500394&goalCode=kve456851&locale=en&channel=web&page=1&pageSize=20'
        db_save.save_results(url, 'GET', environment)
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json',
  'Accept-Language': 'en-GB,en;q=0.9',
  'Connection': 'keep-alive',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'Sec-GPC': '1',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzk2MzA0MjQsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTUwNDczOCwiZXhwIjoxNjc5NzE2ODI0LCJkZXZpY2VJZCI6IjE2NjI3MTE0ODg0ODMiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoieHJjbDRvYXFoZDUubWFyMjJAZW1iaWJlLmNvbSJ9.8d9RuKD0KwJx0JrAufIXcFaZqAbP9rpuv_AGPv4N9yM7G-RHOT0mXJt0bK5ENWu4IEvtq6X8UpPrnGUXXvtyJA',
  'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['embibe-token'] = token[rnum][1]
        url = "/userhome/liveclasses/v2/home?examCode=kve500394&goalCode=kve456851&locale=en&channel=web&page=1&pageSize=20"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
