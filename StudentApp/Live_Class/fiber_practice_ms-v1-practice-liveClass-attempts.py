import resource
import time
import pandas as pd
import json

import gevent
from random import randrange
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv("Embibe_Token - preprod profile token.csv")
token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'DNT': '1',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Pragma': 'no-cache',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'Cookie': 'preprod_embibe-refresh-token=d0606bd5-4611-4069-b11d-8e893ecc4e7c; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }
        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.body = json.dumps({
            "userId": token[rnum][2],
            "blendedDifficulty": 4,
            "correctness": -1,
            "timeSpent": 3.432,
            "questionCode": "4912164",
            "category": "radio",
            "status": "answered",
            "attemptTypeBadge": "Wasted",
            "createdAt": 1662970955295,
            "language": "en",
            "sessionLearningMap": {
                "exam_code": "kve99731",
                "exam_name": "9th cbse",
                "level": "topic",
                "current_mean_dl": 3,
                "bundle_code": "kve103097",
                "bundle_version": 0,
                "type": "Normal",
                "goal_name": "CBSE",
                "format_id": "5ec5867a0c88fe5860961943",
                "lm_name": "5ec5867a0c88fe5860961943--cbse--9th cbse--science--chemistry--matter in our surroundings--physical nature of matter",
                "chapter_code": "kve103097"
            },
            "learnpath_name_format": "goal--exam--subject--unit--chapter--topic",
            "learnpath_name": "cbse--9th cbse--science--chemistry--matter in our surroundings--physical nature of matter",
            "device": "Web",
            "conceptCodes": [],
            "periodId": "6222034009a8894bfaafafe2",
            "lessonId": "6363c600067d5b7e656cb1d6",
            "source": "Teach",
            "platform_session_id": "00329cbe-c0c1-4205-a944-26c8d65cb9e2",
            "bloom_level": "Visualization",
            "timeStamp": 1662970955295
        })
        url = "/fiber_practice_ms/v1/practice/liveClass/attempts"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
