import resource
import time
from codecs import encode
import json

import gevent
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNoZWlraCIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTE1IDE0OjAyOjEzIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4OTk4OTk5MDAiLCJyb290T3JnSWQiOiI2MjIxZmZlZGFmOTZlMTQ0ODNhMjJlZjYiLCJkZXZpY2VJZCI6IjAuOTA4ODAxNDg3NTg3MzQ1OCIsImZpcnN0TmFtZSI6IkFud2FyIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6MywicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.v268aLvtyRDlhE89akTBDrP64p9sz6UXbMfCqBoP_ObNp6_a3mD7CDcOuq_z9ydMK0yNbFDn2wBYXA6ep2ROKQ',
            'Content-Type': 'application/json'        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {
            "kvCode": "kve97670--kve97915--kve98045",
            "classId": "62220082fdd2612096bcc795",
            "slotId": "640eb5d44ab5a132b907ce25",
            "isLiveClass": False
        }
        url = "/track/v2/remedial-action?locale=en"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
