import resource
import time
import pandas as pd
import json

import gevent
from random import randrange
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv("Embibe_Token - TOKEN.csv")
token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Accept-Language': 'en-GB,en;q=0.9',
            'Connection': 'keep-alive',
            'Origin': 'https://staging-fiber-web.embibe.com',
            'Referer': 'https://staging-fiber-web.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Sec-GPC': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzk2MzA0ODEsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJpZCI6MTUwMTUwODY0OCwiZXhwIjoxNjc5NzE2ODgxLCJkZXZpY2VJZCI6IjE2NjI3MTE0ODg0ODMiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoid2p2bHBnNWlydmEubWFyMjJAZW1iaWJlLmNvbSJ9.b50oETRcJNLMHOUhHF38LmwqPtXarCtyIosg9CIB4S0CeiA6rWsd0Nsysmy7ZRWIzFf_NQ-YtgIdGZQ6VTkWFg',
            'Cookie': 'preprod_embibe-refresh-token=e250392d-f61e-4842-961e-81a6a0c587bf; school_preprod_embibe-refresh-token=a8531e3f-871c-4a33-9e38-2714301f06ad; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['embibe-token'] = token[rnum][1]
        url = "/userhome/liveclasses/v1/in-class-performance?examCode=kve4127573&locale=en&liveClassId=639191b9accfeb776933c48b&channel=web"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
