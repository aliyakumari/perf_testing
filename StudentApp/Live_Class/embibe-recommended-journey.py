import resource
import time
import json

import gevent
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlYWNoZXIiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMS0yOSAwNzowOTowMyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODk5ODk5OTAwIiwicm9vdE9yZ0lkIjoiNjIyMWZmZWRhZjk2ZTE0NDgzYTIyZWY2IiwiZGV2aWNlSWQiOiIwLjgwMjUzOTEwMzgyMDI2NTQiLCJmaXJzdE5hbWUiOiJBbndhciBTaGVpa2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.fxpLEgEs9pZ5kt0H4CmRlbFvt0T287Sj07ISTrvcIou10nAaedzAbhDt2QODJJSNFlMkYpE2zp_0CW4o-n-YwQ',
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        self.body = {
            "classId": "62220082fdd2612096bcc794",
            "subjectId": "kve97670--kve99731--kve99742",
            "kvCode": [
                "kve103114"
            ]
        }
        url = "/track/v2/embibe-recommended-journey?locale=en"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
