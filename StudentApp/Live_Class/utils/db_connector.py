import pymongo

class DbConnector:
    def __init__(self, db_url, db_name, collection_name):
        self.client = pymongo.MongoClient(db_url)
        self.db = self.client[db_name]
        self.collection = self.db[collection_name]
        
    def fetch_data(self, query, projection):
        data = self.collection.find(query, projection)
        return list(data)
        
