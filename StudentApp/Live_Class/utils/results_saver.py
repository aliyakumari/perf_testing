import pymongo
from locust import runners
import logging

class ResultSaver:
    
    
    def __init__(self, db_url, db_name, collection_name):
        self.client = pymongo.MongoClient(db_url)
        self.collection =  self.client[db_name][collection_name]
        
    def save_results(self, api_name, api_type, environment, **_kwargs):
        logger = logging.getLogger(__name__)
        
        stats = environment.stats

        
        report = {
            'api_name': api_name,
            'api_type': api_type,
            'fail_ratio' : stats.total.fail_ratio,
            'start_time' : stats.start_time,
            'end_time' : stats.last_request_timestamp,
            'requests' : {}
        }
        
            
        report['requests'][stats.total.name] = {
            'request_count': stats.total.num_requests,
            'failure_count': stats.total.num_failures,
            'median_response_time': stats.total.median_response_time,
            'average_response_time': stats.total.avg_response_time,
            'min_response_time': stats.total.min_response_time,
            'max_response_time': stats.total.max_response_time,
            'average_content_size': stats.total.avg_content_length,
            'throughput(RPS)': stats.total.total_rps,
            'failures_per_second': stats.total.fail_ratio,
            '50_percentile': stats.total.get_response_time_percentile(0.5),
            '60_percentile': stats.total.get_response_time_percentile(0.6),
            '70_percentile': stats.total.get_response_time_percentile(0.7),
            '80_percentile': stats.total.get_response_time_percentile(0.8),
            '90_percentile': stats.total.get_response_time_percentile(0.9),
            '95_percentile': stats.total.get_response_time_percentile(0.95),
            '99_percentile': stats.total.get_response_time_percentile(0.99),
            '100_percentile': stats.total.get_response_time_percentile(1.0)
        }
        
        
        self.collection.insert_one(report)
        logger.info(
            "\n\n-----------------------Data Pushed--------------------------\n")