from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

print(embibe_token[0])


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyZWxhdGlvbl9vcGVyYXRpb25fdHlwZV9pZCI6MSwidXNlcl90eXBlIjoxLCJwYXJlbnRfdXNlcl9pZCI6MTUwMTE5ODc5NSwiY3JlYXRlZCI6MTY0Mzc4NTg4NCwib3JnYW5pemF0aW9uX2lkIjoiMSIsImlkIjoxNTAxMTk4Nzk2LCJleHAiOjE2NDM3OTMwODQsImRldmljZUlkIjoic2Nob29sLWFwcC10ZWFjaGVyLTE1MDAwNDUwMTIiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDExOTg3OTVfMjYwOTIzNjUxMzA4Mjk4N0BlbWJpYmUtdXNlci5jb20ifQ.pWm8bjMdB9spYcYMMScjUxelQ6dVKycpGm0PHICW-oB_5k7wjmDxKSuqizXUNciGO1wJ9nnElC02ryUdMD_lPw',
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=202185E4526DFC447C1B8EC7AAA24597; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

        self.Question_No = 51444

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        self.Question_No = self.Question_No + 1
        rnum = randrange(len(embibe_token))
        self.body = json.dumps([{"eorder": 0, "event_info": "", "event_type": "", "question_code": "1929636",
                                 "round": 0, "section": "", "sequence": 0, "t": 0}])
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/fiber_practice_ms/app/practice/evaluation/1929636?language=en&namespace=embibe&version=1"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
