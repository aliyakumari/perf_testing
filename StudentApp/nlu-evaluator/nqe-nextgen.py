from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


nqe_data_df = pd.read_csv("Embibe_Token - preprod profile token (1).csv")
nqe_data_data = nqe_data_df.values.tolist()
nqe_data_data.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = f"/nqe/api/v2/nqe-nextgen"
        self.headers =  {
  'accept': 'application/json',
  'Content-Type': 'application/json',
  'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def currentServerTime(self):
        rnum = randrange(len(nqe_data_data))
        self.body = json.dumps({
    "lm_level": "topic",
    "user_id": nqe_data_data[rnum][2],
    "paj_id": None,
    "session_id": nqe_data_data[rnum][3],
    "concept_code": None,
    "source": "chat_bot",
    "locale": "en",
    "request_id": "344791e0-4154-4f66-8695-73393a1f9196",
    "lm_level_code": "5ec5867a0c88fe5860961943--cbse--10th cbse--science--physics--electricity--electric power",
    "continue_post_mastery": False,
    "last_attempt": {
        "difficulty_level": 3,
        "correctness": 1,
        "lm_level": "topic",
        "primary_concepts": [
            "new_KG2294"
        ],
        "all_concepts": [
            "new_KG2294"
        ],
        "attempted_at": 1673249497118,
        "object_type": "question",
        "object_id": "3054783",
        "question_id": "3054783",
        "bloom_level": "Rote Learning",
        "attempt_id": "63bbc2d5b0e9d11d2b184ec2",
        "attempt_type": "practice_attempt",
        "attemptTypeBadge": "Perfect",
        "lm_level_code": "5ec5867a0c88fe5860961943--cbse--10th cbse--science--physics--electricity--electric power"
    },
    "exam_code": "kve101929"
})

        response = self.client.post(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
