from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


Token_data = pd.read_csv("Embibe_Token - preprod profile token (1).csv")
Token_data_user_id = Token_data.values.tolist()
Token_data_user_id.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = f"/concept_mastery/api/v1/generate_mastery_score"
        self.headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def currentServerTime(self):
        rnum = randrange(len(Token_data_user_id))
        self.body = json.dumps({
            "user_id": Token_data_user_id[rnum][2],
            "concepts": [
                {
                    "concept_code": "KG1124",
                    "mastery": 0.1,
                    "skill": "Beginner",
                    "activity": [
                        {
                            "activity_type": "learn",
                            "media_id": "string",
                            "completion": 87,
                            "accuracy": 1
                        },
                        {
                            "activity_type": "test",
                            "is_primary_concept": False,
                            "question_id": "string",
                            "correctness": 1,
                            "difficulty_level": 1,
                            "status": "notVisited",
                            "badge": "test"
                        },
                        {
                            "activity_type": "practice",
                            "is_primary_concept": False,
                            "question_id": "string",
                            "correctness": 0,
                            "difficulty_level": 1,
                            "badge": "test"
                        }
                    ]
                }
            ]
        })
        response = self.client.post(self.url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
