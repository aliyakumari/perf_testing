from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


nlu_evaluator_data_df = pd.read_csv("nlu_evaluator_data.csv")
nlu_evaluator_data = nlu_evaluator_data_df.values.tolist()
nlu_evaluator_data.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url = f"/nlu-evaluator/api/v1/evaluate/"
        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=554e21bc-e039-4a0d-bc9d-4cf586603390'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def currentServerTime(self):
        rnum = randrange(len(nlu_evaluator_data))
        payload = json.dumps({
            "writtenAnswer": nlu_evaluator_data[rnum][2],
            "givenAnswer": nlu_evaluator_data[rnum][3],
            "language": nlu_evaluator_data[rnum][0],
            "subject": nlu_evaluator_data[rnum][1]
        })

        response = self.client.post(self.url, data=payload, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
