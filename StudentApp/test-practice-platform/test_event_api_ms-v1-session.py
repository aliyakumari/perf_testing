import json
import resource
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('TestMetaData.csv')
token = df.values.tolist()
token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'Accept': 'application/json',
            'browser-id': '1607621528095',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
            'embibe-token': 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOjE1MDE1MDkxMjksInNlc3Npb25JZCI6IjFjOGMwOTQ2NmU0MzdjYjBhOWMyMTQ5NmI1Njc0MTZmZDRkZmRlY2QiLCJ0eXBlIjoiUFJBQ1RJQ0UiLCJzdWJUeXBlIjoiQk9PS19QUkFDVElDRSIsImxlbmd0aCI6MTgwMDAwMCwiZXhwaXJ5VHlwZSI6IkRZTkFNSUMiLCJleHBpcnkiOjE2ODA2ODg4MzAxMzh9.UMBjtr6fD7SNab6CfC3lKR5-Ad5T9hjXBKWA78xXm14Wso_sC47zbOIdvMnvuHRDFPjwrIl6Zd1SZh3WkkrE-g',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://fiber-demo-web.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'expiry-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxNTA5MTI4LCJjcmVhdGVkIjoxNjgwNjg2NjAxLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDE1MDkxMjksImV4cCI6MTY4MDc3MzAwMSwiZGV2aWNlSWQiOiIxNjYyNzExNDg4NDgzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxNTA5MTI4XzExNDM1MzM2MjQ0MTkwODk5QGVtYmliZS11c2VyLmNvbSJ9.BXn-7FLqCxI4TOdrEKbNpTywK6l6l7THcmeN4-aj0fZwlza70XbBtskywOx-uN75alhiEOX7E6IICVfFixEYSg,eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOjE1MDE1MDkxMjksInNlc3Npb25JZCI6IjFjOGMwOTQ2NmU0MzdjYjBhOWMyMTQ5NmI1Njc0MTZmZDRkZmRlY2QiLCJ0eXBlIjoiUFJBQ1RJQ0UiLCJzdWJUeXBlIjoiQk9PS19QUkFDVElDRSIsImxlbmd0aCI6MTgwMDAwMCwiZXhwaXJ5VHlwZSI6IkRZTkFNSUMiLCJleHBpcnkiOjE2ODA2ODg4MzAxMzh9.UMBjtr6fD7SNab6CfC3lKR5-Ad5T9hjXBKWA78xXm14Wso_sC47zbOIdvMnvuHRDFPjwrIl6Zd1SZh3WkkrE-g'
        }

        self.body = json.dumps([
            {
                "order": 1,
                "type": "VIEW_QUESTION",
                "timestamp": 1650438026,
                "questionId": "EM6984844"
            },
            {
                "order": 2,
                "type": "VIEW_QUESTION",
                "timestamp": 1650438089,
                "questionId": "EM6984884"
            },
            {
                "order": 3,
                "type": "SAVE_ATTEMPT",
                "answers": [
                    {
                        "id": "1",
                        "body": "a"
                    }
                ],
                "timestamp": 1650438026,
                "questionId": "EM6984844"
            }
        ])

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['expiry-token'] = token[rnum][1]
        self.headers['embibe-token'] = token[rnum][2]
        sessionid = token[rnum][0]
        url = f"/test_event_api_ms/v1/session/{sessionid}/events"
        response = self.client.post(url, data=self.body, headers=self.headers)
        


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
