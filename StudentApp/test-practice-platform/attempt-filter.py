import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('id_expiry_and_embibe_token.csv')
session_id = df.values.tolist()
session_id.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(session_id))
        self.body = json.dumps({
            "filters": [
                {
                    "operationType": "AND",
                    "fields": [
                        {
                            "field": "QUESTION_ID",
                            "values": [
                                "EM6300616"
                            ]
                        },
                        {
                            "field": "SESSION_ID",
                            "values": [
                                session_id[rnum][0]
                            ]
                        }
                    ]
                }
            ],
            "fields": ["SESSION_ID"],
            "views": ["ATTEMPT_LIST_VIEW"],
            "sorts": [
                {
                    "field": "ID",
                    "order": "ASC"
                }
            ],
            "page": 10,
            "pageSize": 10
        })
        url = f"/attempts_ms/v1/filter"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
