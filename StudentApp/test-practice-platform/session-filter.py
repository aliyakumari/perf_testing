import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - preprod profile token.csv')
token = df.values.tolist()



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {
    "filters": [
        {
            "operationType": "AND",
            "fields": [
                {
                    "field": "USER_ID",
                    "values": [
                        1500048035
                    ]
                },
                {
                    "field": "TYPE",
                    "values": [
                        "TEST"
                    ]
                }
            ]
        }
    ],
    "fields": [
        "USER_ID"
    ],
    "views": [
        "PRACTICE_SESSION_VIEW"
    ],
    "sorts": [
        {
            "field": "CREATED_AT",
            "order": "ASC"
        }
    ],
    "page": 1,
    "pageSize": 10
}
        
        self.headers = {
            'Content-Type': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDA3Njg2LCJjcmVhdGVkIjoxNjgwMjQ5MzkwLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNDgwMzUsImV4cCI6MTY4MDMzNTc5MCwiZGV2aWNlSWQiOiIxNjc5MzE3NjAwODEwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDA3Njg2XzIxNzQwMDk5NTE3NDlAZW1iaWJlLXVzZXIuY29tIiwicGxhdGZvcm0iOiJXRUIifQ.q-adr_zQgT8soeqUYPrmcpZ91bufyrfKgpfEEkDyOqKgKnIkLUsNOBp5v8nPAG4Yp07Hste8MprmMYEIC2xBGw',
            'api-access-key': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTIzNDU2NywiZXhwIjoxNjg1NTIzMDkzfQ.1sJ7Dg4vbW8yinby1xpuSEMVbnN_d2SaeJxGnG5AJtc',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['embibe-token'] = token[rnum][1]
        self.body.get('filters')[0].get('fields')[0].get('values')[0] = token[rnum][2]
        url = f"/tpp-svc/v1/sessions/filter"
        response = self.client.post(url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
