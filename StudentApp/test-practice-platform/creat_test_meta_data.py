import requests
import json
import pandas as pd

def callCreateSessionAPi(token):
    url = "https://preprodms.embibe.com/tpp-svc/v1/sessions"
    payload = json.dumps({
    "type": "TEST",
    "testId": "mb56045",
    "subType": "SUBJECT_TEST",
    "clientIdentity": {
        "device": "MOBILE",
        "consumptionMedium": "BROWSER",
        "os": "ANDROID",
        "property": "STUDENT_APP",
        "platformSessionId": "rsrs44s4sytythvvh7"
    },
    "referer": [
        {
        "type": "HOMEWORK",
        "id": "Idasdasdasd"
        }
    ],
    "locale": "en",
    "examId": "kve97915"
    })
    
    headers = {
    'Accept': 'application/json',
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-site',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
    'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ0ZW5hbnRfaWQiOiI2MmZiNmZjM2U1NjZmMTUxNjZjZjRlOGIiLCJjb3VudHJ5IjoxLCJwYXJlbnRfdXNlcl9pZCI6MTUwNDgxNjM2NywiY3JlYXRlZCI6MTY3OTU1NDYwNywidGVuYW50X3R5cGUiOiJTY2hvb2wiLCJkZXZpY2VJZCI6IjE2NzUyNTY0OTUwODAiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsInVzZXJfdHlwZSI6MSwib3JnYW5pemF0aW9uX2lkIjoiMSIsImlkIjoxNTA0ODE2MzY4LCJleHAiOjE2Nzk2NDEwMDcsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDQ4MTYzNjdfNTg1NDI0MjcwMjgyMDIyOEBlbWJpYmUtdXNlci5jb20ifQ.5lbroNLN6oiypSlOyevQRLbJOUtOnZesTVjrHUz5ajnriOGdIuVrVfC5UrTLBGteJsRFSwr-zRKMwuyofhaAdw',
    'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'browser-id': '123456789',
    'Content-Type': 'application/json',
    'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
    }
    
    headers['embibe-token'] = token
    

    response = requests.request("POST", url, headers=headers, data=payload)
    

    return response

def main():
    df = pd.read_csv('Embibe_Token - preprod profile token.csv')
    embibe_token = df.values.tolist()
    embibe_token.pop()
    testMetaData = []
    for token in embibe_token:
        meta_data = []
        response = callCreateSessionAPi(token[1])
        meta_data.append(response.json().get('data').get('id'))
        meta_data.append(response.headers.get('expiry-token'))
        meta_data.append(token[1])
        meta_data.append(token[2])
        testMetaData.append(meta_data)
        
        df = pd.DataFrame(testMetaData)
        df.to_csv("TestMetaData.csv", index=False)
    
main()
