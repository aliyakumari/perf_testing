import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - preprod profile token.csv')
embibe_token = df.values.tolist()
embibe_token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxNTA0NzM2LCJjcmVhdGVkIjoxNjczODUwNDgwLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDE1MDQ3MzcsImV4cCI6MTY3MzkzNjg4MCwiZGV2aWNlSWQiOiIxNjYyNzExNDg4NDgzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxNTA0NzM2XzExNDU2NTMwNDI3ODUyMTYzQGVtYmliZS11c2VyLmNvbSJ9.1RQPIONqhsI6GzQhMBt00FKQ2rHhZqrLwsBGrrhGFd-dnsFMbuX1wE1Js9OSEXhx0xgLv5VbdZFMjvI8ESJgQA',
            'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'browser-id': '123456789',
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }
        
        self.id_expiry_and_embibe_token = []

        self.body = json.dumps({
            "type": "PRACTICE",
            "subType": "BOOK_PRACTICE",
            "syllabus": ["kve17228"],
            "clientIdentity": {
                "device": "MOBILE",
                "consumptionMedium": "BROWSER",
                "os": "ANDROID",
                "property": "STUDENT_APP",
                "platformSessionId": "rsrs44s4sytythvvh7"
            },
            "referer": [{
                "type": "HOMEWORK",
                "id": "Idasdasdasd"
            }],
            "locale": "en",
            "examId": "kve479076"

        })

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        meta_data = []
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/tpp-svc/v1/sessions"
        response = self.client.post(url, data=self.body, headers=self.headers)
        meta_data.append(response.json().get('data').get('id'))
        meta_data.append(embibe_token[rnum][1])
        meta_data.append(response.headers.get('expiry-token'))
        self.id_expiry_and_embibe_token.append(meta_data)
        df = pd.DataFrame(self.id_expiry_and_embibe_token)
        df.to_csv("id_expiry_and_embibe_token.csv", index=False)
        

def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
