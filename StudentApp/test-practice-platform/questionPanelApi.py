import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('id_expiry_and_embibe_token.csv')
session_id = df.values.tolist()
session_id.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers =  {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxNTA3Njg2LCJjcmVhdGVkIjoxNjczODUwNTQ0LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDE1MDc2ODcsImV4cCI6MTY3MzkzNjk0NCwiZGV2aWNlSWQiOiIxNjYyNzExNDg4NDgzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxNTA3Njg2XzExNDU0MzQzODEzODc3OTI3QGVtYmliZS11c2VyLmNvbSJ9.qwrvAIsrOAmBoPcTOuIEbqgxI8645GGfVS4PzIp_f_fVJDXr7drtA7lehafZaV0Bt29tKSO19RagFOsuSgAANQ',
  'Cookie': 'preprod_embibe-refresh-token=cab46aa1-0198-47ff-aa89-c74a78457b93; prod_ab_version=0; prod_embibe-refresh-token=b6982c4d-184a-41a9-b13c-687df5930d80; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2NzUzMjkzOTYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3NDc3ODU4NTg2IiwiaWQiOjIwMDI3OTE4NDUsImV4cCI6MTY3NjUzODk5NiwiZGV2aWNlSWQiOiIxNjczMzQ5MzQ3NDAxIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.5nf_S7U4gwb1k-Whcisi0_uDO9kyfhr4PbQUaIY9hZwc9sGhM1CKMsB9IxhVT0NN7t-3RqzLdyRwTTC3FUJuLw; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(session_id))
        self.headers['embibe-token'] = session_id[rnum][1]
        self.body = {}
        url = f"/tpp-svc/v1/sessions/{session_id[rnum][0]}/questions"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
