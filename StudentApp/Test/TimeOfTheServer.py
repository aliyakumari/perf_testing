from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.headers = {
  'Connection': 'keep-alive',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
  'Accept': '*/*',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Accept-Language': 'en-IN,en;q=0.9,hi-IN;q=0.8,hi;q=0.7,en-GB;q=0.6,en-US;q=0.5',
  'Cookie': 'V_ID=ultimate.2020-10-29.f38ec371e42b5c457356b53331e95207; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQ2ODA3NjU5MywiZW1haWwiOiJndWVzdF8xNjA1ODY3OTk0NDAzOTgzQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTIwVDEwOjI2OjM0Ljg5MFoifQ.9s55rQU9IVkbDIVxJFBxZJH9RtEWLvdr1SXyz-8wh6FsfeKU9MEoe0TkDJewPEXhwbjQXHjNICIfxwX_7KGaHQ; access-token=rpwK685MdHJqXrKPB3cpNg; uid=guest_1605867994403983%40embibe.com; client=EoLoCYMkob3NM0pRMkXxBw; _test_app_session=fbaac235491c4314c46ee811bfc37d49; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}





        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = {}
        self.headers['Cookie'] = f"V_ID=ultimate.2020-10-29.f38ec371e42b5c457356b53331e95207; embibe-token={embibe_token[rnum][0]}; _test_app_session=fbaac235491c4314c46ee811bfc37d49; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57"
        url = f"/testsubmission_ms_fiber/now"

        with self.client.get(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 200 :
                return response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
