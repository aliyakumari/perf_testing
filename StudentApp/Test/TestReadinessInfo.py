from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTE5IDE5OjQzOjAxIFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwiaWQiOjU2NzA0NTcsImVtYWlsIjoiYzFjOGQ4NTI0MjhjQGppby1lbWJpYmUuY29tIn0.A3WthFqnACclgbdeG410MpSsfcBCIcHCxlOdxK-lu56-r-8dUygyvWsQjTP424HBXvxy6iZdElqsbWzkj8OVIg',
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=451C17FBC5E564CFE5106A4FE5C428B3; V_ID=ultimate.2020-03-24.7eb27b96897836c6e0c121c3f9459f61; V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQxOTUyMTc1MSwiZW1haWwiOiJndWVzdF8xNTk5NzQ5MzgyNzU2NDYzQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTEwVDE0OjQ5OjQzLjY5OFoifQ.WsdZ3XHmClwmyg78Q8mFZwvihfvJWkPGcFetUdYXwRbzWjYapeHTdBMV3ijNgKtEgIQyCtgDMvTw7iW-f8mQ0w; access-token=J7KbD-OynCcbU5yfq-GVUg; uid=guest_1599749382756463%40embibe.com; client=dmFTCgklbEi-ULx1Ku-cJw; V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.headers['embibe-token'] = embibe_token[rnum][1]
        self.body = json.dumps({
            "user_id": embibe_token[rnum][1],
            "test_code": "mb36849"
        })
        url = f"/fiber_ms/home/test-readiness-info"

        with self.client.post(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 405:
                print(response.text)
            if response.status_code == 200:
                return response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
