from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.headers = {
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0',
  'Accept': 'application/json',
  'Accept-Language': 'en-US,en;q=0.5',
  'Accept-Encoding': 'gzip, deflate, br',
  'Content-Type': 'application/json;charset=utf-8',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA3LTMxIDIwOjAxOjQyIFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6MTg5MTI0NiwiZW1haWwiOiJndWVzdF8xNTk2MjI1NzAyNDcxMzI1NjQ2QGVtYmliZS5jb20ifQ.WCWrmjCEIrr2ca3oCY_pBNtqkFDJzJQMkt5avecKEvRDcRcXfeHeRGqelkvKeQu70Ch2OSKgQ4nsH_lw2v9qfg',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Connection': 'keep-alive',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'Cookie': 'JSESSIONID=wjfpUqhyEADxidnf2CnozHVnDM9QhZqmAF8Uz12A; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({"format_reference_id":"5ef2ff88e52bc182d359b8f7","grade":"7","learn_path":"rajasthan board--7th rbse--mathematics","query":"*","section":"more tests","size":50,"source":"Test Page Summary","test_code":"mb93444","user_id":embibe_token[rnum][1],"locale":"en","xpath":"/mock-test/7th-rbse/full-test/mathematics-paper---2-2-3-4-5-6"})
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/fiber_ms/home/more-search-test"

        with self.client.post(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 200 :
                return response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
