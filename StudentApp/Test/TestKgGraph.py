from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': '*/*',
            'embibe-token': 'eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTExNzA3MTY0NywiZW1haWwiOiJkcGppaXRrZ3BAZ21haWwuY29tIiwib3JnYW5pemF0aW9uX2lkIjpudWxsLCJpc19ndWVzdCI6ZmFsc2UsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMjAtMDQtMTRUMTM6MjE6MzcuOTQ1WiJ9.ZcqRD87bE7RmyhQBQhg2BWcTzVLLxWmMaENbTkrtiTZqQe8LIzkXyv9ozudHd5cVsYHgBOBW0Yvk5ziVY6JxPQ',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQwMTkwMDQ1NCwiZW1haWwiOiJndWVzdF8xNTk4MjYxMzk5ODM2NjEyQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTI0VDA5OjI5OjU5Ljk4NVoifQ.JuByJP7F-9hyG13dIRtHZ6FZRYAfnSFuJaUo7a_DtXb2MvBSLDmgdYOphUucQYFd5tVzf0pibnDw0bOFeC-4Tw; access-token=-4nqrGHNlAsV9g6Ngn7MWA; uid=guest_1598261399836612%40embibe.com; client=waaqM90dxY5hRLrsYCxE7Q; V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQxOTUyMTc1MSwiZW1haWwiOiJndWVzdF8xNTk5NzQ5MzgyNzU2NDYzQGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA5LTEwVDE0OjQ5OjQzLjY5OFoifQ.WsdZ3XHmClwmyg78Q8mFZwvihfvJWkPGcFetUdYXwRbzWjYapeHTdBMV3ijNgKtEgIQyCtgDMvTw7iW-f8mQ0w; access-token=J7KbD-OynCcbU5yfq-GVUg; uid=guest_1599749382756463%40embibe.com; client=dmFTCgklbEi-ULx1Ku-cJw; V_ID=ultimate.2020-04-06.47e63a57ee497c0baba9fa7f897236c5; _test_app_session=e98e5bdef72bf1fb8f24911d6831bdda; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQzOTcyMTc0MSwiZW1haWwiOiJndWVzdF8xNjAxOTk2NDYyNDM3OTc4QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTEwLTA2VDE1OjAxOjAyLjk5OVoifQ.UYPfAiP_jrGmOZuBlX7HaE_USSGTYz9Yp_pXcmRGixDcy8Z0vJFfF4Z4zidAEirT_aHTPFf9uklA7Nf0Xq9d2g; access-token=DzqwlWhWikrEUYXMy6oaxA; uid=guest_1601996462437978%40embibe.com; client=perRjO8_Oyx_jVh_PmJQ2A; V_ID=ultimate.2020-10-29.f38ec371e42b5c457356b53331e95207; JSESSIONID=vBEt5qgKiarT3DyAcA1-TG8DICp_ZZ-uatvgdU_f; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({
            "test_code": "mb43035"
        })
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/fiber_ms/home/test-kg-graph"

        with self.client.post(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 422:
                print(response.text)
            if response.status_code == 400 or response.status_code == 200:
                return response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
