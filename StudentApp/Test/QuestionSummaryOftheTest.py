from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()
embibe_token.pop()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0',
  'Accept': 'application/json',
  'Accept-Language': 'en-US,en;q=0.5',
  'Accept-Encoding': 'gzip, deflate, br',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAxMTk4ODA0LCJjcmVhdGVkIjoxNjQxMzY2OTUzLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTg4MDUsImV4cCI6MTY0MTUzOTc1MywiZGV2aWNlSWQiOiIxNjQxMzAzODk1NjQ4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAxMTk4ODA0XzI2ODYwMzYwMjQ5MDM0NzRAZW1iaWJlLXVzZXIuY29tIn0.XLkD8HKi9L6dcoBAcY-QEOqMCJ6sSHhJP1ioeWqz5mw-wAUcWNPxF6mrGv4xAFZDvv6ILutHQz0bXfn_0yO8Hw',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Connection': 'keep-alive',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'Cookie': 'JSESSIONID=wjfpUqhyEADxidnf2CnozHVnDM9QhZqmAF8Uz12A; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}




        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = {}
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = f"/fiber_ms/test/question/summary?test_code=mb43475&child_id={embibe_token[rnum][1]}&exam_code=kve271673&goal_code=kve266089&version=27&locale=en"

        with self.client.get(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 200 :
                return response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
