from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.headers = {
  'Connection': 'keep-alive',
  'Accept': 'application/json, text/plain, */*',
  'browser-id': '1606195187801',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTIzIDE5OjMzOjQxIFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6NTg3MzQwNCwiZW1haWwiOiJndWVzdF8xNjA2MTYwMDIxMzEyMzU2NDY2QGVtYmliZS5jb20ifQ.88dX56FP1VwrQWGgSrJb508QS8OdG3wwhJ3FfA8elgUY2VOTA6r15JSRxPRZUK50lgAt_XfkAYOJFkibFfWYSw',
  'Origin': 'https://preprod.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://preprod.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQ2OTgxMTU2NCwiZW1haWwiOiJndWVzdF8xNjA2MTcxNTQ4NDI3ODY3QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTExLTIzVDIyOjQ1OjQ4LjA2OVoifQ.dZyq4O11KSVG_Agh_ezySTF3bS3KxVAQGQ9zJ-ta75bYKj-T85btjvXpDxX3ztpLMqnsud_YZ_M7PuW1K4l7uA; access-token=M_reeI3AO7oYHc5E-oix1Q; uid=guest_1606171548427867%40embibe.com; client=wqdkX2NGL4eYy7v6mo9eQw; _test_app_session=f45866bbf83aff0ba58bc3942aee335e; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = {}
        url = f"/testsubmission_ms_fiber/v1/test/mb1289/check"
        self.headers['embibe-token'] = embibe_token[rnum][1]
        with self.client.get(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 200 :
                return response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
