from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode
# from utils.CreateToken import creatEmbibeToken


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

df  = pd.read_csv('Embibe-Token.csv' )
embibe_token = df.values.tolist()

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val



class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMjg4NDE2LCJjcmVhdGVkIjoxNjQxMjE1NDA0LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTcxNTEsImV4cCI6MTY0MTMwMTgwNCwiZGV2aWNlSWQiOiJ0ZW1wLXNlc3Npb24tNGM2MjZjNjgxYzI2NDhhYWRmZDA0NTY1NzJkZmFmYjgiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6IjE1MDAyODg0MTZfODIyOTM3MDgxMTAzNTI2QGVtYmliZS11c2VyLmNvbSJ9.X6NeUsgefFFB7xRmbAtHl3d4lpKqR8biGlOsNT_rohbc0UbwvQh_zRjfEQ48tB19_EnDNfDiXRlz6VQXcron3Q',
  'Content-Type': 'application/json',
  'Connection': 'Keep-Alive',
  'User-Agent': 'okhttp/4.7.2',
  'Cookie': 'V_ID=ultimate.2020-10-29.f38ec371e42b5c457356b53331e95207; JSESSIONID=SleAqqb23fwSGkvscF7LSdtKJk-xrZtb_gdK81HE; V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57; access-token=oQOk5ieZFj3QO-XPFrzqWA; client=U6RNS7UxSgMOtFtky78JPg; embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6MTc3NTExNTIwNywiZW1haWwiOiJndWVzdF8xNjQxMTk1NDc1Mjg4OTg4QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIyLTAxLTAzVDA3OjM3OjU1LjYyMFoifQ.EroBSNBY_jDvUpdcgiDNr0kREkYz6snfggsAkYk8g48xoYK0uSBby7LYaiK4ya7DELYeQ6VNo0i2O3S1upvoJA; uid=guest_1641195475288988%40embibe.com'
}


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({"board": "CBSE", "chapter_name": "microorganism friend and foe",
                                "child_id": embibe_token[rnum][1], "exam": "8th-cbse", "goal": "CBSE", "grade": "8"})
        self.headers['embibe-token'] = embibe_token[rnum][1]
        url = "/fiber_ms/home/filter-test"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
