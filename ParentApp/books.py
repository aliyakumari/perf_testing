from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatEmbibeToken

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

embibe_token = creatEmbibeToken()
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):

        self.body = json.dumps({
            "grade": f"{randrange(6, 12)}th Maharashtra Board",
            "locale": "en",
            "pageNumber": 1,
            "pageSize": 20,
            "subject": "Science"
        })

        rnum = randrange(len(embibe_token))
        headers = {
            'embibe-token': embibe_token[rnum],
            'app_version': 'debug-[4.0.0]',
            'platform': 'Android',
            'os_version': '29',
            'device-model': 'RMX2027',
            'device-brand': 'realme',
            'device-name': 'RMX2027',
            'lang': 'en',
            'Content-Type': 'application/json',
        }

        url = f"/cg-fiber-ms/school/books"
        response = self.client.post(url, data=self.body, headers=headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
