from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatEmbibeToken

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

embibe_token = creatEmbibeToken()
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        headers = {
            'app_version': 'debug-[4.0.0]',
            'platform': 'Android',
            'os_version': '29',
            'device-model': 'vivo 1806',
            'device-brand': 'vivo',
            'device-name': 'vivo 1806',
            'lang': 'en',
            'embibe-token': embibe_token[rnum],
        }

        url = "/cg-fiber-ms/learning_objects?where={%22type%22:%22Video%22,%22content.key_attributes.type%22:{%22$in%22:%20[%22Topic%20Explainer%22,%22Experiment%22,%22Solved%20Problems%20asked%20in%20exams%22]}}&lo_type=Video&lm_filter={%22format_reference%22:%225ef30dcce52bc182d359ccfd%22,%22learnpath_name%22:{%22$regex%22:%22maharashtra%20board--8th%20maharashtra%20board--science--biology--living%20world%20and%20classification%20of%20microbes--biodiversity%20and%20need%20of%20classification%22}}&locale=en"
        response = self.client.get(url, data=self.body, headers=headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
