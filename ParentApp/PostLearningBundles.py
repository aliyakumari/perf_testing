from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import createParentToken

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

embibe_token = createParentToken()
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({
            "assignedToList": {
                "STUDENT": [
                    6949851
                ]
            },
            "components": {
                "LEARN": [
                    {
                        "book_type": "bigbook",
                        "categoryUrl": "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/desingmate_cateogry.png",
                        "cdn_url": "https://jioembibe.cdn.jio.com/hlsmedia/embibe/3/43717_1411210546_mobile.m3u8",
                        "content_type": "Topic Explainer",
                        "currency": 0,
                        "custom": True,
                        "description": "Why does our population continue to grow? Why do we look similar but yet so different from our parents? Learn more about the process of reproduction and its two modes.",
                        "id": "7378488",
                        "learningMap": {
                            "formatReference": "5ec5867a0c88fe5860961943",
                            "tags": [],
                            "topicLearnpathName": "cbse--12th cbse--biology--reproduction--reproduction in organisms"
                        },
                        "learningObjectCategory": "Learn",
                        "learningObjectSource": "CG",
                        "learningObjectType": "VIDEO",
                        "length": 414,
                        "objectId": "60423f0ce669e6fa9111c44f",
                        "ownerInfo": {
                            "copyLogo": "",
                            "copyright": "",
                            "key": "",
                            "teaserKey": ""
                        },
                        "previewUrl": "https://visualintelligence.blob.core.windows.net/video-summary/prod/7378488/summary.webp",
                        "sequence": 0,
                        "source": "DesignMate",
                        "subject": "Biology",
                        "thumbUrl": "https://embibe.blob.core.windows.net/images/video_tiles/713754f0-9f17-4a15-b430-ffb3ff7c84ac_3x.webp",
                        "title": "Reproduction and Its Two Modes",
                        "url": "https://vimeo.com/499668681",
                        "watchedDuration": "0%"
                    }
                ],
                "PRACTICE": [
                    {
                        "categoryUrl": "",
                        "code": "kve103637",
                        "custom": True,
                        "duration": 2400,
                        "examCode": "kve97671",
                        "id": "5ec58cd60c88fe5860977980",
                        "learningObjectCategory": "Practice",
                        "learningObjectSource": "CG",
                        "learningObjectType": "EMBIBE_PRACTICE",
                        "learnmapId": "5ec5867a0c88fe5860961943--cbse--12th cbse--biology--reproduction--reproduction in organisms--modes of reproduction",
                        "learnpathName": "cbse--12th cbse--biology--reproduction--reproduction in organisms--modes of reproduction",
                        "lmLevel": "topic",
                        "questionCount": 45,
                        "thumbUrl": "https://embibe.blob.core.windows.net/images/practice_tiles/69bac083-6c82-4224-8485-5227fa808b5b_3x.webp",
                        "title": "Modes of Reproduction"
                    }
                ],
                "TEST": [
                    {
                        "bundleCode": "mb100287",
                        "category": "CUSTOM_TEST",
                        "custom": True,
                        "duration": 900,
                        "id": "0",
                        "learningObjectCategory": "Test",
                        "learningObjectSource": "CMS",
                        "learningObjectType": "TEST",
                        "learnpathName": "cbse--12th cbse--biology",
                        "lmFormatReferenceId": "5ec5867a0c88fe5860961943",
                        "questionCount": 7,
                        "schedule": {
                            "liveStatus": "",
                            "locksAt": "",
                            "solutionsPublicAt": "",
                            "startsAt": "",
                            "stopsAt": ""
                        },
                        "startedAtInMilliSeconds": 0,
                        "subject": "Biology",
                        "testQualityScore": 86,
                        "testStatus": "not_started",
                        "thumbUrl": "https://content-grail-production.s3.amazonaws.com/practice-temp-tiles/Maths_1.webp",
                        "title": "own test 3",
                        "totalMarks": 21,
                        "type": "Test",
                        "xpath": "/mock-test/12th-cbse/custom-test/own-test-3"
                    }
                ]
            },
            "device_type": "Android Mobile",
            "dueDate": 1640087460000,
            "dueDateImage": "dueDateImage.png",
            "eventId": "af83725662dde8741639741909988",
            "locale": "en",
            "name": "Test Screen Time Assignment",
            "platform": "Android",
            "progress": "0%",
            "rewardIconImage": "https://s3-ap-southeast-1.amazonaws.com/uts-production.embibe.com/production/Fiber/assets/images/reward_screen_time.png",
            "rewardId": 3531978,
            "rewardImage": "https://s3.ap-southeast-1.amazonaws.com/uts-production.embibe.com/production/web-assets/images/assignments/prize_screen_time.png",
            "scheduleInformation": {
                "STUDENT": [
                    {
                        "assignedToId": 6949851,
                        "scheduleEnd": 1640087460000,
                        "scheduleStart": 1639741911511
                    }
                ]
            },
            "source": "create-your-own"
        })
        rnum = randrange(len(embibe_token))
        headers = {
            'parent-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9'
                            '.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjIsImNyZWF0ZWQiOjE2Mzk3NDE4NTIsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5OTUyNjIwNDkwIiwiaWQiOjE1MDAwMDEwODQsImV4cCI6MTYzOTkxNDY1MiwiZGV2aWNlSWQiOiIwYWI5YmU0ZC04Yzk2LTRhMGYtYTYyNi00MjA3ODMzYTI1YWIiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6dHJ1ZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.aEo0oRaUj4Kj5ctZPP-0C8BOHbyBpPQv-m9m-uf1UI9m_cM_Td9LKWlulspe9eot8G5jrvAJSYa4vyFNN-m_4g',
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=D66B3748EE0C8D57219D6A66372A2EF6; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; '
                      'embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; '
                      'preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9; '
                      'preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9'
                      '.eyJkZXZpY2VfaWQiOiI1ZDIxODYzOC0zNjZkLTRlMzItOTNiYS0xZDU5ZmUwMzNlZTAiLCJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVNXNWZNaUlzSW05eVoxOXBaQ0k2SWpFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNklqWTFZVFl5TkRnM0xXVmhNV1V0TkRjNFpTMDVZVEpqTFdJeE9HTmhaV1E0WVdVMllpSXNJbWxrSWpvaU9EY3dPVEV4TVRVM01TSXNJbXh2WTJGc1pTSTZJaUo5Lm4zY0ROZUUySTNqWlotSVJRbDR5UkNwX1N3Y3hvYmJkbzd6OGZPWjN0RDNVWl84Y3RhMHJ4czNzbFozLV9uNXdmblowd19FZGdJeW5TNnZEQU1BTEh3IiwiY3JlYXRlZCI6MTYzOTczMTk5Miwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjpmYWxzZSwicmVsYXRpb25zaGlwX3R5cGUiOjAsInVzZXJfaWQiOiI4NzA5MTExNTcxIiwib3JnX2lkIjoiMSIsImNvdW50cnlfY29kZV9pZCI6MSwidXNlcl90eXBlX2lkIjoyLCJpZCI6MTUwMDA0MTQyOSwiZXhwaXJ5IjoxNjM5NzMyNTkyLCJleHAiOjE2Mzk3MzI1OTJ9.MDep5YjiLvCA8LZCijOKTAfwwCjxnFPqVxHis6aOTFHvCRZW6QbXlGsSymBRRvvmZYB9Cmk0xXx9H_Ysa8UlUQ '
        }

        url = f"/ps_generate_ms/v1/learning-bundles?locale=en"
        response = self.client.post(url, data=self.body, headers=headers)
        print(response.text)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
