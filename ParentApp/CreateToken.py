from model.userms_connect import connect
import jwt


def creatEmbibeToken():
    user_id_and_parent_id_data = connect("select user_id, is_child_of from user_relations ur;")

    parent_app_embibe_token = []
    for data in user_id_and_parent_id_data:
        payload = {
            "country": 1,
            "user_type": 1,
            "parent_user_id": data[1],
            "created": 1639481960,
            "organization_id": "1",
            "id": data[0],
            "exp": 1639737249,
            "deviceId": "1639080427620",
            "mobile_verification_status": False,
            "email_verification_status": False,
            "email": "1500288416_822937081103526@embibe-user.com"
        }
        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af5"
                             "89c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")
        parent_app_embibe_token.append(encoded.decode('utf-8'))

    return parent_app_embibe_token


def creatEmbibeTokenWithChildId():
    user_id_and_parent_id_data = connect("select user_id, is_child_of from user_relations ur;")

    parent_app_embibe_token = []
    for data in user_id_and_parent_id_data:
        temp_data = []
        # temp_data[0] = data[0]
        payload = {
            "country": 1,
            "user_type": 1,
            "parent_user_id": data[1],
            "created": 1639481960,
            "organization_id": "1",
            "id": data[0],
            "exp": 1639737249,
            "deviceId": "1639080427620",
            "mobile_verification_status": False,
            "email_verification_status": False,
            "email": "1500288416_822937081103526@embibe-user.com"
        }
        temp_data.append(data[0])
        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af5"
                             "89c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")
        temp_data.append(encoded.decode('utf-8'))
        parent_app_embibe_token.append(temp_data)

    return parent_app_embibe_token


def createParentToken():
    user_id_and_parent_id_data = connect("select user_id, is_child_of from user_relations ur;")

    parent_app_embibe_token = []
    for data in user_id_and_parent_id_data:
        temp_data = []
        payload = {
            "country": 1,
            "user_type": 2,
            "created": 1639559460,
            "organization_id": "1",
            "mobile": "8709111571",
            "id": data[1],
            "exp": 1639732260,
            "deviceId": "5d218638-366d-4e32-93ba-1d59fe033ee0",
            "mobile_verification_status": True,
            "email_verification_status": False
        }
        temp_data.append(data[0])
        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af5"
                             "89c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")
        temp_data.append(encoded.decode('utf-8'))
        temp_data.append(data[1])
        parent_app_embibe_token.append(temp_data)
    return parent_app_embibe_token
