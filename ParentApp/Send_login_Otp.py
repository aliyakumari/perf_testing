from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatEmbibeTokenWithChildId

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

embibe_token = creatEmbibeTokenWithChildId()
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        num = randrange(1000000,9000000)
        rnum = randrange(len(embibe_token))
        self.body = json.dumps({
  "connected_response": False,
  "device_id": "5d218638-366d-4e32-93ba-1d59fe033ee0",
  "mobile": f"870{num}"
})
        headers = {
  'Origin': 'https://parent.staging-fiber-web.embibe.com',
  'embibe-token': '',
  'app_version': 'debug-[4.0.0]',
  'platform': 'Android',
  'os_version': '29',
  'device-model': 'vivo 1806',
  'device-brand': 'vivo',
  'device-name': 'vivo 1806',
  'lang': 'en',
  'Content-Type': 'application/json',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiI1ZDIxODYzOC0zNjZkLTRlMzItOTNiYS0xZDU5ZmUwMzNlZTAiLCJvdHBfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpVeE1pSjkuZXlKamIzVnVkSEo1WDJOdlpHVWlPaUk1TVNJc0ltVjJaVzUwWDNSNWNHVWlPaUpUYVdkdVNXNWZNaUlzSW05eVoxOXBaQ0k2SWpFaUxDSnZkSEJmY21WeGRXVnpkRjlwWkNJNklqaGpZMk0zTmpZeExXRmxZV1V0TkdaaE5DMWlZbVkwTFdKbU9HRXpNakl4TURrNE5pSXNJbWxrSWpvaU9EY3dPVEV4TVRVM01TSXNJbXh2WTJGc1pTSTZJaUo5Lk9aN2dsakV2bkZDbXM5a2ZHTXJBTDBQenZWdVdQQ0FzdXY0WnlENEFGc0RIUmI2NGxsaU1uS3h0ZzQ2WWd6UHIxamIxY2Q4T0I3alp5MDR4QWlCNEhRIiwiY3JlYXRlZCI6MTYzOTczMTUxOSwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjpmYWxzZSwicmVsYXRpb25zaGlwX3R5cGUiOjAsInVzZXJfaWQiOiI4NzA5MTExNTcxIiwib3JnX2lkIjoiMSIsImNvdW50cnlfY29kZV9pZCI6MSwidXNlcl90eXBlX2lkIjoyLCJpZCI6MTUwMDA0MTQyOSwiZXhwaXJ5IjoxNjM5NzMyMTE5LCJleHAiOjE2Mzk3MzIxMTl9.6qgMC_qt7c2hM7Rj2vvbvtjfXl4HiBGWFOUC0OwiXvDm0sUMe-zXJRIPh89aJWsFqy10DavHw788n16TOpoWlQ'
}


        url = "/user_auth_ms/send-login-otp?locale="
        response = self.client.post(url, data=self.body, headers=headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
