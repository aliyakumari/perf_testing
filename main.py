import glob
import os
import time
import sys

folder_name = sys.argv[1]
all_scripts = glob.glob(folder_name + "/*.py")

os.system(f"sudo rm -r {folder_name}/Results")
os.system(f"sudo mkdir {folder_name}/Results")

for script in all_scripts:
    file_name = str(script).replace(folder_name+"/", "").replace(".py", "")
    script_name = str(script).replace(folder_name+"/", "").replace(".py", "")

    command = f'cd {folder_name} && sudo locust -f {script_name} --csv-full-history --csv=Results/{file_name} --headless -u 1000 -r 100 --run-time 2m'
    time.sleep(120)
    print(command)
    os.system(command)
