from json import JSONDecodeError

import requests
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
embibe_tokens = []
goal_exam_codes = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
        }

        self.body = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def content_status_v3(self):
        self.body = json.dumps()
        self.headers['embibe-token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDUzMTM0LCJjcmVhdGVkIjoxNjM2MzcxOTU2LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTgzNTksImV4cCI6MTYzNjU0NDc1NiwiZGV2aWNlSWQiOiIxNjM2MzUyNDgzNTcyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDUzMTM0XzgzMzkxODEyODAzMzk4NDNAZW1iaWJlLXVzZXIuY29tIn0.4TJ4BUNZ4poRkN4j7HWVohhM72G9Y2OQ1QF2zT2JKNKy3xz-sBwKgt1Nw-J7PQW6RZHgUynTbYPuFBjoRZ4N0Q'

        url = f"fiber_ms/virtual-experiment?experiment_id=219&lm_code=lm265146&locale=en"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    global all_commands

    with open("all_commands.txt") as f:
        for line in f:
            (key, val) = line.split()
            all_commands[key] = val

    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
