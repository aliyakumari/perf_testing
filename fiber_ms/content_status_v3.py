from json import JSONDecodeError

import requests
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
embibe_tokens = []
goal_exam_codes = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
        }

        self.body = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def content_status_v3(self):
        self.body = json.dumps({
            "content_id": "4396410",
            "learnpath_format_name": "goal--exam--subject--unit--chapter--topic",
            "content_status": "COMPLETED",
            "child_id": "1500058359",
            "content_type": "Video",
            "concept_id": "",
            "topic_learnpath_name": "cbse--10th cbse--science--biology--life processes--introduction to life processes",
            "format_reference": "5ec5867a0c88fe5860961943",
            "goal": "cbse",
            "exam_name": "10th cbse",
            "learnpath_name": "cbse--10th cbse--science--biology--life processes--introduction to life processes",
            "events": [
                {
                    "timeStamp": 1635851996,
                    "watched_duration": 70.13414764404297
                },
                {
                    "watched_duration": 456.786865234375,
                    "timeStamp": 1635852002
                },
                {
                    "watched_duration": 65.5573501586914,
                    "timeStamp": 1635851991
                },
                {
                    "timeStamp": 1635852024,
                    "watched_duration": 565.6016235351562
                },
                {
                    "timeStamp": 1635852009,
                    "watched_duration": 550.5799560546875
                },
                {
                    "watched_duration": 560.594970703125,
                    "timeStamp": 1635852019
                },
                {
                    "watched_duration": 26.137643814086914,
                    "timeStamp": 1635851976
                },
                {
                    "watched_duration": 427.8852233886719,
                    "timeStamp": 1635851981
                },
                {
                    "watched_duration": 16.118810653686523,
                    "timeStamp": 1635851970
                },
                {
                    "timeStamp": 1635852004,
                    "watched_duration": 546.3114624023438
                },
                {
                    "timeStamp": 1635851988,
                    "watched_duration": 406.7377014160156
                },
                {
                    "watched_duration": 31.14617347717285,
                    "timeStamp": 1635851981
                },
                {
                    "timeStamp": 1635852054,
                    "watched_duration": 595.6488647460938
                },
                {
                    "watched_duration": 570.6087036132812,
                    "timeStamp": 1635852029
                },
                {
                    "timeStamp": 1635851997,
                    "watched_duration": 238.2622833251953
                },
                {
                    "timeStamp": 1635852034,
                    "watched_duration": 575.6163940429688
                },
                {
                    "watched_duration": 590.6409301757812,
                    "timeStamp": 1635852049
                },
                {
                    "watched_duration": 600.6583862304688,
                    "timeStamp": 1635852059
                },
                {
                    "timeStamp": 1635851984,
                    "watched_duration": 159.31146240234375
                },
                {
                    "timeStamp": 1635851999,
                    "watched_duration": 405.327880859375
                },
                {
                    "timeStamp": 1635851989,
                    "watched_duration": 144.50819396972656
                },
                {
                    "timeStamp": 1635852039,
                    "watched_duration": 580.6243286132812
                },
                {
                    "watched_duration": 0.0,
                    "timeStamp": 0
                },
                {
                    "timeStamp": 1635852014,
                    "watched_duration": 555.587890625
                },
                {
                    "timeStamp": 1635852044,
                    "watched_duration": 585.6326293945312
                },
                {
                    "timeStamp": 1635851986,
                    "watched_duration": 277.7377014160156
                }
            ],
            "context": "",
            "book_id": "",
            "watched_duration": 608.0,
            "source": "ios",
            "length": 602.0
        })
        self.headers['embibe-token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDUzMTM0LCJjcmVhdGVkIjoxNjM2MzcxOTU2LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTgzNTksImV4cCI6MTYzNjU0NDc1NiwiZGV2aWNlSWQiOiIxNjM2MzUyNDgzNTcyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDUzMTM0XzgzMzkxODEyODAzMzk4NDNAZW1iaWJlLXVzZXIuY29tIn0.4TJ4BUNZ4poRkN4j7HWVohhM72G9Y2OQ1QF2zT2JKNKy3xz-sBwKgt1Nw-J7PQW6RZHgUynTbYPuFBjoRZ4N0Q'

        url = f"/fiber_ms/v3/content-status"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    global all_commands

    with open("all_commands.txt") as f:
        for line in f:
            (key, val) = line.split()
            all_commands[key] = val

    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
