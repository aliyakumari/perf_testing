import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - TOKEN.csv')
token = df.values.tolist()
token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {
            "exam_name": "21st Century Teacher Training",
            "goal": "Teacher Training and Certification",
            "content_section_type": "SKILLS_10",
            "offset": 1,
            "size": 10,
            "locale": "en"
        }
        self.headers = {
            'authority': 'preprodms.embibe.com',
            'accept': 'application/json',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'content-type': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDIyNDU0LCJjcmVhdGVkIjoxNjgyNTc5MjY0LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwMjI0NTUsImV4cCI6MTY4MjY2NTY2NCwiZGV2aWNlSWQiOiIxNjgxNzI0MDU3Mjc0IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDIyNDU0XzIyNjMzNzM0Mjk4MTQyNDg3QGVtYmliZS11c2VyLmNvbSIsInBsYXRmb3JtIjoiV0VCIn0.tNgO1ONqlbxGh4s2OMIVIfemyrGmbiY-sB38AJmJPfajObSQJt-n9VcO2vibKFYdS6XQhaDlrEmRPADNgV6TuA',
            'origin': 'https://staging-fiber-web.embibe.com',
            'referer': 'https://staging-fiber-web.embibe.com/learn/home',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'Cookie': '_test_app_session=df77db44af56109e833c62238a224a17; preprod_embibe-refresh-token=9abed2c8-5ab6-4286-a76d-f38167da4afe'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['embibe-token'] = token[rnum][1]
        url = f"/fiber_ms/v1/home/sections"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
