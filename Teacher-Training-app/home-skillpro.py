import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - TOKEN.csv')
token = df.values.tolist()
token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
            'authority': 'preprodms.embibe.com',
            'accept': 'application/json',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDIyNDU0LCJjcmVhdGVkIjoxNjgyNTc4NDI1LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwMjI0NTUsImV4cCI6MTY4MjY2NDgyNSwiZGV2aWNlSWQiOiIxNjgxNzI0MDU3Mjc0IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDIyNDU0XzIyNjMzNzM0Mjk4MTQyNDg3QGVtYmliZS11c2VyLmNvbSIsInBsYXRmb3JtIjoiV0VCIn0.-WFHr9IRUfd1kcTsn37ikeC5ilXJtrqaB6kCy7hORIUU1qxQQa1nWOm3yeChdwuPtRGP1mfNeUKdN99Rgmemjg',
            'origin': 'https://preprodms.embibe.com/',
            'referer': 'https://preprodms.embibe.com/',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
            }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        self.headers['embibe-token'] = token[rnum][1]
        url = f"/fiber_ms/v1/home/skillpro?exam_code=kve15997203&exam_name=21st%20Century%20Teacher%20Training%26goal_name%3DTeacher%20Training%20and%20Certification%26goal_code%3Dkve457846%26locale%3Den%26channel%3Dweb%26subject_name%3D21st%20century%20skills"
        response = self.client.get(url, data=self.body, headers=self.headers)
        print(response.text)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
