from json import JSONDecodeError

from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

data = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.data = []
        with open('testTilesData.csv', 'r') as csvfile:
            self.data = list(csv.reader(csvfile, delimiter=','))

        self.body = {}
        self.rnum = None
        self.headers = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        try:
            self.rnum = randrange(len(self.data))
            if self.rnum == 0:
                self.rnum = randrange(len(self.data))

            self.headers = {
                'accept': '*/*',
                'embibe-token': self.data[self.rnum][3],
                'Cookie': 'JSESSIONID=44CCA15DE692E42E8BBCFA537455F4A3; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
            }
        except:
            pass

        url = f"/achieve_ms/v3/achieve/test/tiles?session_id={self.data[self.rnum][4]}"
        with self.client.get(url, data=self.body, headers=self.headers,
                              name="test_generate", catch_response=True) as response:
            try:
                if response.status_code != 200:
                    response.failure(f"response code {response.status_code}")

                if response.status_code == 200:
                    data = response.json()
                    tests = response.json().get("data", [])
                    if len(tests) == 0:
                        response.failure(f"Zero test codes. {self.data[self.rnum][4]}")

            except JSONDecodeError:
                response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
