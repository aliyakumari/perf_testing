from json import JSONDecodeError

import requests
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
embibe_tokens = []
goal_exam_codes = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
        }

        self.body = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def dt_generate_check(self):
        # goalCode, examCode = random.choice(goal_exam_codes)
        self.headers['embibe-token'] = random.choice(embibe_tokens)
        # chapter_codes = self.fetch_chapter_codes(goalCode, examCode)

        # if len(chapter_codes) > 0:
        self.body = json.dumps({
            "codes": [
                "kve97670--kve99731--kve99732--kve102904--kve102914",
                "kve97670--kve99731--kve99732--kve99733--kve102869",
                "kve97670--kve99731--kve99732--kve99733--kve102895",
                "kve97670--kve99731--kve99742--kve103072--kve103118",
                "kve97670--kve99731--kve99742--kve99743--kve103051"
            ],
            "examCode": "kve99731",
            "goalCode": "kve97670",
            "locale": "en"
        })

        with self.client.post("/achieve_ms/v3/achieve/test/generate", data=self.body, headers=self.headers,
                              name="test_generate", catch_response=True) as response:
            try:
                if response.status_code == 409:
                    response.failure(f"Duplicate Request")
                elif response.status_code != 200:
                    response.failure(f"response code {response.status_code}")

                if response.status_code == 200:
                    data = response.json()
                    session_id = response.json().get("data", {}).get("session_id")

                    if session_id is None:
                        response.failure("Null session Id")
                    else:
                        dt_status = self.check_session_status(session_id)
                        if dt_status:
                            response.success()
                        else:
                            response.failure("Test not generated")
            except JSONDecodeError:
                response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")

    def check_session_status(self, session_id):
        for i in range(100):
            status_api_curl = f"{self.host}/achieve_ms/v3/achieve/paj/status?session_id={session_id}"
            with self.client.get(status_api_curl, headers=self.headers,
                                 name="achieve_paj_status", catch_response=True) as response:
                try:
                    if response.status_code == 200 and response.json().get("data") == "Test Generated":
                        response.success()
                        return True
                    elif response.status_code != 200:
                        response.failure(f"response code {response.status_code}")
                        return False

                    if i == 99:
                        response.failure(f"Test status: {response.json().get('data')}")
                        return False
                    else:
                        time.sleep(.5)
                        continue

                except JSONDecodeError:
                    response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")
                    return False

        return True

    def fetch_chapter_codes(self, goal_code, exam_code):
        with self.client.get(f"/achieve_ms/v3/achieve/form/subjects?exam_code={exam_code}&goal_code={goal_code}&"
                             f"level=chapter&locale=en", headers=self.headers, name="form_subjects API",
                             catch_response=True) as response:
            try:
                if response.status_code != 200:
                    response.failure(f"response code {response.status_code} Exam: {exam_code} Goal: {goal_code}")
                    return []

                data = response.json()
                all_chapters = []
                for subject in data.get("data", []):
                    for chapter in subject.get("chapter_data", []):
                        all_chapters.append(chapter.get("code"))

                if len(all_chapters) > 0:
                    response.success()
                else:
                    print(response.text)
                    print(self.headers['embibe_token'])
                    response.failure(f"No subject-chapter/s found for the goal - {goal_code}, exam: {exam_code}")

                return all_chapters

            except JSONDecodeError:
                response.failure(f"Response could not be decoded as JSON. Status Code: {response.status_code}, "
                                 f"Exam: {exam_code} Goal Code: {goal_code}")

            return []


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    global embibe_tokens
    global all_commands
    global goal_exam_codes

    df = pd.read_csv("AchieveUserIds.csv")
    embibe_tokens = df["embibe_token"].to_list()

    with open("all_commands.txt") as f:
        for line in f:
            (key, val) = line.split()
            all_commands[key] = val

    all_commands["limit_fail_ratio"] = 0.05

    df = pd.read_csv("goal_exam_codes_prod.csv")
    goal_exam_codes = [(val.get("Goal code"), val.get("Exam Code")) for ind, val in df.iterrows()]

    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
