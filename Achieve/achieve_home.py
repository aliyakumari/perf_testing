from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

data = []
with open('testTilesData.csv', 'r') as csvfile:
    data = list(csv.reader(csvfile, delimiter=','))


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.body = None
        self.headers = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(data))

        self.body = json.dumps({
            "exam_code": data[rnum][1],
            "goal_code": data[rnum][2]
        })
        self.headers = {
            'embibe-token': data[rnum][3],
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=F012F4BFE43BE8A03BE10FF0934FB5E9; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod-ab_version=6; preprod-access-token=access-token-value; preprod-client=client-token-value; preprod-embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIxLTEyLTEzIDA5OjMzOjIwIFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwib3JnYW5pemF0aW9uX2lkIjo5NjEsImlkIjozMDI1MzQ3NCwiZW1haWwiOiJhbGl5YS5rdW1hcmlAZW1iaWJlLmNvbV9udGEifQ.4fFUINkS6ATW6Mhky0cvo9DGIWItqMTjcmc3u0kRv_k0TSnGRe7ZIBMi2cx4NtCBAJ-oWUVYZUo817sjaxKPdg; preprod-uid=aliya.kumari@embibe.com_nta; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }
        print(self.headers)
        print(self.body)

        response = self.client.post("/achieve_ms/v3/achieve/home", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) + 0.04 or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
