import datetime
import traceback
from json import JSONDecodeError

import requests
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
goal_exam_code_paj_ids_embibe_tokens = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'Cookie': 'JSESSIONID=9010094A2130FF3B658B709B0A283111'
        }

        self.body = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def current_status(self):
        goalCode, examCode, pajId, embibe_token = random.choice(goal_exam_code_paj_ids_embibe_tokens)
        self.headers['embibe-token'] = embibe_token
        url = f"/achieve_ms/v3/achieve/paj/get_next_activity?step_id={pajId}%7C0"

        with self.client.get(url, headers=self.headers, name="current_activity", catch_response=True) as response:
            try:
                if response.status_code == 204:
                    response.success()

                if response.status_code != 200:
                    response.failure(f"response code {response.status_code}")
                    print(self.headers)
                    print(goalCode, examCode, pajId)
                    # print(response.text)
                    # print()
                else:
                    response.success()

            except JSONDecodeError:
                response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    global all_commands
    global goal_exam_code_paj_ids_embibe_tokens

    df = pd.read_csv("current_next_activity_data.csv")

    with open("all_commands.txt") as f:
        for line in f:
            (key, val) = line.split()
            all_commands[key] = val

    all_commands["limit_fail_ratio"] = 0.05

    goal_exam_code_paj_ids_embibe_tokens = [
        (val.get("goalCode"), val.get("examCode"), val.get("pajId"), val.get("embibe_token")) for ind, val in
        df.iterrows()]

    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
