from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

data = []
with open('ExploreMasteryData.csv', 'r') as csvfile:
    data = list(csv.reader(csvfile, delimiter=','))


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        try:
            self.rnum = randrange(len(data))
            if self.rnum == 0:
                self.rnum = randrange(len(data))
            self.headers = {
                'Connection': 'keep-alive',
                'Accept': 'application/json',
                'embibe-token': data[self.rnum][3],
                'Cookie': 'JSESSIONID=150030227E52C55034A1DE871650D944; V_ID=ultimate.2020-10-27.60527546dd78c763621cd1a3f61e4242'
            }
        except:
            pass
        url = f"/achieve_ms/v3/exploreMastery/topics?exam_code={data[self.rnum][1]}&goal_code={data[self.rnum][2]}&subject_code={data[self.rnum][5]}&chapter_code={data[self.rnum][6]}"
        response = self.client.get(url, data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
