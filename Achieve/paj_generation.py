import datetime
import traceback
from json import JSONDecodeError

import requests
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
goal_exam_code_paj_ids_embibe_tokens = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'Cookie': 'JSESSIONID=9010094A2130FF3B658B709B0A283111'
        }

        self.body = None

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def dt_generate_check(self):
        goalCode, examCode, sessionId, embibe_token = random.choice(goal_exam_code_paj_ids_embibe_tokens)
        self.headers['embibe-token'] = embibe_token
        self.generate_paj(goalCode, examCode, sessionId)

    def check_session_status(self, session_id):
        for i in range(100):
            status_api_curl = f"{self.host}/achieve_ms/v3/achieve/paj/status?session_id={session_id}"
            with self.client.get(status_api_curl, headers=self.headers,
                                 name="achieve_paj_status", catch_response=True) as response:
                try:
                    if response.status_code == 200 and response.json().get("data") == "PAJ Generated":
                        response.success()
                        return True
                    elif response.status_code != 200:
                        response.failure(f"response code {response.status_code}")
                        return False

                    if i == 99:
                        response.failure(f"PAJ status: {response.json().get('data')}")
                        return False
                    else:
                        time.sleep(.5)
                        continue

                except JSONDecodeError:
                    response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")
                    return False

        return True

    def generate_paj(self, goalCode, examCode, sessionId):
        try:
            paj_type = randint(0, 4)
            paj_name = f"{sessionId}__{paj_type}"
            if paj_type == 0:
                time_duration_per_day = randint(15, 60 * 2)
                total_days = randint(1, 10)
            elif paj_type == 1:
                time_duration_per_day = randint(15, 60 * 1)
                total_days = randint(150, 220)
            elif paj_type == 2:
                time_duration_per_day = randint(60 * 10, 60 * 20)
                total_days = randint(1, 5)
            elif paj_type == 3:
                time_duration_per_day = randint(15, 60 * 4)
                total_days = randint(1, 150)
            else:
                time_duration_per_day = randint(60 * 10, 60 * 20)
                total_days = 1

            time_duration_per_day = time_duration_per_day - (time_duration_per_day % 15)
            last_day = datetime.datetime.now() + datetime.timedelta(seconds=total_days * 24 * 3600)
            last_day = last_day.strftime("%d-%m-%Y")
        except Exception:
            print(traceback.print_exc())

        url = "/achieve_ms/v3/achieve/paj/generate"
        payload = {
            "question_response": [
                {
                    "question_type": "single_select",
                    "question_id": 1,
                    "value": "1"
                },
                {
                    "question_type": "spinner",
                    "question_id": 2,
                    "value": int(time_duration_per_day)
                },
                {
                    "question_type": "dd-mm-yyyy",
                    "question_id": 3,
                    "value": str(last_day)
                },
                {
                    "question_type": "text",
                    "question_id": 4,
                    "value": paj_name
                }
            ],
            "exam_code": examCode,
            "goal_code": goalCode,
            "session_id": sessionId
        }

        with self.client.post(url, headers=self.headers, data=json.dumps(payload), name="Paj Generate",
                              catch_response=True) as response:
            try:
                if response.status_code == 500:
                    try:
                        if response.json().get("data") == "PAJ already Generated":
                            response.failure("Duplicate Request")
                    except Exception:
                        response.failure(f"response code {response.status_code}")

                if response.status_code != 200:
                    response.failure(f"response code {response.status_code}")
                    return

                if response.status_code != 200:
                    print(self.headers)
                    print(goalCode, examCode, sessionId)
                    # print(response.text)
                    # print()
                if response.status_code == 200:
                    data = response.json()
                    paj_id = response.json().get("data", {}).get("paj_id")

                    if paj_id is None:
                        response.failure("Null PAJ Id")
                    else:
                        paj_status = self.check_session_status(sessionId)
                        if paj_status:
                            response.success()
                        else:
                            response.failure("PAJ not generated")
            except JSONDecodeError:
                response.failure(f"Response could not be decoded as JSON. response code {response.status_code}")


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    global all_commands
    global goal_exam_code_paj_ids_embibe_tokens

    df = pd.read_csv("pajgenerate.csv")

    with open("all_commands.txt") as f:
        for line in f:
            (key, val) = line.split()
            all_commands[key] = val

    all_commands["limit_fail_ratio"] = 0.05

    goal_exam_code_session_ids_embibe_tokens = [
        (val.get("goalCode"), val.get("examCode"), val.get("sessionId"), val.get("embibe_token")) for ind, val in
        df.iterrows()]

    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
