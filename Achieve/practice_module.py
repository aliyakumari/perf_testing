from json import JSONDecodeError

from locust import task, between, HttpUser, events

from common.common_utils import get_random_parameter
from common.data_utils import fetch_goals_and_exams
from common.validations import health_check

GOAL_AND_EXAM_LIST = []


class PracticeDetailTasks(HttpUser):
    wait_time = between(5, 7)

    @task
    def practice_detail_flow(self):
        # goal = get_random_parameter(GOAL_AND_EXAM_LIST)
        # kv_code = goal["code"]
        kv_code = "kve97670"
        practice_detail_list = self.get_practice_details_by_kv_code(kv_code)
        if len(practice_detail_list) == 0:
            return
        practice_detail = get_random_parameter(practice_detail_list)
        cg_object_id = practice_detail["cgObjectId"]
        self.get_practice_detail_by_cg_object_id(cg_object_id)

    def get_practice_details_by_kv_code(self, kv_code):
        with self.client.get(f"/csp/practice_details?parent_kv_code={kv_code}", name="getPracticeDetailsByKVCode",
                             catch_response=True) as response:
            try:
                practice_detail_list = response.json()
                if len(practice_detail_list) >= 0:
                    response.success()
                else:
                    response.failure("No Practice detail found for the kv_code - " + kv_code)
                return practice_detail_list
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
            return []

    def get_practice_detail_by_cg_object_id(self, cg_object_id):
        with self.client.get(f"/csp/practice_details?cg_object_id={cg_object_id}", name="getPracticeDetails",
                             catch_response=True) as response:
            try:
                if response.json()["cgObjectId"] == cg_object_id:
                    response.success()
                else:
                    response.failure("Incorrect response returned")
            except JSONDecodeError:
                response.failure("Response could not be decoded as JSON")
            except KeyError:
                response.failure("Response did not contain the key cg_object_id")


@events.test_start.add_listener
def on_test_start(environment, **kwargs):
    global GOAL_AND_EXAM_LIST
    health_check_status = health_check()
    GOAL_AND_EXAM_LIST = fetch_goals_and_exams()
    if health_check_status is False or len(GOAL_AND_EXAM_LIST) == 0:
        exit()
