import glob
import os
import time
import sys
import datetime
from datetime import timedelta

folder_name = sys.argv[1]
all_scripts = glob.glob(folder_name + "/*.py")

# os.system(f"sudo rm -r {folder_name}/Results")
os.system(f"sudo mkdir {folder_name}/Results")
c = 0
print(len(all_scripts))
exe_time_str = sys.argv[2]
type = sys.argv[3]
executionStartTime = datetime.datetime.fromisoformat(exe_time_str)

for script in all_scripts:
    if script not in ["SendLoginOtp.py"] :
        c = c + 1
        if c > 10:
            break
        # ------Provide time in this format datetime.datetime.fromisoformat('2023-02-03T03:13:00-00:00').strftime('%s')--------

        file_name = str(script).replace(folder_name, "").replace(".py", "")
        script_name = str(script).replace(folder_name, "")
        print(f"##-------------Running for Script {script}------------------##")
        while 1:
            now = datetime.datetime.now(datetime.timezone.utc)
            print(
                f"current time = {now}, Execution start time = {executionStartTime}")
            if now >= executionStartTime:
                # try:
                if type.lower() == "master":
                    command = f'cd {folder_name} && sudo locust -f {script_name} --csv-full-history --csv=Results/{file_name} --headless -u 1000 -r 100 --run-time 10m --master --expect-workers 5'
                    print(f"Executing command: {command}")
                    os.system(command)
                if type.lower() == "worker":
                    time.sleep(2)
                    command = f"cd {folder_name} && sudo locust -f {script_name} --csv-full-history --csv=Results/{file_name} --headless -u 1000 -r 100 --worker --master-host='10.26.2.25' "
                    print(f"Executing command: {command}")
                    os.system(command)
                # except:
                #     print(f"##-------------Script Fail for {script}------------------##")

                break

            time.sleep(1)

    print("Updating excutionStartTime, updated time: ")
    executionStartTime += timedelta(minutes=10, seconds=30)
    print(executionStartTime)
