import random
from locust import HttpUser, task, between, User, events
import mysql.connector
import time
import logging


class SqlQueryUser(User):
    wait_time = between(1, 5)
    query_count = 0
    total_response_time = 0
    total_results = 0

    @task
    def sql_query(self):
        
        start = time.perf_counter()
        exec_ = None
        response_length = 0
        response = None
        try:
            # Connect to the database
            conn =  mysql.connector.connect(
                database='wordpress-madhyapradesh',
                user='devops@blog-microsite-langauge-lp-preprod-01',
                password='hbs799H@rekr1shnahsnU',
                host= 'blog-microsite-langauge-lp-preprod-01.mysql.database.azure.com',
                ssl_ca='/home/vmadmin/perf_testing/Microsites/sql_queries/BaltimoreCyberTrustRoot.crt.pem'           
            )

            # Execute the SQL query
            cursor = conn.cursor()
            start_time = time.time()
            queries = [
                "SELECT  t.term_id FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('language') ORDER BY t.name ASC",
                "SELECT t.*, tt.* FROM wp_terms AS t INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE t.term_id IN (3,4)",
                "SELECT term_id, meta_key, meta_value FROM wp_termmeta WHERE term_id IN (3,4) ORDER BY meta_id ASC",
                "SELECT   wp_posts.ID FROM wp_posts  LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 1=1  AND ( wp_term_relationships.term_taxonomy_id IN (5) ) AND wp_posts.post_type = 'wp_global_styles' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 1",
                "SELECT DISTINCT t.term_id, tr.object_id FROM wp_terms AS t  INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN ('wp_theme') AND tr.object_id IN (560) ORDER BY t.name ASC",
                "SELECT   wp_posts.ID FROM wp_posts WHERE 1=1  AND wp_posts.post_parent = 1082  AND wp_posts.post_type = 'revision' AND ((wp_posts.post_status = 'inherit')) ORDER BY wp_posts.post_date ASC, wp_posts.ID ASC LIMIT 0, 1"
            ]
            
            print("Stage 1")
            cursor.execute(queries[0])
            rows = cursor.fetchall()
            response_length = len(str(rows))
            end_time = time.time()

            print("Stage 2")
            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)
            cursor.execute(queries[1])
            rows = cursor.fetchall()
            response_length += len(str(rows))
            end_time = time.time()
            
            print("Stage 3")
            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)
            cursor.execute(queries[2])
            rows = cursor.fetchall()
            response_length += len(str(rows))
            
            print("Stage 4")
            end_time = time.time()
            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)
            cursor.execute(queries[3])
            rows = cursor.fetchall()
            response_length += len(str(rows))
            
            print("Stage 5")
            end_time = time.time()
            # Update the query count, total response time, and total results
            # self.query_count += 1
            # self.total_response_time += (end_time - start_time)
            # self.total_results += len(rows)
            # cursor.execute(queries[4])
            # rows = cursor.fetchall()
            # response_length += len(str(rows))
            
            print("Stage 6")
            end_time = time.time()
            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)
            cursor.execute(queries[5])
            rows = cursor.fetchall()
            response_length += len(str(rows))
            
            print("Stage 7")
            end_time = time.time()
            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)
            response_length += len(str(rows))
            
            
            print("Stage 8")
            end_time = time.time()

            # Update the query count, total response time, and total results
            self.query_count += 1
            self.total_response_time += (end_time - start_time)
            self.total_results += len(rows)

            # Close the cursor and connection
            cursor.close()
            conn.close()
            total_time = self.query_count * 5

        except Exception as e:
            print("Unable to execute SQL query")
            print(e)
            exec_ = e
        
        # Firing events
        print("Firing events")
        request_time = (time.perf_counter() - start) * 1000

        event_kwargs = {"request_type": "POST", "name": "testName",
                        "response_time": request_time, "response_length": response_length,
                        "exception": exec_, "context": None}

        if not exec_:
            print("Success")
            events.request.fire(**event_kwargs)
            return

        # if request_time > 5000:
        #     event_kwargs["exception"] = ">5000ms response time"
        #     events.request.fire(**event_kwargs)
        #     return

        # if response.status_code == 200:
        #     events.request.fire(**event_kwargs)
        else:
            print("Faliure")
            event_kwargs["exception"] = exec_
            events.request.fire(**event_kwargs)
        
        print(event_kwargs)

    def on_stop(self):
        # Calculate the total time taken to execute all queries
        total_time = self.query_count * 5

        # Calculate the number of results per second and the average response time per result
        results_per_second = self.total_results / total_time if total_time > 0 else 0
        avg_response_time_per_result = self.total_response_time / self.total_results if self.total_results > 0 else 0

        # Print the results
        logging.info(f"Executed {self.query_count} queries with a total of {self.total_results} results")
        logging.info(f"Results per second: {results_per_second}")
        logging.info(f"Average response time per result: {avg_response_time_per_result} seconds")
        
 