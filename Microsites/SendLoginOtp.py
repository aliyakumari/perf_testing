from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

df = pd.read_csv("org_token.csv")
org_token_and_email = df.values.tolist()

class UserBehaviour(FastHttpUser):

    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/user_auth_ms/send-login-otp?locale=en", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'org-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2NDI4ZThiY2Y1YzNjOTI0NjZjZDY4ZTIiLCJzdWJkb21haW4iOiJodHRwczovLzY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.hvS1543GxbOeZaWqqqMa97LKawUknylqYENy54Yli9hPKEw8PnypPm4rJ5ZAz0xsHKYBVglkIORAH1Ejr-xvfQ',
            'Content-Type': 'text/plain',
            'Cookie': 'org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2NDI4ZThiY2Y1YzNjOTI0NjZjZDY4ZTIiLCJzdWJkb21haW4iOiJodHRwczovLzY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.hvS1543GxbOeZaWqqqMa97LKawUknylqYENy54Yli9hPKEw8PnypPm4rJ5ZAz0xsHKYBVglkIORAH1Ejr-xvfQ; preprod_ab_version=0; preprod_embibe-refresh-token=ff96719e-b167-4243-91be-3ddb5c1ce727; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2ODEzNzkxMDcsIm9yZ2FuaXphdGlvbl9pZCI6IjY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMiIsIm1vYmlsZSI6IjgyMTA5NTA5MjYiLCJpZCI6MTUwNDk2OTUwMiwiZXhwIjoxNjgxNDY1NTA3LCJkZXZpY2VJZCI6IjE2NjEyMzcwODY2MTEiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoiYWxpeWEua3VtYXJpQGVtYmliZS5jb20iLCJwbGF0Zm9ybSI6IldFQiJ9.IGXdunCLuae2vTieLnhOii8DN7mBzEaLDF00CP38TRWEM_UuBMTonqp-Y4lMKvsiTf1peawkNT3XOjgaXDI0Bg; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjYxMjM3MDg2NjExIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJMk5ESTRaVGhpWTJZMVl6TmpPVEkwTmpaalpEWTRaVElpTENKMGNtRmphMTlwWkNJNklqUmxNMlExTVdWaUxUSmtNVGN0TkRVNE9DMWhObUppTFRFMllqTmpNMlkyTVRWbU5DSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2laalZpT0RVeU0yRXRPR00wWXkwME1ETTVMVGhtWVRVdFpUZGlORFpoTlRkbVltTm1JaXdpYVdRaU9pSmhiR2w1WVM1cmRXMWhjbWxBWlcxaWFXSmxMbU52YlNJc0lteHZZMkZzWlNJNkltVnVJaXdpY0d4aGRHWnZjbTBpT2lKWFJVSWlmUS5GZ2lZektldW16ZTF2SXNzbUlLZ2hKa1huOUlhTWtQTzYwQWc5cFoxUjZhX0dTeW9hbmNnaTl4LUZNVTZpUDVnLTVfM1pqVjYwdUlWR3BVTy1wb2lrdyIsImNyZWF0ZWQiOjE2ODEzNzkwMzEsIm90cF9vcGVyYXRpb25fdHlwZSI6IlNpZ25JbiIsImF0dGVtcHQiOjEsInR0bCI6IjYwMCIsInBsYXRmb3JtIjoiV0VCIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6ImFsaXlhLmt1bWFyaUBlbWJpYmUuY29tIiwib3JnX2lkIjoiNjQyOGU4YmNmNWMzYzkyNDY2Y2Q2OGUyIiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjQsImlkIjoxNTA0OTY5NTAyLCJleHBpcnkiOjE2ODE0NjU0MzEsImpvdXJuZXlfaWQiOiJTSUdOX0lOXzc3NTY0N2RiLWIxZGItNDczYy04YTM4LTIwMmU5ZTk1Y2MwZiIsImV4cCI6MTY4MTQ2NTQzMX0.s5V1VMNukT57ETSm19654cYljWNa5p8b32ISEkJrUctOz4aBdxTkj8MgXJpeQLNbXWGob5tlRYm1qspRqyRvOA; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlc3QxIiwidGltZV9zdGFtcCI6IjIwMjMtMDQtMTMgMDk6NDU6NTMgVVRDIiwibW9iaWxlIjoiODIxMDk1MDkyNiIsInJvb3RPcmdJZCI6IjY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMiIsImRldmljZUlkIjoiMTY2MTIzNzA4NjYxMSIsImZpcnN0TmFtZSI6IlNhbmthciIsIm9yZ1R5cGUiOiJDaGFpbk9mU2Nob29scyIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2NDJkMTcxZDgyYzJhMDQ3MjQwZjk0YzYiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2NDM3Y2ViM2NkMGE4NDcxNzI1MjcxZWUiLCJpZCI6MTUwNDk2OTUwMiwiZW1haWwiOiJhbGl5YS5rdW1hcmlAZW1iaWJlLmNvbSJ9.B2UGp9YGKdZ26c98IUaD9OCVebz-hnxC6yhbSNw3iZgkZksIJeVF05MVaW9QuGi_rLQzA5-U5TFhGJPlcndQqg'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(org_token_and_email))
        self.headers['org-token'] = org_token_and_email[rnum][2]
        self.body = {
            "device_id": "1661237086611",
            "email": org_token_and_email[rnum][0],
            "connected_response": True,
            "country_code_id": 1
        }
        meta_data = []
        url = f"/user_auth_ms/send-login-otp?locale=en"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)

        org_token = None
        if response.status_code == 200:
            meta_data.append(org_token_and_email[rnum][0])
            meta_data.append(org_token_and_email[rnum][1])
            org_token = response.headers['otp-token']
            meta_data.append(org_token)
            data.append(meta_data)
            df = pd.DataFrame(data)
    
            df.to_csv('otp_token.csv', index=False)
        


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
