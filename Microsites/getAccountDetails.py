from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

with open('reseller_token.json', 'r') as f:
    token = json.load(f)

print(len(token))

class UserBehaviour(FastHttpUser):

    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/narad/microsite/getOptionsForField/boards?", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRva2VuIiwidGltZV9zdGFtcCI6IjIwMjMtMDQtMTcgMDc6NTU6NTggVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODQ3NjcwOTg3OCIsInJvb3RPcmdJZCI6IjY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMiIsImRldmljZUlkIjoiMC45OTg2MjQxMDYwNjkwNzUzIiwiZmlyc3ROYW1lIjoiRHVtbXkiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQzMmE4NTQ5OTNiZjE2NWQ1NmY2YWNmIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQzY2YxNDgxNmYwYmUyY2NiOTFiODRhIiwiaWQiOiIxNTA0OTc3MjQwIiwiZW1haWwiOiJqaXJ5d2lsaUBhZmlhLnBybyJ9.HOstQl2joGw02NuFg2E6pHtbgSiNKwChfLAGU2g87njNVZgXeanusVlEvk2D92HDFVg2fvDWKGiuYM12Tic59A',
  'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'Cookie': 'preprod_embibe-refresh-token=17293aef-5d6e-4405-9f9b-2b2dca641d8a; school_preprod_embibe-refresh-token=fffde069-dcd9-49a9-8750-580a136557df; JSESSIONID=0B014243964F5C75F243BFB8153DA71C; preprod_embibe-refresh-token=9abed2c8-5ab6-4286-a76d-f38167da4afe; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRva2VuIiwidGltZV9zdGFtcCI6IjIwMjMtMDQtMTcgMDk6MjQ6MTUgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODQ3NjcwOTg3OCIsInJvb3RPcmdJZCI6IjY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMiIsImRldmljZUlkIjoiMC45OTg2MjQxMDYwNjkwNzUzIiwiZmlyc3ROYW1lIjoiRHVtbXkiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQzMmE4NTQ5OTNiZjE2NWQ1NmY2YWNmIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQzY2YxNDgxNmYwYmUyY2NiOTFiODRhIiwiZW1haWwiOiJqaXJ5d2lsaUBhZmlhLnBybyJ9.oZ0hMbizZ1Akfc_1u2NO3MH8pjbCQH_NtTHyMijpN0ChSpt6SXxqPvefAyp-Cw_28e6_FzHldfcLfnAIsjsv4Q'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum_token = randrange(len(token))
        self.headers['reseller-jwt-token'] = token[rnum_token]
        url = f"/narad/v1/udise/principal"
        
        response = self.client.get(
            url, data=json.dumps(self.body), headers=self.headers)
        
        if response.status_code != 200 :
            print(self.headers)
        


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
