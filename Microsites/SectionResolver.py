from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []


with open('embibeClasses.json', 'r') as f:
    data_val = json.load(f)

data_val = data_val[0]

df = pd.read_csv("udisecodes.csv")
State_data = df.values.tolist()
State_data.pop()


with open('reseller_token.json', 'r') as f:
    token = json.load(f)


for key, value in data_val["micrositeMapping"].items():
    meta_data = []
    meta_data.append(key)
    data.append(meta_data)


class UserBehaviour(FastHttpUser):

    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/narad/microsite/getOptionsForField/boards?", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': '*/*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Origin': 'https://stage.embibe.com',
            'Referer': 'https://stage.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'content-type': 'application/json',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlc3QiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wNC0wOSAxNzoxMTo0MSBVVEMiLCJtb2JpbGUiOiI5NjI5MzY3ODcwIiwicm9vdE9yZ0lkIjoiNjQyOGU4YmNmNWMzYzkyNDY2Y2Q2OGUyIiwiZmlyc3ROYW1lIjoiU2Fua2FyIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidXNlcl90eXBlIjo0LCJwYXJlbnRPcmdJZCI6IjY0MmQxNTYwZGJlZjUxMmYwMDg4MWZmYyIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjY0MzJkNjcyZWIyMWU0MjM4MTUyYjk0NyIsImlkIjoxNTA0OTY3Njc5LCJlbWFpbCI6InNhbmthci52QGVtYmliZS5jb20ifQ.73S_oLMtnK51oBzAwHQf-cphZCThbad5LThQIDSSfBUD9V7CbEga2ZppSK2ilySoy1N2exzsgSLbRnHpqWlLZw',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

        self.body = json.dumps({
            "resolver": "SECTION"
        })

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum_token = randrange(len(token))
        self.headers['reseller-jwt-token'] = token[rnum_token]
        num = randrange(len(State_data))
        rnum = randrange(len(data))
        url = f"/narad/microsite/getOptionsForField?kveCode={data[rnum][0]}2388&udiseCode={State_data[num][1]}"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
