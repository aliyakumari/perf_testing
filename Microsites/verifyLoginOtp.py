from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

df = pd.read_csv("otp_token.csv")
otp_token_and_email = df.values.tolist()


class UserBehaviour(FastHttpUser):

    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/user_auth_ms/verify-login-otp", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'otp-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjYxMjM3MDg2NjExIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJMk5ESTRaVGhpWTJZMVl6TmpPVEkwTmpaalpEWTRaVElpTENKMGNtRmphMTlwWkNJNklqYzBOR0kzTXpaakxUUmtObVl0TkRCbU1pMWlOVFJoTFRjNU9XRXlOREkzTURjd055SXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2lZelpoWlRKaVpURXRPVEl4TkMwME9EVmlMV0kzWW1JdE9UVXpabUUwTkRKaU9USm1JaXdpYVdRaU9pSmhiR2w1WVRFeExtdDFiV0Z5YVRFeE1VQmxiV0pwWW1VdVkyOXRJaXdpYkc5allXeGxJam9pWlc0aUxDSndiR0YwWm05eWJTSTZJbGRGUWlKOS5xOVpJVzYtVDE2NjVFMGM0QWhzazIzSGcwdHJzNnNPNWFLQ1dsOUp6X2J4QlZHckNtSXhpN24wVlUzRHQ4TFJUMUd0d29FQWFXbnF2SDk2blJVU3MxQSIsImNyZWF0ZWQiOjE2ODEzODI2MjcsIm90cF9vcGVyYXRpb25fdHlwZSI6IlNpZ25JbiIsImF0dGVtcHQiOjEsInR0bCI6IjYwMCIsInBsYXRmb3JtIjoiV0VCIiwiY29ubmVjdGVkX3Jlc3BvbnNlIjp0cnVlLCJyZWxhdGlvbnNoaXBfdHlwZSI6MCwidXNlcl9pZCI6ImFsaXlhMTEua3VtYXJpMTExQGVtYmliZS5jb20iLCJvcmdfaWQiOiI2NDI4ZThiY2Y1YzNjOTI0NjZjZDY4ZTIiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6NCwiaWQiOjE1MDQ5Njk1MTUsImV4cGlyeSI6MTY4MTQ2OTAyNywiam91cm5leV9pZCI6IlNJR05fSU5fYmEyM2VkMDgtZDFjNS00ZTg5LWFjNjQtNTJiMTBkZTE1Njc1IiwiZXhwIjoxNjgxNDY5MDI3fQ.YFfdu2_UQlYDENyHVLUApgL8bHlHe1DQl1CckQ_g1c2Im_KKkTw0yHnjo7VR-e910cMQO5p3PQZkMr_keK2r5A'
        }

        self.body = {
            "otp": "123456"
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        meta_data = []
        
        rnum = randrange(len(otp_token_and_email))
        self.headers['otp-token'] = otp_token_and_email[rnum][2]
        print(otp_token_and_email[rnum][2])
        url = f"/user_auth_ms/verify-login-otp"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)
        if response.status_code == 200:
            reseller_token = response.headers['embibe-token']
            meta_data.append(otp_token_and_email[rnum][0])
            meta_data.append(otp_token_and_email[rnum][1])
            meta_data.append(reseller_token)
            data.append(meta_data)
            df = pd.DataFrame(data)
            # df.to_csv('reseller_token.csv', index=False)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
