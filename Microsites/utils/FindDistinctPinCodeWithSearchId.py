from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
# from utils import ResultSaver
import pandas as pd


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

class UserBehaviour(FastHttpUser):

    # @events.test_stop.add_listener
    # def push_data(environment):
    #     logger.info(
    #         "\n\n-----------------------Initiating Data Pushing--------------------------\n")
    #     db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
    #     db_name = 'qa-mongodb-preprod'
    #     collection_name = 'performance_testing'
    #     db_save = ResultSaver(db_url, db_name, collection_name)
    #     db_save.save_results(
    #         "/narad/v1/embibe/search?screen=", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
  'authority': 'preprodms.embibe.com',
  'accept': 'application/json, text/plain, */*',
  'accept-language': 'en-US,en;q=0.9',
  'content-type': 'application/json',
  'origin': 'http://development.embibe-ui.embibe.com',
  'sec-ch-ua': '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
  'sec-ch-ua-mobile': '?1',
  'sec-ch-ua-platform': '"Android"',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'cross-site',
  'user-agent': 'Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
  }

        self.body = json.dumps(["pincode"])

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v1/udise/search?screen=udise-school-internal&searchValue={randrange(10000,60000)}&distinct=true&kveCode=kve719352"
        response = self.client.post(
            url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
