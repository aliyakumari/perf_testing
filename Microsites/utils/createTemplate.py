from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
# from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

# df = pd.read_csv("reseller_token.csv")
# org_token_and_email = df.values.tolist()


class UserBehaviour(FastHttpUser):

    # @events.test_stop.add_listener
    # def push_data(environment):
    #     logger.info(
    #         "\n\n-----------------------Initiating Data Pushing--------------------------\n")
    #     db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
    #     db_name = 'qa-mongodb-preprod'
    #     collection_name = 'performance_testing'
    #     db_save = ResultSaver(db_url, db_name, collection_name)
    #     db_save.save_results(
    #         "/calendar/microsite/create/template", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRva2VuIiwidGltZV9zdGFtcCI6IjIwMjMtMDQtMTcgMDc6NTU6NDcgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODgwMDAwMDAwMCIsInJvb3RPcmdJZCI6IjY0MjhlOGJjZjVjM2M5MjQ2NmNkNjhlMiIsImRldmljZUlkIjoiMC45OTg2MjQxMDYwNjkwNzUzIiwiZmlyc3ROYW1lIjoiRHVtbXkiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQzMjliZWYyZjQxNzI0NmVlNGRkM2YwIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQzMjliZjAyZjQxNzI0NmVlNGRkM2YxIiwiaWQiOiIxNTA0OTY3NjY4IiwiZW1haWwiOiJtb2VAZW1iaWJlLmNvbSJ9.dn9Dh_KZJZ1waEgCEMRk-ZgXLG9v1NagM0GcqZgqL32iaED44K8t6xXaD7sr4q3m5LMd-Hjao_WYSEDq9xJnXw',
  'Content-Type': 'application/json'
}

        self.body = {
    "udiseCode": "23250608002",
    "numberOfPeriods": 8,
    "periodDuration": 45,
    "startTime": "09:00 AM",
    "breaks": [
        {
            "afterPeriod": 2,
            "duration": 10
        },
        {
            "afterPeriod": 4,
            "duration": 30
        },
        {
            "afterPeriod": 6,
            "duration": 10
        }
    ]
}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        # rnum = randrange(len(org_token_and_email))
        # self.headers['reseller-jwt-token'] = org_token_and_email[rnum][2]
        url_ = f"/calendar/microsite/create/template"
        with self.client.request("POST", url=url_,headers=self.headers, data=json.dumps(self.body), catch_response=True) as response:
            if response.status_code == 200 or response.status_code == 400 :
                response.success()

def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
