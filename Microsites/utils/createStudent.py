from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange, randint
import time
import json
import logging
import gevent
import resource
import pandas as pd
# from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

df = pd.read_csv("reseller_token.csv")
org_token_and_email = df.values.tolist()


class UserBehaviour(FastHttpUser):

    # @events.test_stop.add_listener
    # def push_data(environment):
    #     logger.info(
    #         "\n\n-----------------------Initiating Data Pushing--------------------------\n")
    #     db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
    #     db_name = 'qa-mongodb-preprod'
    #     collection_name = 'performance_testing'
    #     db_save = ResultSaver(db_url, db_name, collection_name)
    #     db_save.save_results(
    #         "/narad/microsite/student/create", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Origin': 'https://stage.embibe.com',
            'Referer': 'https://stage.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'content-type': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDA0ODcwLCJjcmVhdGVkIjoxNjgxNzExNzQ5LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwMjIwODgsImV4cCI6MTY4MTc5ODE0OSwiZGV2aWNlSWQiOiIxNjgxNzExNzQ5MzU5IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDA0ODcwXzY0MDE3MjY4NDQyNTU3MjRAZW1iaWJlLXVzZXIuY29tIn0.s8hYzYSW8KDMeWbovhi-WlmMWxnusd6ZqRD0tg77_9XJIc2DRLGExQpiXC4HulgHcQwBsdEsn2ZD5FAo70VaNQ',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

        self.body = {
            "micrositeStudentCreationDTO": {
                "udiseCode": "06211012345",
                "grade": "11th Madhya Pradesh Board",
                "section": "A",
                "locale": "en",
                "firstName": "Aliya",
                "lastName": "kumari",
                "email": "kumarialiya36@gmail.com",
                "mobile": "9982824470",
                "dob": "27/03/1998",
                "gender": "Female",
                "category": "gen",
                "parentName": "RajKumar Singh",
                "parentMobile": "8210950926",
                "password": "Aliy@2817",
                "whatsAppOpt": False
            },
            "micrositeStudentCustomField": {
                "personalDeviceType": [
                    "Smart phone"
                ],
                "personalInternetAccess": "Yes",
                "ownership": "No, I share my parent’s device(s)"
            }
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        mobile = f"99822{randint(10000,99999)}"
        self.body['micrositeStudentCreationDTO']['mobile'] = mobile
        rnum = randrange(len(org_token_and_email))
        self.headers['reseller-jwt-token'] = org_token_and_email[rnum][2]
        url_ = f"/narad/microsite/student/create"
        with self.client.request("POST", url=url_, headers=self.headers, data=json.dumps(self.body), catch_response=True) as response:
            if response.status_code == 200 or response.status_code == 400:
                response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
