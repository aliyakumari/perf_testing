from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import time
import gevent
import resource


resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = {
  'Accept': 'application/json',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Accept-Encoding': 'gzip, deflate, br',
  'Host': 'preprodms-cf.embibe.com',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15',
  'Referer': 'https://staging-fiber-web.embibe.com/mock-test/mb1054844/session?paj_id=55972402-a865-412d-9460-861e339afac9&sessionId=01bb6e7e-5631-416d-83ec-509c28239c5c&type=improvement&code=mb1054843',
  'Accept-Language': 'en-gb',
  'Connection': 'keep-alive',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDE1OTQwLCJjcmVhdGVkIjoxNjM1NDk5OTUwLCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTY2MjEsImV4cCI6MTYzNTY3Mjc1MCwiZGV2aWNlSWQiOiIxNjM1MzI0MTc1MzE4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDE1OTQwXzcxMzE1NzcyMTg5NjQzOTJAZW1iaWJlLXVzZXIuY29tIn0.JaJk6_C4bgau07ftnNodDvnCGDt2pURuBNgA_xCL6TrxsqELUSJ1jQRY7XxlIAqaFe4selua9RDAKnrL9gOcqg',
  'browser-id': '1631122982924',
  'Cookie': 'V_ID=ultimate.2021-06-02.b8a67bd9856bd99d7d9c2c9025d35af5; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = {}
        self.url = "/testsubmission_ms_fiber/v1/test/mb130708/questions?goal_code=kve383630&exam_code=kve383631&version=16"
        self.method = "GET"
        self.host = "https://preprodms-cf.embibe.com"

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def ondemandautomated_script(self):
        response = self.client.get(self.url, data=self.body, headers=self.headers)

def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)

