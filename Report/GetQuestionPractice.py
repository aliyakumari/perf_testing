from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
  'Accept': 'application/json',
  'browser-id': 'MzQ1NDYyMzExMzE0X3dlYg==',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDIxMzUwLCJjcmVhdGVkIjoxNjM3MzE1OTQ3LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwMjE0MTgsImV4cCI6MTYzNzQ4ODc0NywiZGV2aWNlSWQiOiIxNjM2OTc5MDM2MTc2IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDIxMzUwXzIwOTk1Nzg2OTg4NTMzNDI1QGVtYmliZS11c2VyLmNvbSJ9.tYHr1XhVPanYoKl02U1vNMvvZtgUCwLGPQkSCVNEeZSRf8IkUdfVjEfFtLJ5yAAyNw3WErz9BeOCp6nhSRGYgQ',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://staging-fiber-web.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://staging-fiber-web.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/fiber_practice_ms/v1/practice/619777e57cabe36f3b230320/question?questionCode=&nextQuestion=false", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
