import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - preprod profile token.csv')
token = df.values.tolist()
token.pop()

data = []

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {"userId": "123123211", "book": {"id": "5fe0e99d5890a255853f33e7", "title": "Embibe Big Book for Science for Class 10",
                                                     "type": "big-book"}, "topics": [{"id": "kve98525", "title": "Chemical Changes and Equations"}], "class": "640f4514a684583f465837b6"}
        self.headers = {
            'authority': 'preprodms.embibe.com',
            'accept': 'application/json, text/plain, */*',
            'accept-language': 'en-GB,en;q=0.9',
            'axios-request-from': 'https://paas-v3-staging.embibe.com/teach/lite/view-class',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'dnt': '1',
            'origin': 'https://paas-v3-staging.embibe.com',
            'pragma': 'no-cache',
            'referer': 'https://paas-v3-staging.embibe.com/',
            'sec-ch-ua': '"Chromium";v="112", "Brave";v="112", "Not:A-Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'sec-gpc': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(token))
        meta_data = []
        self.body["userId"] = token[rnum][2]
        url = f"/coobo_ms/lite-session?locale=en"
        response = self.client.post(url, data=json.dumps(self.body), headers=self.headers)
        
        if response.status_code == 200:
            session_id = response.json().get('_id')
            meta_data.append(token[rnum][2])
            meta_data.append(session_id)
            data.append(meta_data)
            df = pd.DataFrame(data)
            df.to_csv("session_id_and_user_id.csv", index=False)
        else:
            print(response.text, self.body)
            
        


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
