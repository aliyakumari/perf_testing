import ast
import csv
import json
import logging
import mimetypes
import random
import resource
import string
import time
from codecs import encode
from random import randint, randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('session_id_and_user_id.csv')
user_id_and_session_id = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {"userId": "1501504777","id": 1681713681251,"selectedGoal": {"code": "kve97670","display_name": "CBSE","exam_category": "K12"},"selectedExam": {"code": "kve99731","display_name": "9th CBSE"},"selectedSubject": {"code": "kve99742","display_name": "Science"},"locale": "en"}
        self.headers = {
            'authority': 'beta-api.embibe.com',
            'accept': 'application/json, text/plain, */*',
            'accept-language': 'en-GB,en;q=0.9',
            'axios-request-from': '',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'dnt': '1',
            'origin': '',
            'pragma': 'no-cache',
            'referer': '',
            'sec-ch-ua': '"Chromium";v="112", "Brave";v="112", "Not:A-Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-site',
            'sec-gpc': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36',
            'Cookie': 'embibe-refresh-token=0ecda315-24c8-4fa8-b831-0625a5b3141f; preprod_embibe-refresh-token=0ecda315-24c8-4fa8-b831-0625a5b3141f; __cf_bm=bhukHtvBhT4KW9nm0AkuYbIaSo6vnk_2fxDK2rCHNDQ-1683529948-0-AT3RGYu2sncanJi8/2P0SvO7mKCoBvkVhqbbUnztUouAhz1ZqNFpn+aXv8t6jgYjCmj+1QrXqfLzEsUWZaPqXcg='
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(user_id_and_session_id))
        self.body["userId"] = user_id_and_session_id[rnum][0]
        url = f"/coobo_ms/lite-session?locale=en"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
