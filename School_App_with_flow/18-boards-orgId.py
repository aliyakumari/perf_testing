from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJMb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IkxvYWRfdGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0xMyAxMDo1Nzo0MyBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNDdkZmQ3YmMwMGEwNGJlNGVkYmY5MyIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODIxMDk1MDkyNiIsImFkbWluSWQiOiI2MzQ3ZGZkN2JjMDBhMDRiZTRlZGJmOTQiLCJpZCI6MTUwMzE4MDE3Miwicm9vdE9yZ0lkIjoiNjM0N2RmZDdiYzAwYTA0YmU0ZWRiZjkzIiwiZW1haWwiOiJzdHlAZW1iaWJlLmNvbSJ9.kDrjmgkbN7xgWg2_s8UUbP2AlYRn-2CytJkERrsxPJlivImqYsR-mILAUMsHCs3JLGoV8lb2mBHWERKG3aTaSA',
            'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        root_org = reseller_token[rnum][1]
        body = json.dumps([{"refId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670", "name": "CBSE", "isCustom": False},
                           {"refId": "5eef299c420f624a7cd798db|ICSE|icse|kve265325", "name": "ICSE",
                            "isCustom": False}])
        url = f"/narad/v2/boards?orgId={root_org}"
        response = self.client.put(url, data=body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
