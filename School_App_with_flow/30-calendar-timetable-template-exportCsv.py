import json

from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import requests
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
            'accept': '*/*',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNoYXJtYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAxLTE3IDA2OjA4OjMzIFVUQyIsIm1vYmlsZSI6Ijk4NzM4MzIxMDQiLCJyb290T3JnSWQiOiI2M2M1N2RkY2VlYWY3ZDU2NzNhNWQxNDUiLCJkZXZpY2VJZCI6IjAuMzMxMjA1Nzc0MDMxMjIyMiIsImZpcnN0TmFtZSI6Ikxha3NoeWEiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjNjNTdkZGNlZWFmN2Q1NjczYTVkMTQ1IiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjNjNTdkZGNlZWFmN2Q1NjczYTVkMTQ2IiwiaWQiOjE1MDQ4NTQ5NTUsImVtYWlsIjoibGFrc2h5YS5jb3NAZW1iaWJlLmNvbSJ9.lAiLMPB0-p1NiBVriY-FQiok55hGITmuE0YEq6BO1x9TdFzYLLTjxthEMQKmqxIygcP3r0YyjLkgfGO8JnCHqw',
            'Cookie': 'JSESSIONID=F7E040853A6D9749DE5B355958A45BFB; preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        url = f"/calendar/timetable/template/exportCsv?recordsRequired=true"
        response = self.client.get(url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
