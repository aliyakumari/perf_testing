import json
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admin.csv')
reseller_token = df.values.tolist()
reseller_jwt_tokens = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbGl5YSAiLCJsYXN0TmFtZSI6Ikt1bWFyaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0xMyAwODoyMjoxMCBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNDdiNmRiYmMwMGEwNGJlNGVkYmUyNCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODIxMDk1MDkyNiIsImFkbWluSWQiOiI2MzQ3YjZkYmJjMDBhMDRiZTRlZGJlMjUiLCJpZCI6MTUwMzE4MDEwOCwicm9vdE9yZ0lkIjoiNjM0N2I2ZGJiYzAwYTA0YmU0ZWRiZTI0IiwiZW1haWwiOiJsb2FkX3Rlc3QuNzUzNDA1NDcxQGVtYmliZS5jb20ifQ.7AR6kRsax9yuIRZt8AOIqHo-dPW5OivRD9TREPhFtQxWL6US20YH6ihDy1HONAyO5B_yMqg9wV5XampNQhPrzQ'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        token_data = []
        random_num = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[random_num][0]
        url = f"/narad/v2/rootOrg"
        self.body = json.dumps({"instituteName": "Load_test", "orgType": "School", "fields": [
            {"name": "firstName", "value": "Load_test", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "lastName", "value": "Load_test", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "mobile", "value": f"82109{randint(10000, 99999)}", "fieldGroupId": "1",
             "fieldType": "BasicField"},
            {"name": "instituteName", "value": "Load_test", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "instituteHead", "value": "Load_test", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "instituteHeadEmail", "value": f"Perf_test_QA_Automation_qwerty{randint(1, 99999)}@embibe.com",
             "fieldGroupId": "1",
             "fieldType": "BasicField"},
            {"name": "instituteHeadDesignation", "value": "CEO", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "country", "value": "INDIA", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "state", "value": "Bihar", "fieldGroupId": "1", "fieldType": "BasicField"},
            {"name": "city", "value": "Patna", "fieldGroupId": "1", "fieldType": "BasicField"}]})

        response = self.client.post(url, data=self.body, headers=self.headers)
        root_org = response.json().get('rootOrgId')
        token_data.append(reseller_token[random_num][0])
        token_data.append(root_org)
        token_data.append(reseller_token[random_num][1])
        reseller_jwt_tokens.append(token_data)
        df = pd.DataFrame(reseller_jwt_tokens)
        df.to_csv('reseller_token_admins.csv', index=False)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
