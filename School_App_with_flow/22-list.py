from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()
c = 0


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Cookie': 'JSESSIONID=82990444F8387E7795F65CF7660A0596; _ga_7N8QWZ4X8C=GS1.1.1655897351.1.1.1655897974.59; ajs_anonymous_id=f37770f3-7990-4ff0-b60b-46abc4a1f1e8; WZRK_G=5fc78604fc1c4c739916e8ffb0748d10; V_ID=ultimate.2022-09-14.419ba0bdf228a7abb0f6029c9bea68a8; _gcl_au=1.1.1157395199.1663141141; ajs_user_id=1843066425; embibe-refresh-token=5a9cf5ea-ff4a-4e4d-8f1f-a2840108d902; prod_embibe-refresh-token=1db16dcf-cdaf-4289-b880-2a8af93759b3; _ga_TT8L73VP3H=GS1.1.1663826914.23.0.1663826914.60.0.0; preprod_embibe-refresh-token=7faf7732-0cce-4e5b-bc36-062fb18dbfe3; __insp_wid=875671237; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tLw%3D%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_sess=true; _ga_YHBNHKH43S=GS1.1.1664783284.89.1.1664783293.51.0.0; _ga=GA1.1.1423279882.1655298606; __insp_slim=1664783293888; _ga_QQPR95BLCG=GS1.1.1666163529.7.1.1666164358.54.0.0; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MzU3ODA3YmI1NjU0MzM2ZDI4MjY0ZjAiLCJzdWJkb21haW4iOiJodHRwczovLzYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMC1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.08xEm9uoiuLrm7Tto1oSu3_gZgOmKbGuUwyPNY6fgevCxUTjCucW5uL6dH9zKyDdKwarATL9AlG6LSpocOV50Q; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=262b2949-9db4-4cc7-a652-d1f797e109be; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjY2Nzg5MDgsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMCIsIm1vYmlsZSI6Ijk5MjgxMTgyMzQiLCJpZCI6MTUwMzE4MTA2OSwiZXhwIjoxNjY2NzY1MzA4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZF90ZXN0MTExMTFAZ21haWwuY29tIn0.nVjgGrYITsso4IICji4ikj6c_N0zlrsTYj5QLulohIWHScdOoPLOHTWesG2dHkYN9cDJf_Q8hif_EKz29dK4nQ; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjY2Nzg5MDgsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMCIsIm1vYmlsZSI6Ijk5MjgxMTgyMzQiLCJpZCI6MTUwMzE4MTA2OSwiZXhwIjoxNjY2NzY1MzA4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZF90ZXN0MTExMTFAZ21haWwuY29tIn0.nVjgGrYITsso4IICji4ikj6c_N0zlrsTYj5QLulohIWHScdOoPLOHTWesG2dHkYN9cDJf_Q8hif_EKz29dK4nQ; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJMb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IkxvYWRfdGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0yNSAwNjoyOTozOSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTkyODExODIzNCIsImFkbWluSWQiOiI2MzU3ODA3Y2I1NjU0MzM2ZDI4MjY0ZjEiLCJpZCI6MTUwMzE4MTA2OSwicm9vdE9yZ0lkIjoiNjM1NzgwN2JiNTY1NDMzNmQyODI2NGYwIiwiZW1haWwiOiJsb2FkX3Rlc3QxMTExMUBnbWFpbC5jb20ifQ.CuOfVcPr9r8YHH-S9ygirqr_OjUzRdLs_LsFSlnIzWvQUU6jLEEtvIcXF8qf6i4XuChNKRSAR0qz9LNTluRMFA; JSESSIONID=FBD5CE5C1B026A073AE90A84C2FAC3D6; embibe-refresh-token=d671179d-b4bd-4ca1-ab6c-1d6b43398eb4; school_preprod_embibe-refresh-token=e53e2fbb-67fd-4ae2-9cf6-a3a38a2eda46',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJMb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IkxvYWRfdGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0yNSAwNjoyOTozOSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTkyODExODIzNCIsImFkbWluSWQiOiI2MzU3ODA3Y2I1NjU0MzM2ZDI4MjY0ZjEiLCJpZCI6MTUwMzE4MTA2OSwicm9vdE9yZ0lkIjoiNjM1NzgwN2JiNTY1NDMzNmQyODI2NGYwIiwiZW1haWwiOiJsb2FkX3Rlc3QxMTExMUBnbWFpbC5jb20ifQ.CuOfVcPr9r8YHH-S9ygirqr_OjUzRdLs_LsFSlnIzWvQUU6jLEEtvIcXF8qf6i4XuChNKRSAR0qz9LNTluRMFA'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        root_org = reseller_token[rnum][1]
        body = {}
        url = f"/narad/v2/school/{root_org}/list"
        with self.client.get(url, data=body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 400:
                response.success()
            if response.status_code == 200:
                response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
