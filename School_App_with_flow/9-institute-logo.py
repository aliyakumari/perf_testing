from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import json
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbGl5YSIsImxhc3ROYW1lIjoiS3VtYXJpIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIyLTEwLTEwIDA2OjUzOjMzIFVUQyIsInBhcmVudE9yZ0lkIjoiNjM0M2MxM2JmZTg3ZTczODdhOGI3YmNhIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI4MjEwOTUwOTI2IiwiYWRtaW5JZCI6IjYzNDNjMTNjZmU4N2U3Mzg3YThiN2JjYiIsImlkIjoxNTAzMTcwMDc1LCJyb290T3JnSWQiOiI2MzQzYzEzYmZlODdlNzM4N2E4YjdiY2EiLCJlbWFpbCI6Imt1bWFyaWFsaXlhZ2V1QGdtYWlsLmNvbSJ9.7tXJ-HWZDvRQ-PBh7JijHq5epMrtyGgQWYm8Wku-mK-I06WEk3EfAH1XA9-ff467mnY40r_JmMeJpGZKWGTYMA',
            'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        org_id = reseller_token[rnum][1]

        body = {'fileName': 'Screenshot 2022-06-22 at 5.16.15 PM.png',
                'fileType': 'image/jpeg',
                'orgId': '63c7b95b23436d7f4388f26b'}

        files = [
            ('file',
             ('20221111_105453.jpg', open('20221111_105453.jpg', 'rb'), 'image/jpeg'))
        ]

        url = f"/narad/v2/instituteLogo"
        response = self.client.post(url, data=json.dumps(body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
