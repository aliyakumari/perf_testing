import json

from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import requests
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()




class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers =  {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json;charset=UTF-8',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbGl5YSAiLCJsYXN0TmFtZSI6Ikt1bWFyaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wOS0zMCAxMDoxNDo0MiBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzMzZiYjgxMmY1YTkwMmIxYzdmNzYxZSIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODIxMDk1MDkyNiIsImFkbWluSWQiOiI2MzM2YmI4MTJmNWE5MDJiMWM3Zjc2MWYiLCJpZCI6MTUwMzE2NzU4Mywicm9vdE9yZ0lkIjoiNjMzNmJiODEyZjVhOTAyYjFjN2Y3NjFlIiwiZW1haWwiOiJsb2FkX3Rlc3QuMzg2NTNAZW1iaWJlLmNvbSJ9.C-zyCv9YlEyM8-ly_-1e7vdWAbYcr52thAoaQJqWtY3c0SrKhjbSJg0SYeVoh2xELIlzh8mzK9PF94KH95rCgg'
}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        onboarding_url = f"https://preprodms-cf.embibe.com/narad/v2/onboardingState?fieldGroupId=2&orgId={reseller_token[rnum][1]}"
        requests.request("PUT", onboarding_url, headers=self.headers, data={})
        root_org = reseller_token[rnum][1]
        url = f"/narad/v2/levelMap?orgId={root_org}"
        response = self.client.get(url, data=self.body, headers=self.headers)




def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
