from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import pandas as pd
import json

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = ["udiseSchCode", "pincode"]
        self.headers = {
            'accept': 'application/json',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkFkbWluIiwidGltZV9zdGFtcCI6IjIwMjMtMDMtMDMgMTQ6MzI6MTggVVRDIiwibW9iaWxlIjoiODk5MDg5OTAwOSIsInJvb3RPcmdJZCI6IjYzZmUwYTQyODY2ODhjN2FmYzYxMzNkMiIsImRldmljZUlkIjoiMC4yNTQzMzAyNTAzMTUwMzc3NSIsImZpcnN0TmFtZSI6IlN1cGVyIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2M2ZlMGE0Mjg2Njg4YzdhZmM2MTMzZDIiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2ZlMGE0Mzg2Njg4YzdhZmM2MTMzZDMiLCJpZCI6MTUwNDk1MTgwMSwiZW1haWwiOiJzdXBlcjEuYWRtaW4xQGVtYmliZS5jb20ifQ.Qqjq6BS5m7XDISu51XVChKREbGwVERk3MVXs5wIWjqm1ZIzoFI9HMgLQkjJ1YLg8zVnbvAKvDgaZUju0uMlocQ',
            'Content-Type': 'application/json',
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v1/udise/search?enableSorting=false&screen=udise-school&searchValue=635010&unpaged=true"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
