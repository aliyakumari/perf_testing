import json
import pandas as pd
from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
main_data = []
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'JSESSIONID=D4988BE40119D2979CA90EDC999933E0; preprod_embibe-refresh-token=37b1a32c-dc35-4592-8d87-1d3afce3d1f3; _gcl_au=1.1.246905907.1675256556; WZRK_G=60d76e4009e84b8faea1b478d07f32df; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc3MTU3MTQ5LCJnYV9pZCI6IjE5OTY1NjExMTQuMTY3NDEzNDI1MCIsInNvdXJjZV90eXBlIjoiRU1CSUJFX0hPTUUiLCJzb3VyY2UiOiJ0ZXN0IiwiZXhwIjoxNjc4MzY2NzQ5LCJ1dWlkIjoiMWYzMzU5MTUtZjllNy00ZTI0LWFkNTAtYWRmODQ5ODI5OGIxIiwic2VuZGVyX2lkIjoidGVzdCJ9.tnLh6g3lQtFGpyMBFtzDYPcTa-CZ4DYIflelmqTQjYc; guest-session-id=1f335915-f9e7-4e24-ad50-adf8498298b1; prod_ab_version=0; prod_embibe-refresh-token=f33b355b-665a-456d-a6ec-d4188b4b4f51; _ga_QQPR95BLCG=GS1.1.1677156732.21.1.1677157371.60.0.0; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2M2Y4NzIwMjNmM2ViNTZkMjkyZWUxNTYiLCJzdWJkb21haW4iOiJodHRwczovLzYzZjg3MjAyM2YzZWI1NmQyOTJlZTE1Ni1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.X4ci7rcZfrXg6GcbLjeF1Rrnn-1agQn6dlst3vYSYeRtV2GLTjBCJ8kxifk51USWsCLNLnE5VbzVyvZqjut5aA; school_preprod_embibe-refresh-token=7c419381-5a5f-48c3-a7ec-f21d8f41670e; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkdVSVBLaGpDMTIiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMi0yNCAwODoxNDo1OSBVVEMiLCJtb2JpbGUiOiI4MjEwOTUwOTI2Iiwicm9vdE9yZ0lkIjoiNjNmODcyMDIzZjNlYjU2ZDI5MmVlMTU2IiwiZmlyc3ROYW1lIjoiTG9hZF90ZXN0Iiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2M2Y4NzIwMjNmM2ViNTZkMjkyZWUxNTYiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2Y4NzIwMzNmM2ViNTZkMjkyZWUxNTciLCJpZCI6MTUwNDk1MTM5NCwiZW1haWwiOiJsb2FkX3Rlc3RAZ21haWwuY29tIn0.qA-NK7IBDHF4UHY_9Wuvy_4EquSbJj0d4-mvoWudNoFvkcfX9xz46zeC2H1gY9wOCt0KGQtMPlo1Z8qQ34FyHg; __insp_uid=664387785; _ga_TT8L73VP3H=GS1.1.1677480414.14.0.1677480424.50.0.0; session=.eJxNkF1vgjAUhv_K0mtTa92MkJjMTO1IdMZloHDXQgNl7UqkRdH438WNwC7OzXmeN-fjCriiQo7GwAVUiprCb6voUbxyxQTjMNYKDEB61LYA7hU8scZjxMmTwweKyAqFu9kM3AZA9Ji-f6J4oav1ZXla5x5uym5yb7JdzJ-3rS51bEvTRTbzv3ZpqLHlY5kW-D-BZUra6PRfwKgTdocoo_tzFqqzfAhGp6nkHY5xViVt1tRFD77aG7w3lPokqCPl1Gy_siH204AEl4T0Q23Jk98HZcYU7nA4QhBPIIYYu1M0dZr_VAq4L7c72Upkzw.ZACLeQ.Woe_EJDNn0nVPlrDD78cXuSbKcI; himachalpradesh-curlang=hi; gujarat-curlang=gu; locale=gu; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzc4MjcxNzAsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI4Nzk5MTYwMzI5IiwiaWQiOjE1MDQ4MTYzNjcsImV4cCI6MTY3NzkxMzU3MCwiZGV2aWNlSWQiOiIxNjc1MjU2NDk1MDgwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.z-2G7zf1-EfzrwhjE3XAQv6ibPcx6YsrfbbZ-xJ-sf5vpuVmg1iletuLg-sNVnmvdw9-KNJlQXj2lXnmWDuQYQ; _ga_X772T5L8EH=GS1.1.1677835477.27.0.1677835477.60.0.0; _ga=GA1.2.1996561114.1674134250; __insp_wid=82124457; __insp_slim=1677835478068; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL3ByYWN0aWNlL2hvbWU%3D; __insp_targlpt=UHJhY3RpY2UgMTF0aCBDQlNFIFN1YmplY3RzLCBCb29rIENoYXB0ZXJzICYgVmlkZW8gU29sdXRpb25zIE9ubGluZSAtIEVtYmliZQ%3D%3D; __insp_norec_howoften=true; __insp_norec_sess=true; _ga_YHBNHKH43S=GS1.1.1677835477.71.1.1677835491.46.0.0; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkdVSVBLaGpDMTIiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMi0yNCAwODoxNDo1OSBVVEMiLCJtb2JpbGUiOiI4MjEwOTUwOTI2Iiwicm9vdE9yZ0lkIjoiNjNmODcyMDIzZjNlYjU2ZDI5MmVlMTU2IiwiZmlyc3ROYW1lIjoiTG9hZF90ZXN0Iiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2M2Y4NzIwMjNmM2ViNTZkMjkyZWUxNTYiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2Y4NzIwMzNmM2ViNTZkMjkyZWUxNTciLCJpZCI6MTUwNDk1MTM5NCwiZW1haWwiOiJsb2FkX3Rlc3RAZ21haWwuY29tIn0.qA-NK7IBDHF4UHY_9Wuvy_4EquSbJj0d4-mvoWudNoFvkcfX9xz46zeC2H1gY9wOCt0KGQtMPlo1Z8qQ34FyHg',
            'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v2/superAdmin"
        num = randint(1000000, 10000000000)
        Data = []
        body = json.dumps({"firstName": f"Perf_test$${num}load_test", "lastName": f"QA_automation{num}",
                           "emailId": f"{num}loadTest125backend.{num}@embibe.com",
                           "mobile": f"99828{randint(10000,99999)}", "password": "Aliya1@2817", "orgType": "School"})

        response = self.client.post(url, data=body, headers=self.headers)
        Data.append(
            f"{num}loadTest125backend.{num}@embibe.com")
        Data.append(response.headers['Set-Cookie'].replace('org-token=',
                    "").replace(" ;Path=/ ;Domain=.embibe.com", ""))
        Data.append(response.json().get('id'))
        main_data.append(Data)
        df = pd.DataFrame(main_data)
        df.to_csv('org_token.csv', index=False)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
