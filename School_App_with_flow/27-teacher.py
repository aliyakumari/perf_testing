import json

from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import requests
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Cookie': 'JSESSIONID=DA13FEA5CA0F35D6B46DA32B73D03662; school_preprod_ab_version=0; __insp_slim=1674108195891; _ga_YHBNHKH43S=GS1.1.1674114965.25.0.1674114965.60.0.0; _ga_X772T5L8EH=GS1.1.1674114965.20.0.1674114965.60.0.0; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2M2M4ZmE0YWRhYWVkZTZhNTgwNmFjMGQiLCJzdWJkb21haW4iOiJodHRwczovLzYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZC1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-EQPGOHEjijgX-KF720Glv-hKdCsgcff1snIEAjavyqLUtTaMP7kwV3kuRnkCBDVlfiZYqcvRYuHR1E9VbXPyQ; school_preprod_embibe-refresh-token=39ce1838-546c-41bf-b490-305a5ffe0053; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMTU2NTksIm9yZ2FuaXphdGlvbl9pZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsIm1vYmlsZSI6IjgyMTA5MjA5MjYiLCJpZCI6MTUwNDkwMTk3NywiZXhwIjoxNjc0MjAyMDU5LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZG9sbHlzaW5nQGVtYmliZS5jb20ifQ.DzF80u7OXr5MBR3QmcSSioOeYbjwRmIEAoyj5rJou-gNzDksqPCVGTwk5fUM8hMNfrEWftPw0gvz_Wtln9w8IA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMTU2NTksIm9yZ2FuaXphdGlvbl9pZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsIm1vYmlsZSI6IjgyMTA5MjA5MjYiLCJpZCI6MTUwNDkwMTk3NywiZXhwIjoxNjc0MjAyMDU5LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZG9sbHlzaW5nQGVtYmliZS5jb20ifQ.DzF80u7OXr5MBR3QmcSSioOeYbjwRmIEAoyj5rJou-gNzDksqPCVGTwk5fUM8hMNfrEWftPw0gvz_Wtln9w8IA; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNpbmdoIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMTkgMDg6MDg6MzIgVVRDIiwibW9iaWxlIjoiODIxMDkyMDkyNiIsInJvb3RPcmdJZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsImZpcnN0TmFtZSI6IkRvbGx5Iiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidXNlcl90eXBlIjo0LCJwYXJlbnRPcmdJZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZSIsImlkIjoxNTA0OTAxOTc3LCJlbWFpbCI6ImRvbGx5c2luZ0BlbWJpYmUuY29tIn0.kp_2DZfdbQwvhTS7pm16dcx3QYy_BKSuaRZ8SriKwojTK0VxKp3yiceHXM4lzfzGmd9wOG_48PRSjnJHfOv4XA; JSESSIONID=8D2C5EDE4B37E6514DA521FE1AB23808; preprod_embibe-refresh-token=0cd0ed83-ef6e-4489-a532-ad81a543df33; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMDg2NjMsIm9yZ2FuaXphdGlvbl9pZCI6IjYzYzdkMjRiZGFhZWRlNmE1ODA2NDEyNiIsIm1vYmlsZSI6IjkwNjA5MDIyNDUiLCJpZCI6MTUwNDg3NzAwNywiZXhwIjoxNjc0MTk1MDYzLCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZW1iaWJlX3FhX2F1dG9tXzI3MThAZ21haWwuY29tIn0.kV0qXqzFTIMDlR2U3EchhUkcMIOclV5KqJEQveiUZB_lzxtY3DCA4GtVjfnr-K4ipYvXKz5S6yGXRD6bS29uSQ; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMDg2NjMsIm9yZ2FuaXphdGlvbl9pZCI6IjYzYzdkMjRiZGFhZWRlNmE1ODA2NDEyNiIsIm1vYmlsZSI6IjkwNjA5MDIyNDUiLCJpZCI6MTUwNDg3NzAwNywiZXhwIjoxNjc0MTk1MDYzLCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZW1iaWJlX3FhX2F1dG9tXzI3MThAZ21haWwuY29tIn0.kV0qXqzFTIMDlR2U3EchhUkcMIOclV5KqJEQveiUZB_lzxtY3DCA4GtVjfnr-K4ipYvXKz5S6yGXRD6bS29uSQ',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNpbmdoIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMTkgMDg6MDg6MzIgVVRDIiwibW9iaWxlIjoiODIxMDkyMDkyNiIsInJvb3RPcmdJZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsImZpcnN0TmFtZSI6IkRvbGx5Iiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidXNlcl90eXBlIjo0LCJwYXJlbnRPcmdJZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzYzhmYTRhZGFhZWRlNmE1ODA2YWMwZSIsImlkIjoxNTA0OTAxOTc3LCJlbWFpbCI6ImRvbGx5c2luZ0BlbWJpYmUuY29tIn0.kp_2DZfdbQwvhTS7pm16dcx3QYy_BKSuaRZ8SriKwojTK0VxKp3yiceHXM4lzfzGmd9wOG_48PRSjnJHfOv4XA',
            'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        root_org = reseller_token[rnum][1]
        self.body = {
            "fields": [
                {
                    "fieldGroupId": 1,
                    "name": "firstName",
                    "value": "Load_4teestbxvvwk,wq",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 3,
                    "name": "password",
                    "value": "Aliya@8171",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 3,
                    "name": "PasswordOptions",
                    "value": "manual",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 1,
                    "name": "emailId",
                    "value": f"GUIPKh4jC{reseller_token[rnum][1]}1{randrange(1, 873832728272626)}212@embibe.com",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 1,
                    "name": "mobile",
                    "value": f"90601{randint(10000, 99999)}",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 1,
                    "name": "salutation",
                    "value": "MS",
                    "fieldType": "BasicField"
                },
                {
                    "fieldGroupId": 1,
                    "name": "lastName",
                    "value": "Load_testjhsbjjkwjkw,kw,",
                    "fieldType": "BasicField"
                }
            ],
            "teacherBranchRequests": [
                {
                    "branchStatus": "Create",
                    "orgSubjectAssociations": [
                        {
                            "schoolId": "63c8fb2adaaede6a5806ac1a",
                            "schoolName": "Load_test",
                            "grade": "10th CBSE-en",
                            "gradeId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE|cbse--10th cbse|kve97670--kve97915$en",
                            "section": "A",
                            "sectionId": "A",
                            "subject": "Science",
                            "subjectId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE/Science|cbse--10th cbse--science|kve97670--kve97915--kve98045",
                            "isClassTeacher": False
                        }
                    ]
                }
            ]
        }
        url = f"/narad/v2/teacher"
        response = self.client.post(url, data=json.dumps(self.body), headers=self.headers)
        print(response.text)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
