from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()
c = 0


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJMb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IkxvYWRfdGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0yNSAwNjoyOTozOSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNTc4MDdiYjU2NTQzMzZkMjgyNjRmMCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTkyODExODIzNCIsImFkbWluSWQiOiI2MzU3ODA3Y2I1NjU0MzM2ZDI4MjY0ZjEiLCJpZCI6MTUwMzE4MTA2OSwicm9vdE9yZ0lkIjoiNjM1NzgwN2JiNTY1NDMzNmQyODI2NGYwIiwiZW1haWwiOiJsb2FkX3Rlc3QxMTExMUBnbWFpbC5jb20ifQ.CuOfVcPr9r8YHH-S9ygirqr_OjUzRdLs_LsFSlnIzWvQUU6jLEEtvIcXF8qf6i4XuChNKRSAR0qz9LNTluRMFA',
            'Cookie': 'JSESSIONID=FBD5CE5C1B026A073AE90A84C2FAC3D6; embibe-refresh-token=d671179d-b4bd-4ca1-ab6c-1d6b43398eb4; school_preprod_embibe-refresh-token=e53e2fbb-67fd-4ae2-9cf6-a3a38a2eda46'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        root_org = reseller_token[rnum][1]
        body = json.dumps({"classRoomRequests": [
            {"board": "CBSE", "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670",
             "grade": f"en | 1011{rnum}th CBSE",
             "gradeId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE|cbse--10th cbse|kve97670--kve97915|en",
             "isCustomGrade": False, "sections": ["B"], "subjects": [{"name": "Mathematics",
                                                                      "refId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE/Mathematics|cbse--10th cbse--mathematics|kve97670--kve97915--kve97916",
                                                                      "isCustom": False}, {"name": "Science",
                                                                                           "refId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE/Science|cbse--10th cbse--science|kve97670--kve97915--kve98045",
                                                                                           "isCustom": False},
                                                                     {"name": "Social Science",
                                                                      "refId": "5ec5867a0c88fe5860961943|CBSE/10th CBSE/Social Science|cbse--10th cbse--social science|kve97670--kve97915--kve744878",
                                                                      "isCustom": False}]}], "fields": []})
        url = f"/narad/v2/school/{root_org}/class"
        with self.client.post(url, data=body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 400:
                response.success()
            if response.status_code == 200:
                response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
