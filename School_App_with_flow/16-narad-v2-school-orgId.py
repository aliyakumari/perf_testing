from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token_admins.csv')
reseller_token = df.values.tolist()
c = 0

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJsYTQyODI3MzQzOTNsb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IlFBX2F1dG9tYXRpb240MjgyNzM0MzkzIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIyLTEwLTE0IDE1OjU5OjI5IFVUQyIsInBhcmVudE9yZ0lkIjoiNjM0OTg0YTgwY2NjNzk2MTVmYTE4OTVkIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5OTEwOTUxMTc4IiwiYWRtaW5JZCI6IjYzNDk4NGE4MGNjYzc5NjE1ZmExODk1ZSIsImlkIjoxNTAzMTgwNTI0LCJyb290T3JnSWQiOiI2MzQ5ODRhODBjY2M3OTYxNWZhMTg5NWQiLCJlbWFpbCI6IjQyODI3MzQzOTNhbGl5YV9sb2FkX3Rlc3QuNDI4MjczNDM5M0BlbWJpYmUuY29tIn0.rfXAP9Bx7aLL89CVmAix5kRtHVKLPrwQEE7IY_Y_pwQidoULaNQrxMjXoiLL-AmUhBc8GIqbBztK8RvoEM-uuQ',
            'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
            'Content-Type': 'application/json;charset=UTF-8'
        }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][0]
        root_org = reseller_token[rnum][1]
        body = {}
        url = f"/narad/v2/school/{root_org}"
        with self.client.get(url, data=body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 400:
                response.success()
            if response.status_code == 200:
                response.success()



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
