from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import pandas as pd
import time
import gevent
import resource
import json

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('org_token.csv')
org_token_data = df.values.tolist()
used_data = []

reseller_token = []


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json;charset=UTF-8',
  'Cookie': 'preprod_embibe-refresh-token=37b1a32c-dc35-4592-8d87-1d3afce3d1f3; _gcl_au=1.1.246905907.1675256556; WZRK_G=60d76e4009e84b8faea1b478d07f32df; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc3MTU3MTQ5LCJnYV9pZCI6IjE5OTY1NjExMTQuMTY3NDEzNDI1MCIsInNvdXJjZV90eXBlIjoiRU1CSUJFX0hPTUUiLCJzb3VyY2UiOiJ0ZXN0IiwiZXhwIjoxNjc4MzY2NzQ5LCJ1dWlkIjoiMWYzMzU5MTUtZjllNy00ZTI0LWFkNTAtYWRmODQ5ODI5OGIxIiwic2VuZGVyX2lkIjoidGVzdCJ9.tnLh6g3lQtFGpyMBFtzDYPcTa-CZ4DYIflelmqTQjYc; guest-session-id=1f335915-f9e7-4e24-ad50-adf8498298b1; prod_ab_version=0; prod_embibe-refresh-token=f33b355b-665a-456d-a6ec-d4188b4b4f51; _ga_QQPR95BLCG=GS1.1.1677156732.21.1.1677157371.60.0.0; school_preprod_embibe-refresh-token=7c419381-5a5f-48c3-a7ec-f21d8f41670e; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkdVSVBLaGpDMTIiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMi0yNCAwODoxNDo1OSBVVEMiLCJtb2JpbGUiOiI4MjEwOTUwOTI2Iiwicm9vdE9yZ0lkIjoiNjNmODcyMDIzZjNlYjU2ZDI5MmVlMTU2IiwiZmlyc3ROYW1lIjoiTG9hZF90ZXN0Iiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2M2Y4NzIwMjNmM2ViNTZkMjkyZWUxNTYiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2Y4NzIwMzNmM2ViNTZkMjkyZWUxNTciLCJpZCI6MTUwNDk1MTM5NCwiZW1haWwiOiJsb2FkX3Rlc3RAZ21haWwuY29tIn0.qA-NK7IBDHF4UHY_9Wuvy_4EquSbJj0d4-mvoWudNoFvkcfX9xz46zeC2H1gY9wOCt0KGQtMPlo1Z8qQ34FyHg; __insp_uid=664387785; _ga_TT8L73VP3H=GS1.1.1677480414.14.0.1677480424.50.0.0; session=.eJxNkF1vgjAUhv_K0mtTa92MkJjMTO1IdMZloHDXQgNl7UqkRdH438WNwC7OzXmeN-fjCriiQo7GwAVUiprCb6voUbxyxQTjMNYKDEB61LYA7hU8scZjxMmTwweKyAqFu9kM3AZA9Ji-f6J4oav1ZXla5x5uym5yb7JdzJ-3rS51bEvTRTbzv3ZpqLHlY5kW-D-BZUra6PRfwKgTdocoo_tzFqqzfAhGp6nkHY5xViVt1tRFD77aG7w3lPokqCPl1Gy_siH204AEl4T0Q23Jk98HZcYU7nA4QhBPIIYYu1M0dZr_VAq4L7c72Upkzw.ZACLeQ.Woe_EJDNn0nVPlrDD78cXuSbKcI; himachalpradesh-curlang=hi; gujarat-curlang=gu; locale=gu; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzc4MjcxNzAsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI4Nzk5MTYwMzI5IiwiaWQiOjE1MDQ4MTYzNjcsImV4cCI6MTY3NzkxMzU3MCwiZGV2aWNlSWQiOiIxNjc1MjU2NDk1MDgwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlfQ.z-2G7zf1-EfzrwhjE3XAQv6ibPcx6YsrfbbZ-xJ-sf5vpuVmg1iletuLg-sNVnmvdw9-KNJlQXj2lXnmWDuQYQ; _ga_X772T5L8EH=GS1.1.1677835477.27.0.1677835477.60.0.0; _ga=GA1.2.1996561114.1674134250; __insp_wid=82124457; __insp_slim=1677835478068; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL3ByYWN0aWNlL2hvbWU%3D; __insp_targlpt=UHJhY3RpY2UgMTF0aCBDQlNFIFN1YmplY3RzLCBCb29rIENoYXB0ZXJzICYgVmlkZW8gU29sdXRpb25zIE9ubGluZSAtIEVtYmliZQ%3D%3D; __insp_norec_howoften=true; __insp_norec_sess=true; _ga_YHBNHKH43S=GS1.1.1677835477.71.1.1677835491.46.0.0; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJzdWJkb21haW4iOiJodHRwczovLzY0MDU4YjI2NzNmMzE3MDAyN2NhODFhYi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.UrBt_utJgoBmF5EFZxMgVbbjXKXIiRgAm_SDrYcn8yMASGvibc7OkXDbM42Wy6SuvuHjwOiGzNAc0nOIiIrlag; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjYxMjM3MDg2NjExIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzVmTWlJc0ltOXlaMTlwWkNJNklqRWlMQ0owY21GamExOXBaQ0k2SW1WaVpXSTVPREZoTFdJNFlXWXROR1ZsTVMwNE1tVTRMVFJqTURZNFpEY3laamxtTXlJc0ltOTBjRjl5WlhGMVpYTjBYMmxrSWpvaVpqZzFPVGhqTnpFdE9HSXlaaTAwWXpOakxUazBOak10T1RZek9HTmtZakkzWlRobElpd2lhV1FpT2lJNE1qRXdPVFV3T1RJMklpd2liRzlqWVd4bElqb2laVzRpTENKd2JHRjBabTl5YlNJNklsZEZRaUo5LkFjQl9WZkc5SnI1RTRqc055cnZ0ajMzZ1JnTFV1MDFjR1Y5WVlxeWtLQXdQM1RLdWsxMXJlbHV4QTdYOUlBVExOWVJuMVp6cjh2N3dkWjY3YnJMbmVRIiwiY3JlYXRlZCI6MTY3ODA5MTQ4NCwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwicGxhdGZvcm0iOiJXRUIiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiODIxMDk1MDkyNiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MiwiaWQiOjE1MDAwMjExOTUsImV4cGlyeSI6MTY3ODE3Nzg4NCwiam91cm5leV9pZCI6IlNJR05fSU5fODYyYzlmZTMtNGYyOC00ODM0LTg1NjgtMGYzMDJkOGJlMzQyIiwiZXhwIjoxNjc4MTc3ODg0fQ.ZGdzAO3a_TlkmSZ0AoLvIl7nW4eGPRVmaVUEQLDx_FPpPZ50Yw0xntS4TyKSy-8b01ejIyRDxY1YO9g84DVUew; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Imt1bWFyaSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTA2IDA5OjE0OjM2IFVUQyIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJyb290T3JnSWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJmaXJzdE5hbWUiOiJBbGl5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQwNThiMjY3M2YzMTcwMDI3Y2E4MWFiIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQwNThiMjc3M2YzMTcwMDI3Y2E4MWFjIiwiaWQiOjE1MDQ5NTI1NzYsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.l3g0xsRJtqaMCy-nHMuQA2WYJsRgzghgN4jsO8NOV4hH2NkNvO63kfFdAFZ0cpqaaEszhTwAqmFLhz3SBGgw9w; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzgwOTk3MjgsIm9yZ2FuaXphdGlvbl9pZCI6IjY0MDU4YjI2NzNmMzE3MDAyN2NhODFhYiIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJpZCI6MTUwNDk1MjU3NiwiZXhwIjoxNjc4MTg2MTI4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.AMtZDKya8nmGBeguQYr8JZrT75WuaYRWR0CjfQGIkvfdtF509ijweHEmdQLICLG6_QTR3JBqjxe4B8IvJhOfpA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzgwOTk3MjgsIm9yZ2FuaXphdGlvbl9pZCI6IjY0MDU4YjI2NzNmMzE3MDAyN2NhODFhYiIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJpZCI6MTUwNDk1MjU3NiwiZXhwIjoxNjc4MTg2MTI4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.AMtZDKya8nmGBeguQYr8JZrT75WuaYRWR0CjfQGIkvfdtF509ijweHEmdQLICLG6_QTR3JBqjxe4B8IvJhOfpA',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
  'org-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJzdWJkb21haW4iOiJodHRwczovLzY0MDU4YjI2NzNmMzE3MDAyN2NhODFhYi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.UrBt_utJgoBmF5EFZxMgVbbjXKXIiRgAm_SDrYcn8yMASGvibc7OkXDbM42Wy6SuvuHjwOiGzNAc0nOIiIrlag',
  'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}


    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        reseller_token_temp = []
        rnum = randrange(len(org_token_data))
        if rnum not in used_data:
            self.body = json.dumps(
                {"email": org_token_data[rnum][0], "password": "Aliya1@2817", "org_id": org_token_data[rnum][2],
                 "device_id": "", "connected_response": True})
            url = f"/user_auth_ms/sign-in"
            self.headers['org-token'] = org_token_data[rnum][1]
            reseller_token_temp.append(org_token_data[rnum][0])
            response = self.client.post(url, data=self.body, headers=self.headers)
            reseller_token_temp.append(response.headers['embibe-token'])
            reseller_token_temp.append(org_token_data[rnum][2])
            reseller_token.append(reseller_token_temp)
            used_data.append(rnum)
        df = pd.DataFrame(reseller_token)
        df.to_csv('reseller_token.csv', index=False)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
