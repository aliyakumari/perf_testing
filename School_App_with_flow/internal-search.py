from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import pandas as pd
import json

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {
            "where": {
                "operations": {
                    "and": [
                        {
                            "op": "or",
                            "scalar": {
                                "fieldType": "BasicField",
                                "name": "udiseSchCode",
                                "value": [
                                    "8070900402"
                                ]
                            }
                        }
                    ]
                }
            },
            "page": {
                "pageNumber": 1,
                "pageSize": 10
            },
            "personaType": "MasterSchool"
        }
        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'api-narad-key': 'FsBcueZ0zpwDZUjrvmRle7rPidZp0SlE0qk7UfCOVsp4fq9cCLCMHYMGZ5moKk87',
            'Cookie': 'JSESSIONID=4B0DBC17D04E53FF106D1EF5423A930A; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkFkbWluIiwidGltZV9zdGFtcCI6IjIwMjMtMDMtMDMgMTE6MTI6NTIgVVRDIiwibW9iaWxlIjoiOTg3NjU0MzI5MCIsInJvb3RPcmdJZCI6IjYxZDU0Y2I5Yzk1YzQyNzhlYWRhMWVkYSIsImRldmljZUlkIjoiMC44MzE0ODUwNzg3NDkwNjYyIiwiZmlyc3ROYW1lIjoiUmFqdSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjFkNTRjYjljOTVjNDI3OGVhZGExZWRhIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjFkNTRjYjljOTVjNDI3OGVhZGExZWRiIiwiaWQiOjE1MDExOTkxOTIsImVtYWlsIjoicmFqdV9hZG1pbkBlbWJpYmUuY29tIn0.uWoeKVR4juA7cVqKBvEF-Kg2fYNy8DiSHNfb3DPZxdf_6q4UkLTxOU7kfdUB_LKeZKz1nSMi1kr7yke6R558Ew; school_preprod_embibe-refresh-token=fffde069-dcd9-49a9-8750-580a136557df; JSESSIONID=FE1DD63E5E5F6075B6FEADB13C8A54A4; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Imt1bWFyaSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTA2IDA5OjE0OjM2IFVUQyIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJyb290T3JnSWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJmaXJzdE5hbWUiOiJBbGl5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQwNThiMjY3M2YzMTcwMDI3Y2E4MWFiIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQwNThiMjc3M2YzMTcwMDI3Y2E4MWFjIiwiaWQiOjE1MDQ5NTI1NzYsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.l3g0xsRJtqaMCy-nHMuQA2WYJsRgzghgN4jsO8NOV4hH2NkNvO63kfFdAFZ0cpqaaEszhTwAqmFLhz3SBGgw9w; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }
    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v1/internal/search?screen=udise-school-internal&personaType=MasterSchool"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
