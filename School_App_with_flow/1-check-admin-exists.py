from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
      'Accept': 'application/json, text/plain, */*',
      'Accept-Language': 'en-GB,en;q=0.9',
      'Connection': 'keep-alive',
      'Content-Type': 'application/json;charset=UTF-8',
      'Origin': 'https://paas-v3-staging.embibe.com',
      'Referer': 'https://paas-v3-staging.embibe.com/',
      'Sec-Fetch-Dest': 'empty',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Site': 'same-site',
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
      'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"macOS"',
      'Cookie': 'JSESSIONID=65E66DFB90810983CD93A2DAA2F93499; embibe-refresh-token=cd3aec37-278c-46df-aa64-c900b1764415; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MzM2YTQ4MWVjOWQyMjFlNzQ4ZmMzYjIiLCJzdWJkb21haW4iOiJodHRwczovLzYzMzZhNDgxZWM5ZDIyMWU3NDhmYzNiMi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.QA09EQ9d38GorhIMKl_CCeJSmrxS3rsVI7Mp_MYEVRqS6eP-7HwJ0g9lqmxSXAGroemoH3NQBItSVcsH6ODwVg; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbGl5YSAiLCJsYXN0TmFtZSI6Ikt1bWFyaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wOS0zMCAwNzoyOToxNSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzMzY5MWNlZDk1MmYxMTBlNTg3OWViMCIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODIxMDk1MDkyNiIsImFkbWluSWQiOiI2MzM2OTFjZmQ5NTJmMTEwZTU4NzllYjEiLCJpZCI6MTUwMzE2Njk5Niwicm9vdE9yZ0lkIjoiNjMzNjkxY2VkOTUyZjExMGU1ODc5ZWIwIiwiZW1haWwiOiJhdXRvbWF0aW9uLnVpQGVtYmliZS5jb20ifQ.OkjJ2txlQiSmkNAaZx6rpwmQQw2hbiR0uQFiU0VNRNFeaqrOxW9osLcrRYVMPVxFt08-qBBLk8etI7uLyT27gQ; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=b80aa0bc-6f99-481a-926d-8094ef9d4282; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjQ1MjQ4MDYsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMzZhMWYwZWM5ZDIyMWU3NDhmYzNhZSIsIm1vYmlsZSI6IjgyMTA5NTA5MjYiLCJpZCI6MTUwMzE2NzAyMCwiZXhwIjoxNjY0NjExMjA2LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiYXV0b21hdGlvbi51aTExMTFAZW1iaWJlLmNvbSJ9.A9RO4-WU_n4SsgQdA2CKRiOg4mbOxAiFKYyRX2mXMyWwAMWYtnFy7rHlxwFX1OaIrIeAQBcHIFaSblPjGZ_MzA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjQ1MjQ4MDYsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMzZhMWYwZWM5ZDIyMWU3NDhmYzNhZSIsIm1vYmlsZSI6IjgyMTA5NTA5MjYiLCJpZCI6MTUwMzE2NzAyMCwiZXhwIjoxNjY0NjExMjA2LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiYXV0b21hdGlvbi51aTExMTFAZW1iaWJlLmNvbSJ9.A9RO4-WU_n4SsgQdA2CKRiOg4mbOxAiFKYyRX2mXMyWwAMWYtnFy7rHlxwFX1OaIrIeAQBcHIFaSblPjGZ_MzA'
    }

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v2/check-admin-exists?email=perf_test_QA_backend_automation{randint(1, 100000)}@embibe.com"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
