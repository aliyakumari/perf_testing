from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import pandas as pd
import json

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {
            "mobile": "9982824470",
            "personaType": "Administrator"
        }
        self.headers = {
            'reseller-jwt-token': '',
            'Accept': 'application/json, text/plain, */*',
            'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
            'sec-ch-ua-platform': '"macOS"',
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=72012D8326BBCF095D79F2DCB52972D5; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjYxMjM3MDg2NjExIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzVmTWlJc0ltOXlaMTlwWkNJNklqRWlMQ0owY21GamExOXBaQ0k2SW1WaVpXSTVPREZoTFdJNFlXWXROR1ZsTVMwNE1tVTRMVFJqTURZNFpEY3laamxtTXlJc0ltOTBjRjl5WlhGMVpYTjBYMmxrSWpvaVpqZzFPVGhqTnpFdE9HSXlaaTAwWXpOakxUazBOak10T1RZek9HTmtZakkzWlRobElpd2lhV1FpT2lJNE1qRXdPVFV3T1RJMklpd2liRzlqWVd4bElqb2laVzRpTENKd2JHRjBabTl5YlNJNklsZEZRaUo5LkFjQl9WZkc5SnI1RTRqc055cnZ0ajMzZ1JnTFV1MDFjR1Y5WVlxeWtLQXdQM1RLdWsxMXJlbHV4QTdYOUlBVExOWVJuMVp6cjh2N3dkWjY3YnJMbmVRIiwiY3JlYXRlZCI6MTY3ODA5MTQ4NCwib3RwX29wZXJhdGlvbl90eXBlIjoiU2lnbkluIiwiYXR0ZW1wdCI6MSwidHRsIjoiNjAwIiwicGxhdGZvcm0iOiJXRUIiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiODIxMDk1MDkyNiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MiwiaWQiOjE1MDAwMjExOTUsImV4cGlyeSI6MTY3ODE3Nzg4NCwiam91cm5leV9pZCI6IlNJR05fSU5fODYyYzlmZTMtNGYyOC00ODM0LTg1NjgtMGYzMDJkOGJlMzQyIiwiZXhwIjoxNjc4MTc3ODg0fQ.ZGdzAO3a_TlkmSZ0AoLvIl7nW4eGPRVmaVUEQLDx_FPpPZ50Yw0xntS4TyKSy-8b01ejIyRDxY1YO9g84DVUew; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Imt1bWFyaSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTA2IDA5OjE0OjM2IFVUQyIsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJyb290T3JnSWQiOiI2NDA1OGIyNjczZjMxNzAwMjdjYTgxYWIiLCJmaXJzdE5hbWUiOiJBbGl5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQwNThiMjY3M2YzMTcwMDI3Y2E4MWFiIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQwNThiMjc3M2YzMTcwMDI3Y2E4MWFjIiwiaWQiOjE1MDQ5NTI1NzYsImVtYWlsIjoibG9hZHRlc3QxMjVAZ21haWwuY29tIn0.l3g0xsRJtqaMCy-nHMuQA2WYJsRgzghgN4jsO8NOV4hH2NkNvO63kfFdAFZ0cpqaaEszhTwAqmFLhz3SBGgw9w; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
        }

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/narad/v2/availableOrgs"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
