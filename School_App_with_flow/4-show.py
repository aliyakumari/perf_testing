from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import time
import gevent
import resource
import pandas as pd

host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('reseller_token.csv')
reseller_token = df.values.tolist()

reseller_jwt_tokens = []

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Cookie': 'JSESSIONID=71C3345560106C6FC6E91194B7804FBF; _ga_7N8QWZ4X8C=GS1.1.1655897351.1.1.1655897974.59; ajs_anonymous_id=f37770f3-7990-4ff0-b60b-46abc4a1f1e8; WZRK_G=5fc78604fc1c4c739916e8ffb0748d10; V_ID=ultimate.2022-09-14.419ba0bdf228a7abb0f6029c9bea68a8; _gcl_au=1.1.1157395199.1663141141; ajs_user_id=1843066425; embibe-refresh-token=5a9cf5ea-ff4a-4e4d-8f1f-a2840108d902; prod_embibe-refresh-token=1db16dcf-cdaf-4289-b880-2a8af93759b3; _ga_TT8L73VP3H=GS1.1.1663826914.23.0.1663826914.60.0.0; preprod_embibe-refresh-token=7faf7732-0cce-4e5b-bc36-062fb18dbfe3; __insp_wid=875671237; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tLw%3D%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_sess=true; _ga_YHBNHKH43S=GS1.1.1664783284.89.1.1664783293.51.0.0; _ga=GA1.1.1423279882.1655298606; __insp_slim=1664783293888; _ga_QQPR95BLCG=GS1.1.1665766118.6.1.1665766199.60.0.0; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MzQ5OTM2NzgxNGYwMzA0ZjZkZjAxMmIiLCJzdWJkb21haW4iOiJodHRwczovLzYzNDk5MzY3ODE0ZjAzMDRmNmRmMDEyYi1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.DZVuZVUviZnfTTQmEeak3kM2fYTa0EEWjP9UPO4j667no3Jhf-hUB_09oH4ESkmSUPpf5Wu6q2s1iZHYX5g7xg; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=43c5555e-cf32-4d6e-838f-233cbd5c9a9a; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjU3NjYyNDgsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk5MzY3ODE0ZjAzMDRmNmRmMDEyYiIsIm1vYmlsZSI6Ijg4MTk4MTI3MjgiLCJpZCI6MTUwMzE4MDUyNiwiZXhwIjoxNjY1ODUyNjQ4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZF90ZXN0QGVtYmliZS5jb20ifQ.s66Z30uYaQXsTImaOAoMBMEyraG-_kJKzs-VGeatuVzDbvOj9q0tYk74eQNCCamDvsG6eFLaDhiw8OZeDR32vg; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjU3NjYyNDgsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk5MzY3ODE0ZjAzMDRmNmRmMDEyYiIsIm1vYmlsZSI6Ijg4MTk4MTI3MjgiLCJpZCI6MTUwMzE4MDUyNiwiZXhwIjoxNjY1ODUyNjQ4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZF90ZXN0QGVtYmliZS5jb20ifQ.s66Z30uYaQXsTImaOAoMBMEyraG-_kJKzs-VGeatuVzDbvOj9q0tYk74eQNCCamDvsG6eFLaDhiw8OZeDR32vg; JSESSIONID=541A7F87D3CEB6DC3E82BB66E59015E3; embibe-refresh-token=d671179d-b4bd-4ca1-ab6c-1d6b43398eb4; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJMb2FkX3Rlc3QiLCJsYXN0TmFtZSI6IkxvYWRfdGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMC0xNCAxNjo1NDo0MyBVVEMiLCJwYXJlbnRPcmdJZCI6IjYzNDk5MzY3ODE0ZjAzMDRmNmRmMDEyYiIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODgxOTgxMjcyOCIsImFkbWluSWQiOiI2MzQ5OTM2ODgxNGYwMzA0ZjZkZjAxMmMiLCJpZCI6MTUwMzE4MDUyNiwicm9vdE9yZ0lkIjoiNjM0OTkzNjc4MTRmMDMwNGY2ZGYwMTJiIiwiZW1haWwiOiJsb2FkX3Rlc3RAZW1iaWJlLmNvbSJ9.qWbz11IxDxhtqugj4sbUXaMynBlmcMvtUVVNPUMCW6ywLvjce9FrOwd5L4Yg91rArclz23tX5fOEQokNeeRUDQ; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=e53e2fbb-67fd-4ae2-9cf6-a3a38a2eda46; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjU3NjYxNzQsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk4NGE1ODE0ZjAzMDRmNmRmMDBiMSIsIm1vYmlsZSI6Ijk5MTA5Mzk3NjEiLCJpZCI6MTUwMzE4MDUyMCwiZXhwIjoxNjY1ODUyNTc0LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiNDM4ODE3MDQ0MWFsaXlhX2xvYWRfdGVzdC40Mzg4MTcwNDQxQGVtYmliZS5jb20ifQ.Aid1Kn4_KE5TJnuJA_Cs9bLn0P_LXzf_d5tUkDFCOBBYorBjrOFi8-vJvwrTR1pf27sQUCuD_Lgwby3zg6mdcA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjU3NjYxNzQsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk4NGE1ODE0ZjAzMDRmNmRmMDBiMSIsIm1vYmlsZSI6Ijk5MTA5Mzk3NjEiLCJpZCI6MTUwMzE4MDUyMCwiZXhwIjoxNjY1ODUyNTc0LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiNDM4ODE3MDQ0MWFsaXlhX2xvYWRfdGVzdC40Mzg4MTcwNDQxQGVtYmliZS5jb20ifQ.Aid1Kn4_KE5TJnuJA_Cs9bLn0P_LXzf_d5tUkDFCOBBYorBjrOFi8-vJvwrTR1pf27sQUCuD_Lgwby3zg6mdcA',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NjU3NjYyNDgsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk5MzY3ODE0ZjAzMDRmNmRmMDEyYiIsIm1vYmlsZSI6Ijg4MTk4MTI3MjgiLCJpZCI6MTUwMzE4MDUyNiwiZXhwIjoxNjY1ODUyNjQ4LCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibG9hZF90ZXN0QGVtYmliZS5jb20ifQ.s66Z30uYaQXsTImaOAoMBMEyraG-_kJKzs-VGeatuVzDbvOj9q0tYk74eQNCCamDvsG6eFLaDhiw8OZeDR32vg',
  'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        token_data = []
        rnum = randrange(len(reseller_token))
        self.headers['reseller-jwt-token'] = reseller_token[rnum][1]
        url = f"/narad/v2/show"
        response = self.client.get(url, data=self.body, headers=self.headers)
        token_data.append(response.headers['reseller-jwt-token'])
        token_data.append(reseller_token[rnum][0])
        reseller_jwt_tokens.append(token_data)
        df = pd.DataFrame(reseller_jwt_tokens)
        df.to_csv('reseller_token_admin.csv', index=False)






def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
