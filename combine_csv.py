import glob
import os
import os
import sys

import traceback
import pandas as pd

folder_name = sys.argv[1]


def combine_csvs():
    os.system("sudo rm -r Results")
    os.system("sudo mkdir Results")

    all_files = glob.glob(f"{folder_name}/Results/*stats.csv")
    print("Combining csvs: ", len(all_files))

    try:
        df_list = []
        for csv in all_files:
            try:
                df = pd.read_csv(csv)
                df_list.append(df)
            except Exception as e:
                print(e)
                print(csv)

        df = pd.concat(df_list).drop_duplicates()
        df = df.drop_duplicates()

        name = folder_name.split("/")
        df.to_csv(f"Results/combined_results_{name[len(name) - 1]}.csv", index=False)
    except Exception as e:
        print(e)


combine_csvs()
