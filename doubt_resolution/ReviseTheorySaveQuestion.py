from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

df = pd.read_csv("data.csv")
df = df.values.tolist()
all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'auth-token': 'g8LeVSfKzuwfuoY88kdSkNG0pQ7zdmtBzQjxC1CP8GI',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9'
                            '.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMjg4NDE2LCJjcmVhdGVkIjoxNjQwNTk4MTk1LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDExOTcxNTEsImV4cCI6MTY0MDc3MDk5NSwiZGV2aWNlSWQiOiIxNjM5MDgwNDI3NjIwIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMjg4NDE2XzgyMjkzNzA4MTEwMzUyNkBlbWJpYmUtdXNlci5jb20ifQ.Ac9jDtWvRU1Vc7cPSZblRpsOBLUAnhDuh_2W0b3BAKgyeNpUNsooUHnAuz_WuxDGuU4qGzPFreENFHpM4UtFZw',
            'Content-Type': 'application/json'
        }


    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(df))
        try:
            self.body = json.dumps({
                "question_id": df[rnum][2],
                "user_answer": df[rnum][3]
            })
        except AttributeError:
            pass

        self.headers['embibe-token'] = df[rnum][0]
        url = f"/dsl-revise-book-theory/api/v1/{df[rnum][1]}/save-question-answer"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
