import json
import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
df = pd.read_csv('txt.csv')
Questions = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'auth-token': 'PJ45UMuJZ1v03GQImeFJTRbslW9a46gWFxnrhD3tX6Q',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzg2OTk2NzEsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDIzNTA5NDIyIiwiaWQiOjE1MDQ5NjUyNDksImV4cCI6MTY3ODc4NjA3MSwiZGV2aWNlSWQiOiIxNjc4Njk5NjU2NDQzIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJwbGF0Zm9ybSI6IldFQiJ9.-hhNGJdWU-kVesWB3B0QxR-OIzPQ6J0Dy9C4YrrrKsT3VIPmQR5WaxD1I65aanwKJN9aqNv1HjaUap5R00SvxA',
            'embibe-token-type': 'profile',
            'Content-Type': 'application/json'
        }

        self.body = json.dumps({
            "top_k": 5,
            "locale": "kn",
            "question_text": "ಕ್ಷಾರ ಅಂಶಗಳಲ್ಲಿ ಯಾವ ಧಾತುವು ಘನ ಬೈಕಾರ್ಬನೇಟ್&zwnj;ಗಳನ್ನು ರೂಪಿಸುವುದಿಲ್ಲ?"

        })

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        num = randrange(len(Questions))
        quest = Questions[num][0]
        self.body = json.dumps({
            "top_k": 5,
            "locale": "en",
            "question_text": quest

        })
        response = self.client.post("/dsl-doubt-resolution/api/v1/whatsapp-bot?cluster_search=true&source_type=whatsapp&return_ocr=false", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
