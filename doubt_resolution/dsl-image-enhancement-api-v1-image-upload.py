import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': '*',
            'Content-Type': 'application/json',
            'auth-token': 'Rd9dh9dpcjJIWA66dxX5jHyeTdSf6YEd026GTL3Ezmc',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
        }

        self.body = {}
        self.files = [
            ('uploaded_im', ('DR-DSL.jpeg',
             open('DR-DSL.jpeg', 'rb'), 'image/jpeg'))
        ]

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/dsl-image-enhancement/api/v1/image-upload", data=self.body,
                                    headers=self.headers, files=self.files)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
