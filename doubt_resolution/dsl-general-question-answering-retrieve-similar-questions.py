import json
import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
df = pd.read_csv('txt.csv')
questions = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzg2OTE3NzYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5MDMzNzk2ODk5IiwiaWQiOjE1MDAwMTkzMzMsImV4cCI6MTY3ODc3ODE3NiwiZGV2aWNlSWQiOiJ0ZW1wLXNlc3Npb24tZTFmMWYxNTRmYjc0YTY3YWU3YjQ2M2ZlNGY3MWUzNGYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6dHJ1ZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.zGSHm9c_GpAEps5O429JFa0Qr2jPzSwVhzVkDG6fp9url1PlsArNBP8Iti18rbxwSNbInKt-ED6tlIc6art7bg',
            'embibe-token-type': 'account',
            'auth-token': 'QVnIKazFBoTj0UPO0usjm5h79kbRpEpH6DdIwFUpcuw',
            'Content-Type': 'application/json',
        }

        self.body = json.dumps({
            "question_text": "अवक्षेपण अभिक्रिया के  उदाहरण",
            "k": 5
            })

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        num = randrange(len(questions))
        quest = questions[num][0]
        self.body = json.dumps({
            "question_text": quest,
            "k": 5
        })
        response = self.client.post("/dsl-general-question-answering/api/v3/retrieve-similar-questions?cluster_search=true&source_type=other", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
