import json
import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
df = pd.read_csv('Image_Url.csv')
urls = df.values.tolist()
urls.remove(urls[0])

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'auth-token': 'Rd9dh9dpcjJIWA66dxX5jHyeTdSf6YEd026GTL3Ezmc',
        }

        self.body = json.dumps({
            "image_url": "https://cdn.yellowmessenger.com/533970db9b82c962186fe4c7df744373/603766058014130.jpg",
            "type": "PIL"
        })

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        num = randrange(len(urls))
        url = urls[num][0]
        self.body = json.dumps({
            "image_url": url,
            "type": "PIL"
        })
        response = self.client.post("/dsl-image-enhancement/api/v1/crop", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
