import requests
import json
import pandas as pd


def creatEmbibeToken():
    embibe_token = []
    for i in range(0, 10000):

        url = "https://preprodms.embibe.com/user_auth_ms/user/details?mobile=8210950926"

        payload = {}
        headers = {
            'auth-token': 'ipTt6XyE4e67QDvRy4iw8a3XLH2P4vaW2P5y59Sg4Tvbgg33xqw6fuh9QDv8Wrc88QyzJTFUEQmgxpjpnrE5GQ8KEAHFtAZ5TarC5MqSZg4xexbUQWNBNK2zPafthyyDzdDiS8KH9CaYPBDMJgBxNeG62BNpUBXADcSSgJGHGaaBUPYiMeKDCnZyxAr2fDQEJg8V4cpv'
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        for token in response.json().get('data').get('profiles'):
            embibe_token.append(token.get('embibe-token'))

    return embibe_token


def createSession():
    session_data = []
    for i in range(0, 1000):

        url = "https://preprodms.embibe.com/user_auth_ms/user/details?mobile=8210950926"

        payload = {}
        headers = {
            'auth-token': 'ipTt6XyE4e67QDvRy4iw8a3XLH2P4vaW2P5y59Sg4Tvbgg33xqw6fuh9QDv8Wrc88QyzJTFUEQmgxpjpnrE5GQ8KEAHFtAZ5TarC5MqSZg4xexbUQWNBNK2zPafthyyDzdDiS8KH9CaYPBDMJgBxNeG62BNpUBXADcSSgJGHGaaBUPYiMeKDCnZyxAr2fDQEJg8V4cpv '
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        data = []
        for token in response.json().get('data').get('profiles'):
            embibe_token = token.get('embibe-token')
            data.append(embibe_token)
            session_url = "https://preprodms.embibe.com/dsl-revise-book-theory/api/v1/create-quiz-session"
            session_payload = json.dumps({
                "locale": "en",
                "goal": "blc28",
                "exam": "blc228",
                "subject": "blc493",
                "book": "blc3067",
                "chapter": "blc21296"
            })

            session_headers = {
                'auth-token': 'g8LeVSfKzuwfuoY88kdSkNG0pQ7zdmtBzQjxC1CP8GI',
                'embibe-token': embibe_token,
                'Content-Type': 'application/json'
            }

            session_response = requests.request("POST", session_url, headers=session_headers, data=session_payload)
            try:
                if session_response.json().get('data').get('session_id') is not None:
                    data.append(session_response.json().get('data').get('session_id'))
                    data.append(session_response.json().get('data').get('question').get('question_id'))
                    data.append(session_response.json().get('data').get('question').get('answer'))
                    session_data.append(data)
            except:
                pass

    df = pd.DataFrame(session_data)
    df.to_csv("data.csv", index=False)
    return session_data
