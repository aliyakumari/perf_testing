import json
import resource
import time
from random import randrange

import gevent
import pandas as pd
from locust import between, events, task
from locust.contrib.fasthttp import FastHttpUser
from locust.runners import (STATE_CLEANUP, STATE_STOPPED, STATE_STOPPING,
                            WorkerRunner)

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"


all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val
df = pd.read_csv('url_paths_bytes_long.csv')
urls = df.values.tolist()
urls.remove(urls[0])

df = pd.read_csv('Embibe_Token - TOKEN.csv')
token = df.values.tolist()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'auth-token': 'W5qrsEHWuV1JTYkAdRoX2FCgpOOmSfe9h0qEOwhczHS',
            'embibe-token-type': 'profile',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2ODAwODg5MzgsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5NjUwMTc4MTAxIiwiaWQiOjE1MDE5MDA4MjUsImV4cCI6MTY4MDE3NTMzOCwiZGV2aWNlSWQiOiJ0ZW1wLXNlc3Npb24tNjJjYjUxYzIxN2Y4OGJjM2Q3ZjczM2NmMDA2OTM4ODQiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6dHJ1ZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.bjH0YZw17E2sMz3kanwiuuoaorQSl68x5I53sthYwXzfHYIilFJV8UfnxqRWjGxvw2TnKV7DsOClG0kSO1BaFA',
            'Cookie': 'embibe-refresh-token=1db52aef-297c-49a8-b87b-063014fa7dff; preprod_embibe-refresh-token=0c1558b6-b811-4e79-b08a-32f17bc49a64; preprod_embibeops-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6IjY0MjEzYzQ4YzFjM2NlMzhjZjlmOGYyZSIsInR5cGUiOiJFTUJJQkVfT1BTIiwiZXhwIjoxNjc5OTg2MzcyLCJlbWFpbCI6InVtYW5nLnZhcnNobmV5QGVtYmliZS5jb20ifQ.nwry7cThAvb0oWs92_THF1RXfntnUbv5sP-NZAtmlWsLxaRWlqBtbceAknLzXSu3nfq82DrhosirVWmN2TF4PQ; school_preprod_embibe-refresh-token=322ed536-0bed-43ab-86cd-5246cee85aec; school_prod_embibe-refresh-token=20b87024-1ec3-43c7-90e3-32fce85fcf58; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        self.body = {}
    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @ task
    def userDB(self):
        num = randrange(len(urls))
        url = urls[num][1]
        token_rnum = randrange(len(token))
        self.body = json.dumps({
            "keep_duplicate": True,
            "metatags_type": "all",
            "image_content": url,
            "locale": "en",
            "search_type": "es",
            "k": 2,
            "method": "embibe-lens-search",
            "overlaps": "NO_SUB",
            "doc_fields": "weight,entity_type,subject",
            "query_expansion": "meta_tags",
            "fq": ["asset_type:3d"],
            "goal": None,
            "exam": None
        })
        self.headers['embibe-token'] = token[token_rnum][1]
        response = self.client.post("/embibe-lens-search/api/v1/dsl-entity-detection?raw_data_flag=false", data=self.body,
                                    headers=self.headers)
        print(response.text)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
