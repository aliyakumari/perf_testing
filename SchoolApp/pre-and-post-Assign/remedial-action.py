from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
import time
import json
import logging
import gevent
import resource
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

# @events.test_start.add_listener
def fetch_data():
    logger.info(
        "\n\n-----------------------Initiating DB Connection to Fetch Data--------------------------\n")

    db_url = 'mongodb://preprdpsgeneratepreprodembibumrwuser:rff3efaf233d2wdd42345frdeer@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/ps_generate_preprod?authSource=admin&readPreference=primary&ssl=false'
    db_name = 'ps_generate_preprod'
    collection_name = 'atgV3Collection'
    db = DbConnector(db_url, db_name, collection_name)
    query = {}
    projection = {"slotId": 1, "classIds": 1, "subjectId": 1}
    cursor = db.fetch_data(query, dict(projection))

    for doc in cursor:
        meta_data = []
        slotId = doc.get('slotId')
        class_ids = doc.get('classIds')
        subjectId = doc.get('subjectId')

        if slotId != None and class_ids != None and subjectId != None:

            kv_code = subjectId.split('|')
            kv_code = kv_code[-1]
            meta_data.append(class_ids[0])
            meta_data.append(slotId)
            meta_data.append(kv_code)
            data.append(meta_data)

    logger.info(
        "\n\n-----------------------Closing DB Connection of Fetch Data--------------------------\n")
    
    return data


class UserBehaviour(FastHttpUser):
    
    fetch_data()
    
    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/track/v2/remedial-action?locale=en", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNoZWlraCIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTE1IDE0OjAyOjEzIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4OTk4OTk5MDAiLCJyb290T3JnSWQiOiI2MjIxZmZlZGFmOTZlMTQ0ODNhMjJlZjYiLCJkZXZpY2VJZCI6IjAuOTA4ODAxNDg3NTg3MzQ1OCIsImZpcnN0TmFtZSI6IkFud2FyIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6MywicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.v268aLvtyRDlhE89akTBDrP64p9sz6UXbMfCqBoP_ObNp6_a3mD7CDcOuq_z9ydMK0yNbFDn2wBYXA6ep2ROKQ',
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        self.body = {}
        self.data = data

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(self.data))
        self.body = {
            "kvCode": self.data[rnum][2],
            "classId": self.data[rnum][1],
            "slotId": self.data[rnum][0],
            "isLiveClass": False
        }
        url = '/track/v2/remedial-action?locale=en'
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
