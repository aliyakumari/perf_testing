from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
import time
import json
import logging
import gevent
import resource
from utils import DbConnector, ResultSaver


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

logger = logging.getLogger(__name__)
data = []

# @events.test_start.add_listener
def fetch_data():
    logger.info(
        "\n\n-----------------------Initiating DB Connection to Fetch Data--------------------------\n")

    db_url = 'mongodb://preprdpsgeneratepreprodembibumrwuser:rff3efaf233d2wdd42345frdeer@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/ps_generate_preprod?authSource=admin&readPreference=primary&ssl=false'
    db_name = 'ps_generate_preprod'
    collection_name = 'atgV3Collection'
    db = DbConnector(db_url, db_name, collection_name)
    query = {}
    projection = {"slotId": 1, "classIds": 1,
                    "subjectId": 1, "dueDate": 1, "schoolId": 1, "topicList": 1}
    cursor = db.fetch_data(query, dict(projection))

    for doc in cursor:
        meta_data = []
        slotId = doc.get('slotId')
        class_ids = doc.get('classIds')
        subjectId = doc.get('subjectId')
        dueDate = doc.get('dueDate')
        schoolId = doc.get('schoolId')
        topicList = doc.get('topicList')

        if slotId != None and class_ids != None and subjectId != None and schoolId != None and dueDate != None and topicList != None:

            kv_code = subjectId.split('--')
            kv_code = kv_code[-1]
            meta_data.append(class_ids[0])
            meta_data.append(slotId)
            meta_data.append(kv_code)
            meta_data.append(dueDate)
            meta_data.append(schoolId)
            meta_data.append(topicList)
            
            data.append(meta_data)
    logger.info(
        "\n\n-----------------------Closing DB Connection of Fetch Data--------------------------\n")

    return data
    
   


class UserBehaviour(FastHttpUser):
    
    fetch_data()

    @events.test_stop.add_listener
    def push_data(environment):
        logger.info(
            "\n\n-----------------------Initiating Data Pushing--------------------------\n")
        db_url = 'mongodb://prprdembiberwuserqamongodbpreprod:Qtttqwerfvgutiourf54678hh2334rfffgfgf12@vm-preprod-mongodb-001.srv.embibe.com:27017,vm-preprod-mongodb-002.srv.embibe.com:27017,vm-preprod-mongodb-003.srv.embibe.com:27017/qa-mongodb-preprod?replicaSet=rs0&authSource=admin'
        db_name = 'qa-mongodb-preprod'
        collection_name = 'performance_testing'
        db_save = ResultSaver(db_url, db_name, collection_name)
        db_save.save_results(
            "/de/generic/query?id=bfeb93bc-5112-4a96-b030-37ab3fb01f07", 'POST', environment)

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'application/json',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        self.body = {}
        self.data = data

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(self.data))
        subjectCode = self.data[rnum][2]
        classId = self.data[rnum][0]
        schoolId = self.data[rnum][4]
        topic_codes = self.data[rnum][5]
        self.body = {
            "school_id": schoolId,
            "class_id": classId,
            "subject_id": subjectCode,
            "kv_codes": topic_codes
        }

        url = f"/de/generic/query?id=bfeb93bc-5112-4a96-b030-37ab3fb01f07"
        response = self.client.post(
            url, data=json.dumps(self.body), headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
