# -*- coding:utf-8 -*-
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import json
import uuid
import time
import gevent

from websocket import create_connection
import six
from random import randint, randrange
from utils.CreateToken import creatEmbibeToken
from locust import HttpUser, TaskSet, task, events




class ChatLocust(HttpUser):

    def on_start(self):

        self.start_at = time.time()
        ws = create_connection(f"wss://preprodms.embibe.com/coobo_ms/socketio/?emit=true&userId={randint(1,1000)}&sessionId=zoom-6222034009a8894bfaafaf6d&teacher=true&EIO=4&transport=websocket")
        self.ws = ws


        def _receive():
            while True:
                ws = create_connection(f"wss://preprodms.embibe.com/coobo_ms/socketio/?emit=true&userId={randint(1,1000)}&sessionId=zoom-6222034009a8894bfaafaf6d&teacher=true&EIO=4&transport=websocket"
                    )
                res = ws.recv()
                data = res
                end_at = time.time()
                response_time = int((end_at - self.start_at) * 1000000)
                events.request_success.fire(
                    request_type='WebSocket Recv',
                    name='test/ws/chat',
                    response_time=response_time,
                    response_length=len(data),
                )

        gevent.spawn(_receive)

    def on_quit(self):
        self.ws.close()

    @task
    def sent(self):

        body = randint(0,10000)
        self.ws.send(str(body))
        events.request_success.fire(
            request_type='WebSocket Sent',
            name='test/ws/chat',
            response_time=int((time.time() - self.start_at) * 1000000),
            response_length=len(str(body)),
        )

    host = 'wss://preprodms.embibe.com'
    min_wait = 0
    max_wait = 100