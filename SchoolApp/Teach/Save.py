from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Content-Type': 'application/json',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; loadtest_ab_version=0; loadtest_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Mzc2NjgxNjYsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI3MDgyNjI3ODIyIiwiaWQiOjUyNDMzMjg0LCJleHAiOjE2Mzg4Nzc3NjYsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2V9.hV3F3CeNrr9JQu4Ed0C9cjJNiwxY4MbbQEI1h0MK2nGO2XvCFR2VqH2sffZIGa4MgtSyXNIcB2MdaARtDFYCUQ; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}



        self.body = json.dumps({
    "data": "{\"labels\":{\"hover\":{\"enabled\":true},\"number\":{\"enabled\":false,\"quizzing\":false}},"
            "\"assets\":{\"textures\":{},\"materials\":{},\"geometries\":{},\"objects\":{},\"lights\":{},\"audio\":{"
            "},\"references\":[]},\"layers\":{\"layer3D\":{\"index\":0,\"name\":\"3D\"},\"layers2D\":{}},\"setups\":{"
            "\"toneMapping\":4,\"toneMappingExposure\":2,\"showAutoPlayBtn\":true,\"gammaInput\":true,"
            "\"gammaOutput\":true,\"postprocess\":{\"fxaa\":true,\"ssaa\":true,\"nonPermanent\":true},"
            "\"background\":{\"type\":\"none\"},\"fog\":{},\"controls\":{\"enableDamping\":true,\"zoomSpeed\":0.18,"
            "\"rotateSpeed\":0.1,\"panSpeed\":0.2,\"zoomDampingFactor\":0.07,\"panDampingFactor\":0.08,"
            "\"rotateDampingFactor\":0.08},\"camera\":{\"fov\":45,\"near\":0.001,\"far\":1000,\"zoom\":1},"
            "\"currentTheme\":{\"name\":\"Embibe\",\"background\":{\"type\":\"color\",\"value\":{\"color\":{"
            "\"primary\":\"#2a2399\",\"secondary\":\"#098cf1\",\"gradient\":{\"enabled\":true,"
            "\"style\":\"diagonal\"}},\"image\":{\"texture\":null,\"textureMobile\":null,\"offset\":{\"x\":0,"
            "\"y\":0},\"scale\":{\"x\":1,\"y\":1}},\"video\":null,\"fitMode\":\"cover\"}},\"headingsOptions\":{"
            "\"Title\":{\"title\":\"Title\",\"font-size\":\"2.4rem\",\"font-family\":\"Gilroy Semibold\","
            "\"font-style\":\"normal\",\"color\":\"#FFF\"},\"Body\":{\"title\":\"Body\",\"font-size\":\"1.8rem\","
            "\"font-family\":\"Gilroy Regular\",\"font-style\":\"normal\",\"color\":\"#FFF\"},\"Small\":{"
            "\"title\":\"Small\",\"font-size\":\"1.2rem\",\"font-family\":\"Gilroy Regular\","
            "\"font-style\":\"italic\"}},\"boxStyles\":{\"Title\":{\"textBoxBgColor\":\"#000\","
            "\"textBoxBgOpacity\":\"25\"},\"Body\":{\"textBoxBgColor\":\"#000\",\"textBoxBgOpacity\":\"0\"}}},"
            "\"autoplayDelay\":10,\"refsUpdateHistory\":{},\"differentialSlidesAnimation\":true},\"liveLabels\":[],"
            "\"quizData\":{},\"slides\":[{\"notes\":\"\",\"background\":{\"type\":\"color\",\"value\":{\"color\":{"
            "\"primary\":\"#2a2399\",\"secondary\":\"#098cf1\",\"gradient\":{\"enabled\":true,"
            "\"style\":\"diagonal\"}},\"image\":{\"texture\":null,\"textureMobile\":null,\"offset\":{\"x\":0,"
            "\"y\":0},\"scale\":{\"x\":1,\"y\":1}},\"video\":null,\"fitMode\":\"cover\"}},\"camera\":{\"position\":{"
            "\"x\":2.5,\"y\":2.1648901405887235e-16,\"z\":2.4999999999999996},\"target\":{\"x\":0,\"y\":0,\"z\":0},"
            "\"zoom\":1,\"fov\":45,\"gate\":null},\"controls\":{\"autoCentering\":false,\"autoRotate\":false,"
            "\"interactionType\":\"objectRotation\",\"objectRotationMode\":\"orbit\",\"version\":1},"
            "\"dynamicPerspective\":true,\"animation\":{},\"visibilityType\":\"hidden\",\"renderer\":{\"top\":0,"
            "\"left\":0,\"width\":1,\"height\":1},\"layers\":{\"Renderer\":true},"
            "\"uuid\":\"4233232A-AC3B-407F-BC27-4808271B3F47\",\"audioThisSlideOnly\":true,\"cameraLimits\":{"
            "\"version\":4,\"zoomLimitEnabled\":false,\"minDistance\":3,\"maxDistance\":7,"
            "\"horizontalAngleLimitEnabled\":false,\"minAzimuthAngle\":0,\"maxAzimuthAngle\":90,"
            "\"verticalAngleLimitEnabled\":false,\"minPolarAngle\":0,\"maxPolarAngle\":90,\"panLimitEnabled\":true,"
            "\"panLimits\":{\"center\":{\"x\":0,\"y\":0,\"z\":0},\"quaternion\":{\"x\":0,\"y\":0,\"z\":0,\"w\":1},"
            "\"offset\":0},\"objectScaleLimitEnabled\":true,\"objectScaleLimit\":2},\"visible\":true,"
            "\"title\":\"Slide\",\"envLight\":\"/assets-qbix/F16EFB92-4B2C-46EB-8F86-8AC2322E4FAE.env\",\"fog\":{},"
            "\"layers2D\":{},\"selectedAt\":1637676100382}],\"s3Id\":\"E0ADB0D0-4C65-11EC-9F36-F9B76722587B\","
            "\"highlightColor\":7864183,\"title\":\"Untitled\",\"description\":\"\",\"views\":0,\"public\":false,"
            "\"ratio\":\"4:3\",\"id\":\"new\",\"_id\":\"new\","
            "\"createdWith\":\"34a71c4a7fd7adaffaf6e1d923162b563fd69556\",\"owner\":{\"confirmed\":true,"
            "\"blocked\":false,\"_id\":\"60a62bbc271f372dc8ecf034\",\"username\":\"admin\","
            "\"email\":\"admin@admin.com\","
            "\"password\":\"$2a$10$xmcP4RRbP3i7lR8sVL6SUeQel3BDhuzkrWQdK/DCohpVedlCVpuha\",\"superUser\":true,"
            "\"cooboTeam\":true,\"provider\":\"local\",\"createdAt\":\"2021-05-20T09:28:28.852Z\","
            "\"updatedAt\":\"2021-05-20T09:28:28.860Z\",\"__v\":0,\"role\":{\"_id\":\"60a5201c2a9b87038030ac7b\","
            "\"name\":\"Authenticated\",\"description\":\"Default role given to authenticated user.\","
            "\"type\":\"authenticated\",\"createdAt\":\"2021-05-19T14:26:36.948Z\","
            "\"updatedAt\":\"2021-05-20T09:30:02.221Z\",\"__v\":0,\"id\":\"60a5201c2a9b87038030ac7b\"},"
            "\"sharePermissions\":[],\"avatar\":null,\"id\":\"60a62bbc271f372dc8ecf034\",\"demos\":[],"
            "\"slidenotes\":[]},\"textContent\":\"\",\"updatedBy\":\"admin(admin@admin.com)\","
            "\"savedWith\":\"34a71c4a7fd7adaffaf6e1d923162b563fd69556\",\"hierarchy\":[]}",
    "img": None,
    "type": "demo"
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/coobo_ms/save", data=self.body,
                                    headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
