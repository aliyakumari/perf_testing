from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../../Report/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Content-Type': 'application/json',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyX3R5cGUiOjEsInBhcmVudF91c2VyX2lkIjoxNTAwMDU3NDc5LCJjcmVhdGVkIjoxNjM2OTc5NzI3LCJvcmdhbml6YXRpb25faWQiOiIxIiwiaWQiOjE1MDAwNTc0ODAsImV4cCI6MTYzNzE1MjUyNywiZGV2aWNlSWQiOiIxNjM2OTc5NzE4MDM2IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiIxNTAwMDU3NDc5Xzc0NTg1NDIzMDgzMjA3NDNAZW1iaWJlLXVzZXIuY29tIn0.IiN_2Wh1AP3bI12moAgHGtgRTN3pzT8onfoLWzxaUIbdJd7Kfuk6ndXlpa6ofXcpgBdr7myqQRqrnikcMUZgQw',
  'Cookie': 'embibe-refresh-token=118a2062-5568-4857-8d63-6d38fedd8c14; preprod_embibe-refresh-token=18bcc4d0-e732-4675-a2bc-39d91188fdf5; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlYiLCJ0aW1lX3N0YW1wIjoiMjAyMS0wOC0yMCAwOTozODozNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI3NjU0MzIxNzg5Iiwicm9vdE9yZ0lkIjoiNjExMGY1N2Q2YjEwZTkwMzhmMmY5MDZmIiwiZmlyc3ROYW1lIjoiU2Fua2FyIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwicGFyZW50T3JnSWQiOiI2MTEwZmJlOTZiMTBlOTAzOGYyZjkwOTQiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MTEwZmJlYTZiMTBlOTAzOGYyZjkwOTYiLCJpZCI6MTUwMDA0ODE1NSwiZW1haWwiOiJzYW5rYXIudkBlbWJpYmUuY29tIn0.0NQ9yewr9NbDFAAL-F4K80PMSSnTTjte2Nn5y4lQyTrpx4RiLt2nZvE86NONc3NvDBRSzZAawaUIl8SYf5OBbA; embibe-refresh-token=118a2062-5568-4857-8d63-6d38fedd8c14; preprod_embibe-refresh-token=ecd84549-41cd-4eef-b752-f9a763ead303; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTEgMDY6MTI6MTcgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.O0X3nEL4Mf11PZAiDxBBlXeJyt05p44atlGYKvxdFN4-0B90I5cH4EkckGiSU2og4V-jTtN8v92qRQjx_iRLtw; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTUgMDc6NDk6NDcgVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.sF0JknB7q-c2qyGPMRd3YeAGQ_bbojyqK3xgtZlDBUVHfN33ru3-a3G-blo0odwGbPlN7SmA_ujv1XRNkLBBHQ'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/calendar/student/v1/?startTime=1632742006&endTime=1633174006", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
