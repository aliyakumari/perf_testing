from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../../Report/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers =  {
  'Connection': 'keep-alive',
  'Content-Length': '0',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xMyAxMDo1MDo0NCBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.b9dOMr1xAOYXJlPYxrWZMvSgQCXcfvcn6QLBC07sSuINeKPqUbGFc4WZwH-_HuceTL7Iyt-avGSsqSsOm4Ynww',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
  'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': '_ga=GA1.2.693867182.1629562474; WZRK_G=7077dad7bf0f4854b41fd196036316af; _gcl_au=1.1.2071031195.1633073202; prod_ab_version=0; prod_embibe-refresh-token=9c8f8cbc-c656-4aa7-b23f-d757b94dc084; __insp_uid=1751496284; demo_embibe-refresh-token=ace96613-3640-41ad-b161-0f65423ea422; demo_ab_version=0; _hp2_id.3200020366=%7B%22userId%22%3A%227268306340091084%22%2C%22pageviewId%22%3A%226914221354918342%22%2C%22sessionId%22%3A%22632525864388602%22%2C%22identity%22%3A%221500019336%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; ajs_user_id=1500032266; ajs_anonymous_id=%22758744d9-7648-477f-a7e1-cddcb56e004f%22; _hp2_id.2573504162=%7B%22userId%22%3A%224994825314556702%22%2C%22pageviewId%22%3A%226595604763861399%22%2C%22sessionId%22%3A%221324985127185359%22%2C%22identity%22%3A%222000122775%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; session=.eJx9kW9vqjAYxb8K6euFtYiLkiy5bPyZyYCobAhvbkrpBKRiaEF02Xe_gkyXZbuv2pzze9pz8rwDwqu3v6Lc0C3Q3oEUAw14fsTcYHb0bAe5Qdh6vqk4R72NDItFxvzgGYvUNZLc6e5-lIe5eQz39_fg4wZQhrPi9IbAVb2VOU7xH8riLKYyKRkYfDQ6EThOsy1u5E3NcPUDpMATxOscZ_I3e12V9e6SNranebJyYWRbMPRnfYrsauOnBSRG2Twfzf1zbkJnqbbOo3oq9XLnGbrqzc_Bi5LUXFzGHP0s76rsor0Yeq9xgUXNuxKDMV9FKQ7aNGRtcQW6Ar8Agn75yh8KzCw3J6zYJ48PD8lq0cSBBXEwrT8HCOaUXwMOVUXGaC8mnVhubySEJI8ISYEKkpCiqUgbK5Lt-D1drtcFvTxClLRJhs0Jsbk2tV8PM9OaL5f6ofcOO_q_vE9RMC4Is2A8OqeqOU36LadC7LTbWwRlpI5kJE8m2gROpuCM9Dv-iiC5O6bjT6hhQBt9_ANw1suT.YWVO-A.wZ1OEH3xoloT7DB3dBhcV_l0GBw; __insp_wid=875671237; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL2FjY291bnQtdmVyaWZ5; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_sid=1304319643; __insp_pad=6; _hp2_id.2562464476=%7B%22userId%22%3A%228434973324182918%22%2C%22pageviewId%22%3A%223638758179306429%22%2C%22sessionId%22%3A%225375597115099238%22%2C%22identity%22%3A%221500032266%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; __insp_slim=1634041280552; preprod_embibe-refresh-token=ffc52ccf-9682-40a4-a5c1-017d366fcd13; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzQxMjIwMDAsIm9yZ2FuaXphdGlvbl9pZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImlkIjoxNTAwMDQ1MDEyLCJleHAiOjE2MzQyOTQ4MDAsImRldmljZUlkIjoiMC4xNTYzOTkyMDAwMTgzODUzNiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.9i6Qxv6Po5IQ_KjOuNsiRE3_VVtf6eOaas9rwCgT7TCkohvnx-ymPgq8MNopb2k4nLrF-q9OlaOjK92657V4gw; preprod_ab_version=0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xMyAxMDo1MDo0NCBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.b9dOMr1xAOYXJlPYxrWZMvSgQCXcfvcn6QLBC07sSuINeKPqUbGFc4WZwH-_HuceTL7Iyt-avGSsqSsOm4Ynww; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = {}
        self.data = True
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"



    @task
    def userDB(self):
        try :
            if self.data == True:
                self.data = False
                response = self.client.put(
                    f"/calendar/teacher/v1/period/classType?classType=Online&id=614c595407320a0de41e8b75",
                    data=self.body,
                    headers=self.headers)

            if self.data == False:
                self.data = True
                response = self.client.put(
                            f"/calendar/teacher/v1/period/classType?classType=Offline&id=614c595407320a0de41e8b75",
                            data=self.body,
                            headers=self.headers)
        except :
            pass


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
