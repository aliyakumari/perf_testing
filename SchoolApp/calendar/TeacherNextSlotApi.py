from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../../Report/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xMSAwODozNjowNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0._k6c6s7b69L6pWavf4iH1GlmVX3xhAF-KqQsMecMen4ufztBIzjFTqnnKiTSFXD4lmOVoZ_NHGbiuxhRPOpuuQ',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
  'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
  'Content-Type': 'application/json;charset=UTF-8',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-US,en;q=0.9,ta;q=0.8',
  'Cookie': 'reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJzY2hvb2wtdGVzdCIsImxhc3ROYW1lIjoiVGVzdCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0wMy0xMSAxMDoxMTo1NCBVVEMiLCJwYXJlbnRPcmdJZCI6IjYwNDllY2JjNjkyNWQ1NjljNjZiZjEzNyIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MTc0NiwibW9iaWxlIjoiOTg3NjU0MzIxMCIsImFkbWluSWQiOiI2MDQ5ZWNiZDY5MjVkNTY5YzY2YmYxMzkiLCJpZCI6NzI5LCJyb290T3JnSWQiOiI2MDQ5ZWNiYzY5MjVkNTY5YzY2YmYxMzciLCJlbWFpbCI6InNjaG9vbC10ZXN0QGVtYmliZS5jb20ifQ.Si2JwDV7WIOMxPWA9wYQf5SwEuul4lE6O-qqbN4o7ZyQ_lSnDQGf8omJSpKOupsGR6mq-oSws5LEIaP1_TIoeg; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/calendar/teacher/v1/next/slot", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
