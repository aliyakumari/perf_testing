from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json',
  'Cookie': 'JSESSIONID=93C1584D9A78E1E389B34269A6F7C191; _gcl_au=1.1.1851913510.1673592514; WZRK_G=525636bf7c254e868ee74a6a02af8efd; _fbp=fb.1.1673593752126.519356708; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2M2M2NDUzNmNjNjQ1ZTZkZDM5MTZhNzEiLCJzdWJkb21haW4iOiJodHRwczovLzYzYzY0NTM2Y2M2NDVlNmRkMzkxNmE3MS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.jFElYJO_tsihU5ENsz5QKCJS82pigZmOLUtyMrmLBV_3ncMgrUrMKbErOfkvUDbYW2x4zkva-wuY2YYagAnb_A; __insp_wid=1955736467; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS90ZWFjaC82M2M3OWZhOTBhYWVlZDVmMjllMzIzNWM%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_howoften=true; __insp_norec_sess=true; _ga=GA1.1.1595632488.1673592515; __insp_slim=1674028850348; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc0MDI4ODUwLCJnYV9pZCI6IjE1OTU2MzI0ODguMTY3MzU5MjUxNSIsInNvdXJjZV90eXBlIjoiRU1CSUJFX0hPTUUiLCJzb3VyY2UiOiJ0ZXN0IiwiZXhwIjoxNjc1MjM4NDUwLCJ1dWlkIjoiYzU4YmQyN2UtMGY0Yi00ZThlLWI1OGYtNmQ2OTg4MjY3NzM2Iiwic2VuZGVyX2lkIjoidGVzdCJ9.eVE0NX10S2LlVtLL1FVFs3XAYUdmlFIjW0DY1JkmfnE; guest-session-id=c58bd27e-0f4b-4e8e-b58f-6d6988267736; _ga_TT8L73VP3H=GS1.1.1674028714.7.1.1674028856.51.0.0; school_prod_ab_version=0; school_prod_embibe-refresh-token=b16c6779-5c0d-41d8-acc9-5880f81ddf53; school_prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMTY0NDcsIm9yZ2FuaXphdGlvbl9pZCI6IjYyY2U5MTUyZDk5NTgwMGNiMGNmZWMxYSIsIm1vYmlsZSI6IjgxMTk5NjI5MDciLCJpZCI6MjAwNTU5MjIxMywiZXhwIjoxNjc1MzI2MDQ3LCJkZXZpY2VJZCI6IjAuODM0MzQ1MTg4NDAzMjYyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhdHNoZXRlLndlemFoQGdtYWlsLmNvbSJ9.aGyUJWwzBiJQ4Z665mzpUvHbFMO0rhfpOaiq-cBvuLQkcVn70DS2jeH0TCfAkdJTHknl9saFiQhTUMEjwdS4zA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzQxMTY0NDcsIm9yZ2FuaXphdGlvbl9pZCI6IjYyY2U5MTUyZDk5NTgwMGNiMGNmZWMxYSIsIm1vYmlsZSI6IjgxMTk5NjI5MDciLCJpZCI6MjAwNTU5MjIxMywiZXhwIjoxNjc1MzI2MDQ3LCJkZXZpY2VJZCI6IjAuODM0MzQ1MTg4NDAzMjYyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJhdHNoZXRlLndlemFoQGdtYWlsLmNvbSJ9.aGyUJWwzBiJQ4Z665mzpUvHbFMO0rhfpOaiq-cBvuLQkcVn70DS2jeH0TCfAkdJTHknl9saFiQhTUMEjwdS4zA; _ga_7N8QWZ4X8C=GS1.1.1674133334.2.1.1674133335.59.0.0; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=a2d55be1-b020-4c4d-8820-e899642692a3; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzQ0NjAwOTUsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsIm1vYmlsZSI6Ijc2NTQzMjkwNzgiLCJpZCI6MTUwMzE4MDUwOCwiZXhwIjoxNjc0NTQ2NDk1LCJkZXZpY2VJZCI6IjAuNjU2NTI3NzU5NjQxMTU4MyIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoibGFrc2h5YS50ZXN0dGVhY2hlckBlbWJpYmUuY29tIn0.xFTuVbHBgUTYCNzu1FO5YRxerdH7f0B2wzbCUJ6O3aNRPe3pXIx3IKswBJis5tSGa5H50IT6N-z4jhcNIUybeQ; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRlc3RUZWFjaGVyIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMjMgMDc6NDg6MTUgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiNzY1NDMyOTA3OCIsInJvb3RPcmdJZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsImRldmljZUlkIjoiMC42NTY1Mjc3NTk2NDExNTgzIiwiZmlyc3ROYW1lIjoibGFrc2h5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjM0OTc4N2Y4MTRmMDMwNGY2ZGYwMDgwIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjM0OTc4ZjYwY2NjNzk2MTVmYTE4OTQwIiwiaWQiOjE1MDMxODA1MDgsImVtYWlsIjoibGFrc2h5YS50ZXN0dGVhY2hlckBlbWJpYmUuY29tIn0.NW5vvvS3RxBad3P7F-Wm5rNL770WSwGy9nT9hltDkiVMEJHRSGZ5PdHo5ibHwq09wn8P2DxT_saDQrr8itUwaw; _ga_QQPR95BLCG=GS1.1.1674460071.8.1.1674460106.25.0.0; JSESSIONID=B6F4A1C2FE7891FBA1A096738840181B; JSESSIONID=4E9E3333A6D7405CBE9033995E9CBE16; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNoYXJtYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAxLTE3IDA2OjUzOjI1IFVUQyIsIm1vYmlsZSI6Ijk4NzEyMzY3MDAiLCJyb290T3JnSWQiOiI2M2M2NDUzNmNjNjQ1ZTZkZDM5MTZhNzEiLCJmaXJzdE5hbWUiOiJMYWtzaHlhIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6NCwicGFyZW50T3JnSWQiOiI2M2M2NDUzNmNjNjQ1ZTZkZDM5MTZhNzEiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2M2NDUzN2NjNjQ1ZTZkZDM5MTZhNzIiLCJpZCI6MTUwNDg2MjM1MSwiZW1haWwiOiJ0ZXN0b3JnY29zQGdtYWlsLmNvbSJ9.Io-FaOcU04FOk_H3zqzg30bJvZ5-Px6WxVhHGrjoLnJica6HoAkwJkSJs10t6kT9ZX0dHYX3XODxK6xpIxMC7Q; school_preprod_embibe-refresh-token=5bf0a320-e6d0-4bfb-9f10-5d1e06a9d726; JSESSIONID=809C924B38B8EA2D7706911DEA6A7BD3; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRlc3RUZWFjaGVyIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMjMgMDc6NDg6MTUgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiNzY1NDMyOTA3OCIsInJvb3RPcmdJZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsImRldmljZUlkIjoiMC42NTY1Mjc3NTk2NDExNTgzIiwiZmlyc3ROYW1lIjoibGFrc2h5YSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjM0OTc4N2Y4MTRmMDMwNGY2ZGYwMDgwIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjM0OTc4ZjYwY2NjNzk2MTVmYTE4OTQwIiwiaWQiOjE1MDMxODA1MDgsImVtYWlsIjoibGFrc2h5YS50ZXN0dGVhY2hlckBlbWJpYmUuY29tIn0.NW5vvvS3RxBad3P7F-Wm5rNL770WSwGy9nT9hltDkiVMEJHRSGZ5PdHo5ibHwq09wn8P2DxT_saDQrr8itUwaw',
  'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/calendar/teacher/v1/list/", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
