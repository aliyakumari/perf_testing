import jwt
import pandas as pd

import model.db as db


def creatResellerToken():
    db.initNaradUserCollection()
    cursor = db.narad_user.find({},
                                {"mobile": 1, "firstName": 1, "lastName": 1, "rootOrgId": 1, "emailId": 1,
                                 "orgType": 1})
    user_token = []

    for data in list(cursor):
        payload = {
            "lastName": data.get("lastName"),
            "time_stamp": "2021-10-11 08:36:07 UTC",
            "sub_organization_id": 1,
            "mobile": data.get("mobile"),
            "rootOrgId": data.get("rootOrgId"),
            "firstName": data.get("firstName"),
            "orgType": data.get("orgType"),
            "parentOrgId": "60c1e569335f5a5d446f6bd2",
            "personaType": "Teacher",
            "organization_id": 1,
            "adminId": "60c22944847bda7e14397a15",
            "id": 1500045012,
            "email": data.get("emailId")
        }

        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")

        user_token.append(encoded)

    df = pd.DataFrame(user_token)
    df.to_csv('Reseller_token.csv', index=False)


def getPeriodId():
    db.initCalendarCollections()
    cursor = db.slots.find({"teacherId": "rajeshk@embibe.com", "slotType": "Period", "timetableId": {"$ne": ""}},
                           {"_id": 1})
    period_ids = []

    for period_id in list(cursor):
        id_ = period_id.get("_id")
        if id_ != "612e720f788a5106d1697d6b":
            period_ids.append(id_)

    df = pd.DataFrame(period_ids)
    df.to_csv('period_id.csv', index=False)

def creatAdminstratorResellerToken():
    db.initNaradUserCollection()
    cursor = db.narad_user.find({},
                                { "mobile": 1, "firstName": 1, "lastName": 1, "rootOrgId": 1, "emailId": 1,
                                 "orgType": 1})
    user_token = []

    for data in list(cursor):
        id_ = data.get("_id")
        payload = {
            "lastName": data.get("lastName"),
            "orgType": "School",
            "time_stamp": "2021-09-03 11:44:13 UTC",
            "parentOrgId": data.get("rootOrgId"),
            "personaType": "Administrator",
            "organization_id": 1,
            "mobile": data.get("mobile"),
            "adminId": str(id_),
            "id": 1500043474,
            "rootOrgId": data.get("rootOrgId"),
            "email": data.get("emailId")
                    }

        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")

        user_token.append(encoded)

    df = pd.DataFrame(user_token)
    df.to_csv('AdminstratorReseller_token.csv', index=False)

creatAdminstratorResellerToken()