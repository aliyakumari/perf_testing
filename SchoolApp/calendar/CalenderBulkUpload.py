from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import mimetypes
from codecs import encode


host = 'https://preprodms-cf.embibe.com'

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../../Report/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.files = {'media': open('timetable_bulk _ankush - Ankush_timetable.csv', 'rb')}
        self.body = json.dumps({})
        self.headers = {
  'Connection': 'keep-alive',
  'Content-Type' : 'multipart/form-data; boundary=WebKitFormBoundaryTj00FRym6KTHcZZs',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJSaXRlc2giLCJsYXN0TmFtZSI6IkoiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMDktMDEgMTQ6NTc6MzEgVVRDIiwicGFyZW50T3JnSWQiOiI2MTJmOTQ4NTJkMWQzZjc2MmI1MWQ4MjgiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjcwMDAwMTIzNDUiLCJhZG1pbklkIjoiNjEyZjk0ODYyZDFkM2Y3NjJiNTFkODI5IiwiaWQiOjE1MDAwNDg2MTMsInJvb3RPcmdJZCI6IjYxMmY5NDg1MmQxZDNmNzYyYjUxZDgyOCIsImVtYWlsIjoicml0ZXNoakBlbWJpYmUuY29tIn0.Jtu4fv5SpTFfZQmLLFEWvpUiRmJckX61GrIVk92YvwZRxdzdbUIpVXDyrz0nvvdfq4UhRXlj8rOdNmCAmowp_A',
  'Origin': 'https://paas-v3-staging.embibe.com',
   'Content-Type': 'text/csv  ',
  'Accept': ' * / * ',
  'Accept': 'application/json, text/plain,  */*',
  'Accept-Encoding':  'gzip, deflate, br',
  'Referer' : 'https://paas-v3-staging.embibe.com/'
}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"


    @task
    def userDB(self):
        response = self.client.post("/calendar/timetable/v1/bulkUpload", data=self.body, files=self.files,
                                    headers= self.headers )


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
