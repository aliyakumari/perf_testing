u 10000
r 100
run_time 20m
step_users 100
step_time 10s
limit_fail_ratio 0.5
limit_avg_response_time_in_ms 1100
limit_90_percentile_response_time_in_ms 60000
