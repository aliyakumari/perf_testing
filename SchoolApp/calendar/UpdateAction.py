from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json',
  'Cookie': 'JSESSIONID=BE33B6A156CFAC22C367E4C7F42E873D; _gcl_au=1.1.1851913510.1673592514; WZRK_G=525636bf7c254e868ee74a6a02af8efd; _fbp=fb.1.1673593752126.519356708; _gid=GA1.2.22881938.1673858975; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2M2M2NDUzNmNjNjQ1ZTZkZDM5MTZhNzEiLCJzdWJkb21haW4iOiJodHRwczovLzYzYzY0NTM2Y2M2NDVlNmRkMzkxNmE3MS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.jFElYJO_tsihU5ENsz5QKCJS82pigZmOLUtyMrmLBV_3ncMgrUrMKbErOfkvUDbYW2x4zkva-wuY2YYagAnb_A; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=4cb59ed0-6237-4702-9f75-efc509194993; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzQwMjY5NTAsIm9yZ2FuaXphdGlvbl9pZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsIm1vYmlsZSI6Ijc2NTQzMjkwNzgiLCJpZCI6MTUwMzE4MDUwOCwiZXhwIjoxNjc0MTEzMzUwLCJkZXZpY2VJZCI6IjAuMDM3MjEwNjE1ODQxNjM4NzYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6Imxha3NoeWEudGVzdHRlYWNoZXJAZW1iaWJlLmNvbSJ9.2OjyWpvnfMP7dHkBB7Bs_wIKxd3oC7ka-uyIbjDpjzKFD4jsr5i9JOOeigYY6FrCcfBdE_7nZhZrZ2HZN5qiYQ; __insp_wid=1955736467; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS90ZWFjaC82M2M3OWZhOTBhYWVlZDVmMjllMzIzNWM%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_howoften=true; __insp_norec_sess=true; _ga=GA1.1.1595632488.1673592515; WZRK_S_8RZ-869-956Z=%7B%22p%22%3A3%2C%22s%22%3A1674028714%2C%22t%22%3A1674028850%7D; __insp_slim=1674028850348; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc0MDI4ODUwLCJnYV9pZCI6IjE1OTU2MzI0ODguMTY3MzU5MjUxNSIsInNvdXJjZV90eXBlIjoiRU1CSUJFX0hPTUUiLCJzb3VyY2UiOiJ0ZXN0IiwiZXhwIjoxNjc1MjM4NDUwLCJ1dWlkIjoiYzU4YmQyN2UtMGY0Yi00ZThlLWI1OGYtNmQ2OTg4MjY3NzM2Iiwic2VuZGVyX2lkIjoidGVzdCJ9.eVE0NX10S2LlVtLL1FVFs3XAYUdmlFIjW0DY1JkmfnE; guest-session-id=c58bd27e-0f4b-4e8e-b58f-6d6988267736; _ga_TT8L73VP3H=GS1.1.1674028714.7.1.1674028856.51.0.0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRlc3RUZWFjaGVyIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMTggMDg6MDc6NDUgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiNzY1NDMyOTA3OCIsInJvb3RPcmdJZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsImRldmljZUlkIjoiMC4wMzcyMTA2MTU4NDE2Mzg3NiIsImZpcnN0TmFtZSI6Imxha3NoeWEiLCJvcmdUeXBlIjoiU2Nob29sIiwidXNlcl90eXBlIjozLCJwYXJlbnRPcmdJZCI6IjYzNDk3ODdmODE0ZjAzMDRmNmRmMDA4MCIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzNDk3OGY2MGNjYzc5NjE1ZmExODk0MCIsImlkIjoxNTAzMTgwNTA4LCJlbWFpbCI6Imxha3NoeWEudGVzdHRlYWNoZXJAZW1iaWJlLmNvbSJ9.SQJtTFfz_IcInc9jOiZ2Z6GuG2-jGrpU4YL_ANkSFlN6Hvcl2ylMTW1KG3QJEc6wGRwhIxmV2IMwtgUb_HGtXQ; _ga_QQPR95BLCG=GS1.1.1674024307.7.1.1674029313.60.0.0; JSESSIONID=17E2CDAA488532157C4341F9D7DD3730; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InRlc3RUZWFjaGVyIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMTggMDg6MDc6NDUgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiNzY1NDMyOTA3OCIsInJvb3RPcmdJZCI6IjYzNDk3ODVlMGNjYzc5NjE1ZmExODkzOCIsImRldmljZUlkIjoiMC4wMzcyMTA2MTU4NDE2Mzg3NiIsImZpcnN0TmFtZSI6Imxha3NoeWEiLCJvcmdUeXBlIjoiU2Nob29sIiwidXNlcl90eXBlIjozLCJwYXJlbnRPcmdJZCI6IjYzNDk3ODdmODE0ZjAzMDRmNmRmMDA4MCIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzNDk3OGY2MGNjYzc5NjE1ZmExODk0MCIsImlkIjoxNTAzMTgwNTA4LCJlbWFpbCI6Imxha3NoeWEudGVzdHRlYWNoZXJAZW1iaWJlLmNvbSJ9.SQJtTFfz_IcInc9jOiZ2Z6GuG2-jGrpU4YL_ANkSFlN6Hvcl2ylMTW1KG3QJEc6wGRwhIxmV2IMwtgUb_HGtXQ',
  'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}
        self.body = json.dumps({"chapterKveCode":"kve97670--kve97915--kve744878--kve744929--kve744930","chapterName":"The Rise of Nationalism in Europe","chapterDisplayName":"The Rise of Nationalism in Europe","parentKveCode":"","parentName":"","parentDisplayName":"","learningMaps":[{"description":"This topic covers concepts, such as, Emergence of Nationalism in Europe, Frédéric Sorrieu's Utopian Vision, Democratic and Social Republics & Idea of Nation by Ernest Renan etc.","learningPath":"cbse--10th cbse--social science--history--the rise of nationalism in europe--vision of democratic and social republics","name":"Vision of Democratic and Social Republics","learningPathFormatName":"goal--exam--subject--unit--chapter--topic","topicKveCode":"kve97670--kve97915--kve744878--kve744929--kve744930--kve744931","meta":{"classId":"","display_name":"Vision of Democratic and Social Republics","topic":"cbse--10th cbse--social science--history--the rise of nationalism in europe--vision of democratic and social republics","topic_code":"kve744931","topic_name":"Vision of Democratic and Social Republics","bookId":"5ec5867a0c88fe5860961943/CBSE/10th CBSE/Social Science","bookType":"BigBook","book_path":"5ec5867a0c88fe5860961943/CBSE/10th CBSE/Social Science","bookLearningPathFormatName":"goal--exam--subject--unit--chapter--topic","unit":"cbse--10th cbse--social science--history","unit_code":"kve744929","unit_name":"History","unit_display_name":"History","chapter":"cbse--10th cbse--social science--history--the rise of nationalism in europe","chapter_code":"kve744930","chapter_name":"The Rise of Nationalism in Europe","chapter_display_name":"The Rise of Nationalism in Europe"}}],"createLesson": False})

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.put("/calendar/teacher/v1/period/action?id=63c79fa90aaeed5f29e3235e&locale=en", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
