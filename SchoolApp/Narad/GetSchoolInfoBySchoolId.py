from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers = {
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Imt1bWFyIiwidGltZV9zdGFtcCI6IjIwMjMtMDItMTYgMDc6NDM6MTggVVRDIiwibW9iaWxlIjoiOTY2MTgzMTIzNCIsInJvb3RPcmdJZCI6IjYyOWYzNThlYzdiOWY4MGRmOTRlMDI2NyIsImRldmljZUlkIjoiMC42Mzk2OTcwMDQwMDUxOTg2IiwiZmlyc3ROYW1lIjoic2hhc2hpICIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjI5ZjM1OGVjN2I5ZjgwZGY5NGUwMjY3IiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjI5ZjM1OGVjN2I5ZjgwZGY5NGUwMjY4IiwiaWQiOjE1MDE4NTE4MzEsImVtYWlsIjoic2hhc2hpYmh1c2hhbkBlbWJpYmUuY29tIn0.FQYiXBsvBU-IhDaCNa85hBiQi90lMlUiuL0tBIs2m7xGVZAyhL11879UvcMQYGQNt6VH_e-gxpERdBM45zurTA',
  'Cookie': 'school_preprod_embibe-refresh-token=b192828c-fd80-41d2-880c-c38263b97466; JSESSIONID=865D2D199922E25DFAAFAFB5B6427CFD; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkVuZ2luZWVyaW5nIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMjUgMDY6NDk6MjAgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODg3NzY2NTU0NCIsInJvb3RPcmdJZCI6IjYzODg0ZGYwMzkyY2M5Nzc4NTJjN2UwNyIsImRldmljZUlkIjoiMC44NjcxODkxODExNTg4ODQzIiwiZmlyc3ROYW1lIjoiVmlzaGFsIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6MywicGFyZW50T3JnSWQiOiI2Mzg4NGU2YTM5MmNjOTc3ODUyYzdlMGIiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2EwMTI3YjU5ZWQ1YjQ0ZTY0ZDk1NzQiLCJpZCI6MTUwNDgxNjM2NCwiZW1haWwiOiJ2aXNoYWxfamVlQGVtYmliZS5jb20ifQ.qmrU29cu-CJTWg0Wz1PAZxvxOWZAXMIpYgRMdmPLp7_NV1nDW3_hFD1nVKtT4GZdH-Y11FmQn8UsqdYa2HoDxw; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
}


    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/narad/v2/school/629f358ec7b9f80df94e0267", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
