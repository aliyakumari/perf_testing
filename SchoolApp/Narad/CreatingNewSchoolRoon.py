from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'accept': '*/*',
  'Content-Type': 'application/json',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJKYW5nYSIsImxhc3ROYW1lIjoicmFqIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTEwLTI1IDA2OjQ4OjA2IFVUQyIsInBhcmVudE9yZ0lkIjoiNjE3NjM3Yzk5ODNiODcyNzU0Nzk1ZTBmIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5OTMzOTkyMjE5IiwiYWRtaW5JZCI6IjYxNzYzN2NhOTgzYjg3Mjc1NDc5NWUxMCIsImlkIjoxNTAwMDU2NTYwLCJyb290T3JnSWQiOiI2MTc2MzdjOTk4M2I4NzI3NTQ3OTVlMGYiLCJlbWFpbCI6InJhal9hZG1pbjEyM0BnbWFpbC5jb20ifQ.chKHJndiQIRpjt_FiP09IkEL2Wv3ypFZGtSyJNFmE_X6Xo-N0HOcJb5ZE_9RzyUgN__RZF_M-Frtr9SUOMeG5w',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MTZlODhmMWUyMWQ0MTY4OWFhMjkxYmUiLCJzdWJkb21haW4iOiJodHRwczovLzYxNmU4OGYxZTIxZDQxNjg5YWEyOTFiZS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-NDjpfceBqoueiPaF1Hy-MqUKvuTbAc4vo24YuxHBoQo1kP7uiXZBKkl4Blky7Lat0Q20X-mfxQYAwGXh5ZB8g; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTkgMTE6MDc6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.JlJpAWmv9xBL9E_3x5aDC4SkIC9-I23gAvIcxKrVVVxl7xUbv5ayD7OQhiajQVw4FifMGqYffa4BYPeuWDWPvQ'
}

        self.body = json.dumps({
    "fields": [
        {
            "fieldGroupId": 2,
            "name": "branchName",
            "value": "TyughjhEdSgTE",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 2,
            "name": "branchHead",
            "value": "Sankar",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 4,
            "name": "Demo",
            "value": "TEST",
            "fieldType": "CustomField"
        }
    ],
    "orgId": "610bd0f8a87a697c72cd127a",
    "userDetails": [
        {
            "firstName": "admin",
            "password": "Embibe@1234",
            "orgType": "ChainOfSchools",
            "lastName": "dps",
            "emailId": "admsdgfps@embibe.com",
            "mobile": "9872843219",
            "isAutoPassword": True,
            "sendInvite": False
        }
    ]
})
        self.test = [123]
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        try:
            num = randint(1, 1000000000000000000)
            num = str(num)
            grade = f"{num}th CBSE"
            self.body = json.dumps({
    "fields": [
        {
            "fieldGroupId": 2,
            "name": "branchName",
            "value": f"TyughjhEdSgTE{num}",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 2,
            "name": "branchHead",
            "value": "Sankar",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 4,
            "name": "Demo",
            "value": "TEST",
            "fieldType": "CustomField"
        }
    ],
    "orgId": "610bd0f8a87a697c72cd127a",
    "userDetails": [
        {
            "firstName": "admin",
            "password": "Embibe@1234",
            "orgType": "ChainOfSchools",
            "lastName": "dps",
            "emailId": "admsdgfps@embibe.com",
            "mobile": "9872843219",
            "isAutoPassword": True,
            "sendInvite": False
        }
    ]
})

        except:
            pass

        if num not in self.test:
            response = self.client.post("/narad/v2/school/create?orgId=610bd0f8a87a697c72cd127c", data=self.body,
                                        headers=self.headers)

            self.test.append(num)
        else:
            num = randint(1, 1000000000000000000)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
