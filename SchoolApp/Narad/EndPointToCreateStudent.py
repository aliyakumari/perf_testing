from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.used_num = []
        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJKb3NoIiwibGFzdE5hbWUiOiJIb2VnZXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTcgMDk6NDM6MzEgVVRDIiwicGFyZW50T3JnSWQiOiI2MTk0Y2U3YWRlYzk0OTMxYmI2MGE4MzEiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjYwMDAwODM4NjIiLCJhZG1pbklkIjoiNjE5NGNlN2JkZWM5NDkzMWJiNjBhODMyIiwiaWQiOjE1MDExNjI3NjQsInJvb3RPcmdJZCI6IjYxOTRjZTdhZGVjOTQ5MzFiYjYwYTgzMSIsImVtYWlsIjoiZGVtYXJjdXM2NzcwOTcxOEBndWVycmlsbGFtYWlsLmNvbSJ9.ZhX8FKO8iKN5arJ9ulG0cq35_4ZYomLBAabU55qV-LRzs_gBTebquI_KY4GnKt0kk8tZujanVCYX4Flvmu1whQ',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?1',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Mobile Safari/537.36',
  'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"Android"',
  'Content-Type': 'application/json;charset=UTF-8',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': '_ga=GA1.2.1186392671.1636978978; ajs_anonymous_id=%22a11965e8-69e1-4a76-8281-8529601d5469%22; ajs_user_id=1500000639; _hp2_id.2562464476=%7B%22userId%22%3A%221019754269245132%22%2C%22pageviewId%22%3A%225077767293840833%22%2C%22sessionId%22%3A%221975409904295115%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; _ga_TT8L73VP3H=GS1.1.1637053951.2.0.1637053951.60; _gcl_au=1.1.1423008489.1637053954; preprod_ab_version=0; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2MzcxNDIxOTcsIm9yZ2FuaXphdGlvbl9pZCI6IjYxOTRjZTdhZGVjOTQ5MzFiYjYwYTgzMSIsIm1vYmlsZSI6IjYwMDAwODM4NjIiLCJpZCI6MTUwMTE2Mjc2NCwiZXhwIjoxNjM3MzE0OTk3LCJkZXZpY2VJZCI6IjAuMzAxODY5ODA5ODQ0NTU1MzYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6ImRlbWFyY3VzNjc3MDk3MThAZ3VlcnJpbGxhbWFpbC5jb20ifQ.B9MD0D6HQHMjGNfaF9VJflgEANkEcb0Q_3m16aqWj_MdpccHSaLtNdPjDZdHdQeU6zoisIu7m0eiMI-5uUTNSQ; preprod_embibe-refresh-token=1da611ec-59d1-4286-ade9-f4cccc2fc5e4; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJKb3NoIiwibGFzdE5hbWUiOiJIb2VnZXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTcgMDk6NDM6MzEgVVRDIiwicGFyZW50T3JnSWQiOiI2MTk0Y2U3YWRlYzk0OTMxYmI2MGE4MzEiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjYwMDAwODM4NjIiLCJhZG1pbklkIjoiNjE5NGNlN2JkZWM5NDkzMWJiNjBhODMyIiwiaWQiOjE1MDExNjI3NjQsInJvb3RPcmdJZCI6IjYxOTRjZTdhZGVjOTQ5MzFiYjYwYTgzMSIsImVtYWlsIjoiZGVtYXJjdXM2NzcwOTcxOEBndWVycmlsbGFtYWlsLmNvbSJ9.ZhX8FKO8iKN5arJ9ulG0cq35_4ZYomLBAabU55qV-LRzs_gBTebquI_KY4GnKt0kk8tZujanVCYX4Flvmu1whQ; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randint(1000000000000000000000,1000000000000000000000000000)
        if rnum not in self.used_num :
            self.used_num.append(rnum)
            self.body = json.dumps({
        "fields": [
            {
                "fieldGroupId": 1,
                "name": "firstName",
                "value": f"Student{rnum}",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 4,
                "name": "password",
                "value": "Embibe@123",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 4,
                "name": "PasswordOptions",
                "value": "manual",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "lastName",
                "value": "Lname",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "emailId",
                "value": f"sutdentpertest{rnum}@guerrillamail.com",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "rollNumber",
                "value": "23423",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "category",
                "value": "general",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "mobile",
                "value": "6000010001",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "dob",
                "value": "01-11-2010",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "gender",
                "value": "male",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "state",
                "value": "Karnataka",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 1,
                "name": "city",
                "value": "Bangalore",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 2,
                "name": "parentName",
                "value": "parent01",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 2,
                "name": "parentMobile",
                "value": "8000010001",
                "fieldType": "BasicField"
            },
            {
                "fieldGroupId": 2,
                "name": "parentEmailId",
                "value": "parent01@embibe.com",
                "fieldType": "BasicField"
            }
        ],
        "orgSubjectAssociations": [
            {
                "schoolId": "6194ce7adec94931bb60a831",
                "schoolName": "instituteName",
                "board": "CBSE",
                "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670",
                "grade": "12th CBSE",
                "gradeId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse|kve97670--kve97671",
                "section": "A",
                "sectionId": "A",
                "subject": "Mathematics",
                "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Mathematics|cbse--12th cbse--mathematics|kve97670--kve97671--kve97721"
            },
            {
                "schoolId": "6194ce7adec94931bb60a831",
                "schoolName": "instituteName",
                "board": "CBSE",
                "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670",
                "grade": "12th CBSE",
                "gradeId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse|kve97670--kve97671",
                "section": "A",
                "sectionId": "A",
                "subject": "Physics",
                "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Physics|cbse--12th cbse--physics|kve97670--kve97671--kve97687"
            },
            {
                "schoolId": "6194ce7adec94931bb60a831",
                "schoolName": "instituteName",
                "board": "CBSE",
                "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670",
                "grade": "12th CBSE",
                "gradeId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse|kve97670--kve97671",
                "section": "A",
                "sectionId": "A",
                "subject": "Chemistry",
                "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Chemistry|cbse--12th cbse--chemistry|kve97670--kve97671--kve97672"
            },
            {
                "schoolId": "6194ce7adec94931bb60a831",
                "schoolName": "instituteName",
                "board": "CBSE",
                "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse|kve97670",
                "grade": "10th CBSE",
                "gradeId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse|kve97670--kve97671",
                "section": "A",
                "sectionId": "A",
                "subject": "Biology",
                "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Biology|cbse--12th cbse--biology|kve97670--kve97671--kve97678"
            }
        ]
    })
        response = self.client.post("/narad/v2/student", data=self.body,
                                        headers=self.headers)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
