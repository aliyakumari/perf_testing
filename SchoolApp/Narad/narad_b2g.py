from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers ={
  'authority': 'preprodms.embibe.com',
  'accept': 'application/json, text/plain, */*',
  'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'cache-control': 'no-cache',
  'cookie': 'JSESSIONID=C1D40752D28B7C4F84571EA43378C6DE; _gcl_au=1.1.1157367009.1682672354; WZRK_G=3696693ba62f41b99ee4567dab67ac2f; _fbp=fb.1.1683005374490.1132183831; _gid=GA1.2.930358691.1683279010; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=6d0b90fb-33cd-46fe-b9d5-08f6d6a0ac69; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2ODMzMTc2MzAsIm9yZ2FuaXphdGlvbl9pZCI6IjY0NTJiZGFiNmYzMDNlMmU0ZDA2YjdmYSIsIm1vYmlsZSI6Ijc4OTA3ODkwMDkiLCJpZCI6MTUwNTA3NzE5NiwiZXhwIjoxNjgzNDA0MDMwLCJkZXZpY2VJZCI6IjAuNjIxMDEwNDIwOTI1ODMwNyIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoic2FpbmlrLnNjaG9vbC5tcEBlbWJpYmUuY29tIn0.CyRnP_OdsFMukZJcu1J8IseZvR3AWQSvinShlISH7kj-At6sE4R6AXaLSlqx5AzgTkgTH_d_6hD_Qu7FLHBWsA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2ODMzMTc2MzAsIm9yZ2FuaXphdGlvbl9pZCI6IjY0NTJiZGFiNmYzMDNlMmU0ZDA2YjdmYSIsIm1vYmlsZSI6Ijc4OTA3ODkwMDkiLCJpZCI6MTUwNTA3NzE5NiwiZXhwIjoxNjgzNDA0MDMwLCJkZXZpY2VJZCI6IjAuNjIxMDEwNDIwOTI1ODMwNyIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoic2FpbmlrLnNjaG9vbC5tcEBlbWJpYmUuY29tIn0.CyRnP_OdsFMukZJcu1J8IseZvR3AWQSvinShlISH7kj-At6sE4R6AXaLSlqx5AzgTkgTH_d_6hD_Qu7FLHBWsA; _dc_gtm_UA-48005755-22=1; __cf_bm=iTtddr04z6HlOj5SB_Q2HSv.VsbJf36Luy426ns_kkk-1683318786-0-Aek08n1c8/h7AXk6Jo4Z1I56nmIxV+WZRQBheYy1JkKWP9O9L4qqd4Gzusx8xFcmNsOxYXJ25vtG+s/TzWrS9CM=; __insp_wid=1955736467; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS90cmFjay9nb3Y%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_sess=true; _ga=GA1.1.96921623.1682672360; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjgzMzE4ODA1LCJnYV9pZCI6Ijk2OTIxNjIzLjE2ODI2NzIzNjAiLCJzb3VyY2VfdHlwZSI6IkVNQklCRV9IT01FIiwic291cmNlIjoidGVzdCIsImV4cCI6MTY4NDUyODQwNSwidXVpZCI6ImU2ZjAzZGQxLWQ3Y2YtNDBmNS05ZDY0LWFiZmE5ZTVkM2FjYiIsInNlbmRlcl9pZCI6InRlc3QifQ.LrzvgyxrOcyEC499m1CXQReeQv_5iWzJ4MJ0goasSJ8; guest-session-id=e6f03dd1-d7cf-40f5-9d64-abfa9e5d3acb; __insp_slim=1683318805303; WZRK_S_8RZ-869-956Z=%7B%22p%22%3A3%2C%22s%22%3A1683318785%2C%22t%22%3A1683318805%7D; _ga_TT8L73VP3H=GS1.1.1683318785.5.1.1683318806.39.0.0; _ga_QQPR95BLCG=GS1.1.1683317607.9.1.1683318807.28.0.0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIzLTA1LTA1IDIwOjMzOjMwIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijc4OTA3ODkwMDkiLCJyb290T3JnSWQiOiI2NDUyYmRhYjZmMzAzZTJlNGQwNmI3ZmEiLCJkZXZpY2VJZCI6IjAuNjIxMDEwNDIwOTI1ODMwNyIsImZpcnN0TmFtZSI6Ik1QIFNuYWluaWsiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjQ1MzJkNDMwYTBiNzA3MDdjNzBlOTdlIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjQ1MzNiOTlhMzI3NmY1YjU3YzJiZTE3IiwiaWQiOjE1MDUwNzcxOTYsImVtYWlsIjoic2FpbmlrLnNjaG9vbC5tcEBlbWJpYmUuY29tIn0.9-1fPZjyM385GV0oTFDiII1zJNXHLKPAYJeGWOOtbn5_MycdxmPuGDIwDHrnDGZDHdeN-RvZTvZxWv83b5qioA; JSESSIONID=112A4279B3CF4577FAB41BC786A5878F; school_prod_embibe-refresh-token=eb1aeac6-c1bc-4439-9d2b-fc07d98e952e; JSESSIONID=00830DDB6531826059518DBF0A0717B4',
  'origin': 'https://paas-v3-staging.embibe.com',
  'pragma': 'no-cache',
  'referer': 'https://paas-v3-staging.embibe.com/',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlByYWRlc2giLCJ0aW1lX3N0YW1wIjoiMjAyMy0wNS0xMCAxNDowODo0NiBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI3ODkwNzg5MDA5Iiwicm9vdE9yZ0lkIjoiNjQ1YWI2ODU5ZWNkYzYwOGQzNzEyYzI3IiwiZGV2aWNlSWQiOiIwLjMzMjA2Njk2ODQ4ODEzMzQ0IiwiZmlyc3ROYW1lIjoiTWFkaHlhIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidXNlcl90eXBlIjo0LCJwYXJlbnRPcmdJZCI6IjY0NWFkOGI2MTM1MmQ1MTNiNzdiNWY0YSIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjY0NWJhNTI4ZjMzZDc4MWZhZGZmYjQ5NiIsImlkIjoxNTA1MDc4MjkyLCJlbWFpbCI6Im1wQGVtYmliZS5jb20ifQ.93DZTnwZJJ4XsM9RX8X2gVSew_z0jD1CRw-8TZGiv_Zv6dySrNaPYcvnsafvc_Rkq10ObtU4-ovquTkygrjCkg',
  'sec-ch-ua': '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-site',
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/narad/b2g/dashboard/v2?locale=en", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
