from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.body = {}
        self.headers ={
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJSYW0iLCJsYXN0TmFtZSI6IkoiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMjIgMTA6Mzg6MDkgVVRDIiwicGFyZW50T3JnSWQiOiI2MTBiNjY4NmE0ODNiZTdmYzk0MTc0YWQiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk2NjY2MDA3NzAiLCJhZG1pbklkIjoiNjEwYjY2ODdhNDgzYmU3ZmM5NDE3NGFlIiwiaWQiOjE1MDAwNDgwNzgsInJvb3RPcmdJZCI6IjYxMGI2Njg2YTQ4M2JlN2ZjOTQxNzRhZCIsImVtYWlsIjoicmFtMTIzQGdtYWlsLmNvbSJ9.e4_ZK4DgSiie-RL2wmABXD7i9fN2iJSNZ3h6d8bNtY0MzuXk2GQa1Ocba7g9CgmpdHsh176kxyqag35ZRpGW2Q',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
  'sec-ch-ua': '"Google Chrome";v="87", " Not;A Brand";v="99", "Chromium";v="87"',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-US,en;q=0.9,ta;q=0.8',
  'Cookie': 'reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJTYW5rYXIiLCJsYXN0TmFtZSI6InYiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ0aW1lX3N0YW1wIjoiMjAyMC0xMi0yMiAxNDo1MzowMCBVVEMiLCJwYXJlbnRPcmdJZCI6IjVmZTIwODIyZGQ3OTI2M2M5ZjFkYmQyMyIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6NzUyLCJtb2JpbGUiOiI5ODc2NTQzMjEwIiwiYWRtaW5JZCI6IjVmZTIwODIzZGQ3OTI2M2M5ZjFkYmQyNCIsImlkIjozMDYsInJvb3RPcmdJZCI6IjVmZTIwODIyZGQ3OTI2M2M5ZjFkYmQyMyIsImVtYWlsIjoic2Nob29sX2RlbW9fbXVsdGlAZW1iaWJlLmNvbSJ9.J1IayHtAfwTRCYLwpvoTkRfPKhV-8mRI9MeQH5hnJ6qLDSwFNVnkA7M6GbcZGmEAV1nezwrQohoOpKIwGpenVg; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MTZlODhmMWUyMWQ0MTY4OWFhMjkxYmUiLCJzdWJkb21haW4iOiJodHRwczovLzYxNmU4OGYxZTIxZDQxNjg5YWEyOTFiZS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-NDjpfceBqoueiPaF1Hy-MqUKvuTbAc4vo24YuxHBoQo1kP7uiXZBKkl4Blky7Lat0Q20X-mfxQYAwGXh5ZB8g; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTkgMTE6MDc6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.JlJpAWmv9xBL9E_3x5aDC4SkIC9-I23gAvIcxKrVVVxl7xUbv5ayD7OQhiajQVw4FifMGqYffa4BYPeuWDWPvQ'
}






    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/narad/v2/school/610b6686a483be7fc94174ad/list?pageNumber=1&pageSize=10", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
