from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers ={
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICIsInRpbWVfc3RhbXAiOiIyMDIxLTA4LTI1IDA2OjQxOjMwIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjkwMDAwMTc4NzkiLCJyb290T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJmaXJzdE5hbWUiOiJSYWtlc2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MGMxZTU2OTMzNWY1YTVkNDQ2ZjZiZDIiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MGMyMjk0NDg0N2JkYTdlMTQzOTdhMTUiLCJpZCI6MTUwMDA0NTAxMiwiZW1haWwiOiJyYWplc2hrQGVtYmliZS5jb20ifQ.RVyJrdhdeQ5EdSuz3xnfF--J-rD07L1yaJjYuGzxwaT0tKNeloa9h_qpjIYpTPIlZt9XniEy0sqS4lGZmaUX-w',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?1',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Mobile Safari/537.36',
  'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
  'Content-Type': 'application/json;charset=UTF-8',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'preprod_embibe-refresh-token=1fec9b92-9b2d-4167-9e53-671c080e976d; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICIsInRpbWVfc3RhbXAiOiIyMDIxLTA4LTI1IDA2OjQxOjMwIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjkwMDAwMTc4NzkiLCJyb290T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJmaXJzdE5hbWUiOiJSYWtlc2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MGMxZTU2OTMzNWY1YTVkNDQ2ZjZiZDIiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MGMyMjk0NDg0N2JkYTdlMTQzOTdhMTUiLCJpZCI6MTUwMDA0NTAxMiwiZW1haWwiOiJyYWplc2hrQGVtYmliZS5jb20ifQ.RVyJrdhdeQ5EdSuz3xnfF--J-rD07L1yaJjYuGzxwaT0tKNeloa9h_qpjIYpTPIlZt9XniEy0sqS4lGZmaUX-w; V_ID=ultimate.2021-07-01.1467e9e769aeeb97a6568d5c1c40aafa; embibe-refresh-token=8e862ae7-f48d-4c4c-8473-3bd4aba333bc; preprod_embibe-refresh-token=48db71ac-56aa-4045-9eee-37fdaf30d5bb; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MTZlODhmMWUyMWQ0MTY4OWFhMjkxYmUiLCJzdWJkb21haW4iOiJodHRwczovLzYxNmU4OGYxZTIxZDQxNjg5YWEyOTFiZS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-NDjpfceBqoueiPaF1Hy-MqUKvuTbAc4vo24YuxHBoQo1kP7uiXZBKkl4Blky7Lat0Q20X-mfxQYAwGXh5ZB8g; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTkgMTE6MDc6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.JlJpAWmv9xBL9E_3x5aDC4SkIC9-I23gAvIcxKrVVVxl7xUbv5ayD7OQhiajQVw4FifMGqYffa4BYPeuWDWPvQ'
}



        self.body = json.dumps({
    "where": {
        "operations": {
            "and": [
                {
                    "op": "or",
                    "scalar": {
                        "fieldType": "BasicField",
                        "name": "classId",
                        "value": [
                            "60c1fe9d9c95f329eb741a51"
                        ]
                    }
                },
                {
                    "op": "or",
                    "scalar": {
                        "fieldType": "BasicField",
                        "name": "className",
                        "value": [
                            "12th CBSE-B"
                        ]
                    }
                },
                {
                    "op": "or",
                    "scalar": {
                        "fieldType": "BasicField",
                        "name": "subject",
                        "value": [
                            "Chemistry"
                        ]
                    }
                }
            ]
        }
    },
    "orgId": "60c1e569335f5a5d446f6bd2",
    "page": {
        "pageNumber": 1,
        "pageSize": 10
    },
    "personaType": "Student"
})
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/narad/v1/search?screen=attendance", data=self.body,
                                        headers=self.headers)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
