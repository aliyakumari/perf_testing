from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-US,en;q=0.9',
  'Connection': 'keep-alive',
  'Content-Type': 'application/json;charset=UTF-8',
  'Cookie': 'JSESSIONID=650C9378AE43A6B8DF14C0AE54AA3771; _fbp=fb.1.1675167244786.1802933014; _gcl_au=1.1.320623952.1675167880; WZRK_G=fcaa1904c32c410197e7cde51afde4d1; __insp_wid=1955736467; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS9zaWdudXA%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_howoften=true; __insp_norec_sess=true; _gid=GA1.2.299862950.1675834939; _ga_7N8QWZ4X8C=GS1.1.1675835296.15.1.1675835312.44.0.0; _ga=GA1.1.835198762.1668145087; WZRK_S_8RZ-869-956Z=%7B%22p%22%3A2%2C%22s%22%3A1675834939%2C%22t%22%3A1675835328%7D; __insp_slim=1675835328021; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc1ODM1MzI4LCJnYV9pZCI6IjgzNTE5ODc2Mi4xNjY4MTQ1MDg3Iiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzcwNDQ5MjgsInV1aWQiOiJjMDlkZDNiYS1hMDBiLTQ3ZGYtOTM1NC0xOTEzMjczNGFmNjQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.z_p2wgRCsNf_oi8vG-DsFZ_XMW1sJF-O5czI9UgnOng; guest-session-id=c09dd3ba-a00b-47df-9354-19132734af64; _ga_TT8L73VP3H=GS1.1.1675834940.56.1.1675835384.60.0.0; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=270f59d7-dc72-443b-be88-de8d129e2f9f; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzU4MzU0MDEsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMGRlY2MyNzVmZDViNjQ5NWZhNTA2MCIsIm1vYmlsZSI6Ijk2NjE4MzIwMDAiLCJpZCI6MTUwMTkwMTAyNiwiZXhwIjoxNjc1OTIxODAxLCJkZXZpY2VJZCI6IjAuMzc4NzMwMzMxNTYzNzYyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJzaGFzaGltZWh0YUBlbWJpYmUuY29tIn0.puNucvZKV4sHzVXX4ZxKRhYtJ7uWGIuUjNT2tGuOJidmka129uuHh35XQxO3weJYxmLu84igcKVTqNFGOdOcJA; school_reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2NzU4MzU0MDEsIm9yZ2FuaXphdGlvbl9pZCI6IjYzMGRlY2MyNzVmZDViNjQ5NWZhNTA2MCIsIm1vYmlsZSI6Ijk2NjE4MzIwMDAiLCJpZCI6MTUwMTkwMTAyNiwiZXhwIjoxNjc1OTIxODAxLCJkZXZpY2VJZCI6IjAuMzc4NzMwMzMxNTYzNzYyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJzaGFzaGltZWh0YUBlbWJpYmUuY29tIn0.puNucvZKV4sHzVXX4ZxKRhYtJ7uWGIuUjNT2tGuOJidmka129uuHh35XQxO3weJYxmLu84igcKVTqNFGOdOcJA; _ga_QQPR95BLCG=GS1.1.1675835385.23.1.1675835402.43.0.0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ik1laHRhIiwidGltZV9zdGFtcCI6IjIwMjMtMDItMDggMDU6NTA6MDYgVVRDIiwibW9iaWxlIjoiOTY2MTgzMjAwMCIsInJvb3RPcmdJZCI6IjYzMGRlY2MyNzVmZDViNjQ5NWZhNTA2MCIsImRldmljZUlkIjoiMC4zNzg3MzAzMzE1NjM3NjIiLCJmaXJzdE5hbWUiOiJTaGFzaGkiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjMwZGVjYzI3NWZkNWI2NDk1ZmE1MDYwIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjMwZGVjYzM3NWZkNWI2NDk1ZmE1MDYxIiwiaWQiOjE1MDE5MDEwMjYsImVtYWlsIjoic2hhc2hpbWVodGFAZW1iaWJlLmNvbSJ9.-fASXvz6IVAmMUpA-vAKhhIxLgdsmkWzMTakAO1atj5_RChzYQuDcIl5X84LkK0j6nWWBSy_jPRkDEShDzcKDg; JSESSIONID=184429473D209F249F784434DFF9760E; school_preprod_embibe-refresh-token=b192828c-fd80-41d2-880c-c38263b97466; JSESSIONID=D09FD3B1ABA9FD5F06E066E7057614CB; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkVuZ2luZWVyaW5nIiwidGltZV9zdGFtcCI6IjIwMjMtMDEtMjUgMDY6NDk6MjAgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODg3NzY2NTU0NCIsInJvb3RPcmdJZCI6IjYzODg0ZGYwMzkyY2M5Nzc4NTJjN2UwNyIsImRldmljZUlkIjoiMC44NjcxODkxODExNTg4ODQzIiwiZmlyc3ROYW1lIjoiVmlzaGFsIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6MywicGFyZW50T3JnSWQiOiI2Mzg4NGU2YTM5MmNjOTc3ODUyYzdlMGIiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2M2EwMTI3YjU5ZWQ1YjQ0ZTY0ZDk1NzQiLCJpZCI6MTUwNDgxNjM2NCwiZW1haWwiOiJ2aXNoYWxfamVlQGVtYmliZS5jb20ifQ.qmrU29cu-CJTWg0Wz1PAZxvxOWZAXMIpYgRMdmPLp7_NV1nDW3_hFD1nVKtT4GZdH-Y11FmQn8UsqdYa2HoDxw; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ik1laHRhIiwidGltZV9zdGFtcCI6IjIwMjMtMDItMDggMDU6NTA6MDYgVVRDIiwibW9iaWxlIjoiOTY2MTgzMjAwMCIsInJvb3RPcmdJZCI6IjYzMGRlY2MyNzVmZDViNjQ5NWZhNTA2MCIsImRldmljZUlkIjoiMC4zNzg3MzAzMzE1NjM3NjIiLCJmaXJzdE5hbWUiOiJTaGFzaGkiLCJvcmdUeXBlIjoiQ2hhaW5PZlNjaG9vbHMiLCJ1c2VyX3R5cGUiOjQsInBhcmVudE9yZ0lkIjoiNjMwZGVjYzI3NWZkNWI2NDk1ZmE1MDYwIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjMwZGVjYzM3NWZkNWI2NDk1ZmE1MDYxIiwiaWQiOjE1MDE5MDEwMjYsImVtYWlsIjoic2hhc2hpbWVodGFAZW1iaWJlLmNvbSJ9.-fASXvz6IVAmMUpA-vAKhhIxLgdsmkWzMTakAO1atj5_RChzYQuDcIl5X84LkK0j6nWWBSy_jPRkDEShDzcKDg',
  'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"macOS"'
}


        self.body = {}
        self.test = [123]
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        
        num = randint(1, 1000000000000000000)
        num = str(num)
        self.body = json.dumps([
    {
        "level": 1,
        "levelName": "Zone",
        "orgId": "630dee3a75fd5b6495fa5071",
        "orgName": "Patna",
        "admins": [],
        "subOrgs": [
            {
                "level": 2,
                "levelName": "State",
                "orgId": "630dee3a75fd5b6495fa5072",
                "orgName": "Bihar",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "630dee3a75fd5b6495fa5073",
                        "orgName": "Gaya",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "630dee3a75fd5b6495fa5074",
                        "orgName": "Gurua",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "630dee3a75fd5b6495fa5075",
                        "orgName": "Sherghati",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    }
                ],
                "type": "node"
            },
            {
                "level": 2,
                "levelName": "State",
                "orgId": "63dd028bba68900525d7ffc5",
                "orgName": "Karnataka",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63dd028bba68900525d7ffc6",
                        "orgName": "Bengaluru ",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63dd028bba68900525d7ffc7",
                        "orgName": "Kodihalli",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    }
                ],
                "type": "node"
            },
            {
                "level": 2,
                "levelName": "State",
                "orgId": "63e0f42ef0e3c33da1e9434f",
                "orgName": "Karnatak",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63e0f42ef0e3c33da1e94350",
                        "orgName": "Bengaluru",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63e0f42ef0e3c33da1e94351",
                        "orgName": "Kodihall",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    }
                ],
                "type": "node"
            }
        ],
        "type": "node"
    },
    {
        "level": 1,
        "levelName": "Zone",
        "orgId": "634fe5d0c17e6409f0add565",
        "orgName": "Delhi",
        "admins": [],
        "subOrgs": [
            {
                "level": 2,
                "levelName": "State",
                "orgId": "634fe5d0c17e6409f0add566",
                "orgName": "UP",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "634fe5d0c17e6409f0add567",
                        "orgName": "Varanasi",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "634fe5d0c17e6409f0add568",
                        "orgName": "Lucknow",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    }
                ],
                "type": "node"
            },
            {
                "level": 2,
                "levelName": "State",
                "orgId": "63dd028bba68900525d7ffc8",
                "orgName": "Madhya Pradesh",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63dd028bba68900525d7ffc9",
                        "orgName": "Bhopal",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63dd028bba68900525d7ffca",
                        "orgName": "Gwaliot",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    }
                ],
                "type": "node"
            },
            {
                "level": 2,
                "levelName": "State",
                "orgId": "63e0f42ef0e3c33da1e94352",
                "orgName": "Madhya Pradeshi",
                "admins": [],
                "subOrgs": [
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63e0f42ef0e3c33da1e94353",
                        "orgName": "Bhopala",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "level": 3,
                        "levelName": "City",
                        "orgId": "63e0f42ef0e3c33da1e94354",
                        "orgName": "Gwalior",
                        "admins": [],
                        "subOrgs": [],
                        "type": "node"
                    },
                    {
                        "orgName":f"{num}check70",
                        "adminName": "",
                        "type": "node",
                        "level": 3,
                        "hasAdmin": True,
                        "subOrgs": [],
                        "isValue": True
                    },
                    {
                        "orgName": f"{num}1check80",
                        "adminName": "",
                        "type": "node",
                        "level": 3,
                        "hasAdmin": True,
                        "subOrgs": [],
                        "isValue": True
                    }
                ],
                "type": "node"
            }
        ],
        "type": "node"
    }
])
       
        if num not in self.test:
            print(self.body)
            response = self.client.post("/narad/v2/hierarchy?parentOrgId=630decc275fd5b6495fa5060", data=self.body,
                                        headers=self.headers)

            self.test.append(num)
        else:
            num = randint(10, 1000000000000000000)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
