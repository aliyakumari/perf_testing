from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
  'sec-ch-ua': '""""""""Google Chrome"""""""";v=""""""""87"""""""", """""""" Not;A Brand"""""""";v=""""""""99"""""""", """"""""Chromium"""""""";v=""""""""87""""""""',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-US,en;q=0.9,ta;q=0.8',
  'Cookie': '_ga=GA1.2.1449343851.1608200867; V_ID=ultimate.2020-12-17.7b345be066cf5e60fbf5f793797f0a0e; staging-embibe-token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6NzEyNzgwMCwiZW1haWwiOiJndWVzdF8xNjA4MjAwOTAxNzA0ODg0QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTEyLTE3VDEwOjI4OjIxLjQ4OVoifQ.5nLkBt6VwtefjBAKaLo69YhbtTa_3AArCK9GZ9XOtcQU-5JsYCd0Um9KUrZDDsqHdQ3qF_utTQTovdGWttTdew; staging-access-token=0PqfPfBo700PGfw2DncamQ; staging-uid=guest_1608200901704884%40embibe.com; staging-client=H8pTNKUiYnsq0hNgl0lISw; _hp2_id.1503456953=%7B%22userId%22%3A%221450207577011462%22%2C%22pageviewId%22%3A%225879418366106177%22%2C%22sessionId%22%3A%22563944038726324%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJTYW5rYXIiLCJsYXN0TmFtZSI6IlNhbmthciIsIm9yZ1R5cGUiOiJDb2FjaGluZ0luc3RpdHV0ZVdpdGhCcmFuY2hlcyIsInRpbWVfc3RhbXAiOiIyMDIwLTEyLTIxIDA3OjU3OjI3IFVUQyIsInBhcmVudE9yZ0lkIjoiNWZlMDU0OTQyN2JjMmUyNGM4OTEwODliIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjo2NzIsIm1vYmlsZSI6Ijk4NzY1NDMyMTAiLCJhZG1pbklkIjoiNWZlMDU0OTQyN2JjMmUyNGM4OTEwODljIiwiaWQiOjI2OCwicm9vdE9yZ0lkIjoiNWZlMDU0OTQyN2JjMmUyNGM4OTEwODliIiwiZW1haWwiOiJ0ZXN0ZXIub3JnMTFAZW1iaWJlLmNvbSJ9.MowaAmTjpMeXiog_-X9L_CArgjjG8bTP3zxl37dwcPJ3Mjuyh31cLqapnIXfZE1DPb2c33Ir3G-aCVWN6Eq1Aw; V_ID=ultimate.2020-10-07.f075da1eaaf5129210a099e26aa8d9d0; V_ID=ultimate.2020-10-07.f075da1eaaf5129210a099e26aa8d9d0; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MTZlODhmMWUyMWQ0MTY4OWFhMjkxYmUiLCJzdWJkb21haW4iOiJodHRwczovLzYxNmU4OGYxZTIxZDQxNjg5YWEyOTFiZS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-NDjpfceBqoueiPaF1Hy-MqUKvuTbAc4vo24YuxHBoQo1kP7uiXZBKkl4Blky7Lat0Q20X-mfxQYAwGXh5ZB8g; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTkgMTE6MDc6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.JlJpAWmv9xBL9E_3x5aDC4SkIC9-I23gAvIcxKrVVVxl7xUbv5ayD7OQhiajQVw4FifMGqYffa4BYPeuWDWPvQ'
}



        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/narad/v2/header/clearCache", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
