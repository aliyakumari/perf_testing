from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJIYXJpc2giLCJsYXN0TmFtZSI6IlAiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMjUgMDc6MTY6MjcgVVRDIiwicGFyZW50T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjgxNDcxNzI2OTYiLCJhZG1pbklkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNmIiwiaWQiOjE1MDAwNDQ5NjQsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImVtYWlsIjoiaGFyaXNocEBlbWJpYmUuY29tIn0.4_n22zdbQPmDN6fS-vfgFviQNT3LjmP51mBkcbaqN1bAdi4LgeKKz5rQYq-35BTzNfsxwldHYFzICARwSMpULg',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?1',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Mobile Safari/537.36',
  'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
  'Content-Type': 'application/json;charset=UTF-8',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en;q=0.9',
  'Cookie': 'preprod_ab_version=0; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2Mjk4NzU3ODEsIm9yZ2FuaXphdGlvbl9pZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImlkIjoxNTAwMDQ0OTY0LCJleHAiOjE2MzAwNDg1ODEsImRldmljZUlkIjoiMC41ODAwMDc1NzY1MjQ2NjU1IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJoYXJpc2hwQGVtYmliZS5jb20ifQ.EMY3Kqlmla0DYTUUmjIBz4cpoM8ONEHER5Bf_n_IE6cd59nSe6m5-Qk0Hk42fUnvwjkJkv5KAtm3SDT72CyPFw; preprod_embibe-refresh-token=eb387f0f-d7d0-494f-9a9e-c8efde6e5acd; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJIYXJpc2giLCJsYXN0TmFtZSI6IlAiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMjUgMDc6MTY6MjcgVVRDIiwicGFyZW50T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjgxNDcxNzI2OTYiLCJhZG1pbklkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNmIiwiaWQiOjE1MDAwNDQ5NjQsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImVtYWlsIjoiaGFyaXNocEBlbWJpYmUuY29tIn0.4_n22zdbQPmDN6fS-vfgFviQNT3LjmP51mBkcbaqN1bAdi4LgeKKz5rQYq-35BTzNfsxwldHYFzICARwSMpULg; V_ID=ultimate.2021-07-01.1467e9e769aeeb97a6568d5c1c40aafa; embibe-refresh-token=8e862ae7-f48d-4c4c-8473-3bd4aba333bc; preprod_embibe-refresh-token=48db71ac-56aa-4045-9eee-37fdaf30d5bb; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; org-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJvcmdfaWQiOiI2MTZlODhmMWUyMWQ0MTY4OWFhMjkxYmUiLCJzdWJkb21haW4iOiJodHRwczovLzYxNmU4OGYxZTIxZDQxNjg5YWEyOTFiZS1BLmVtYmliZS5jb20iLCJ1c2VyX3R5cGVfaWQiOjQsIndoaXRlX2xpc3RfaWdub3JlIjpmYWxzZX0.-NDjpfceBqoueiPaF1Hy-MqUKvuTbAc4vo24YuxHBoQo1kP7uiXZBKkl4Blky7Lat0Q20X-mfxQYAwGXh5ZB8g; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJNdW1iYWkiLCJsYXN0TmFtZSI6ImFkbWluIiwib3JnVHlwZSI6IkNoYWluT2ZTY2hvb2xzIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTkgMTE6MDc6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTE2MzY2ZjMzNDgyOTY2ZDlkYmZiZTAiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk4NDQzMjExODgiLCJhZG1pbklkIjoiNjExNjM2NmYzMzQ4Mjk2NmQ5ZGJmYmUxIiwiaWQiOjE1MDAwNDgyMzksInJvb3RPcmdJZCI6IjYxMTYzNjZmMzM0ODI5NjZkOWRiZmJlMCIsImVtYWlsIjoiYWRtaW5icHNAZ3VlcnJpbGxhbWFpbC5jb20ifQ.JlJpAWmv9xBL9E_3x5aDC4SkIC9-I23gAvIcxKrVVVxl7xUbv5ayD7OQhiajQVw4FifMGqYffa4BYPeuWDWPvQ'
}





        self.body =  "{\n    \"fields\": [\n        {\n            \"fieldGroupId\": 4,\n            \"name\": \"password\",\n            \"value\": \"Embibe@1234\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 4,\n            \"name\": \"PasswordOptions\",\n            \"value\": \"manual\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"firstName\",\n            \"value\": \"myfirstname\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"lastName\",\n            \"value\": \"mylastname\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"emailId\",\n            \"value\": \"testschool05@guerrillamail.com\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"rollNumber\",\n            \"value\": \"2423\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"mobile\",\n            \"value\": \"7000012347\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"dob\",\n            \"value\": \"03-08-2011\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"gender\",\n            \"value\": \"Female\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"state\",\n            \"value\": \"Andra Pradesh\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"city\",\n            \"value\": \"Bangalore\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 2,\n            \"name\": \"parentName\",\n            \"value\": \"Mothoer\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 2,\n            \"name\": \"parentMobile\",\n            \"value\": \"70000;.'12879\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 2,\n            \"name\": \"parentEmailId\",\n            \"value\": \"parent@test.com\",\n            \"fieldType\": \"BasicField\"\n        },\n        {\n            \"fieldGroupId\": 1,\n            \"name\": \"category\",\n            \"value\": \"obc\",\n            \"fieldType\": \"BasicField\"\n        }\n    ],\n    \"orgSubjectAssociations\": [\n        {\n            \"schoolId\": \"60c1e3d1335f5a5d446f6bce\",\n            \"schoolName\": \"Delhi Public school\",\n            \"board\": \"CBSE\",\n            \"boardId\": \"5ec5867a0c88fe5860961943|CBSE|cbse\",\n            \"grade\": \"12th CBSE\",\n            \"gradeId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse\",\n            \"section\": \"A\",\n            \"sectionId\": \"A\",\n            \"subject\": \"Biology\",\n            \"subjectId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE/Biology|cbse--12th cbse--biology\"\n        },\n        {\n            \"schoolId\": \"60c1e3d1335f5a5d446f6bce\",\n            \"schoolName\": \"Delhi Public school\",\n            \"board\": \"CBSE\",\n            \"boardId\": \"5ec5867a0c88fe5860961943|CBSE|cbse\",\n            \"grade\": \"12th CBSE\",\n            \"gradeId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse\",\n            \"section\": \"A\",\n            \"sectionId\": \"A\",\n            \"subject\": \"Mathematics\",\n            \"subjectId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE/Mathematics|cbse--12th cbse--mathematics\"\n        },\n        {\n            \"schoolId\": \"60c1e3d1335f5a5d446f6bce\",\n            \"schoolName\": \"Delhi Public school\",\n            \"board\": \"CBSE\",\n            \"boardId\": \"5ec5867a0c88fe5860961943|CBSE|cbse\",\n            \"grade\": \"12th CBSE\",\n            \"gradeId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse\",\n            \"section\": \"A\",\n            \"sectionId\": \"A\",\n            \"subject\": \"Chemistry\",\n            \"subjectId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE/Chemistry|cbse--12th cbse--chemistry\"\n        },\n        {\n            \"schoolId\": \"60c1e3d1335f5a5d446f6bce\",\n            \"schoolName\": \"Delhi Public school\",\n            \"board\": \"CBSE\",\n            \"boardId\": \"5ec5867a0c88fe5860961943|CBSE|cbse\",\n            \"grade\": \"12th CBSE\",\n            \"gradeId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse\",\n            \"section\": \"A\",\n            \"sectionId\": \"A\",\n            \"subject\": \"Physics\",\n            \"subjectId\": \"5ec5867a0c88fe5860961943|CBSE/12th CBSE/Physics|cbse--12th cbse--physics\"\n        }\n    ]\n}"
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.put("/narad/v2/student", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
