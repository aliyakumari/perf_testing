from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../Inbox/all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'accept': '*/*',
  'Content-Type': 'application/json',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJSYW0iLCJsYXN0TmFtZSI6IkoiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTggMTE6Mjg6MjkgVVRDIiwicGFyZW50T3JnSWQiOiI2MTBiNjY4NmE0ODNiZTdmYzk0MTc0YWQiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk2NjY2MDA3NzAiLCJhZG1pbklkIjoiNjEwYjY2ODdhNDgzYmU3ZmM5NDE3NGFlIiwiaWQiOjE1MDAwNDgwNzgsInJvb3RPcmdJZCI6IjYxMGI2Njg2YTQ4M2JlN2ZjOTQxNzRhZCIsImVtYWlsIjoicmFtMTIzQGdtYWlsLmNvbSJ9.0XtgO8JfMwt7SBmhiAY9SziGP7OEAqb3xK7MmdVYIGesR_Auh1u5DPxB92hmFwe1RQ-CUzG-21O6jEs9HV8LKg',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBcnVuIiwibGFzdE5hbWUiOiJLdW1hciIsIm9yZ1R5cGUiOiJDaGFpbk9mU2Nob29scyIsInRpbWVfc3RhbXAiOiIyMDIxLTExLTE4IDE0OjA2OjU3IFVUQyIsInBhcmVudE9yZ0lkIjoiNjBkNDM4ZjE4NzRiZTAwZjEwY2UzNzA3IiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDkwOTg5ODY3IiwiYWRtaW5JZCI6IjYwZDQzOGYyODc0YmUwMGYxMGNlMzcwOCIsImlkIjoxNTAwMDQ1NDc0LCJyb290T3JnSWQiOiI2MGQ0MzhmMTg3NGJlMDBmMTBjZTM3MDciLCJlbWFpbCI6ImFydW4ua3VtYXJAZW1iaWJlLmNvbSJ9.q6n3JHLfzV4o-uLP765NLyddjO1E7Em6n6foGhHanBVQXxmtztRqJh0uuIgnJ1ZUKgMJ2nMbr1aBGUHk5YDw-A; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}

        self.body = json.dumps([
  {
    "board": "ICSE",
    "boardId": "/5ec5867a0c88fe5860961943/CBSE",
    "grade": "4th ICSE",
    "gradeId": "5eef299c420f624a7cd798db|ICSE/4th ICSE|icse--4th icse",
    "isCustomGrade": False,
    "sections": [
      "B"
    ],
    "subjects": [
      {
        "name": "Science",
        "refId": "5eef299c420f624a7cd798db|ICSE/4th ICSE/Science|icse--4th icse--science",
        "isCustom": False
      }
    ],
    "id": "610b6760a483be7fc94174c1"
  }
])
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.put("/narad/v2/school/610b66baa483be7fc94174b1/updateClass", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
