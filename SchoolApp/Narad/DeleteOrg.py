from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import time
import gevent
import resource
import model.db as db

db.initNaradOrganizations()
cursor = db.narad_organisation.find({"name": "Dummy"}, {"rootOrgId":1})
root_org_id = list(cursor)



host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJqIiwibGFzdE5hbWUiOiJSYWoiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMjEgMTE6MzI6NDggVVRDIiwicGFyZW50T3JnSWQiOiI2MTBhYzJhYWE0ODNiZTdmYzk0MTc0N2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjkzODI2NDI2NzgiLCJhZG1pbklkIjoiNjEwYWMyYWFhNDgzYmU3ZmM5NDE3NDdjIiwiaWQiOjE1MDAwNDgwNzMsInJvb3RPcmdJZCI6IjYxMGFjMmFhYTQ4M2JlN2ZjOTQxNzQ3YiIsImVtYWlsIjoianJhajU1NUBnbWFpbC5jb20ifQ.ehSFgv0ksurDILj8BoRiCV5VP0wlMTM59rRMzGDaAlD8TGvPfbaf4XoBbbzr4CFrLtRdBTHagNlEOFZeZ166VQ',
  'Cookie': 'V_ID=ultimate.2020-10-07.f075da1eaaf5129210a099e26aa8d9d0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkRlZWJhIiwidGltZV9zdGFtcCI6IjIwMjEtMDktMDEgMTg6MDg6MjAgVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTY4MjE1Mzk2OSIsInJvb3RPcmdJZCI6IjYxMmY5NDg1MmQxZDNmNzYyYjUxZDgyOCIsImZpcnN0TmFtZSI6IkZhcmFoICIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYxMmY5NGRiMmQxZDNmNzYyYjUxZDgyYyIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYxMmZiNzk0MmQxZDNmNzYyYjUxZDg3NSIsImlkIjoxNTAwMDQ4NjI2LCJlbWFpbCI6IkZhcmFoLmRlZWJhQGVtYmliZS5jb20ifQ.2vrLOEJNhiEftBrDFPR0dRp_YbuRM5NrCXYXPwhLyCkBENA8mxI63OuUe4Qon2HlpW0esgFV3obGqbaPDp6ORA; V_ID=ultimate.2021-07-01.1467e9e769aeeb97a6568d5c1c40aafa; embibe-refresh-token=8e862ae7-f48d-4c4c-8473-3bd4aba333bc; preprod_embibe-refresh-token=48db71ac-56aa-4045-9eee-37fdaf30d5bb; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJKb24iLCJsYXN0TmFtZSI6IlJhdSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0wNyAxMTo0MToxOSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYxNWViYmI3MzQwYjc1MmUwMDk1MmJkMiIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiNjAwMDAyNzY3OCIsImFkbWluSWQiOiI2MTVlYmJiNzM0MGI3NTJlMDA5NTJiZDMiLCJpZCI6MTUwMDA1MzA1NCwicm9vdE9yZ0lkIjoiNjE1ZWJiYjczNDBiNzUyZTAwOTUyYmQyIiwiZW1haWwiOiJqYWNrc29uNDI3NjQ1ODFAZ3VlcnJpbGxhbWFpbC5jb20ifQ.d_rN0XPImnLWyIBQVvclXz77Tg8RGvlaUz9GE48pht5QBMJgvckjh7-d-D1lqZwS9s076eGzxATHgsOl-cMj5g; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        c = 0
        for data in root_org_id:
            c = c + 1
            if c > 10:
                url = f"/narad/v2/org/forceDelete?orgId={data.get('rootOrgId')}"
                response = self.client.delete(url, data=self.body,
                                     headers=self.headers)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
