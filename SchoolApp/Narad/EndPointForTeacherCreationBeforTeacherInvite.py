from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers ={
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJIYXJpc2giLCJsYXN0TmFtZSI6IlAiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMjUgMDc6MTY6MjcgVVRDIiwicGFyZW50T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjgxNDcxNzI2OTYiLCJhZG1pbklkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNmIiwiaWQiOjE1MDAwNDQ5NjQsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImVtYWlsIjoiaGFyaXNocEBlbWJpYmUuY29tIn0.4_n22zdbQPmDN6fS-vfgFviQNT3LjmP51mBkcbaqN1bAdi4LgeKKz5rQYq-35BTzNfsxwldHYFzICARwSMpULg',
  'Accept': 'application/json, text/plain, */*',
  'Content-Type': 'application/json;charset=UTF-8',
  'Cookie': 'preprod_embibe-refresh-token=eb387f0f-d7d0-494f-9a9e-c8efde6e5acd; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJIYXJpc2giLCJsYXN0TmFtZSI6IlAiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMjUgMDc6MTY6MjcgVVRDIiwicGFyZW50T3JnSWQiOiI2MGMxZTNkMTMzNWY1YTVkNDQ2ZjZiY2UiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjgxNDcxNzI2OTYiLCJhZG1pbklkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNmIiwiaWQiOjE1MDAwNDQ5NjQsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImVtYWlsIjoiaGFyaXNocEBlbWJpYmUuY29tIn0.4_n22zdbQPmDN6fS-vfgFviQNT3LjmP51mBkcbaqN1bAdi4LgeKKz5rQYq-35BTzNfsxwldHYFzICARwSMpULg; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}





    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):

        rannum = []
        num = randint(5000000,7000000)
        if not rannum.__contains__(num):
            rannum.append(num)
            self.body = json.dumps({
                "fields": [
                    {
                        "fieldGroupId": 1,
                        "name": "firstName",
                        "value": "TestFname",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 3,
                        "name": "password",
                        "value": "Embibe@123",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 3,
                        "name": "PasswordOptions",
                        "value": "manual",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 1,
                        "name": "lastName",
                        "value": "TestLname",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 1,
                        "name": "emailId",
                        "value": f"testceute{num}@embibe.com",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 1,
                        "name": "salutation",
                        "value": "Mr",
                        "fieldType": "BasicField"
                    },
                    {
                        "fieldGroupId": 1,
                        "name": "mobile",
                        "value": f"704{num}",
                        "fieldType": "BasicField"
                    }
                ],
                "teacherBranchRequests": [
                    {
                        "branchStatus": "Create",
                        "orgSubjectAssociations": [
                            {
                                "schoolId": "60c1e3d1335f5a5d446f6bce",
                                "schoolName": "Delhi Public school",
                                "grade": "10th Delhi Board",
                                "gradeId": "608ab1d5804d7ea1ab8f1cb8|Delhi Board/10th Delhi Board|delhi board--10th delhi board",
                                "section": "A",
                                "sectionId": "A",
                                "subject": "Science",
                                "subjectId": "608ab1d5804d7ea1ab8f1cb8|Delhi Board/10th Delhi Board/Science|delhi board--10th delhi board--science",
                                "isClassTeacher": False
                            }
                        ]
                    }
                ]
            })
            response = self.client.post("/narad/v2/teacher", data=self.body,
                                        headers=self.headers)
            if response.status_code == 400 or response.status_code == 200:
                return response.success()




def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
