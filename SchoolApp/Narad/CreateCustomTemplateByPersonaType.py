from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'accept': '*/*',
  'Content-Type': 'application/json',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJSYWoiLCJsYXN0TmFtZSI6IkhwcyIsIm9yZ1R5cGUiOiJDaGFpbk9mU2Nob29scyIsInRpbWVfc3RhbXAiOiIyMDIxLTA4LTI2IDE0OjQ0OjA5IFVUQyIsInBhcmVudE9yZ0lkIjoiNjExMmFiZWI4ZWQ5Y2QxMWY3ZjdmNWMzIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODY2NzU4ODk5IiwiYWRtaW5JZCI6IjYxMTJhYmViOGVkOWNkMTFmN2Y3ZjVjNCIsImlkIjoxNTAwMDQ4MTk4LCJyb290T3JnSWQiOiI2MTEyYWJlYjhlZDljZDExZjdmN2Y1YzMiLCJlbWFpbCI6InJhamhwc0BndWVycmlsbGFtYWlsLmNvbSJ9.TCmu4F5m12rGtQKOj2R_XZWyGpvlqlmU_WNV2ZjayYc04Zq7A3SJ-iUJ9RprwdPx3Y1BUArpPGll8yYOlG595w',
  'Cookie': 'V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}




        self.body = json.dumps([
    {
        "allowedValues": [
            {}
        ],
        "editable": True,
        "fieldGroupId": 0,
        "fieldType": "BasicField",
        "isAggregatable": True,
        "isPreFilled": True,
        "label": "string",
        "name": "string",
        "required": True,
        "rootAttribute": True,
        "uiElementType": "TEXT",
        "valueType": "STRING",
        "visibility": True
    }
])
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/narad/v2/customTemplate?personaType=Teacher", data=self.body,
                                        headers=self.headers)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
