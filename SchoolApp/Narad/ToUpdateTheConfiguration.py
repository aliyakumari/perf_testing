from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6InMiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMi0yNCAxMToxODoyNSBVVEMiLCJtb2JpbGUiOiI4OTg4OTk5OTk5Iiwicm9vdE9yZ0lkIjoiNjNmODljZTkxZGIxNGYwNTVmNDcxMTVhIiwiZmlyc3ROYW1lIjoiYWEiLCJvcmdUeXBlIjoiU2Nob29sIiwidXNlcl90eXBlIjo0LCJwYXJlbnRPcmdJZCI6IjYzZjg5Y2U5MWRiMTRmMDU1ZjQ3MTE1YSIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYzZjg5Y2U5MWRiMTRmMDU1ZjQ3MTE1YiIsImlkIjoxNTA0OTUxNDI4LCJlbWFpbCI6ImFzQGVtYmliZS5jb20ifQ.StwjqGv2DH9yOVvo-sI-f9xoBiNEJbi_2ml4PS49GKkYDtFoo_lVDTfW8ZGjxwXwxxvjzKvA9ycgXL3iHDlPyA',
  'Content-Type': 'application/json',
  'Cookie': 'prod_embibe-refresh-token=c942d4bf-aa60-4f57-867d-b8768af1b301; V_ID=ultimate.2022-03-23.03ab9d8616cf9a563f29d849fc297a1d; school_preprod_embibe-refresh-token=edd418b8-dcfd-4524-af3e-14307a08d414; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJDaGV0YW4iLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjItMDgtMjIgMTc6MjQ6MzQgVVRDIiwicGFyZW50T3JnSWQiOiI2MzAzNjNjMWNjODAzZDA5ZTE4ZjA2ZWYiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijg4OTAzMDA0NDEiLCJhZG1pbklkIjoiNjMwMzYzYzFjYzgwM2QwOWUxOGYwNmYwIiwiaWQiOjIwMDY2NDA0NTksInJvb3RPcmdJZCI6IjYzMDM2M2MxY2M4MDNkMDllMThmMDZlZiIsImVtYWlsIjoibWRhY2FkZW15dWRhaXB1ckBnbWFpbC5jb20ifQ.E2MaqvhxnAlSfAhP8WkvomIrsR3JSVVmN4bxmQVUc1B4NTXSlXpnmq6ijYhcwmzt9zOIzGZdjpY-LfAIACxCtA; JSESSIONID=110E2F32C32B6A363533BA072C9D92BB; JSESSIONID=591F025C674E4E4F3DBD7E6C3C0D806F; preprod_ab_version=0; preprod_embibe-refresh-token=67841aa2-445b-4c77-b681-6373e760df45; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzcwNjc5MDUsIm9yZ2FuaXphdGlvbl9pZCI6IjYzOTk5ZmNhOGJmOWVkMzEyZjEyOGJkNyIsIm1vYmlsZSI6IjkwMTE3ODY3MDkiLCJpZCI6MTUwNDgxNjIwNywiZXhwIjoxNjc3MTU0MzA1LCJkZXZpY2VJZCI6IjE2NjEyMzcwODY2MTEiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoic2FuZGlwLnNhaHVAZW1iaWJlLmNvbSIsInBsYXRmb3JtIjoiV0VCIn0.7FLXEW953qGi8H6EFfxpzD4kW5SVWirgcfkgfhwTl9ruM4_3iR_Wgl-wtE7Tj1N6Qx3KjAFJWwJsTbKtcuJ-ag; preprod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjYxMjM3MDg2NjExIiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJMk16azVPV1pqWVRoaVpqbGxaRE14TW1ZeE1qaGlaRGNpTENKMGNtRmphMTlwWkNJNklqSTJPV016Wm1abUxXSmxZbVl0TkdZeVlpMDRZV05pTFROak5XWTBaV05pT0dJd01DSXNJbTkwY0Y5eVpYRjFaWE4wWDJsa0lqb2lNalkwTmpnNE5UQXRNMlF6T0MwMFpXRTBMV0l4TVdNdE1ETXdaR05qTkRWak5UVmlJaXdpYVdRaU9pSnpZVzVrYVhBdWMyRm9kVUJsYldKcFltVXVZMjl0SWl3aWJHOWpZV3hsSWpvaVpXNGlMQ0p3YkdGMFptOXliU0k2SWxkRlFpSjkuaVZCMHYtNkJiSU8zdDAySGhySG51WjRESVZhR3RVTlNlT0U1NlhQQU5sUnhXdHJ0QkJ5UkkxdTFhdXJra1JuMHc5aFdVSldMbnNWVHlNcnNaQzZQcHciLCJjcmVhdGVkIjoxNjc3MDY5NTgxLCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduSW4iLCJhdHRlbXB0IjoxLCJ0dGwiOiI2MDAiLCJwbGF0Zm9ybSI6IldFQiIsImNvbm5lY3RlZF9yZXNwb25zZSI6dHJ1ZSwicmVsYXRpb25zaGlwX3R5cGUiOjAsInVzZXJfaWQiOiJzYW5kaXAuc2FodUBlbWJpYmUuY29tIiwib3JnX2lkIjoiNjM5OTlmY2E4YmY5ZWQzMTJmMTI4YmQ3IiwiY291bnRyeV9jb2RlX2lkIjoxLCJ1c2VyX3R5cGVfaWQiOjMsImlkIjoxNTA0ODE2MjA3LCJleHBpcnkiOjE2NzcxNTU5ODEsImpvdXJuZXlfaWQiOiJTSUdOX0lOX2ZkZTFmMmVkLWY1MGQtNDJmYi05Njg4LTllY2MxMWZhMjkxYSIsImV4cCI6MTY3NzE1NTk4MX0.gyWuCQzJxadns1kqjYegt5LsGxQlve6zTrCCO3OgsDgpvNHVOAejdMAKXKBJM90WOmSEsPQbcqMrgqjT9t0kUw; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=7ff23e16-a124-4d3e-ba68-a8f692adcbeb; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzcwNjk4ODUsIm9yZ2FuaXphdGlvbl9pZCI6IjYzOTk5ZmNhOGJmOWVkMzEyZjEyOGJkNyIsIm1vYmlsZSI6Ijk1MjE0NzM0MjMiLCJpZCI6MTUwNDgxOTA5MCwiZXhwIjoxNjc3MTU2Mjg1LCJkZXZpY2VJZCI6IjAuODAxMTM1NjY1NTI1MzA3NSIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6dHJ1ZSwiZW1haWwiOiJhbWFuLnNoZWtoYXdhdEBlbWJpYmUuY29tIn0.onwlUZp67hbKcYbbjbQivev5__Pd362nM6HvExbtHDUPc7YDC1Dki9NpV4EpsX8-6B9udJu0vjKDUILZSMfWbQ; JSESSIONID=D46024B497FE509F38848F19BA32EF5A; preprod_embibe-refresh-token=67841aa2-445b-4c77-b681-6373e760df45; school_preprod_embibe-refresh-token=7ff23e16-a124-4d3e-ba68-a8f692adcbeb; JSESSIONID=8C3C42D3928C7860BD43A43848EF4CE9; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=4ce4f16f-359a-4d52-a11e-be48c5019ce0'
}
        self.body = json.dumps({
    "name": "School Config",
    "description": "School Config",
    "data": {
        "academicYear" : {
            "start_date": "2022-03-01", 
            "end_date" : "2023-03-31"
        }
    },
    "personaType": "School"
})

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        response = self.client.put("/narad/orgConfig/update?personaType=School&orgId=63f89d01b12a106969b51f81", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
