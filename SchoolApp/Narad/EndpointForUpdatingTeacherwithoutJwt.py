from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'Pragma': 'no-cache',
  'Cache-Control': 'no-cache',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMS0xNiAxNTowOTowNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.uvioEgnuIuRR4TZYBoNQKfP0sepLKunc3dboJd3nktZX0s823Ai8r7f7VQQG6wSiPMmc-PI1fkBTY0s7X_btFw',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
  'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"macOS"',
  'Content-Type': 'application/json',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': '_gcl_au=1.1.297177274.1636872121; __insp_uid=2553447123; ajs_anonymous_id=%22d6003a19-feb9-48bd-a31f-9a9180ea9353%22; preprod_ab_version=0; ajs_user_id=1500056716; __insp_wid=875671237; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL2xlYXJuL2hvbWU%3D; __insp_targlpt=TGVhcm4gOXRoIENCU0UgQm9vayBDaGFwdGVycyAmIFZpZGVvIFNvbHV0aW9ucyBPbmxpbmUgLSBFbWJpYmU%3D; __insp_sid=2810221160; _ga_TT8L73VP3H=GS1.1.1636956777.7.1.1636956804.33; _ga=GA1.2.1104003303.1636872121; _hp2_id.2562464476=%7B%22userId%22%3A%224687987964385578%22%2C%22pageviewId%22%3A%227920812827818317%22%2C%22sessionId%22%3A%225349916078919101%22%2C%22identity%22%3A%221500056716%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; __insp_pad=3; __insp_slim=1636956812375; prod_embibe-refresh-token=5b7a6400-3195-45d9-9fbe-677ffdd1569c; prod_ab_version=0; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzcwNDkxMjUsIm9yZ2FuaXphdGlvbl9pZCI6IjYxOGNiZjY5MjJjNzU2MWMzNmMyNDhkNSIsIm1vYmlsZSI6IjgyMzc0OTY0OTkiLCJpZCI6MjAwMTA4NTYzNCwiZXhwIjoxNjM4MjU4NzI1LCJkZXZpY2VJZCI6IjAuODgyNDYwNDU0MDMxNzQzMyIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoidGFuaW1hLnNoYXJtYUBlbWJpYmUuY29tIn0.NNgrjhZRG9B1ocaF8fVU1PQvB7_ars3xdrhp48-612b1sb2FkJrygPPZn6m8BadtwJwWJA6NJ_-enhVXJZLn8A; preprod_embibe-refresh-token=7037a897-959d-4df3-89ae-8d37d76794fc; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzcwNzUzNDUsIm9yZ2FuaXphdGlvbl9pZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImlkIjoxNTAwMDQ1MDEyLCJleHAiOjE2MzcyNDgxNDUsImRldmljZUlkIjoiMC4xMjg2NzQyMjMyNzg3NTE5IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjp0cnVlLCJlbWFpbCI6InJhamVzaGtAZW1iaWJlLmNvbSJ9.nx_dPjGDYTYp3zLrZo3-2VNlab3kypf_f1s4flVoid_v42M63Zq9Hdq1FSAnvqfFxFKEL9DbAEVoXIBiH_zQww; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMS0xNiAxNTowOTowNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.uvioEgnuIuRR4TZYBoNQKfP0sepLKunc3dboJd3nktZX0s823Ai8r7f7VQQG6wSiPMmc-PI1fkBTY0s7X_btFw; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoicmVkZHkiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTEtMTYgMTI6Mzg6MzggVVRDIiwicGFyZW50T3JnSWQiOiI2MTVkM2YwMjM0MGI3NTJlMDA5NTJiM2IiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5MzM5OTg4NzYiLCJhZG1pbklkIjoiNjE1ZDNmMDMzNDBiNzUyZTAwOTUyYjNjIiwiaWQiOjE1MDAwNTMwMjAsInJvb3RPcmdJZCI6IjYxNWQzZjAyMzQwYjc1MmUwMDk1MmIzYiIsImVtYWlsIjoiYWRtaW5yZWRkeUBnbWFpbC5jb20ifQ.jyH8JLBCziCt3KvNA-85fEu7-L4BxLK1j37we1KCRff-CB-YrO0m7qj0lXWUlLQOkDGqct21rWagNqzDEpYleg'
}



        self.body = json.dumps({
    "fields": [
        {
            "fieldGroupId": 1,
            "name": "salutation",
            "value": "Mr",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "firstName",
            "value": "Rakesh",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "lastName",
            "value": "Kumar  ",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "emailId",
            "value": "rajeshk@embibe.com",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "mobile",
            "value": "9000017879",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "college",
            "value": "clg1632507005",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "qualification",
            "value": "Btech",
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 1,
            "name": "experience",
            "value": 9,
            "fieldType": "BasicField"
        },
        {
            "fieldGroupId": 0,
            "name": "profilePictureUrl",
            "row": 0,
            "value": "https://d3ae2d36axjvwk.cloudfront.net/preprod/instituteLogo/50532010996907embibee.png",
            "fieldType": "BasicField"
        }
    ],
    "teacherBranchRequests": [
        {
            "orgSubjectAssociations": [
                {
                    "subject": "Chemistry",
                    "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Chemistry|cbse--12th cbse--chemistry",
                    "schoolName": "Delhi Public school",
                    "schoolId": "60c1e569335f5a5d446f6bd2",
                    "className": "12th CBSE-B",
                    "classId": "60c1fe9d9c95f329eb741a51",
                    "isClassTeacher": True,
                    "grade": "12th CBSE",
                    "gradeId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse",
                    "section": "B",
                    "board": "CBSE",
                    "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse",
                    "books": []
                },
                {
                    "subject": "Physics",
                    "subjectId": "5ec5867a0c88fe5860961943|CBSE/11th CBSE/Physics|cbse--11th cbse--physics",
                    "schoolName": "Delhi Public school",
                    "schoolId": "60c1e569335f5a5d446f6bd2",
                    "className": "11th CBSE-A",
                    "classId": "60c1fe9d9c95f329eb741a52",
                    "isClassTeacher": True,
                    "grade": "11th CBSE",
                    "gradeId": "5ec5867a0c88fe5860961943|CBSE/11th CBSE|cbse--11th cbse",
                    "section": "A",
                    "board": "CBSE",
                    "boardId": "5ec5867a0c88fe5860961943|CBSE|cbse",
                    "books": []
                }
            ]
        }
    ]
})
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.put("/narad/v2/teacher", data=self.body,
                                        headers=self.headers)



def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
