from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'Accept': 'application/json, text/plain, */*',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJUIiwibGFzdE5hbWUiOiIxIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTEyLTIxIDExOjM2OjA4IFVUQyIsInBhcmVudE9yZ0lkIjoiNjFhZjRiMGFiMTVkNDQ2MDk3NTA4NjZjIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTk4MjgyNDQ3MCIsImFkbWluSWQiOiI2MWFmNGMzYzAzOTkzOTAyMzg2MGVhZDYiLCJpZCI6MTUwMTE4NTUyNCwicm9vdE9yZ0lkIjoiNjFhZjRhZTAyOGU2OTkwNzllOWFjMDA3IiwiZW1haWwiOiJ0MUBzY2hvb2wyLmVtYmliZS5jb20ifQ.4zHhwVhSkN5DySnV0UToP-0uE_Vh7Qs56gNAnXn--lVdW5MMGnvP9Efid5uLr0R1esIW6E7EtIJ41u5DJjEbsw',
            'sec-ch-ua-platform': '"macOS"',
            'Cookie': 'ab_version=6; access-token=access-token-value; client=client-token-value; embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIxLTEyLTI3IDEyOjUwOjQ4IFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwib3JnYW5pemF0aW9uX2lkIjo5NjEsImlkIjo0LCJlbWFpbCI6ImFqYXkucmFud2FpQGVtYmliZS5jb21fbnRhIn0.Mn3jC9u-Yh9e7Qn2HRmo0mJqctk2_9_yLRiYzXa0i7yCtwrRSYvgpIg8hOZiA6nm262koCWg6gMr4Ml19JuUzQ; uid=ajay.ranwai@embibe.com_nta'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get(
            "/track/v1/masteryImprovement?classId=61af4bc94fe5742d75de77af&orgId=61af4b0ab15d44609750866c&startDate=1639481767&endDate=1640086567&kvCode=kve266089--kve288918--kve288919&filter=weekly&locale=en&improvementFilter=bottom",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
