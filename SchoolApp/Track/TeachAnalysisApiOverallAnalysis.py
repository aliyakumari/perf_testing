from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'Accept': 'application/json, text/plain, */*',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJTaGFzaGFuayIsImxhc3ROYW1lIjoiTWFuaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMi0yNCAxMDozNjo0NCBVVEMiLCJwYXJlbnRPcmdJZCI6IjYxYWY0YjBhYjE1ZDQ0NjA5NzUwODY2YyIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJhZG1pbklkIjoiNjFhZjRjM2MwMzk5MzkwMjM4NjBlYWQ2IiwiaWQiOjE1MDExODU1MjQsInJvb3RPcmdJZCI6IjYxYWY0YWUwMjhlNjk5MDc5ZTlhYzAwNyIsImVtYWlsIjoidDFAc2Nob29sMi5lbWJpYmUuY29tIn0.RcRqKO2YmcQLYZZTx_1SPmileFchu43NDuqypiSuzJ5YM5sdL5c1kbHyZHGWuaNh5llsHJUnvGGh74NRtyJzNw',
            'sec-ch-ua-platform': '"macOS"',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': 'JSESSIONID=54DBFA996839BBC6361369C4BA72A8C0; prod_ab_version=0; prod_embibe-refresh-token=900f9fa2-b291-44af-a373-64e2c76392cc; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2Mzk0ODY0MzMsIm9yZ2FuaXphdGlvbl9pZCI6IjYxYjg5MzY4ZjlhMjNlNWU0NjM0Y2UwZSIsIm1vYmlsZSI6Ijk4NzgyNDQ5NjkiLCJpZCI6MjAwMTczNDI2NywiZXhwIjoxNjQwNjk2MDMzLCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZ3Nzc3NwaGFkbWluQGVtYmliZS5jb20ifQ.9xSRzMPfHAIq6gbdtGbEp0ADT-vTdrZf_qaqIrfkdPh4E8ncPHjEG_qVwFB2lnCxyXwE_EmN3CnHaH7CtngLvg; JSESSIONID=A2F48AE36A62C10614D344128DF18289; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }
        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get(
            "/track/v1/teachAnalysis?classId=61af4bc94fe5742d75de77af&filter=week&kvCode=kve266089--kve288918--kve288919&startDate=1639737410&endDate=1640342210&offset=0&orgId=61af4b0ab15d44609750866c&size=10",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
