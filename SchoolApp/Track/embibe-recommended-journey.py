from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

df = pd.read_csv('Embibe_Token - preprod profile token.csv')
embibe_token = df.values.tolist()
embibe_token.pop()


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlYWNoZXIiLCJ0aW1lX3N0YW1wIjoiMjAyMi0xMS0yOSAwNzowOTowMyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODk5ODk5OTAwIiwicm9vdE9yZ0lkIjoiNjIyMWZmZWRhZjk2ZTE0NDgzYTIyZWY2IiwiZGV2aWNlSWQiOiIwLjgwMjUzOTEwMzgyMDI2NTQiLCJmaXJzdE5hbWUiOiJBbndhciBTaGVpa2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.fxpLEgEs9pZ5kt0H4CmRlbFvt0T287Sj07ISTrvcIou10nAaedzAbhDt2QODJJSNFlMkYpE2zp_0CW4o-n-YwQ',
            'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'Cookie': 'preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }
        
        self.body = json.dumps({"classId": "62220082fdd2612096bcc794",
                               "subjectId": "kve97670--kve99731--kve99742", "kvCode": ["kve103114"]})

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(embibe_token))
        # self.headers['embibe-token']=embibe_token[rnum][1]
        url = '/track/v2/embibe-recommended-journey?locale=en'
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
