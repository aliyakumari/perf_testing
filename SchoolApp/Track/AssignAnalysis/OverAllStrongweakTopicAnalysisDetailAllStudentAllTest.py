from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': '*/*',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMTIgMDk6NTc6NDggVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTAwMDAxMjM4NSIsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImZpcnN0TmFtZSI6IlJhamVzaCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYwYzFlNTY5MzM1ZjVhNWQ0NDZmNmJkMiIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYwYzIyOTQ0ODQ3YmRhN2UxNDM5N2ExNSIsImlkIjoxNTAwMDQ1MDEyLCJlbWFpbCI6InJhamVzaGtAZW1iaWJlLmNvbSJ9.um99jY3xfkS3VPSAbggfoKfvy819QRYfIVnP8KcTw2gLZFq71XQzaGpucgnC788MTpihOyvnCTHDCmn_-C353A',
            'Cookie': 'JSESSIONID=C4EC60259F81C2610AB696C75FFDAEEF; V_ID=ultimate.2021-04-21.55a99c16240f5238b79a8983ffc6dc71; JSESSIONID=051A9B2CE5183FBCA85FF54E2F4DD5DE; JSESSIONID=A2B852C66C029955F481FFB87CFFF683; prod_ab_version=0; prod_embibe-refresh-token=900f9fa2-b291-44af-a373-64e2c76392cc; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2Mzk0ODY0MzMsIm9yZ2FuaXphdGlvbl9pZCI6IjYxYjg5MzY4ZjlhMjNlNWU0NjM0Y2UwZSIsIm1vYmlsZSI6Ijk4NzgyNDQ5NjkiLCJpZCI6MjAwMTczNDI2NywiZXhwIjoxNjQwNjk2MDMzLCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZ3Nzc3NwaGFkbWluQGVtYmliZS5jb20ifQ.9xSRzMPfHAIq6gbdtGbEp0ADT-vTdrZf_qaqIrfkdPh4E8ncPHjEG_qVwFB2lnCxyXwE_EmN3CnHaH7CtngLvg; JSESSIONID=1BE13D900C7B406537F6490E895A64EB; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get(
            "/track/v1/assignAnalysis/chapterAnalysisDetail?analysisType=strong&classId=61af4bc94fe5742d75de77af&testId=all&kvCode=kve266089--kve288918--kve288919",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
