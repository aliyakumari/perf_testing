from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJSYWoiLCJsYXN0TmFtZSI6IlRlYWNoZXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTItMDYgMTM6MTY6MTUgVVRDIiwicGFyZW50T3JnSWQiOiI2MTk0YTk4YWYzZDViZjNhZjFlZmUzMDMiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODc3NTQzMTkwIiwiYWRtaW5JZCI6IjYxOTRhYTQ5ZjNkNWJmM2FmMWVmZTMxMCIsImlkIjoxNTAxMTYyNzAzLCJyb290T3JnSWQiOiI2MTk0YTk2NWZkZmUwMjJlOWEwZmYzYmIiLCJlbWFpbCI6InJhal90ZWFjaGVyNTU1QGdtYWlsLmNvbSJ9.-5guo9L-icqKvY2i7vhlkNXkl6y46yoH7E7S537IiA6tvZcCoDfuei0mas_Nd7qfmmdsgm6AwjFaVriidXLLaA',
            'Cookie': 'JSESSIONID=1BE13D900C7B406537F6490E895A64EB; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get(
            "/track/v1/assignAnalysis/avgTimeSpentTest?classId=6194a9c0f3d5bf3af1efe30d&testId=all&kvCode=kve97670--kve97915--kve97916",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
