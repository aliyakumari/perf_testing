from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("../all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'accept': '*/*',
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJUIiwibGFzdE5hbWUiOiIxIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTEyLTIxIDEyOjQwOjU0IFVUQyIsInBhcmVudE9yZ0lkIjoiNjFhZjRiMGFiMTVkNDQ2MDk3NTA4NjZjIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTk4MjgyNDQ3MCIsImFkbWluSWQiOiI2MWFmNGMzYzAzOTkzOTAyMzg2MGVhZDYiLCJpZCI6MTUwMTE4NTUyNCwicm9vdE9yZ0lkIjoiNjFhZjRhZTAyOGU2OTkwNzllOWFjMDA3IiwiZW1haWwiOiJ0MUBzY2hvb2wyLmVtYmliZS5jb20ifQ.ujLTz_TPy1nOoHSD1LLYVDVX2_-8PkAj5ejUHWIlOwslu0el_Xr8YYCJqnYj4HxyTiM6EOJm_Yzq7M1FIk2maQ',
            'Cookie': 'JSESSIONID=C4EC60259F81C2610AB696C75FFDAEEF; JSESSIONID=051A9B2CE5183FBCA85FF54E2F4DD5DE; JSESSIONID=A2B852C66C029955F481FFB87CFFF683; prod_ab_version=0; prod_embibe-refresh-token=900f9fa2-b291-44af-a373-64e2c76392cc; prod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjQsImNyZWF0ZWQiOjE2Mzk0ODY0MzMsIm9yZ2FuaXphdGlvbl9pZCI6IjYxYjg5MzY4ZjlhMjNlNWU0NjM0Y2UwZSIsIm1vYmlsZSI6Ijk4NzgyNDQ5NjkiLCJpZCI6MjAwMTczNDI2NywiZXhwIjoxNjQwNjk2MDMzLCJkZXZpY2VJZCI6IiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiZ3Nzc3NwaGFkbWluQGVtYmliZS5jb20ifQ.9xSRzMPfHAIq6gbdtGbEp0ADT-vTdrZf_qaqIrfkdPh4E8ncPHjEG_qVwFB2lnCxyXwE_EmN3CnHaH7CtngLvg; JSESSIONID=1BE13D900C7B406537F6490E895A64EB; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get(
            "/track/v1/assignAnalysis/testScore?classId=61af4bc94fe5742d75de77af&kvCode=kve266089--kve288918--kve288919",
            data=self.body,
            headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
