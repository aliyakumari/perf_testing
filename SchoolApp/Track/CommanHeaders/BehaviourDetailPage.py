from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'sec-ch-ua': '"Chromium";v="100", "Google Chrome";v="100", ";Not A Brand";v="99"',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4692.56 Safari/537.36',
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJTaGFzaGFuayIsImxhc3ROYW1lIjoiTWFuaSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMi0yOCAxMDoxODoyOSBVVEMiLCJwYXJlbnRPcmdJZCI6IjYxYWY0YjBhYjE1ZDQ0NjA5NzUwODY2YyIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijk5ODI4MjQ0NzAiLCJhZG1pbklkIjoiNjFhZjRjM2MwMzk5MzkwMjM4NjBlYWQ2IiwiaWQiOjE1MDExODU1MjQsInJvb3RPcmdJZCI6IjYxYWY0YWUwMjhlNjk5MDc5ZTlhYzAwNyIsImVtYWlsIjoidDFAc2Nob29sMi5lbWJpYmUuY29tIn0.q-jXp5a-xPLINg7QlsXLFuioGHvc_ByFvg2sO97ififPOhRBAS7WHVPNsWK31Lfa7ieFvKNo4W_G6xhl6g-r1A',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8,hi;q=0.7',
  'Cookie': 'ab_version=6; access-token=access-token-value; client=client-token-value; embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIxLTEyLTI3IDEyOjUwOjQ4IFVUQyIsImlzX2d1ZXN0IjpmYWxzZSwib3JnYW5pemF0aW9uX2lkIjo5NjEsImlkIjo0LCJlbWFpbCI6ImFqYXkucmFud2FpQGVtYmliZS5jb21fbnRhIn0.Mn3jC9u-Yh9e7Qn2HRmo0mJqctk2_9_yLRiYzXa0i7yCtwrRSYvgpIg8hOZiA6nm262koCWg6gMr4Ml19JuUzQ; uid=ajay.ranwai@embibe.com_nta'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        url = "/track/v1/commonHeaders/trackOverviewChildCards?classId=61af4bc94fe5742d75de77af&endDate=1640024999&filter=behaviour&kvCode=kve266089--kve288918--kve288919&orgId=61af4b0ab15d44609750866c&startDate=1639852200"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
