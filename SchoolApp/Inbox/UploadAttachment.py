from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
import uuid
import requests




# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Content-Type': 'multipart/form-data',
            # 'User-Agent':'curl/7.58.0',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJBbmt1c2giLCJsYXN0TmFtZSI6IkFyb3JhIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTA0LTE1IDEwOjM5OjQ2IFVUQyIsInBhcmVudE9yZ0lkIjoiNjA3ODE3ZDBhYzMxNzYwNTNmNTYwMWZhIiwicGVyc29uYVR5cGUiOiJBZG1pbmlzdHJhdG9yIiwib3JnYW5pemF0aW9uX2lkIjoxODM0LCJtb2JpbGUiOiI3OTA4OTA5ODkwIiwiYWRtaW5JZCI6IjYwNzgxN2QxYWMzMTc2MDUzZjU2MDFmYyIsImlkIjo3NjIsInJvb3RPcmdJZCI6IjYwNzgxN2QwYWMzMTc2MDUzZjU2MDFmYSIsImVtYWlsIjoiYW5rdXNoQGdtYWlsLmNvbSJ9.owWi47zX5JIRVkapWrZ8rBHvxwBxuomkKGVSlUHqM9zBMCHGfXAOyINDR6AHv3rO_1wkiXnXObhVkHsdWKP8zQ',
  'Cookie': 'V_ID=ultimate.2020-10-07.4197d896338cbb88d92b882e5d070075; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}

        self.files = open('user_id.csv', 'rb')


        self.body = {}
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        with open("user_id.csv") as f:
            response = self.client.post("/inbox/upload/attachment", data=self.body,
                                        headers=self.headers, files = f)

def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
