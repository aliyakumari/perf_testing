from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJyYWoiLCJsYXN0TmFtZSI6InRlYWNoZXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTggMDk6MTI6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTRkNzkyNGUxMjllODA0ODI5MWFkNTciLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5OTMzOTk4ODkwIiwiYWRtaW5JZCI6IjYxNGQ3YTA1ZTEyOWU4MDQ4MjkxYWQ2YSIsImlkIjoxNTAwMDUxNDgyLCJyb290T3JnSWQiOiI2MTRkNzhhZWUxMjllODA0ODI5MWFkNTMiLCJlbWFpbCI6ImphbmdhX3JhajFAZ21haWwuY29tIn0.YluKukTShGUs4yGstEOLBJuMWtAbH3D9CopUCFpmFpuHdZF75z0SDZZqMP2Kdejsi04edOVODoaEeGA7KQkLtA',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36',
  'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"macOS"',
  'Content-Type': 'application/json',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': '_ga=GA1.2.2014931764.1634196691; ajs_anonymous_id=%224f069bb6-b571-455d-9725-e70bb93227bc%22; ajs_user_id=1500051540; _gcl_au=1.1.1044372505.1634220681; WZRK_G=0bfad27d81034d5ba94e731b5c7cccce; _hp2_id.2562464476=%7B%22userId%22%3A%22396911129165663%22%2C%22pageviewId%22%3A%227847718501910452%22%2C%22sessionId%22%3A%228911022951353137%22%2C%22identity%22%3A%221500051540%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%7D; __insp_wid=875671237; __insp_slim=1634220683856; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL2xlYXJuL2hvbWU%3D; __insp_targlpt=TGVhcm4gU0JJIENsZXJrIFByZWxpbXMgQm9vayBDaGFwdGVycyAmIFZpZGVvIFNvbHV0aW9ucyBPbmxpbmUgLSBFbWJpYmU%3D; __insp_norec_sess=true; preprod_ab_version=0; preprod_embibe-refresh-token=71d93813-b189-49b1-9fd6-537002abe566; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzQ1NDEwNDAsIm9yZ2FuaXphdGlvbl9pZCI6IjYxNGQ3OGFlZTEyOWU4MDQ4MjkxYWQ1MyIsIm1vYmlsZSI6Ijk5MzM5OTg4OTAiLCJpZCI6MTUwMDA1MTQ4MiwiZXhwIjoxNjM0NzEzODQwLCJkZXZpY2VJZCI6IjAuMDAzMjM2ODIzNjM5MzY2OTQ4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJqYW5nYV9yYWoxQGdtYWlsLmNvbSJ9.rztRc58tvF91YOc3YXYTa35JJEsYordxv66thFr2oLLR9ovZncAhx_JIV73ZttLfsHy5dRCwVg2MkWDgiR5P5A; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJyYWoiLCJsYXN0TmFtZSI6InRlYWNoZXIiLCJvcmdUeXBlIjoiU2Nob29sIiwidGltZV9zdGFtcCI6IjIwMjEtMTAtMTggMDk6MTI6NDMgVVRDIiwicGFyZW50T3JnSWQiOiI2MTRkNzkyNGUxMjllODA0ODI5MWFkNTciLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5OTMzOTk4ODkwIiwiYWRtaW5JZCI6IjYxNGQ3YTA1ZTEyOWU4MDQ4MjkxYWQ2YSIsImlkIjoxNTAwMDUxNDgyLCJyb290T3JnSWQiOiI2MTRkNzhhZWUxMjllODA0ODI5MWFkNTMiLCJlbWFpbCI6ImphbmdhX3JhajFAZ21haWwuY29tIn0.YluKukTShGUs4yGstEOLBJuMWtAbH3D9CopUCFpmFpuHdZF75z0SDZZqMP2Kdejsi04edOVODoaEeGA7KQkLtA; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}

        self.body =json.dumps({
  "messageType": "NOTICE",
  "receivers": [],
  "messageBody": "bkbjkkjbk",
  "messageId": "",
  "timestamp": 1634552537965
})
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/inbox/drafts/message", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
