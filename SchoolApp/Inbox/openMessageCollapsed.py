from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJhIiwibGFzdE5hbWUiOiJwIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTEwLTE4IDA3OjEyOjE3IFVUQyIsInBhcmVudE9yZ0lkIjoiNjBiNjJhNzIxYTg1MmY1MGQzNGViMzg0IiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODc4NzU2NTQ2NyIsImFkbWluSWQiOiI2MGI2MzU3YzFhODUyZjUwZDM0ZWIzYzAiLCJpZCI6MTUwMDA0NDUyMSwicm9vdE9yZ0lkIjoiNjBiNjJhNGExYTg1MmY1MGQzNGViMzgwIiwiZW1haWwiOiJ0ZWFjaGVyLnRlc3QxQGVtYmliZS5jb20ifQ.EYlPFTRonf9Ui-pVvWXTYkqcsoyP_avxIHX8uHSt_sgOM746pB-OA0RWHYDLIxct-OiOj7srZyMrZsklJ6gXRA',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
  'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-US,en;q=0.9',
  'Cookie': 'V_ID=ultimate.2021-08-16.9c69ec8260efede2f8f761f246795894; _ga=GA1.2.590726344.1629099463; ORG18012=ea8dcc27-cc67-47f1-b019-0e2509c21507; embibe_landing_page=2; access-token=ErHvbgtSdrTOPY4MQWIvOA; ab_version=6; client=Uh4HOpPKATiyahqn_EdhLA; uid=balharaclasses003%40embibe.com; embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MTQ1MjEzNjcwOCwiZW1haWwiOiJiYWxoYXJhY2xhc3NlczAwM0BlbWJpYmUuY29tIiwib3JnYW5pemF0aW9uX2lkIjoyMTYyLCJpc19ndWVzdCI6ZmFsc2UsInJvbGUiOiJzdHVkZW50IiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMTZUMDc6Mzc6NDYuNjA4WiJ9.snr1h4BR9o1ZmhB_CAH8Gxeq4dvQqeX_lgxPwRMV7xjeP6ls0vdF7b8hdKU83QsKepm60EnK4NkRHeqI-Sk-Ew; _gcl_au=1.1.1583944305.1629099468; _hp2_id.2053155045=%7B%22userId%22%3A%222378546624330400%22%2C%22pageviewId%22%3A%222759626626167004%22%2C%22sessionId%22%3A%228431519617828851%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; _test_app_session=f970e99b3008ccbbf51abdd5506623df; WZRK_G=4a20f31457b2482c80cd5631126adea4; ajs_user_id=2221; __insp_uid=3369972404; ajs_anonymous_id=%22d05985a0-adb6-45dd-9260-c8860ca4de76%22; __insp_wid=676638403; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9ncm93LmVtYmliZS5jb20vaW5zdGl0dXRlL2F0Zw%3D%3D; __insp_targlpt=RW1iaWJlIEluc3RpdHV0ZXM%3D; __insp_pad=1; __insp_sid=2104599085; __insp_slim=1634109152743; preprod_embibe-refresh-token=5bcac653-c8a9-4ec1-b9d9-dfe2b9f4dd37; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzQ1NDExMjAsIm9yZ2FuaXphdGlvbl9pZCI6IjYwYjYyYTRhMWE4NTJmNTBkMzRlYjM4MCIsImlkIjoxNTAwMDQ0NTIxLCJleHAiOjE2MzQ3MTM5MjAsImRldmljZUlkIjoiMC42MDY0MzIxODUxNDA5NDM4IiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJ0ZWFjaGVyLnRlc3QxQGVtYmliZS5jb20ifQ.nvTlbwsqwa1tLZ4Ou4uoxw_4TWM6eqh9VkJdFg24r5rm3z1RFyeW_b6gc3xNN93oYNY-90tOo7QgMWAX8vcd2w; preprod_ab_version=0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJhIiwibGFzdE5hbWUiOiJwIiwib3JnVHlwZSI6IlNjaG9vbCIsInRpbWVfc3RhbXAiOiIyMDIxLTEwLTE4IDA3OjEyOjE3IFVUQyIsInBhcmVudE9yZ0lkIjoiNjBiNjJhNzIxYTg1MmY1MGQzNGViMzg0IiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiODc4NzU2NTQ2NyIsImFkbWluSWQiOiI2MGI2MzU3YzFhODUyZjUwZDM0ZWIzYzAiLCJpZCI6MTUwMDA0NDUyMSwicm9vdE9yZ0lkIjoiNjBiNjJhNGExYTg1MmY1MGQzNGViMzgwIiwiZW1haWwiOiJ0ZWFjaGVyLnRlc3QxQGVtYmliZS5jb20ifQ.EYlPFTRonf9Ui-pVvWXTYkqcsoyP_avxIHX8uHSt_sgOM746pB-OA0RWHYDLIxct-OiOj7srZyMrZsklJ6gXRA; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/inbox/message/collapsed?isTrash=false&threadId=07f0ba76-d2f6-4c55-b819-87729d6a013d", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
