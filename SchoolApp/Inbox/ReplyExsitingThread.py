from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers ={
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xOCAxMjo1ODowMCBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.1fbzwEWUtVI4WNaY-gpuf2ONY4dSIqAJmlTtB7svczbsWh5r4sV35yTf4Gd1oOOPdccsg2lVQ4zfnAQIeAfw8A',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?1',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Mobile Safari/537.36',
  'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"Android"',
  'Content-Type': 'application/json',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': '_gcl_au=1.1.545391589.1633924308; _ga=GA1.2.866272026.1633924310; __insp_uid=1123508973; prod_embibe-refresh-token=48182129-3203-4756-946a-c8557583d1b6; prod_ab_version=0; ajs_user_id=2000007670; ajs_anonymous_id=%22667c460f-d750-46e0-830e-abe91c450cc2%22; _hp2_id.2573504162=%7B%22userId%22%3A%223805501659922921%22%2C%22pageviewId%22%3A%228173818920169077%22%2C%22sessionId%22%3A%227380329324069985%22%2C%22identity%22%3A%222000007670%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%2C%22oldIdentity%22%3Anull%7D; WZRK_G=b91da136eecd4f24b42e12930aa9252b; _hp2_id.2562464476=%7B%22userId%22%3A%221491078686928461%22%2C%22pageviewId%22%3A%223723687649537334%22%2C%22sessionId%22%3A%22422535695588447%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; preprod_embibe-refresh-token=8dbde19a-2ec1-4836-a78d-dafe03aa2b46; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2MzQ1MzA1NTksIm9yZ2FuaXphdGlvbl9pZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImlkIjoxNTAwMDQ1MDEyLCJleHAiOjE2MzQ3MDMzNTksImRldmljZUlkIjoiMC4zNzYxNTA2Mzg3NTE4MzUyIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbF92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWwiOiJyYWplc2hrQGVtYmliZS5jb20ifQ._C-0XUB5mqLu0bgy8DcREvgHNir1NCi-cbWB9aq55mOhSm_rf8i9sg95zwO4bnYiQIpXEcCNKExKSyK8EXNWyA; preprod_ab_version=0; _gid=GA1.2.1007603726.1634547983; __insp_wid=875671237; __insp_slim=1634547990394; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9zdGFnaW5nLWZpYmVyLXdlYi5lbWJpYmUuY29tL2FjY291bnQtdmVyaWZ5; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xOCAxMjo1ODowMCBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.1fbzwEWUtVI4WNaY-gpuf2ONY4dSIqAJmlTtB7svczbsWh5r4sV35yTf4Gd1oOOPdccsg2lVQ4zfnAQIeAfw8A; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}


        self.body =json.dumps({
  "messageBody": "Hi.. ",
  "messageId": "",
  "attachments": [],
  "receivers": [
    {
      "value": "nikhils@embibe.com",
      "displayValue": "Nikhil S",
      "persona": "student",
      "isGroup": False
    }
  ],
  "timestamp": 1634563663089
})
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/inbox/message?parentMsgId=d387fc94-65da-4ef3-b6a3-85b3f9b17a66", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
