from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJwayIsImxhc3ROYW1lIjoicmFuYSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0wNC0xNSAxMDozOTo0NiBVVEMiLCJwYXJlbnRPcmdJZCI6IjYwNzgxN2QwYWMzMTc2MDUzZjU2MDFmYSIsInBlcnNvbmFUeXBlIjoiQWRtaW5pc3RyYXRvciIsIm9yZ2FuaXphdGlvbl9pZCI6MTgzNCwibW9iaWxlIjoiNzkwODkwOTg5MCIsImFkbWluSWQiOiI2MDc4MTdkMWFjMzE3NjA1M2Y1NjAxZmMiLCJpZCI6NzYyLCJyb290T3JnSWQiOiI2MDc4MTdkMGFjMzE3NjA1M2Y1NjAxZmEiLCJlbWFpbCI6InByYXNoYW50QGdtYWlsLmNvbSJ9.CrA8o1qaTVeQLJuBOCTfYkji9FzIZN4k_8jkoGFFJRFzQ0XhE_WuYn_qo7BwklydTIQTe94jDqoUxkX2e0lzlA',
  'Content-Type': 'application/json',
  'Cookie': 'V_ID=ultimate.2020-10-07.4197d896338cbb88d92b882e5d070075; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = json.dumps({
  "receivers": [
    {
      "value": "ankush@gmail.com",
      "displayValue": "Ankush",
      "persona": "Student",
      "isGroup": False
    },
    {
      "value": "rajat@gmail.com",
      "displayValue": "Rajat",
      "persona": "Student",
      "isGroup": False
    },
    {
      "value": "prashant@gmail.com",
      "displayValue": "Prashant",
      "persona": "Teacher",
      "isGroup": False
    }
  ],
  "subject": "Thread 1",
  "timestamp": 1619678988,
  "messageBody": "message 1",
  "messageType": "NOTE",
  "attachments": [
    {
      "url": "1",
      "name": "1",
      "type": "pdf",
      "thumbnailUrl": "1",
      "size": 5,
      "meta": {
        "imageText": "test"
      }
    }
  ]
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/inbox/message", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
