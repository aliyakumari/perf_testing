from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Content-Type': 'application/json',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJha3NoYXQiLCJsYXN0TmFtZSI6ImthbnNhbCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0wNC0xNSAxMDozOTo0NiBVVEMiLCJwYXJlbnRPcmdJZCI6IjYwNzgxN2QwYWMzMTc2MDUzZjU2MDFmYSIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MTgzNCwibW9iaWxlIjoiNzkwODkwOTg5MCIsImFkbWluSWQiOiI2MDc4MTdkMWFjMzE3NjA1M2Y1NjAxZmMiLCJpZCI6NzYyLCJyb290T3JnSWQiOiI2MDc4MTdkMGFjMzE3NjA1M2Y1NjAxZmEiLCJlbWFpbCI6ImFrc2hhdEBnbWFpbC5jb20ifQ.u6_uLDZO6Gr8c1ZDMvMI-VkAMi2t-bzvCg9NxdRWJlWAoX9h_m7vICtxYme49ykfk6VFkir3KCq-GTx7wcMs8g',
  'Cookie': 'V_ID=ultimate.2020-10-07.4197d896338cbb88d92b882e5d070075; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}
        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/inbox/messages?collection=sent&pgNumber=1&pgSize=2", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
