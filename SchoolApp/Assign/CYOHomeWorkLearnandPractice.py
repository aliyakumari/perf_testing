from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6ImN5anl2dXciLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xMSAwODozNjowNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI3OTg3ODg1ODY4Iiwicm9vdE9yZ0lkIjoiNjBiNWU3YTZhYTgyNTA2MDQzOTQwOWU5IiwiZmlyc3ROYW1lIjoibXVzc2F0ZyIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYwYzFlNTY5MzM1ZjVhNWQ0NDZmNmJkMiIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYwYzIyOTQ0ODQ3YmRhN2UxNDM5N2ExNSIsImlkIjoxNTAwMDQ1MDEyLCJlbWFpbCI6Im5ldnVqaW9AbWFpbC5jb20ifQ.3fzHDgCZ0H5QW8gZWNnLoemadwwB4pdrJ5ohBjwBL9-BFxRkeusRAu-jZLUn9Vk25rhiIs4BZFSKZsH9VvG3Jw',
            'Accept': 'application/json, text/plain, */*',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
            'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': 'JSESSIONID=C2C61131169E593CC4F1E37C8FBD06D8; V_ID=ultimate.2020-10-07.4ae83b4c2179e37b38d35e6ca79d8793; _fbp=fb.1.1602139908566.678752438; ORG18012=f9da3ffa-6c58-47a3-a36d-e581633fcd14; WZRK_G=97fbf4703ebb4f4a99c41b6a8ff7bfcd; __insp_uid=3465742284; mp_d391945f84e82a35be9ca63e5b6b8f2c_mixpanel=%7B%22distinct_id%22%3A%20%2217506fae47794d-016a2c906f865e-193b6152-13c680-17506fae4789ff%22%2C%22%24device_id%22%3A%20%2217506fae47794d-016a2c906f865e-193b6152-13c680-17506fae4789ff%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fpaas-staging.embibe.com%2Fschool%2Fonboarding%2Fget-started%22%2C%22%24initial_referring_domain%22%3A%20%22paas-staging.embibe.com%22%7D; _ga=GA1.2.419879381.1602067921; _ga_LG9XC1Q7H4=GS1.1.1618906508.1.1.1618907451.0; _hp2_id.526644712=%7B%22userId%22%3A%226939497551072103%22%2C%22pageviewId%22%3A%228396321882951179%22%2C%22sessionId%22%3A%22699484576891046%22%2C%22identity%22%3A%221523367683%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%2C%22oldIdentity%22%3Anull%7D; _hp2_id.1503456953=%7B%22userId%22%3A%223792802692691085%22%2C%22pageviewId%22%3A%223394848542623321%22%2C%22sessionId%22%3A%222444450970271081%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; access-token=OMC7luxOMbgMnVmwpx1xtg; ab_version=; client=qp7E12QCebVVJLEFGnbhiQ; uid=guest_1624019127378984%40embibe.com; embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MTcwNDEwNDQ2MSwiZW1haWwiOiJndWVzdF8xNjI0MDE5MTI3Mzc4OTg0QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIxLTA2LTE4VDEyOjI1OjI3LjM1NloifQ.WnugSHHxfgQ8joMd7XJyIGsWvULpOjIDpMera3QgFxKrf8lSneWyJcxZFoyutvmpd--7_Fs4fVmVICEl3q5mnA; ajs_anonymous_id=%22e3c9bb0b-18e2-408d-9e1c-52fb6311ed61%22; preprod_embibe-refresh-token=19556fb1-68a6-4b4c-8bb0-ead238de0d89; ajs_user_id=1704104461; _gcl_au=1.1.459282636.1626254259; __insp_wid=676638403; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9ncm93LmVtYmliZS5jb20vaW5zdGl0dXRlL3Byb2ZpbGU%3D; __insp_targlpt=RW1iaWJlIEluc3RpdHV0ZXM%3D; __insp_norec_sess=true; __insp_slim=1626263877045; _hp2_id.2053155045=%7B%22userId%22%3A%222126335483489751%22%2C%22pageviewId%22%3A%221302480419551211%22%2C%22sessionId%22%3A%228120292877852733%22%2C%22identity%22%3A%22guest_1624019127378984%40embibe.com%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%2C%22oldIdentity%22%3Anull%7D; _test_app_session=5f85ad950f1e8363f067d19cae02d407; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJ0ZXN0IiwibGFzdE5hbWUiOiJ0ZXN0Iiwib3JnVHlwZSI6IkNvYWNoaW5nSW5zdGl0dXRlV2l0aEJyYW5jaGVzIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMTggMTI6MjI6MDQgVVRDIiwicGFyZW50T3JnSWQiOiI2MTBjZmFhZDI4Mjg5NjViN2NlNjU1ZWQiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijg5NTI5ODk1NTkiLCJhZG1pbklkIjoiNjEwY2ZhYWQyODI4OTY1YjdjZTY1NWVlIiwiaWQiOjE1MDAwNDgxMTQsInJvb3RPcmdJZCI6IjYxMGNmYWFkMjgyODk2NWI3Y2U2NTVlZCIsImVtYWlsIjoidGVzdDExMkBnbWFpbC5jb21tIn0.ookFnlNXZZ7TcdhvRMEndE91fPLn7jZMAaVuE9B9Xl17UMYafVfkGWUbviPPW2oMbr_LesjeuGuGrZ9KpDUcPA; JSESSIONID=2B3F95FEE6383CF464EBD1DB0783E717; JSESSIONID=64BEC99511FFDD8775F58194969B8585; JSESSIONID=757CD8EA958162DBF4A6CA94AE032573; JSESSIONID=BF90097422F224B2E806D83820B025AB; JSESSIONID=D394FA0CB3A5839B49F96A8CF303B358; JSESSIONID=8B7124349BADB6AC4E254027C815AE94; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({
    "configStepType": "CYO_CHAPTERS_TOPICS",
    "components": {
        "learn": [
            {
                "_created": "Sun, 27 Sep 2020 05:12:14 GMT",
                "_etag": "a6cd04c9157bc757516395eb079300f3cfe8ffea",
                "_id": "5f701f2ee825980db482b6b0",
                "_updated": "Wed, 08 Dec 2021 10:25:59 GMT",
                "_version": 29,
                "author_id": 1961,
                "available_languages": [
                    "en"
                ],
                "content": {
                    "annotation_attributes": [],
                    "augmented_attributes": {
                        "allowed_regions": [
                            "US",
                            "UK",
                            "India",
                            "Singapore",
                            "Middle East"
                        ],
                        "app_description": "Recall the nature of the sec inverse function and the range of an expression involving the sum of a number and its reciprocal. Use this information to solve this problem.",
                        "app_title": "Tricky Problem Based on Value of ITF",
                        "blurb": "",
                        "short_description": "Recall the nature of the sec inverse function and the range of an expression involving the sum of a number and its reciprocal. Use this information to solve...",
                        "trivia": ""
                    },
                    "custom_attributes": {
                        "bg_thumbnail": "https://embibe.blob.core.windows.net/images/video_backgrounds/f0380531-4fc9-414e-9457-56fbf0c52ec0_3x.webp",
                        "hero_banner": None,
                        "teaser_url": "",
                        "teaser_url_mobile": "",
                        "thumbnail": "https://embibe.blob.core.windows.net/images/video_tiles/d9a15db0-e625-4f4e-82af-ccd75a2a012e_3x.webp"
                    },
                    "grades": [],
                    "key_attributes": {
                        "cdn_url": "https://jioembibe.cdn.jio.com/hlsmedia/embibe/0/30279_1808211621_mobile.m3u8",
                        "cdn_url_no_anchor": "",
                        "cdn_url_no_anchor_no_narration": "",
                        "cdn_url_plain": "",
                        "previewURL": "https://visualintelligence.blob.core.windows.net/video-summary/prod/6243483/summary.webp",
                        "source": "Vimeo",
                        "type": "Solved Problems asked in exams",
                        "unique_id": "",
                        "url": "https://vimeo.com/114420859",
                        "url_no_anchor": "",
                        "url_no_anchor_no_narration": "",
                        "url_plain": "",
                        "video_id": "",
                        "video_type": "Vimeo"
                    },
                    "question_meta_tags": [
                        {
                            "learning_maps": [
                                "lm419384",
                                "lm400182",
                                "lm400143",
                                "lm431778",
                                "lm431818",
                                "lm424451",
                                "lm419243",
                                "lm420147",
                                "lm364351",
                                "lm432044",
                                "lm420005",
                                "lm413726",
                                "lm432009",
                                "lm283379",
                                "lm364148",
                                "lm333179",
                                "lm610919",
                                "lm485648",
                                "lm487312",
                                "lm776417",
                                "lm511287",
                                "lm816340",
                                "lm835323",
                                "lm676697",
                                "lm533015",
                                "lm599732",
                                "lm676736",
                                "lm798105",
                                "lm798040",
                                "lm569921",
                                "lm817363",
                                "lm534428",
                                "lm817298",
                                "lm598570",
                                "lm532975",
                                "lm816275",
                                "lm599693",
                                "lm486092",
                                "lm598531",
                                "lm487273",
                                "lm835277",
                                "lm511330",
                                "lm534388",
                                "lm486053",
                                "lm485614",
                                "lm776383",
                                "lm775654",
                                "lm557023",
                                "lm559118",
                                "lm756048",
                                "lm810660"
                            ],
                            "primary_concept": "new_KG15568",
                            "secondary_concept": [
                                "new_KG15562",
                                "new_KG15045",
                                "new_KG15061",
                                "new_KG15056"
                            ]
                        }
                    ],
                    "relevance_attributes": {
                        "age_range": {
                            "from": "15",
                            "to": "20"
                        },
                        "most_relevant_age": {
                            "from": "16",
                            "to": "18"
                        }
                    },
                    "sequence": {
                        "lm283379": 8,
                        "lm333163": 10,
                        "lm333179": 2,
                        "lm364148": 10,
                        "lm364351": 2,
                        "lm400143": 2,
                        "lm400182": 2,
                        "lm413726": 1,
                        "lm419243": 4,
                        "lm419384": 2,
                        "lm420005": 4,
                        "lm420147": 2,
                        "lm424451": 2,
                        "lm431778": 11,
                        "lm431818": 2,
                        "lm432009": 10,
                        "lm432044": 2,
                        "lm485614": 11,
                        "lm485648": 2,
                        "lm486053": 11,
                        "lm486092": 2,
                        "lm487273": 11,
                        "lm487312": 2,
                        "lm511287": 11,
                        "lm511330": 2,
                        "lm532975": 11,
                        "lm533015": 2,
                        "lm534388": 4,
                        "lm534428": 2,
                        "lm556986": 10,
                        "lm557023": 2,
                        "lm559118": 2,
                        "lm569882": 11,
                        "lm569921": 2,
                        "lm590264": 10,
                        "lm598531": 11,
                        "lm598570": 2,
                        "lm599693": 4,
                        "lm599732": 2,
                        "lm610849": 10,
                        "lm610919": 2,
                        "lm676697": 11,
                        "lm676736": 2,
                        "lm756048": 2,
                        "lm775633": 10,
                        "lm775654": 2,
                        "lm776383": 11,
                        "lm776417": 2,
                        "lm798040": 2,
                        "lm798105": 11,
                        "lm805791": 11,
                        "lm810660": 4,
                        "lm810665": 2,
                        "lm816275": 2,
                        "lm816340": 11,
                        "lm817298": 2,
                        "lm817363": 11,
                        "lm835277": 2,
                        "lm835323": 11
                    },
                    "source_attributes": {
                        "channel": {
                            "name": "",
                            "url": ""
                        },
                        "copyright": "CC BY",
                        "description": "Recall the nature of the sec inverse function as well as the range of an expression involving the sum of a number and its reciprocal. Use this information to solve this problem.",
                        "duration": 272,
                        "language": [
                            "English-American"
                        ],
                        "rating": 4,
                        "source_id": "114420859",
                        "title": "EM0067343.wmv"
                    },
                    "wf_info": {
                        "wf_step_no": 1
                    }
                },
                "content_schema_version": 1,
                "created_at": "Sun, 27 Sep 2020 05:12:14 GMT",
                "created_by": 1961,
                "id": 6243483,
                "note": "Deleted irrelevant",
                "owning_partner_id": 10304,
                "primary_language": "en",
                "status": "Published",
                "subtype": "",
                "tenant_id": 1,
                "title": "Tricky Problem Based on Value of ITF",
                "type": "Video",
                "updated_at": "Wed, 08 Dec 2021 10:25:59 GMT",
                "updated_by": 1103,
                "uuid": "3280eff6-e63f-41de-9e8c-8fe4d8289983",
                "version": 1,
                "wf_info": {
                    "wf_step_no": 3
                }
            },
            {
                        "source": "Vimeo",
                        "videoLmSeq": None,
                        "content_id": "",
                        "url": "https://vimeo.com/537218702/7a4b40f8fd",
                        "preview_url": "https://visualintelligence.blob.core.windows.net/video-summary/prod/5868597-en/summary.webp",
                        "teaser_url": "",
                        "rating": 0.0,
                        "type": "Video",
                        "title": "Easy Problem from Stress on Bodies",
                        "description": "What is the minimum diameter required for the wire to withstand a particular amount of force? Let&#39;s understand it through a JEE Main problem.",
                        "short_description": "What is the minimum diameter required for the wire to withstand a particular amount of force? Let's understand it through a JEE Main problem.",
                        "learning_map": {
                            "sequence": 76,
                            "lpcode": "",
                            "subject": "Physics",
                            "subject_code": "",
                            "exam_code": "",
                            "unit": "",
                            "chapter": "",
                            "author": "",
                            "grade": "",
                            "board": "",
                            "concept_id": "",
                            "format_id": "608d74bd804d7ea1ab8f45c3",
                            "lm_name": "",
                            "topic_learnpath_name": "ladakh board--11th ladakh board--physics--mechanical properties of matter--mechanical properties of solids--stress and strain",
                            "display_learnpath_name": "",
                            "learnpath_format_name": "goal--exam--subject--unit--chapter--topic",
                            "lm_code": "lm703089",
                            "lm_seq": 76,
                            "tags": [],
                            "mode": "Normal",
                            "exam": "",
                            "goal": "",
                            "topic": "",
                            "format_reference": "608d74bd804d7ea1ab8f45c3",
                            "learnpath_name_format": "goal--exam--subject--unit--chapter--topic"
                        },
                        "watched_duration": 0,
                        "views": 0,
                        "likes": 0,
                        "currency": 14,
                        "length": 136,
                        "time_duration": {
                            "learn_duration": 0,
                            "practice_duration": 0
                        },
                        "authors": [],
                        "owner_info": {
                            "copyright": "embibe",
                            "copy_logo": "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/badges/badge_embibe_originals.webp",
                            "key": "2ba6e18c8f94ad1cc21f6f010777f3b8",
                            "teaser_key": "2ba6e18c8f94ad1cc21f6f010777f3b8"
                        },
                        "id": "5868597",
                        "category": "Solved Problems asked in exams",
                        "thumb": "https://embibe.blob.core.windows.net/images/video_tiles/edc7e6c5-9131-40d5-b64c-a03b7d354535_3x.webp",
                        "bg_thumb": "https://embibe.blob.core.windows.net/images/video_backgrounds/f21e4f4e-59fe-4020-854d-14d9213b92f8_3x.webp",
                        "subject": "Physics",
                        "subject_display_name": "",
                        "front_page_image_url": "",
                        "category_url": "https://imagin8ors-temp-dump.s3-ap-southeast-1.amazonaws.com/subject/cardicons/pun-icon.png",
                        "concept_count": 0,
                        "annotation_attributes": [],
                        "learnmap_id": None,
                        "modified_at": None,
                        "cheat_sheet": "",
                        "cdn_url": "https://jioembibe.cdn.jio.com/hlsmedia/embibe/2/10219_1004212126_mobile.m3u8",
                        "context": "learning_map",
                        "concept_code": None,
                        "topic_practice_details": None,
                        "content_key_attribute_type": "Solved Problems asked in exams",
                        "learnpath_name": None
                    }
        ],
        "practice": [
            {
                "_id": "5ec58c7e0c88fe586097559c",
                "calendar_sequence": 5,
                "cfu_practice": [],
                "cfu_practice_tile_image": "",
                "cheat_sheet": "",
                "code": "kve101247",
                "concepts": [],
                "description": "This topic introduces us to basic discipline in the category of natural science. It also briefs on the two principal thrusts in physics along with laws and concepts related to physical science.\n",
                "display_name": "Understanding Physics",
                "learn_tile_bg_image": "https://embibe.blob.core.windows.net/images/learn_backgrounds/64818f41-a4d7-46b7-8126-08cc5fa03cbc_3x.webp",
                "learn_tile_image": "https://embibe.blob.core.windows.net/images/learn_tiles/f9c4d654-f330-4c3b-b0e5-999df11d1def_3x.webp",
                "learning_maps": [],
                "learnmap_id": "5ec5867a0c88fe5860961943/CBSE/11th CBSE/Physics/Mechanics/Physical World/Understanding Physics",
                "learnpath_code": "kve97670--kve100513--kve100519--kve100520--kve101224--kve101247",
                "learnpath_format_name": "goal--exam--subject--unit--chapter--topic",
                "learnpath_name": "cbse--11th cbse--physics--mechanics--physical world--understanding physics",
                "name": "Understanding Physics",
                "practice": [],
                "practice_tile_bg_image": "https://embibe.blob.core.windows.net/images/practice_backgrounds/6e87cfa1-e3cc-4b3d-b021-23fb766fc7dd_3x.webp",
                "practice_tile_image": "https://embibe.blob.core.windows.net/images/practice_tiles/707ea5a1-4426-4bef-a51b-210914828cf7_3x.webp",
                "question_count": 8,
                "score": {
                    "complexity_score": 1.0,
                    "difficulty_level": 2.104002909254728,
                    "fundamental_score": 7.35510272196113,
                    "importance": 1.4595370187850314,
                    "length": 2.080144010832508,
                    "oswaal_importance": 4.0447991580254445
                }
            }
        ]
    },
    "nextStep": "CYO_SELECT_QUESTIONS"
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(resellertoken))
        self.headers['reseller-jwt-token'] = resellertoken[rnum]
        url = f"/ps_generate_ms/v2/school/atgConfig/61c30b897b65c4219e95c035"
        response = self.client.put(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
