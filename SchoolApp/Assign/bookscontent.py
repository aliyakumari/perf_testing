from locust import HttpUser, SequentialTaskSet, task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
import json
import csv
import string
from random import randrange
import logging
import random
import time
import gevent
import resource
import pandas as pd
from random import randrange
from random import randint

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val

class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:100.0) Gecko/20100101 Firefox/100.0',
                      'Accept': 'application/json, text/plain, */*',
                      'Accept-Language': 'en-US,en;q=0.5',
                      'Accept-Encoding': 'gzip, deflate, br',
                      'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlYWNoZXIiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wNS0yNyAwNjo0OTozOSBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODk5ODk5OTAwIiwicm9vdE9yZ0lkIjoiNjIyMWZmZWRhZjk2ZTE0NDgzYTIyZWY2IiwiZGV2aWNlSWQiOiIwLjYwMjg4NTM0OTcxNTY0ODYiLCJmaXJzdE5hbWUiOiJBbndhciBTaGVpa2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.2RhMdwt5e8nukwmdTfB2LTk3DVO8nQ7EUIuSyejCKj3LzhvJ0Fp2V53mr3rgni8ffp875cTl4BBZt0HJczRgLw',
                      'Origin': 'https://paas-v3-staging.embibe.com',
                      'Sec-Fetch-Dest': 'empty',
                      'Sec-Fetch-Mode': 'cors',
                      'Sec-Fetch-Site': 'same-site',
                      'Referer': 'https://paas-v3-staging.embibe.com/',
                      'Connection': 'keep-alive',
                      'Cookie': 'JSESSIONID=055194620186E0941BFC8EC5EC03B1EE; _gcl_au=1.1.1567563325.1652161874; _ga_TT8L73VP3H=GS1.1.1653387847.13.1.1653387861.46; _ga=GA1.1.1040839591.1652161874; _fbp=fb.1.1652161875639.872785787; _ga_QQPR95BLCG=GS1.1.1653634175.32.1.1653634185.50; _ga_YHBNHKH43S=GS1.1.1652788549.6.0.1652788551.58; _ga_7N8QWZ4X8C=GS1.1.1653628989.8.0.1653628989.60; preprod_embibe-refresh-token=3b8b91c3-c18c-474a-a5f0-20d24bb673e5; prod_ab_version=0; prod_embibe-refresh-token=0bbc1654-885b-4e2b-a8fa-883035a57193; prod_otp-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkZXZpY2VfaWQiOiIxNjUyNzg4NjE4MTU2Iiwib3RwX3Rva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6VXhNaUo5LmV5SmpiM1Z1ZEhKNVgyTnZaR1VpT2lJNU1TSXNJbVYyWlc1MFgzUjVjR1VpT2lKVGFXZHVTVzRpTENKdmNtZGZhV1FpT2lJeElpd2lkSEpoWTJ0ZmFXUWlPaUkxTkRoaU16SmxZUzA0WmpGbUxUUTRNemd0WW1VNU5pMDJOemd5WW1VMk5XRXhOV01pTENKdmRIQmZjbVZ4ZFdWemRGOXBaQ0k2SWpObVpEWTJNamhsTFdVeE1qUXROR1F3TWkwNE5tSTRMVGc1TkRoaE1XWmxNR0l4WmlJc0ltbGtJam9pT1RnNU9UWXdNRFkyTUNJc0lteHZZMkZzWlNJNkltVnVJbjAuQXp3dlVBczhZMkxhT1dGNkdNcFNOUnBpQ2o2ZmxMY25TTk1MSFVuTzRwMGRKeFE3MlU4dlJlTkx5VlFnN0JLdGxVbnM2cnRnTGh6UUE2UWVWYkc5WXciLCJjcmVhdGVkIjoxNjUyNzg4NjE4LCJvdHBfb3BlcmF0aW9uX3R5cGUiOiJTaWduSW4iLCJhdHRlbXB0IjoxLCJ0dGwiOiI2MDAiLCJjb25uZWN0ZWRfcmVzcG9uc2UiOnRydWUsInJlbGF0aW9uc2hpcF90eXBlIjowLCJ1c2VyX2lkIjoiYW51cmFnLjEyNzk1bWJAc2FpbnRtYXJrc3NjaG9vbC5pbiIsIm9yZ19pZCI6IjEiLCJjb3VudHJ5X2NvZGVfaWQiOjEsInVzZXJfdHlwZV9pZCI6MSwiaWQiOjIwMDQ4NzA5MzksImV4cGlyeSI6MTY1Mjg3NTAxOCwiZXhwIjoxNjUyODc1MDE4fQ.5cyIfAC13nnnTIwkrCe0qcOx3bBGOlCYY9CVJDoSQhaEt5n4OV1cyCPRx60xGnWh24TpfouwjPkm_OjoUwX-GQ; __insp_wid=875671237; __insp_slim=1653387850025; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9uZXh1cy1kZXYuZW1iaWJlLmNvbS8jYnJvd3NlL2Jyb3dzZTplbWJpYmUtbWF2ZW4%3D; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; __insp_norec_sess=true; school_preprod_embibe-refresh-token=695589c9-4b2f-4f96-8b55-5c445d07b772; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlRlYWNoZXIiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wNS0yNyAwNjo0OTozOSBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODk5ODk5OTAwIiwicm9vdE9yZ0lkIjoiNjIyMWZmZWRhZjk2ZTE0NDgzYTIyZWY2IiwiZGV2aWNlSWQiOiIwLjYwMjg4NTM0OTcxNTY0ODYiLCJmaXJzdE5hbWUiOiJBbndhciBTaGVpa2giLCJvcmdUeXBlIjoiU2Nob29sIiwicGFyZW50T3JnSWQiOiI2MjIyMDAyM2I3NjNjZTIyMWZkOGEwN2MiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MjIyMDBkYjRlOWIyZDNlM2QwODYxOTIiLCJpZCI6MTUwMTQxMzIzNywiZW1haWwiOiJhbndhcl90ZWFjaGVyMkBlbWJpYmUuY29tIn0.2RhMdwt5e8nukwmdTfB2LTk3DVO8nQ7EUIuSyejCKj3LzhvJ0Fp2V53mr3rgni8ffp875cTl4BBZt0HJczRgLw; school_preprod_ab_version=0; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NTM2MzQxNzgsIm9yZ2FuaXphdGlvbl9pZCI6IjYyMjFmZmVkYWY5NmUxNDQ4M2EyMmVmNiIsIm1vYmlsZSI6Ijk4OTk4OTk5MDAiLCJpZCI6MTUwMTQxMzIzNywiZXhwIjoxNjUzNzIwNTc4LCJkZXZpY2VJZCI6IjAuNjAyODg1MzQ5NzE1NjQ4NiIsIm1vYmlsZV92ZXJpZmljYXRpb25fc3RhdHVzIjpmYWxzZSwiZW1haWxfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsIjoiYW53YXJfdGVhY2hlcjJAZW1iaWJlLmNvbSJ9.uZQQlR9z-RZ-WkTQ9BreufLBk7dB74xN6FdIdhfyFVNd9BMnmplcZZ4tG8zEkWBRsD4GO3qVfsXdfGVrHcGO7Q; JSESSIONID=7F90F764EEC5C0DEA8F0FE0D09F2BDB1'
                   }
        self.body = {

        }

        dt = pd.read_csv("AtgConfigId  - atgConfig with CYOH.csv", low_memory=False)

        self.atgConfigId = dt["atgConfigId"].values.tolist()

        self.url = "/ps_generate_ms/v3/school/booksContent?atgConfigId=6290748a4f19404422bc4e42&locale=en&size=20&offset=1"

        self.method = "GET"
        self.host = "https://preprodms-cf.embibe.com"

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def ondemandautomated_script(self):

        # self.headers["embibe-token"] = random.choice(self.tokens)
        self.url = f"/ps_generate_ms/v3/school/booksContent?atgConfigId={random.choice(self.atgConfigId)}&locale=en&size=20&offset=1"

        response = self.client.get(self.url, data=self.body, headers=self.headers)
        try:
            if response.status_code != 200:
                print('Header : ', self.headers)
                print('Response Code :', response.status_code)
                print('Response : ', response.json())

        except Exception as e:
            print(e)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
   if not isinstance(environment.runner, WorkerRunner):
       gevent.spawn(checker, environment)

