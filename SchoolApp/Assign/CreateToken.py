import jwt
import pandas as pd

import model.db as db
from model.userms_connect import connect


def creatResellerToken():
    db.initNaradUserCollection()
    cursor = db.narad_user.find({},
                                {"mobile": 1, "firstName": 1, "lastName": 1, "rootOrgId": 1, "emailId": 1,
                                 "orgType": 1})
    user_token = []

    for data in list(cursor):
        payload = {
            "lastName": data.get("lastName"),
            "time_stamp": "2021-10-11 08:36:07 UTC",
            "sub_organization_id": 1,
            "mobile": data.get("mobile"),
            "rootOrgId": data.get("rootOrgId"),
            "firstName": data.get("firstName"),
            "orgType": data.get("orgType"),
            "parentOrgId": "60c1e569335f5a5d446f6bd2",
            "personaType": "Teacher",
            "organization_id": 1,
            "adminId": "60c22944847bda7e14397a15",
            "id": 1500045012,
            "email": data.get("emailId")
        }

        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af589c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")

        user_token.append(encoded.decode('utf-8'))

    return user_token


def creatEmbibeToken():
    user_id_and_parent_id_data = connect("select user_id, is_child_of from user_relations ur;")

    parent_app_embibe_token = []
    for data in user_id_and_parent_id_data:
        payload = {
            "country": 1,
            "user_type": 1,
            "parent_user_id": data[1],
            "created": 1639481960,
            "organization_id": "1",
            "id": data[0],
            "exp": 1639737249,
            "deviceId": "1639080427620",
            "mobile_verification_status": False,
            "email_verification_status": False,
            "email": "1500288416_822937081103526@embibe-user.com"
        }
        encoded = jwt.encode(payload,
                             "1df65b5faf39efdf05746acbc98600290135d9b090eba7fa51d997f275a9f139f55cf3550a8431fe39792af5"
                             "89c8e773ee3d20f368ce1e013be07a7ad3457968",
                             algorithm="HS512")
        parent_app_embibe_token.append(encoded.decode('utf-8'))

    return parent_app_embibe_token
