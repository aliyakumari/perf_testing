from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTE3IDA2OjQyOjU0IFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6OTY1MzQ0LCJlbWFpbCI6Imd1ZXN0XzE1OTc2NDY1NzQzNzA1ODQ4NjRAZW1iaWJlLmNvbSJ9.aaG6_RX8H2bc_I_GBNQh7wIusNagGPbui9g_oNXgQZitACxvOGWhVaIXL_Le-OQv3HCvVKOak2ziZTWZ0CfbgQ',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2021-01-09.c67349946ec9a32856dc6a6ba021ac54; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({
    "name": "Test12",
    "exam_code": "kve97915",
    "language": "en",
    "question_count": 4,
    "duration": 10,
    "total_marks": 16,
    "section_data": {
        "Mathematics": {
            "en": "Mathematics"
        }
    },
    "schedule": {
        "time_bound": {
            "start_time": "1638348255",
            "locks_at": "1638348255",
            "stop_time": "1638348255",
            "solution_public_at": "1638348255"
        }
    },
    "question_data": {
        "Mathematics": {
            "questions": [
                {
                    "code": "EM6227289-en"
                },
                {
                    "code": "EM8376738-en"
                },
                {
                    "code": "EM3110918-en"
                },
                {
                    "code": "EM2405319-en"
                }
            ]
        }
    }
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(resellertoken))
        # self.headers['reseller-jwt-token'] = resellertoken[rnum]
        url = f"/content_ms_fiber/v1/embibe/en/fiber/parent/generate_test"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
