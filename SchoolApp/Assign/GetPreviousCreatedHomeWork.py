from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Connection': 'keep-alive',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6ImN5anl2dXciLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xMSAwODozNjowNyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI3OTg3ODg1ODY4Iiwicm9vdE9yZ0lkIjoiNjBiNWU3YTZhYTgyNTA2MDQzOTQwOWU5IiwiZmlyc3ROYW1lIjoibXVzc2F0ZyIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYwYzFlNTY5MzM1ZjVhNWQ0NDZmNmJkMiIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYwYzIyOTQ0ODQ3YmRhN2UxNDM5N2ExNSIsImlkIjoxNTAwMDQ1MDEyLCJlbWFpbCI6Im5ldnVqaW9AbWFpbC5jb20ifQ.3fzHDgCZ0H5QW8gZWNnLoemadwwB4pdrJ5ohBjwBL9-BFxRkeusRAu-jZLUn9Vk25rhiIs4BZFSKZsH9VvG3Jw',
            'Accept': 'application/json, text/plain, */*',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
            'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cookie': 'JSESSIONID=C2C61131169E593CC4F1E37C8FBD06D8; V_ID=ultimate.2020-10-07.4ae83b4c2179e37b38d35e6ca79d8793; _fbp=fb.1.1602139908566.678752438; ORG18012=f9da3ffa-6c58-47a3-a36d-e581633fcd14; WZRK_G=97fbf4703ebb4f4a99c41b6a8ff7bfcd; __insp_uid=3465742284; mp_d391945f84e82a35be9ca63e5b6b8f2c_mixpanel=%7B%22distinct_id%22%3A%20%2217506fae47794d-016a2c906f865e-193b6152-13c680-17506fae4789ff%22%2C%22%24device_id%22%3A%20%2217506fae47794d-016a2c906f865e-193b6152-13c680-17506fae4789ff%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fpaas-staging.embibe.com%2Fschool%2Fonboarding%2Fget-started%22%2C%22%24initial_referring_domain%22%3A%20%22paas-staging.embibe.com%22%7D; _ga=GA1.2.419879381.1602067921; _ga_LG9XC1Q7H4=GS1.1.1618906508.1.1.1618907451.0; _hp2_id.526644712=%7B%22userId%22%3A%226939497551072103%22%2C%22pageviewId%22%3A%228396321882951179%22%2C%22sessionId%22%3A%22699484576891046%22%2C%22identity%22%3A%221523367683%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%2C%22oldIdentity%22%3Anull%7D; _hp2_id.1503456953=%7B%22userId%22%3A%223792802692691085%22%2C%22pageviewId%22%3A%223394848542623321%22%2C%22sessionId%22%3A%222444450970271081%22%2C%22identity%22%3Anull%2C%22trackerVersion%22%3A%224.0%22%7D; access-token=OMC7luxOMbgMnVmwpx1xtg; ab_version=; client=qp7E12QCebVVJLEFGnbhiQ; uid=guest_1624019127378984%40embibe.com; embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MTcwNDEwNDQ2MSwiZW1haWwiOiJndWVzdF8xNjI0MDE5MTI3Mzc4OTg0QGVtYmliZS5jb20iLCJvcmdhbml6YXRpb25faWQiOm51bGwsImlzX2d1ZXN0Ijp0cnVlLCJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIxLTA2LTE4VDEyOjI1OjI3LjM1NloifQ.WnugSHHxfgQ8joMd7XJyIGsWvULpOjIDpMera3QgFxKrf8lSneWyJcxZFoyutvmpd--7_Fs4fVmVICEl3q5mnA; ajs_anonymous_id=%22e3c9bb0b-18e2-408d-9e1c-52fb6311ed61%22; preprod_embibe-refresh-token=19556fb1-68a6-4b4c-8bb0-ead238de0d89; ajs_user_id=1704104461; _gcl_au=1.1.459282636.1626254259; __insp_wid=676638403; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9ncm93LmVtYmliZS5jb20vaW5zdGl0dXRlL3Byb2ZpbGU%3D; __insp_targlpt=RW1iaWJlIEluc3RpdHV0ZXM%3D; __insp_norec_sess=true; __insp_slim=1626263877045; _hp2_id.2053155045=%7B%22userId%22%3A%222126335483489751%22%2C%22pageviewId%22%3A%221302480419551211%22%2C%22sessionId%22%3A%228120292877852733%22%2C%22identity%22%3A%22guest_1624019127378984%40embibe.com%22%2C%22trackerVersion%22%3A%224.0%22%2C%22identityField%22%3Anull%2C%22isIdentified%22%3A1%2C%22oldIdentity%22%3Anull%7D; _test_app_session=5f85ad950f1e8363f067d19cae02d407; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJ0ZXN0IiwibGFzdE5hbWUiOiJ0ZXN0Iiwib3JnVHlwZSI6IkNvYWNoaW5nSW5zdGl0dXRlV2l0aEJyYW5jaGVzIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMTggMTI6MjI6MDQgVVRDIiwicGFyZW50T3JnSWQiOiI2MTBjZmFhZDI4Mjg5NjViN2NlNjU1ZWQiLCJwZXJzb25hVHlwZSI6IkFkbWluaXN0cmF0b3IiLCJvcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijg5NTI5ODk1NTkiLCJhZG1pbklkIjoiNjEwY2ZhYWQyODI4OTY1YjdjZTY1NWVlIiwiaWQiOjE1MDAwNDgxMTQsInJvb3RPcmdJZCI6IjYxMGNmYWFkMjgyODk2NWI3Y2U2NTVlZCIsImVtYWlsIjoidGVzdDExMkBnbWFpbC5jb21tIn0.ookFnlNXZZ7TcdhvRMEndE91fPLn7jZMAaVuE9B9Xl17UMYafVfkGWUbviPPW2oMbr_LesjeuGuGrZ9KpDUcPA; JSESSIONID=2B3F95FEE6383CF464EBD1DB0783E717; JSESSIONID=64BEC99511FFDD8775F58194969B8585; JSESSIONID=757CD8EA958162DBF4A6CA94AE032573; JSESSIONID=BF90097422F224B2E806D83820B025AB; JSESSIONID=D394FA0CB3A5839B49F96A8CF303B358; JSESSIONID=8B7124349BADB6AC4E254027C815AE94; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(resellertoken))
        self.headers['reseller-jwt-token'] = resellertoken[rnum]
        url = f"/ps_generate_ms/v2/school/listing?type=School_TEST&pageNumber=1&pageSize=10&teacherId=60c22944847bda7e14397a15"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
