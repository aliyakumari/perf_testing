from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkFuc2FyaXMiLCJ0aW1lX3N0YW1wIjoiMjAyMy0wMy0zMSAwODo0MDo0NyBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5NTYwOTE2MzgwIiwicm9vdE9yZ0lkIjoiNjJiYzYzODNjNjcyMjUyYWFmNTU2MDIxIiwiZGV2aWNlSWQiOiIwLjI2NDg0NTEyNTA2Nzk0NzU1IiwiZmlyc3ROYW1lIjoiUXVhc2lmIiwib3JnVHlwZSI6IlNjaG9vbCIsInVzZXJfdHlwZSI6MywicGFyZW50T3JnSWQiOiI2MmJjNjNhZmM2NzIyNTJhYWY1NTYwMjUiLCJwZXJzb25hVHlwZSI6IlRlYWNoZXIiLCJvcmdhbml6YXRpb25faWQiOjEsImFkbWluSWQiOiI2MmJjN2MxZGM2NzIyNTJhYWY1NTYwOGQiLCJpZCI6MTUwMTg1NzEwNCwiZW1haWwiOiJxdWFzaWYuYW5zYXJpQGVtYmliZS5jb20ifQ.4xK4FsKbfFGpSjgDlDjnTQEL3kitZHpJTVJKe1hTxurGhC-fitO9T2A7nIzl1ITmP9SrkBWXYEYO-H50ipwRAg',
            'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"',
            'Cookie': 'Cookie_1=value; JSESSIONID=jd5u8eH6Ap0NSqo6k5FZHmfuh_pbogjMggX4fGHm; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c'
        }

        dt = pd.read_csv(
            "AtgConfigId  - atgConfig with CYOH.csv", low_memory=False)

        self.atgConfigId = dt["atgConfigId"].values.tolist()

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):

        url = f"/ps_generate_ms/v3/school/publish?atgConfigId={random.choice(self.atgConfigId)}&locale=en"
        with self.client.get(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 400:
                response.success()
            elif response.status_code == 200:
                response.success()
            elif response.status_code == 204:
                response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
