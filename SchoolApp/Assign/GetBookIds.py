from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Cookie': 'preprod_embibe-refresh-token=6c29df66-3f04-4750-af34-8510543c39f4; '
            'reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9'
            '.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMS0wMiAxMDo0ODozNSBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.gJbrVjBtrNrQfPsGij-xi3KUd_up6siWAufepqb0w232v4IROB6-P1a-4ASPqt9QKU8DZN1In0HRwxlppCaCjQ; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9 '
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        url = "/cg-fiber-ms/big_books?where={%22book_id%22:{%22$regex%22:%225ec5867a0c88fe5860961943/CBSE/10th%20CBSE/Science%22}}&locale=en"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
