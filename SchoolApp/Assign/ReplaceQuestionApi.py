from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTE3IDA2OjQyOjU0IFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6OTY1MzQ0LCJlbWFpbCI6Imd1ZXN0XzE1OTc2NDY1NzQzNzA1ODQ4NjRAZW1iaWJlLmNvbSJ9.aaG6_RX8H2bc_I_GBNQh7wIusNagGPbui9g_oNXgQZitACxvOGWhVaIXL_Le-OQv3HCvVKOak2ziZTWZ0CfbgQ',
  'Content-Type': 'application/json',
  'Cookie': 'V_ID=ultimate.2021-01-09.c67349946ec9a32856dc6a6ba021ac54; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; JSESSIONID=3BBB59A34789D97E80443A4F89F8ACB7; preprod_embibe-refresh-token=6c29df66-3f04-4750-af34-8510543c39f4; JSESSIONID=2C1E52D6C66A6489B80C27ACCB609311; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJmaXJzdE5hbWUiOiJ0ZWFjaGVyRm5hbWUiLCJsYXN0TmFtZSI6InRlYWNoZXJMbmFtZSIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMi0yMSAxMDozODo1NiBVVEMiLCJwYXJlbnRPcmdJZCI6IjYxYjFlYThkOGQwMGY2NmM4NWJlNTc4YSIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6IjcwMDAwMTAwMDEiLCJhZG1pbklkIjoiNjFiMWVjNTJlNzY0MGUwNDg1NmZlMDEyIiwiaWQiOjE1MDExOTcwMDgsInJvb3RPcmdJZCI6IjYxYjFlYTUwZTMyYWNkMmRiZmI4N2M1ZCIsImVtYWlsIjoib25ib2FyZHRlYWNoZXIwMUBndWVycmlsbGFtYWlsLmNvbSJ9.55VoD4mLEO9rU9TLv8IQyK7ToKPNTgNOLZhtRpgC855oQqgbzhzeS4Rnndh8VW6H_YY38Qgi67vuRkc7T2VsQQ; JSESSIONID=8483801C9C1DC5677A064F3406DAEC4B; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
}


        self.body = json.dumps({
  "questionId": "EM6829403-en",
  "questionType": "SingleChoice",
  "excludeQuestionIDs": ["EM6829401-en","EM6829402-en"],
  "difficultyLevel": "Easy",
  "learningMaps" : {
        "name" : "Electrochemical Cells",
        "description" : "This topic describes electrochemical cells. It can be defined as a device that is capable of generating electrical energy from chemical reactions. It can also use this energy to trigger chemical reactions.",
        "learningPathFormatName" : "goal--exam--subject--unit--chapter--topic",
        "learningPath" : "cbse--12th cbse--chemistry--physical chemistry--electrochemistry--electrochemical cells",
        "meta" : {
            "chapter" : "cbse--12th cbse--chemistry--physical chemistry--electrochemistry",
            "topic" : "cbse--12th cbse--chemistry--physical chemistry--electrochemistry--electrochemical cells",
            "classId" : "5ec5867a0c88fe5860961943|CBSE/12th CBSE|cbse--12th cbse|kve97670--kve97671",
            "display_name" : "Electrochemical Cells",
            "topic_code" : "kve99498",
            "bookId" : "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Chemistry",
            "bookType" : "BigBook",
            "book_path" : "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Chemistry",
            "chapter_name" : "Electrochemistry"
        }
    }
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(resellertoken))
        # self.headers['reseller-jwt-token'] = resellertoken[rnum]
        url = f"/ps_generate_ms/v2/school/replaceQuestion/61c4548411bfb03ff23b3044"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
