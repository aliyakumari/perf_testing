from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatResellerToken

resellertoken = creatResellerToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoic3R1ZGVudCIsInRpbWVfc3RhbXAiOiIyMDIwLTA4LTE3IDA2OjQyOjU0IFVUQyIsImlzX2d1ZXN0Ijp0cnVlLCJpZCI6OTY1MzQ0LCJlbWFpbCI6Imd1ZXN0XzE1OTc2NDY1NzQzNzA1ODQ4NjRAZW1iaWJlLmNvbSJ9.aaG6_RX8H2bc_I_GBNQh7wIusNagGPbui9g_oNXgQZitACxvOGWhVaIXL_Le-OQv3HCvVKOak2ziZTWZ0CfbgQ',
            'Content-Type': 'application/json',
            'Cookie': 'V_ID=ultimate.2021-01-09.c67349946ec9a32856dc6a6ba021ac54; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2020-11-24.1380fd81f7b71f14ea92ada0b35a1d88; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
        }

        self.body = json.dumps({
    "name": "testER111",
    "schoolId": "60c1e569335f5a5d446f6bd2",
    "classIds": [
        "60c1fe9d9c95f329eb741a52"
    ],
    "teacherId": "60c22944847bda7e14397a15",
    "templateId": "610cfaad2828965b7ce65578",
    "subjectId": "5ec5867a0c88fe5860961943|CBSE/11th CBSE/Physics|cbse--11th cbse--mathematics",
    "subjectName": "Mathematics",
    "timetableId": "610cfaad2828965b7ce65574",
    "slotId": "614c595407320a0de41e841c",
    "selectedFormatType": "SCHOOL_ER_TEST",
    "nextStep": "ER_TEST_TYPE",
    "learningMaps": {
        "name": "STRESS AND STRAIN",
        "description": "In this topic, we will study stress and strain. It also explains tensile, compressive stress, longitudinal and shearing strain along with formulas and illustration.",
        "learningPathFormatName": "goal--exam--subject--unit--chapter--topic",
        "learningPath": "cbse--11th cbse--mathematics--mechanical properties of matter--mechanical properties of solids--stress and strain",
        "meta": {
            "chapter": "ncert11p02--11--mathematics--mechanical properties of solids",
            "topic": "ncert11p02--11--mathematics--mechanical properties of solids--stress and strain",
            "classId": "5ec5867a0c88fe5860961943|CBSE/11th CBSE|cbse--11th cbse",
            "display_name": "Stress and Strain",
            "topic_code": "kve97915",
            "bookId": "lmf1841",
            "bookType": "Book",
            "chapter_name": "RAY OPTICS AND OPTICAL INSTRUMENTS",
            "book_path": "5f11d03c7f10acd6461d6c27/NC12P001/12/Physics"
        }
    }
})

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(resellertoken))
        # self.headers['reseller-jwt-token'] = resellertoken[rnum]
        url = f"/ps_generate_ms/v2/school/atgConfig"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
