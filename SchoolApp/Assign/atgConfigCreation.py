from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Cookie': 'JSESSIONID=d-v3ne1ungMhspcYYTp0JT6L3bGJCEhe9mWIQfdx; _fbp=fb.1.1678863789269.1466905908; preprod_embibe-refresh-token=bfe89d21-65c1-4b95-a44e-fc30aefb5c01; preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjEsImNyZWF0ZWQiOjE2Nzg4NjM4MDgsIm9yZ2FuaXphdGlvbl9pZCI6IjEiLCJtb2JpbGUiOiI5ODc4Nzg4MzI2IiwiaWQiOjE1MDE0MTM4MjIsImV4cCI6MTY3ODk1MDIwOCwiZGV2aWNlSWQiOiIxNjc4ODYzODA2NDgxIiwibW9iaWxlX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOnRydWUsImVtYWlsIjoicmlua29vbWFuaTFAZW1iaWJlLmNvbSJ9.lT2HFQ64xPiD6B15qoSEdzZBXfwIQJ6KohCjm9N9ILPBqc6vdLABM78LQZdcJj2CUHbWAkg-TDJfw1Dr06CDgQ; _gcl_au=1.1.334569216.1678864428; WZRK_G=7094910dcd0a42d5b8b9275c4c3546a0; _ga_X772T5L8EH=GS1.1.1679040170.2.0.1679040170.60.0.0; _ga_YHBNHKH43S=GS1.1.1679040170.3.0.1679040170.60.0.0; _gid=GA1.2.1252939323.1679314399; __insp_wid=1955736467; __insp_slim=1679314399396; __insp_nv=true; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS9hc3NpZ24vdGVzdC9saXN0; __insp_targlpt=RU1CSUJFIC0gVGhlIG1vc3QgcG93ZXJmdWwgQUktcG93ZXJlZCBsZWFybmluZyBwbGF0Zm9ybQ%3D%3D; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjc5MzE0Mzk5LCJnYV9pZCI6IjIzMzM2NTk0OC4xNjY1MDU0ODcxIiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2ODA1MjM5OTksInV1aWQiOiJhNzNkYWM2ZC00YWEyLTRkMzItYjg0Mi03OTUyZTIzMGFlOTciLCJzZW5kZXJfaWQiOiJ0ZXN0In0.pv2m7_4uXfG0J6YCJsvNzAru2DtKsNMTIB46jGsQ14c; guest-session-id=a73dac6d-4aa2-4d32-b842-7952e230ae97; __insp_norec_sess=true; _ga=GA1.1.233365948.1665054871; _ga_TT8L73VP3H=GS1.1.1679314399.5.0.1679314546.60.0.0; _ga_7N8QWZ4X8C=GS1.1.1679313600.5.1.1679314934.56.0.0; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=f1feba2c-1445-4080-81b7-4f78cef483e9; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzkzMTY0MjQsIm9yZ2FuaXphdGlvbl9pZCI6IjYyNDFhZTgwOTA3ZTEzNjdjMzk4NDE3MyIsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJpZCI6MTUwMTU2NTQyNiwiZXhwIjoxNjc5NDAyODI0LCJkZXZpY2VJZCI6IjAuMzkwNDg2NjkxMzI4OTg3MDYiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6ImthcGlsLnNheGVuYUBlbWJpYmUuY29tIn0.iFqSXtGr0zXfnCXLMtnhSHGTPYSuNo2sBbNtFwCpjMZdpJUxhawoknMfzgydfO4Ptblyhs7nD7dspOcODub4FQ; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNheGVuYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTIwIDEyOjQ3OjA4IFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJyb290T3JnSWQiOiI2MjQxYWU4MDkwN2UxMzY3YzM5ODQxNzMiLCJkZXZpY2VJZCI6IjAuMzkwNDg2NjkxMzI4OTg3MDYiLCJmaXJzdE5hbWUiOiJLYXBpbCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjI0MWI1NTlhNThkYjkzY2UwYjZlM2RjIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjI0MzE5MjZmYWU1YTIxNGE0ZGY0NmZiIiwiaWQiOjE1MDE1NjU0MjYsImVtYWlsIjoia2FwaWwuc2F4ZW5hQGVtYmliZS5jb20ifQ.Akjl6Ci9VgfJPRoDeawTjjfsIlvyNM4rJIRKoESTc0dESVxO3avqpnGLTZF3ROd2fZvHHziBzckTIhQL4wOmYQ; _ga_QQPR95BLCG=GS1.1.1679308991.71.1.1679316840.48.0.0; JSESSIONID=iRrPsGJm2UPncPb4KmvxrpwKDYTuOKukXU4dNCTQ; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNheGVuYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAzLTIwIDEyOjQ3OjA4IFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJyb290T3JnSWQiOiI2MjQxYWU4MDkwN2UxMzY3YzM5ODQxNzMiLCJkZXZpY2VJZCI6IjAuMzkwNDg2NjkxMzI4OTg3MDYiLCJmaXJzdE5hbWUiOiJLYXBpbCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJ1c2VyX3R5cGUiOjMsInBhcmVudE9yZ0lkIjoiNjI0MWI1NTlhNThkYjkzY2UwYjZlM2RjIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjI0MzE5MjZmYWU1YTIxNGE0ZGY0NmZiIiwiaWQiOjE1MDE1NjU0MjYsImVtYWlsIjoia2FwaWwuc2F4ZW5hQGVtYmliZS5jb20ifQ.Akjl6Ci9VgfJPRoDeawTjjfsIlvyNM4rJIRKoESTc0dESVxO3avqpnGLTZF3ROd2fZvHHziBzckTIhQL4wOmYQ',
            'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

        self.body = json.dumps({"name": "CBSE-12th CBSE-A-en-Biology", "selectedFormatType": "POST_CLASS_AP_HOMEWORK", "nextStep": "AP_CONFIRM_SYLLABUS", "slotId": "64181ae38d0e525288fe02eb", "classIds": ["62b1adf201ab84274cf65167"], "teacherId": "62431926fae5a214a4df46fb", "learningMaps": [{"name": "DNA Structure and Packaging", "description": "This topic covers concepts, such as, DNA, Chemical Composition of DNA, Nucleotides & Non-Histone Chromosomal Proteins etc.", "learningPathFormatName": "goal--exam--subject--unit--chapter--topic", "learningPath": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance--dna structure and packaging", "topicKveCode": "kve97670--kve97671--kve97678--kve97679--kve97680--kve665891", "code": "kve665891", "meta": {"classId": "", "display_name": "DNA Structure and Packaging", "topic": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance--dna structure and packaging", "topic_code": "kve665891", "topic_name": "DNA Structure and Packaging", "bookId": "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Biology", "bookType": "BigBook", "book_path": "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Biology", "bookLearningPathFormatName": "goal--exam--subject--unit--chapter--topic", "unit": "cbse--12th cbse--biology--genetics and evolution", "unit_code": "kve97679", "unit_name": "Genetics and Evolution", "unit_display_name": "Genetics and Evolution", "chapter": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance", "chapter_code": "kve97680", "chapter_name": "Molecular Basis of Inheritance", "chapter_display_name": "Molecular Basis of Inheritance"}}, {
                               "name": "Replication", "description": "This topic covers concepts, such as, DNA Replication, DNA Polymerase, Replication Fork, DNA Ligase, Origin of Replication, Leading and Lagging Strands, Okazaki Fragments, Semi Discontinuous Replication & Meselson and Stahl's Experiment etc.", "learningPathFormatName": "goal--exam--subject--unit--chapter--topic", "learningPath": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance--replication", "topicKveCode": "kve97670--kve97671--kve97678--kve97679--kve97680--kve98780", "code": "kve98780", "meta": {"classId": "", "display_name": "Replication", "topic": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance--replication", "topic_code": "kve98780", "topic_name": "Replication", "bookId": "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Biology", "bookType": "BigBook", "book_path": "5ec5867a0c88fe5860961943/CBSE/12th CBSE/Biology", "bookLearningPathFormatName": "goal--exam--subject--unit--chapter--topic", "unit": "cbse--12th cbse--biology--genetics and evolution", "unit_code": "kve97679", "unit_name": "Genetics and Evolution", "unit_display_name": "Genetics and Evolution", "chapter": "cbse--12th cbse--biology--genetics and evolution--molecular basis of inheritance", "chapter_code": "kve97680", "chapter_name": "Molecular Basis of Inheritance", "chapter_display_name": "Molecular Basis of Inheritance"}}], "schoolId": "6241b559a58db93ce0b6e3dc", "subjectId": "5ec5867a0c88fe5860961943|CBSE/12th CBSE/Biology|cbse--12th cbse--biology|kve97670--kve97671--kve97678", "subjectName": "Biology", "timetableId": "", "templateId": "624a93703dd1e1634bb43787", "chapterKveCode": "kve97670--kve97671--kve97678--kve97679--kve97680", "parentKveCode": None})

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"

    @task
    def userDB(self):
        url = f"/ps_generate_ms/v3/school/atgConfig?locale=en"
        response = self.client.post(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
