from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd

host = "https://preprodms.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Connection': 'keep-alive',
            'Cookie': 'JSESSIONID=98E506065624E43C404DF17C3A3AAFED; locale=en; _gid=GA1.2.253432898.1672744235; _fbp=fb.1.1672744234671.2075372915; preprod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjcyNzQ0MjM1LCJnYV9pZCI6IjIzMzM2NTk0OC4xNjY1MDU0ODcxIiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzI4MzA2MzUsInV1aWQiOiJjMjVhNWMzYS0yZjEyLTQ2OGItOTI0OC02MzQ2YmVjOTYxODQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.SOhByUxugVo0kZ0q0CVwuwJ6DfRxCEXBLva3NDA_2OM; _gcl_au=1.1.561879230.1672744246; _ga_YHBNHKH43S=GS1.1.1672741927.15.1.1672745673.60.0.0; _ga_X772T5L8EH=GS1.1.1672742081.7.1.1672745673.60.0.0; WZRK_G=7094910dcd0a42d5b8b9275c4c3546a0; prod_guest_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkIjoxNjcyODI2NTY0LCJnYV9pZCI6IjIzMzM2NTk0OC4xNjY1MDU0ODcxIiwic291cmNlX3R5cGUiOiJFTUJJQkVfSE9NRSIsInNvdXJjZSI6InRlc3QiLCJleHAiOjE2NzQwMzYxNjQsInV1aWQiOiI0NmEwNDg1NS1hMzA2LTQ4MDItYTQ2OC02Y2E0NjVmOThjYzQiLCJzZW5kZXJfaWQiOiJ0ZXN0In0.pjdO3xuaxyJkW0kFnEEOlPQrovMbL-J08kAbMJAn6zQ; guest-session-id=46a04855-a306-4802-a468-6ca465f98cc4; _ga_7N8QWZ4X8C=GS1.1.1672837597.1.1.1672838405.58.0.0; school_preprod_ab_version=0; school_preprod_embibe-refresh-token=3a081c63-018e-4ba1-9aeb-d23775bc9d1b; school_preprod_embibe-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjb3VudHJ5IjoxLCJ1c2VyX3R5cGUiOjMsImNyZWF0ZWQiOjE2NzI4Mzg0NjQsIm9yZ2FuaXphdGlvbl9pZCI6IjYyNDFhZTgwOTA3ZTEzNjdjMzk4NDE3MyIsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJpZCI6MTUwMTU2NTQyNiwiZXhwIjoxNjcyOTI0ODY0LCJkZXZpY2VJZCI6IjAuMDU5MTQwMTk3MDcxOTUyOTUiLCJtb2JpbGVfdmVyaWZpY2F0aW9uX3N0YXR1cyI6ZmFsc2UsImVtYWlsX3ZlcmlmaWNhdGlvbl9zdGF0dXMiOmZhbHNlLCJlbWFpbCI6ImthcGlsLnNheGVuYUBlbWJpYmUuY29tIn0.BKaj-GxQPD1oU2sFPmWyYAK1nfYA1bpWEP6bmOmGOEKM7nNx0tusM-zoAQUbYTk1r7_sEXHg7jRqT6TF7LRuTA; __insp_uid=4182886806; WZRK_S_8RZ-869-956Z=%7B%22p%22%3A1%2C%22s%22%3A1672906293%2C%22t%22%3A1672906293%7D; __insp_wid=1955736467; __insp_slim=1672906293919; __insp_nv=false; __insp_targlpu=aHR0cHM6Ly9wYWFzLXYzLXN0YWdpbmcuZW1iaWJlLmNvbS9sb2dpbg%3D%3D; __insp_targlpt=RW1iaWJlIFNvY2lhbCBMb2dpbiBQYWdl; _ga=GA1.1.233365948.1665054871; __insp_norec_howoften=true; __insp_norec_sess=true; _ga_TT8L73VP3H=GS1.1.1672906238.3.1.1672906332.60.0.0; reseller-jwt-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNheGVuYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAxLTA1IDA4OjEyOjMyIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJyb290T3JnSWQiOiI2MjQxYWU4MDkwN2UxMzY3YzM5ODQxNzMiLCJkZXZpY2VJZCI6IjAuMDU5MTQwMTk3MDcxOTUyOTUiLCJmaXJzdE5hbWUiOiJLYXBpbCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYyNDFiNTU5YTU4ZGI5M2NlMGI2ZTNkYyIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYyNDMxOTI2ZmFlNWEyMTRhNGRmNDZmYiIsImlkIjoxNTAxNTY1NDI2LCJlbWFpbCI6ImthcGlsLnNheGVuYUBlbWJpYmUuY29tIn0.7mxk-MYWJBAigYYDvCbTLISnHstfick0u87nxNGfMple1weEfDAiUCXDh3paDaiDHbo75wNDUuMUAePHJme26g; _ga_QQPR95BLCG=GS1.1.1672906332.6.1.1672906357.35.0.0; JSESSIONID=g3GD_0E6E7fweM27KkhTtrw1yl99FmyPZ2Aza1i0; preprod_embibe-refresh-token=1bd850f5-1b26-47a5-a7bb-665b50172d0a; school_preprod_embibe-refresh-token=3d3ce591-8b1b-4c1c-82ff-a173d666bb9c',
            'Origin': 'https://paas-v3-staging.embibe.com',
            'Referer': 'https://paas-v3-staging.embibe.com/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNheGVuYSIsInRpbWVfc3RhbXAiOiIyMDIzLTAxLTA1IDA4OjEyOjMyIFVUQyIsInN1Yl9vcmdhbml6YXRpb25faWQiOjEsIm1vYmlsZSI6Ijc3MzM4MTQzMDQiLCJyb290T3JnSWQiOiI2MjQxYWU4MDkwN2UxMzY3YzM5ODQxNzMiLCJkZXZpY2VJZCI6IjAuMDU5MTQwMTk3MDcxOTUyOTUiLCJmaXJzdE5hbWUiOiJLYXBpbCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYyNDFiNTU5YTU4ZGI5M2NlMGI2ZTNkYyIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYyNDMxOTI2ZmFlNWEyMTRhNGRmNDZmYiIsImlkIjoxNTAxNTY1NDI2LCJlbWFpbCI6ImthcGlsLnNheGVuYUBlbWJpYmUuY29tIn0.7mxk-MYWJBAigYYDvCbTLISnHstfick0u87nxNGfMple1weEfDAiUCXDh3paDaiDHbo75wNDUuMUAePHJme26g',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"macOS"'
        }

        dt = pd.read_csv(
            "AtgConfigId  - atgConfig with CYOH.csv", low_memory=False)

        self.atgConfigId = dt["atgConfigId"].values.tolist()

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms.embibe.com"


    @task
    def userDB(self):
        url = f"/ps_generate_ms/v3/school/getAllTest?atgConfigId={random.choice(self.atgConfigId)}&locale=en"
        with self.client.get(url, data=self.body, headers=self.headers, catch_response=True) as response:
            if response.status_code == 400:
                response.success()
            elif response.status_code == 200:
                response.success()
            elif response.status_code == 204:
                response.success()


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
            all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
                all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
                0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
