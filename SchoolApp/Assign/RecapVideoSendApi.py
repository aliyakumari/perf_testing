from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd
from CreateToken import creatEmbibeToken

EmbibeToken = creatEmbibeToken()

# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'embibe-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyIiwidGltZV9zdGFtcCI6IjIwMjEtMDgtMTIgMDk6NTc6NDggVVRDIiwic3ViX29yZ2FuaXphdGlvbl9pZCI6MSwibW9iaWxlIjoiOTAwMDAxMjM4NSIsInJvb3RPcmdJZCI6IjYwYzFlM2QxMzM1ZjVhNWQ0NDZmNmJjZSIsImZpcnN0TmFtZSI6IlJhamVzaCIsIm9yZ1R5cGUiOiJTY2hvb2wiLCJwYXJlbnRPcmdJZCI6IjYwYzFlNTY5MzM1ZjVhNWQ0NDZmNmJkMiIsInBlcnNvbmFUeXBlIjoiVGVhY2hlciIsIm9yZ2FuaXphdGlvbl9pZCI6MSwiYWRtaW5JZCI6IjYwYzIyOTQ0ODQ3YmRhN2UxNDM5N2ExNSIsImlkIjoxNTAwMDQ1MDEyLCJlbWFpbCI6InJhamVzaGtAZW1iaWJlLmNvbSJ9.um99jY3xfkS3VPSAbggfoKfvy819QRYfIVnP8KcTw2gLZFq71XQzaGpucgnC788MTpihOyvnCTHDCmn_-C353A',
  'Cookie': 'V_ID=ultimate.2021-04-21.55a99c16240f5238b79a8983ffc6dc71; V_ID=ultimate.2021-04-21.55a99c16240f5238b79a8983ffc6dc71; V_ID=ultimate.2021-04-21.55a99c16240f5238b79a8983ffc6dc71; JSESSIONID=5FAD6B1DE96DE51F7544EF8810FF3D67; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=8b9c7c71-097d-48bc-8c78-f2fe71031218; preprod_embibe-refresh-token=7af50268-3fb9-4c68-ade0-4441913dedd9'
}


        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        rnum = randrange(len(EmbibeToken))
        # self.headers['embibe-token'] = EmbibeToken[rnum]
        url = f"/track/v1/recapSuggestions?classId=61c338a3043e66223a2c70ff&startDate=1635757390&endDate=1638262990&kvCode=kv12312--kv34123--kv12345"
        response = self.client.get(url, data=self.body, headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
