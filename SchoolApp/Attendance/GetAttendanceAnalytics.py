from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ik11ZGUiLCJ0aW1lX3N0YW1wIjoiMjAyMi0wNC0wMSAwNDo0NToyNSBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5ODc3Njc4ODk5Iiwicm9vdE9yZ0lkIjoiNjFkNTRjYjljOTVjNDI3OGVhZGExZWRhIiwiZGV2aWNlSWQiOiIwLjczNDQ1NTcxMjM5NjA5OTMiLCJmaXJzdE5hbWUiOiJSYWp1Iiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjFkNTRjZGM1YjYwMGE2YzQ3N2FlYjg3IiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjFkNTU0ZGE1YjYwMGE2YzQ3N2FlYjk1IiwiaWQiOjE1MDExOTkxOTcsImVtYWlsIjoicmFqdV90ZWFjaGVyQGVtYmliZS5jb20ifQ.8gsbFvpYf1pLx3LEdNnIQ2JGMOGWD554Dkln32sDn0i2JYZrtVREO9FEIrWgtAbpmIadJttZqBlQ0S524Grdog',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?0',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.83 Safari/537.36',
  'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
  'sec-ch-ua-platform': '"macOS"',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en;q=0.9',
  'Cookie': 'V_ID=ultimate.2022-01-03.6511989e81f3b02b23313732a310ca57'
}

        self.body = {}

    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.get("/attendance-svc/api/v1/attendance/analytics?teacherID=61d554da5b600a6c477aeb95&subject=Science&instituteId=61d54cdc5b600a6c477aeb87&classId=61d54de7e7c57e436ab137ac&subjectKveCode=kve603187--kve603397--kve603398", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
