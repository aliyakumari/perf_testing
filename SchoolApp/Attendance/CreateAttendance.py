from locust import task, between, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP, WorkerRunner
from locust.contrib.fasthttp import FastHttpUser
from random import randrange
from random import randint
import json
import csv
import string
import random
import logging
import time
import gevent
import resource
import ast
import pandas as pd


# resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))
host = "https://preprodms-cf.embibe.com"

resource.setrlimit(resource.RLIMIT_NOFILE, (20240, 20240))

all_commands = {}
with open("all_commands.txt") as f:
    for line in f:
        (key, val) = line.split()
        all_commands[key] = val


class UserBehaviour(FastHttpUser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.headers = {
  'Connection': 'keep-alive',
  'reseller-jwt-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6Ikt1bWFyICAiLCJ0aW1lX3N0YW1wIjoiMjAyMS0xMC0xOCAxMjo1ODowMCBVVEMiLCJzdWJfb3JnYW5pemF0aW9uX2lkIjoxLCJtb2JpbGUiOiI5MDAwMDE3ODc5Iiwicm9vdE9yZ0lkIjoiNjBjMWUzZDEzMzVmNWE1ZDQ0NmY2YmNlIiwiZmlyc3ROYW1lIjoiUmFrZXNoIiwib3JnVHlwZSI6IlNjaG9vbCIsInBhcmVudE9yZ0lkIjoiNjBjMWU1NjkzMzVmNWE1ZDQ0NmY2YmQyIiwicGVyc29uYVR5cGUiOiJUZWFjaGVyIiwib3JnYW5pemF0aW9uX2lkIjoxLCJhZG1pbklkIjoiNjBjMjI5NDQ4NDdiZGE3ZTE0Mzk3YTE1IiwiaWQiOjE1MDAwNDUwMTIsImVtYWlsIjoicmFqZXNoa0BlbWJpYmUuY29tIn0.1fbzwEWUtVI4WNaY-gpuf2ONY4dSIqAJmlTtB7svczbsWh5r4sV35yTf4Gd1oOOPdccsg2lVQ4zfnAQIeAfw8A',
  'Accept': 'application/json, text/plain, */*',
  'sec-ch-ua-mobile': '?1',
  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Mobile Safari/537.36',
  'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
  'sec-ch-ua-platform': '"Android"',
  'Content-Type': 'application/json',
  'Origin': 'https://paas-v3-staging.embibe.com',
  'Sec-Fetch-Site': 'same-site',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Dest': 'empty',
  'Referer': 'https://paas-v3-staging.embibe.com/',
  'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
  'Cookie': 'JSESSIONID=DA0593278E1D084565825D6989F54892; V_ID=ultimate.2021-03-22.12aa9f4f83056383b8915475fb2b9477; embibe-refresh-token=dc312247-39d3-4756-948b-ef56a04f5df2'
}

        self.body = json.dumps({
  "date": 1648799421734,
  "instituteId": "60c1e569335f5a5d446f6bd2",
  "classId": "60c1fe9d9c95f329eb741a51",
  "studentId": "60c22fc9847bda7e14397a23",
  "teacherId": "60c22944847bda7e14397a15",
  "subject": "Chemistry",
  "state": False
})
    wait_time = between(0, 1)
    host = "https://preprodms-cf.embibe.com"

    @task
    def userDB(self):
        response = self.client.post("/attendance-svc/api/v1/attendance/", data=self.body,
                                   headers=self.headers)


def checker(environment):
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(0)
        if environment.stats.total.fail_ratio > float(
                all_commands['limit_fail_ratio']) or environment.stats.total.avg_response_time > int(
            all_commands['limit_avg_response_time_in_ms']) or environment.stats.total.get_response_time_percentile(
            0.90) > int(all_commands['limit_90_percentile_response_time_in_ms']):
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    if not isinstance(environment.runner, WorkerRunner):
        gevent.spawn(checker, environment)
