import pandas as pd
import sys
import os
import json
import time

this = sys.modules[__name__]

df = pd.read_csv("url.csv")
urls = df.values.tolist()


def url_data():
    for url in urls:
        this.link = url

        data = {
            "url": url[0],
        }
        url = url[0].replace("/", "_")
        with open('input_url.json', 'w') as outfile:
            json.dump(data, outfile)
        os.system(f'sudo locust -f locust.py  --csv-full-history --csv=Results/{url} --headless -u 1000 -r 100 --run-time 10m ')
        time.sleep(60)


url_data()

